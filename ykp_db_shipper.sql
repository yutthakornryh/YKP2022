CREATE DATABASE  IF NOT EXISTS `ykp_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ykp_db`;
-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: localhost    Database: ykp_db
-- ------------------------------------------------------
-- Server version	5.5.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `shipper`
--

DROP TABLE IF EXISTS `shipper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipper` (
  `SHIPPERID` int(11) NOT NULL AUTO_INCREMENT,
  `SHIPNAME` varchar(45) DEFAULT NULL,
  `SHIPNICKNAME` varchar(45) DEFAULT NULL,
  `SHIPADD` text,
  `SHIPTELL` varchar(45) DEFAULT NULL,
  `SHIPFAX` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SHIPPERID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipper`
--

LOCK TABLES `shipper` WRITE;
/*!40000 ALTER TABLE `shipper` DISABLE KEYS */;
INSERT INTO `shipper` VALUES (8,'ToonmoveH company','TMH','188 crazy hatyai,songklha 90110','087-755854','000-000000'),(7,'Stonz company','SNC','191 phonphichai hatyai,songklha 90110','085-588575','000-000000'),(10,'AIM COMPANY','AIM','14 rajyindee hatyai songklha 90110','074-200200','000-000000'),(11,'RiverInter Company','TOPLINE','11 rajyindee hatyai songklha 90110','089-966321','000-000000'),(12,'Sinjai company','SC','985 rajutid hatyai songklha 90110','074-253698','000-000000'),(13,'Junekung company','JK','255 thanatip hatyai songkhla','074-563258','000-000000'),(14,'YKP GYPSUM','YKPGYPSUM','154 YKP kantrang trang 54110','075-859685','000-000000'),(15,'GUANGKEN','PSK','748 kantrang trang 90442','075-874575','000-000000');
/*!40000 ALTER TABLE `shipper` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-20 17:09:04
