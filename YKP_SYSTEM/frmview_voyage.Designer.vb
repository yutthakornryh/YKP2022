﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmview_voyage
    Inherits DevComponents.DotNetBar.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ListViewGroup1 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left)
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_voyage = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.txt_from = New System.Windows.Forms.TextBox()
        Me.txt_eta = New System.Windows.Forms.TextBox()
        Me.txt_gross = New System.Windows.Forms.TextBox()
        Me.txt_nett = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_nationality = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_namemaster = New System.Windows.Forms.TextBox()
        Me.txt_vessel = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Voyage = New DevComponents.DotNetBar.RibbonBarMergeContainer()
        Me.RibbonBar5 = New DevComponents.DotNetBar.RibbonBar()
        Me.MaskedTextBoxAdv2 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.MaskedTextBoxAdv3 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.ControlContainerItem4 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ControlContainerItem5 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ButtonItem15 = New DevComponents.DotNetBar.ButtonItem()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.RibbonBar4 = New DevComponents.DotNetBar.RibbonBar()
        Me.dateconfirm = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.MaskedTextBoxAdv1 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.CircularProgress1 = New DevComponents.DotNetBar.Controls.CircularProgress()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ButtonItem9 = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem3 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.RibbonBar3 = New DevComponents.DotNetBar.RibbonBar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem11 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem12 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem8 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar2 = New DevComponents.DotNetBar.RibbonBar()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem13 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem16 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem17 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem18 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem10 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem19 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem14 = New DevComponents.DotNetBar.ButtonItem()
        Me.txt_eta_penang = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.DataGridViewX1 = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AGENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HHMM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C = New DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn()
        Me.DateIn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HHMM1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C1 = New DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Invoice = New DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn()
        Me.Save = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.idborrow1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ctnmainid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.check = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.check1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Voyage.SuspendLayout()
        Me.RibbonBar5.SuspendLayout()
        Me.RibbonBar4.SuspendLayout()
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txt_voyage)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Location = New System.Drawing.Point(-4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1202, 42)
        Me.Panel1.TabIndex = 175
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Cordia New", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(4, 1)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(90, 45)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Voyage"
        '
        'txt_voyage
        '
        Me.txt_voyage.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_voyage.Location = New System.Drawing.Point(946, 2)
        Me.txt_voyage.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.ReadOnly = True
        Me.txt_voyage.Size = New System.Drawing.Size(151, 37)
        Me.txt_voyage.TabIndex = 116
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(853, 2)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 34)
        Me.Label5.TabIndex = 113
        Me.Label5.Text = "VOYAGE :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(1071, 93)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(41, 22)
        Me.Label16.TabIndex = 171
        Me.Label16.Text = "TONS."
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(1071, 57)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(41, 22)
        Me.Label15.TabIndex = 170
        Me.Label15.Text = "TONS."
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(806, 125)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 22)
        Me.Label12.TabIndex = 164
        Me.Label12.Text = "ETA. PENANG :"
        '
        'ListView1
        '
        Me.ListView1.BackColor = System.Drawing.Color.White
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader8, Me.ColumnHeader2, Me.ColumnHeader12, Me.ColumnHeader11, Me.ColumnHeader4})
        Me.ListView1.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListView1.FullRowSelect = True
        Me.ListView1.GridLines = True
        ListViewGroup1.Header = "ListViewGroup"
        ListViewGroup1.Name = "ListViewGroup1"
        Me.ListView1.Groups.AddRange(New System.Windows.Forms.ListViewGroup() {ListViewGroup1})
        Me.ListView1.HideSelection = False
        Me.ListView1.Location = New System.Drawing.Point(6, 22)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.ShowGroups = False
        Me.ListView1.Size = New System.Drawing.Size(404, 264)
        Me.ListView1.TabIndex = 62
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Booking No."
        Me.ColumnHeader1.Width = 70
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Shipper"
        Me.ColumnHeader8.Width = 100
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "FORWARDER"
        Me.ColumnHeader2.Width = 85
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Close Date"
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "FCL"
        Me.ColumnHeader11.Width = 70
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "IDBOOK"
        Me.ColumnHeader4.Width = 0
        '
        'txt_from
        '
        Me.txt_from.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_from.Location = New System.Drawing.Point(520, 88)
        Me.txt_from.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_from.Name = "txt_from"
        Me.txt_from.ReadOnly = True
        Me.txt_from.Size = New System.Drawing.Size(216, 30)
        Me.txt_from.TabIndex = 159
        Me.txt_from.Text = "THAILAND"
        '
        'txt_eta
        '
        Me.txt_eta.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_eta.Location = New System.Drawing.Point(520, 122)
        Me.txt_eta.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_eta.Name = "txt_eta"
        Me.txt_eta.ReadOnly = True
        Me.txt_eta.Size = New System.Drawing.Size(216, 30)
        Me.txt_eta.TabIndex = 158
        '
        'txt_gross
        '
        Me.txt_gross.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_gross.Location = New System.Drawing.Point(889, 88)
        Me.txt_gross.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_gross.Name = "txt_gross"
        Me.txt_gross.Size = New System.Drawing.Size(176, 30)
        Me.txt_gross.TabIndex = 157
        '
        'txt_nett
        '
        Me.txt_nett.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nett.Location = New System.Drawing.Point(889, 55)
        Me.txt_nett.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_nett.Name = "txt_nett"
        Me.txt_nett.Size = New System.Drawing.Size(176, 30)
        Me.txt_nett.TabIndex = 156
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(488, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 22)
        Me.Label7.TabIndex = 155
        Me.Label7.Text = "ETA :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(839, 93)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 22)
        Me.Label8.TabIndex = 154
        Me.Label8.Text = "GROSS :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(767, 57)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(123, 22)
        Me.Label9.TabIndex = 153
        Me.Label9.Text = "NETT REQ. TONNAGE :"
        '
        'txt_nationality
        '
        Me.txt_nationality.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nationality.Location = New System.Drawing.Point(520, 53)
        Me.txt_nationality.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_nationality.Name = "txt_nationality"
        Me.txt_nationality.ReadOnly = True
        Me.txt_nationality.Size = New System.Drawing.Size(216, 30)
        Me.txt_nationality.TabIndex = 152
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(477, 91)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 22)
        Me.Label4.TabIndex = 151
        Me.Label4.Text = "FROM :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(439, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 22)
        Me.Label6.TabIndex = 150
        Me.Label6.Text = "NATIONALITY :"
        '
        'txt_namemaster
        '
        Me.txt_namemaster.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_namemaster.Location = New System.Drawing.Point(144, 88)
        Me.txt_namemaster.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_namemaster.Name = "txt_namemaster"
        Me.txt_namemaster.ReadOnly = True
        Me.txt_namemaster.Size = New System.Drawing.Size(281, 30)
        Me.txt_namemaster.TabIndex = 149
        '
        'txt_vessel
        '
        Me.txt_vessel.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vessel.Location = New System.Drawing.Point(144, 54)
        Me.txt_vessel.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_vessel.Name = "txt_vessel"
        Me.txt_vessel.ReadOnly = True
        Me.txt_vessel.Size = New System.Drawing.Size(281, 30)
        Me.txt_vessel.TabIndex = 148
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(48, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 22)
        Me.Label2.TabIndex = 146
        Me.Label2.Text = "NAME MASTER :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(34, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 22)
        Me.Label1.TabIndex = 145
        Me.Label1.Text = "NAME OF VESSEL. :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ListView1)
        Me.GroupBox1.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(5, 155)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(420, 294)
        Me.GroupBox1.TabIndex = 142
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Booking No."
        '
        'Voyage
        '
        Me.Voyage.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2013
        Me.Voyage.Controls.Add(Me.RibbonBar5)
        Me.Voyage.Controls.Add(Me.Label10)
        Me.Voyage.Controls.Add(Me.RibbonBar4)
        Me.Voyage.Controls.Add(Me.RibbonBar3)
        Me.Voyage.Controls.Add(Me.RibbonBar1)
        Me.Voyage.Controls.Add(Me.RibbonBar2)
        Me.Voyage.Location = New System.Drawing.Point(19, 557)
        Me.Voyage.Name = "Voyage"
        Me.Voyage.Size = New System.Drawing.Size(1318, 100)
        '
        '
        '
        Me.Voyage.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.Voyage.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.Voyage.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Voyage.TabIndex = 176
        Me.Voyage.Visible = False
        '
        'RibbonBar5
        '
        Me.RibbonBar5.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar5.ContainerControlProcessDialogKey = True
        Me.RibbonBar5.Controls.Add(Me.MaskedTextBoxAdv2)
        Me.RibbonBar5.Controls.Add(Me.MaskedTextBoxAdv3)
        Me.RibbonBar5.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar5.DragDropSupport = True
        Me.RibbonBar5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar5.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem5, Me.ControlContainerItem4, Me.ControlContainerItem5, Me.ButtonItem15})
        Me.RibbonBar5.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar5.Location = New System.Drawing.Point(818, 0)
        Me.RibbonBar5.Name = "RibbonBar5"
        Me.RibbonBar5.Size = New System.Drawing.Size(254, 100)
        Me.RibbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar5.TabIndex = 13
        Me.RibbonBar5.Text = "Get Out"
        '
        '
        '
        Me.RibbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar5.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'MaskedTextBoxAdv2
        '
        '
        '
        '
        Me.MaskedTextBoxAdv2.BackgroundStyle.Class = "TextBoxBorder"
        Me.MaskedTextBoxAdv2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MaskedTextBoxAdv2.ButtonClear.Visible = True
        Me.MaskedTextBoxAdv2.Location = New System.Drawing.Point(11, 34)
        Me.MaskedTextBoxAdv2.Mask = "00/00/0000"
        Me.MaskedTextBoxAdv2.Name = "MaskedTextBoxAdv2"
        Me.MaskedTextBoxAdv2.Size = New System.Drawing.Size(90, 19)
        Me.MaskedTextBoxAdv2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.MaskedTextBoxAdv2.TabIndex = 182
        Me.MaskedTextBoxAdv2.Text = ""
        Me.MaskedTextBoxAdv2.ValidatingType = GetType(Date)
        '
        'MaskedTextBoxAdv3
        '
        '
        '
        '
        Me.MaskedTextBoxAdv3.BackgroundStyle.Class = "TextBoxBorder"
        Me.MaskedTextBoxAdv3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MaskedTextBoxAdv3.ButtonClear.Visible = True
        Me.MaskedTextBoxAdv3.Location = New System.Drawing.Point(106, 34)
        Me.MaskedTextBoxAdv3.Mask = "90:00"
        Me.MaskedTextBoxAdv3.Name = "MaskedTextBoxAdv3"
        Me.MaskedTextBoxAdv3.Size = New System.Drawing.Size(90, 19)
        Me.MaskedTextBoxAdv3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.MaskedTextBoxAdv3.TabIndex = 183
        Me.MaskedTextBoxAdv3.Text = ""
        Me.MaskedTextBoxAdv3.ValidatingType = GetType(Date)
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        '
        'ControlContainerItem4
        '
        Me.ControlContainerItem4.AllowItemResize = False
        Me.ControlContainerItem4.Control = Me.MaskedTextBoxAdv2
        Me.ControlContainerItem4.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem4.Name = "ControlContainerItem4"
        '
        'ControlContainerItem5
        '
        Me.ControlContainerItem5.AllowItemResize = False
        Me.ControlContainerItem5.Control = Me.MaskedTextBoxAdv3
        Me.ControlContainerItem5.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem5.Name = "ControlContainerItem5"
        '
        'ButtonItem15
        '
        Me.ButtonItem15.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem15.Name = "ButtonItem15"
        Me.ButtonItem15.SubItemsExpandWidth = 14
        Me.ButtonItem15.Symbol = ""
        Me.ButtonItem15.SymbolSize = 30.0!
        Me.ButtonItem15.Text = "Confirm Get Out"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(787, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(0, 13)
        Me.Label10.TabIndex = 12
        '
        'RibbonBar4
        '
        Me.RibbonBar4.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar4.ContainerControlProcessDialogKey = True
        Me.RibbonBar4.Controls.Add(Me.dateconfirm)
        Me.RibbonBar4.Controls.Add(Me.MaskedTextBoxAdv1)
        Me.RibbonBar4.Controls.Add(Me.CircularProgress1)
        Me.RibbonBar4.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar4.DragDropSupport = True
        Me.RibbonBar4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar4.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem4, Me.ControlContainerItem1, Me.ControlContainerItem2, Me.ButtonItem9, Me.ControlContainerItem3})
        Me.RibbonBar4.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar4.Location = New System.Drawing.Point(494, 0)
        Me.RibbonBar4.Name = "RibbonBar4"
        Me.RibbonBar4.Size = New System.Drawing.Size(324, 100)
        Me.RibbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar4.TabIndex = 11
        Me.RibbonBar4.Text = "Report"
        '
        '
        '
        Me.RibbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar4.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'dateconfirm
        '
        '
        '
        '
        Me.dateconfirm.BackgroundStyle.Class = "TextBoxBorder"
        Me.dateconfirm.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dateconfirm.ButtonClear.Visible = True
        Me.dateconfirm.Location = New System.Drawing.Point(7, 33)
        Me.dateconfirm.Mask = "00/00/0000"
        Me.dateconfirm.Name = "dateconfirm"
        Me.dateconfirm.Size = New System.Drawing.Size(90, 19)
        Me.dateconfirm.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.dateconfirm.TabIndex = 181
        Me.dateconfirm.Text = ""
        Me.dateconfirm.ValidatingType = GetType(Date)
        '
        'MaskedTextBoxAdv1
        '
        '
        '
        '
        Me.MaskedTextBoxAdv1.BackgroundStyle.Class = "TextBoxBorder"
        Me.MaskedTextBoxAdv1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MaskedTextBoxAdv1.ButtonClear.Visible = True
        Me.MaskedTextBoxAdv1.Location = New System.Drawing.Point(102, 33)
        Me.MaskedTextBoxAdv1.Mask = "90:00"
        Me.MaskedTextBoxAdv1.Name = "MaskedTextBoxAdv1"
        Me.MaskedTextBoxAdv1.Size = New System.Drawing.Size(90, 19)
        Me.MaskedTextBoxAdv1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.MaskedTextBoxAdv1.TabIndex = 182
        Me.MaskedTextBoxAdv1.Text = ""
        Me.MaskedTextBoxAdv1.ValidatingType = GetType(Date)
        '
        'CircularProgress1
        '
        Me.CircularProgress1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.CircularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CircularProgress1.Location = New System.Drawing.Point(246, 6)
        Me.CircularProgress1.Name = "CircularProgress1"
        Me.CircularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Spoke
        Me.CircularProgress1.ProgressColor = System.Drawing.Color.MediumSeaGreen
        Me.CircularProgress1.Size = New System.Drawing.Size(72, 74)
        Me.CircularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP
        Me.CircularProgress1.TabIndex = 179
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.dateconfirm
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.Control = Me.MaskedTextBoxAdv1
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'ButtonItem9
        '
        Me.ButtonItem9.Image = Global.YKP_SYSTEM.My.Resources.Resources.report_go
        Me.ButtonItem9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem9.Name = "ButtonItem9"
        Me.ButtonItem9.SubItemsExpandWidth = 14
        Me.ButtonItem9.Text = "Confirm Booking"
        '
        'ControlContainerItem3
        '
        Me.ControlContainerItem3.AllowItemResize = False
        Me.ControlContainerItem3.Control = Me.CircularProgress1
        Me.ControlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem3.Name = "ControlContainerItem3"
        '
        'RibbonBar3
        '
        Me.RibbonBar3.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar3.ContainerControlProcessDialogKey = True
        Me.RibbonBar3.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar3.DragDropSupport = True
        Me.RibbonBar3.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.ButtonItem11, Me.ButtonItem12})
        Me.RibbonBar3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar3.Location = New System.Drawing.Point(404, 0)
        Me.RibbonBar3.Name = "RibbonBar3"
        Me.RibbonBar3.Size = New System.Drawing.Size(90, 100)
        Me.RibbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar3.TabIndex = 10
        Me.RibbonBar3.Text = "Gate"
        '
        '
        '
        Me.RibbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar3.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        '
        'ButtonItem11
        '
        Me.ButtonItem11.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem11.Name = "ButtonItem11"
        Me.ButtonItem11.SubItemsExpandWidth = 14
        Me.ButtonItem11.Text = "GET OUT"
        '
        'ButtonItem12
        '
        Me.ButtonItem12.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem12.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem12.Name = "ButtonItem12"
        Me.ButtonItem12.SubItemsExpandWidth = 14
        Me.ButtonItem12.Text = "GET  IN"
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar1.DragDropSupport = True
        Me.RibbonBar1.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem3, Me.ButtonItem5, Me.ButtonItem8, Me.ButtonItem7})
        Me.RibbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar1.Location = New System.Drawing.Point(250, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(154, 100)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar1.TabIndex = 9
        Me.RibbonBar1.Text = "Report"
        '
        '
        '
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        '
        'ButtonItem5
        '
        Me.ButtonItem5.Image = Global.YKP_SYSTEM.My.Resources.Resources.report_go
        Me.ButtonItem5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.SubItemsExpandWidth = 14
        Me.ButtonItem5.Text = "Container Movement"
        '
        'ButtonItem8
        '
        Me.ButtonItem8.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem8.Name = "ButtonItem8"
        Me.ButtonItem8.SubItemsExpandWidth = 14
        Me.ButtonItem8.Text = "BL Report"
        '
        'ButtonItem7
        '
        Me.ButtonItem7.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.SubItemsExpandWidth = 14
        Me.ButtonItem7.Text = "YKP Ocean"
        '
        'RibbonBar2
        '
        Me.RibbonBar2.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.ContainerControlProcessDialogKey = True
        Me.RibbonBar2.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar2.DragDropSupport = True
        Me.RibbonBar2.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem2, Me.ButtonItem13, Me.ButtonItem1, Me.ButtonItem3, Me.ButtonItem14})
        Me.RibbonBar2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar2.Location = New System.Drawing.Point(0, 0)
        Me.RibbonBar2.Name = "RibbonBar2"
        Me.RibbonBar2.Size = New System.Drawing.Size(250, 100)
        Me.RibbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar2.TabIndex = 8
        Me.RibbonBar2.Text = "Manage Voyage"
        '
        '
        '
        Me.RibbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        '
        'ButtonItem13
        '
        Me.ButtonItem13.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem13.Name = "ButtonItem13"
        Me.ButtonItem13.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem16, Me.ButtonItem17, Me.ButtonItem18})
        Me.ButtonItem13.SubItemsExpandWidth = 14
        Me.ButtonItem13.Symbol = ""
        Me.ButtonItem13.SymbolSize = 35.0!
        Me.ButtonItem13.Text = "Booking"
        '
        'ButtonItem16
        '
        Me.ButtonItem16.Image = Global.YKP_SYSTEM.My.Resources.Resources.document_comment_above
        Me.ButtonItem16.Name = "ButtonItem16"
        Me.ButtonItem16.Text = "Add Booking"
        '
        'ButtonItem17
        '
        Me.ButtonItem17.Image = Global.YKP_SYSTEM.My.Resources.Resources.page_edit
        Me.ButtonItem17.Name = "ButtonItem17"
        Me.ButtonItem17.Text = "Edit Booking"
        '
        'ButtonItem18
        '
        Me.ButtonItem18.Name = "ButtonItem18"
        Me.ButtonItem18.Symbol = ""
        Me.ButtonItem18.SymbolSize = 25.0!
        Me.ButtonItem18.Text = "Delete Booking"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.Image = Global.YKP_SYSTEM.My.Resources.Resources.images2
        Me.ButtonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem2, Me.ButtonItem10, Me.ButtonItem19})
        Me.ButtonItem1.SubItemsExpandWidth = 14
        Me.ButtonItem1.Text = "Container"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.Image = Global.YKP_SYSTEM.My.Resources.Resources.addcontainer
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Text = "Add Container"
        '
        'ButtonItem10
        '
        Me.ButtonItem10.Image = Global.YKP_SYSTEM.My.Resources.Resources.Info
        Me.ButtonItem10.Name = "ButtonItem10"
        Me.ButtonItem10.Text = "Edit Container"
        '
        'ButtonItem19
        '
        Me.ButtonItem19.Name = "ButtonItem19"
        Me.ButtonItem19.Symbol = ""
        Me.ButtonItem19.SymbolSize = 25.0!
        Me.ButtonItem19.Tag = ""
        Me.ButtonItem19.Text = "Delete Container"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem4})
        Me.ButtonItem3.SubItemsExpandWidth = 14
        Me.ButtonItem3.Symbol = ""
        Me.ButtonItem3.SymbolSize = 35.0!
        Me.ButtonItem3.Text = "Invoice"
        '
        'ButtonItem4
        '
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.Symbol = ""
        Me.ButtonItem4.Text = "Create Invoice"
        '
        'ButtonItem14
        '
        Me.ButtonItem14.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem14.Name = "ButtonItem14"
        Me.ButtonItem14.SubItemsExpandWidth = 14
        Me.ButtonItem14.Symbol = ""
        Me.ButtonItem14.SymbolSize = 30.0!
        Me.ButtonItem14.Text = "ย้าย Booking"
        '
        'txt_eta_penang
        '
        Me.txt_eta_penang.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_eta_penang.Location = New System.Drawing.Point(889, 122)
        Me.txt_eta_penang.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_eta_penang.Name = "txt_eta_penang"
        Me.txt_eta_penang.ReadOnly = True
        Me.txt_eta_penang.Size = New System.Drawing.Size(176, 30)
        Me.txt_eta_penang.TabIndex = 177
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(56, 125)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 22)
        Me.Label3.TabIndex = 178
        Me.Label3.Text = "จำนวนตู้ทั้งหมด"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(144, 122)
        Me.TextBox1.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(93, 30)
        Me.TextBox1.TabIndex = 179
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(243, 122)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(93, 30)
        Me.TextBox2.TabIndex = 180
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(11, 455)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(88, 41)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.TabIndex = 205
        Me.ButtonX1.Text = "Back"
        '
        'DataGridViewX1
        '
        Me.DataGridViewX1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewX1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewX1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.AGENT, Me.Column1, Me.Column5, Me.Column4, Me.HHMM, Me.C, Me.DateIn, Me.HHMM1, Me.C1, Me.Status, Me.Invoice, Me.Save, Me.idborrow1, Me.ctnmainid, Me.check, Me.check1})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewX1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewX1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.DataGridViewX1.EnableHeadersVisualStyles = False
        Me.DataGridViewX1.GridColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.DataGridViewX1.Location = New System.Drawing.Point(431, 169)
        Me.DataGridViewX1.MultiSelect = False
        Me.DataGridViewX1.Name = "DataGridViewX1"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewX1.RowTemplate.Height = 21
        Me.DataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.DataGridViewX1.Size = New System.Drawing.Size(906, 327)
        Me.DataGridViewX1.TabIndex = 206
        '
        'Column2
        '
        Me.Column2.HeaderText = "Container ID"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 90
        '
        'Column3
        '
        Me.Column3.HeaderText = "Seal ID"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 90
        '
        'AGENT
        '
        Me.AGENT.HeaderText = "AGENT"
        Me.AGENT.Name = "AGENT"
        Me.AGENT.Width = 80
        '
        'Column1
        '
        Me.Column1.HeaderText = "Transport"
        Me.Column1.Name = "Column1"
        '
        'Column5
        '
        Me.Column5.HeaderText = "No-Car"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 80
        '
        'Column4
        '
        Me.Column4.HeaderText = "DateOut"
        Me.Column4.Name = "Column4"
        Me.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column4.Width = 75
        '
        'HHMM
        '
        Me.HHMM.HeaderText = "HHMM"
        Me.HHMM.Name = "HHMM"
        Me.HHMM.Width = 40
        '
        'C
        '
        Me.C.Checked = True
        Me.C.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.C.CheckValue = Nothing
        Me.C.HeaderText = "C1"
        Me.C.Name = "C"
        Me.C.Width = 15
        '
        'DateIn
        '
        Me.DateIn.HeaderText = "DateIn"
        Me.DateIn.Name = "DateIn"
        Me.DateIn.Width = 75
        '
        'HHMM1
        '
        Me.HHMM1.HeaderText = "HHMM1"
        Me.HHMM1.Name = "HHMM1"
        Me.HHMM1.Width = 40
        '
        'C1
        '
        Me.C1.Checked = True
        Me.C1.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.C1.CheckValue = Nothing
        Me.C1.HeaderText = "C1"
        Me.C1.Name = "C1"
        Me.C1.Width = 15
        '
        'Status
        '
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Status.DefaultCellStyle = DataGridViewCellStyle2
        Me.Status.HeaderText = "สถานะ"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        Me.Status.Width = 35
        '
        'Invoice
        '
        Me.Invoice.DisplayMember = "Text"
        Me.Invoice.DropDownHeight = 106
        Me.Invoice.DropDownWidth = 121
        Me.Invoice.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Invoice.HeaderText = "Invoice"
        Me.Invoice.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Invoice.IntegralHeight = False
        Me.Invoice.ItemHeight = 15
        Me.Invoice.Name = "Invoice"
        Me.Invoice.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Invoice.Width = 76
        '
        'Save
        '
        Me.Save.HeaderText = "Save"
        Me.Save.Name = "Save"
        Me.Save.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.Save.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Save.Text = "บันทึก"
        Me.Save.UseColumnTextForButtonValue = True
        Me.Save.Width = 40
        '
        'idborrow1
        '
        Me.idborrow1.HeaderText = "idborrow"
        Me.idborrow1.MinimumWidth = 2
        Me.idborrow1.Name = "idborrow1"
        Me.idborrow1.ReadOnly = True
        Me.idborrow1.Width = 2
        '
        'ctnmainid
        '
        Me.ctnmainid.HeaderText = "ctnmainid"
        Me.ctnmainid.MinimumWidth = 2
        Me.ctnmainid.Name = "ctnmainid"
        Me.ctnmainid.ReadOnly = True
        Me.ctnmainid.Width = 2
        '
        'check
        '
        Me.check.HeaderText = "check"
        Me.check.MinimumWidth = 50
        Me.check.Name = "check"
        Me.check.Visible = False
        '
        'check1
        '
        Me.check1.HeaderText = "check1"
        Me.check1.MinimumWidth = 50
        Me.check1.Name = "check1"
        Me.check1.Visible = False
        '
        'frmview_voyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1386, 694)
        Me.Controls.Add(Me.DataGridViewX1)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_eta_penang)
        Me.Controls.Add(Me.Voyage)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txt_from)
        Me.Controls.Add(Me.txt_eta)
        Me.Controls.Add(Me.txt_gross)
        Me.Controls.Add(Me.txt_nett)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txt_nationality)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_namemaster)
        Me.Controls.Add(Me.txt_vessel)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmview_voyage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmview_voyage"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Voyage.ResumeLayout(False)
        Me.Voyage.PerformLayout()
        Me.RibbonBar5.ResumeLayout(False)
        Me.RibbonBar4.ResumeLayout(False)
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_voyage As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txt_from As System.Windows.Forms.TextBox
    Friend WithEvents txt_eta As System.Windows.Forms.TextBox
    Friend WithEvents txt_gross As System.Windows.Forms.TextBox
    Friend WithEvents txt_nett As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_nationality As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_namemaster As System.Windows.Forms.TextBox
    Friend WithEvents txt_vessel As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents Voyage As DevComponents.DotNetBar.RibbonBarMergeContainer
    Private WithEvents RibbonBar2 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem8 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar3 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem11 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem12 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar4 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem9 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents dateconfirm As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents MaskedTextBoxAdv1 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents txt_eta_penang As System.Windows.Forms.TextBox
    Friend WithEvents CircularProgress1 As DevComponents.DotNetBar.Controls.CircularProgress
    Friend WithEvents ControlContainerItem3 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonItem14 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar5 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents MaskedTextBoxAdv3 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents MaskedTextBoxAdv2 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ControlContainerItem4 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ControlContainerItem5 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ButtonItem15 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem13 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem16 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem17 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem18 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem10 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem19 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents DataGridViewX1 As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AGENT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HHMM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C As DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn
    Friend WithEvents DateIn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HHMM1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C1 As DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Invoice As DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn
    Friend WithEvents Save As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents idborrow1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ctnmainid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents check As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents check1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
