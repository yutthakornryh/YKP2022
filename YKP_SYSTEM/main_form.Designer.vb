﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class main_form
    Inherits DevComponents.DotNetBar.RibbonForm
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(main_form))
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer2 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer3 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem27 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem28 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem29 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem30 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem31 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer1 = New DevComponents.DotNetBar.GalleryContainer()
        Me.labelItem8 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem36 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem37 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem38 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem39 = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemContainer4 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem40 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem41 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonTabItem6 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem5 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem7 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem2 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem1 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.LayoutGroup1 = New DevComponents.DotNetBar.Layout.LayoutGroup()
        Me.ItemContainer5 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer6 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer2 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer7 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer8 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer3 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer9 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer10 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer4 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.DockContainerItem1 = New DevComponents.DotNetBar.DockContainerItem()
        Me.ItemContainer11 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer12 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer5 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem6 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer13 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer14 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer6 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem7 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer15 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer16 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer7 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem9 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer17 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer18 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer8 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem10 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer19 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer20 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer9 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem11 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer21 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer22 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer10 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem12 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer23 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer24 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer11 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem13 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer25 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer26 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer12 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem14 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer27 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer28 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer13 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem15 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer29 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer30 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer14 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer31 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer32 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer15 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem16 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer33 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer34 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer16 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem17 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer35 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer36 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer17 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem18 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer37 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer38 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer18 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem19 = New DevComponents.DotNetBar.LabelItem()
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.ItemContainer39 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer40 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer19 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem20 = New DevComponents.DotNetBar.LabelItem()
        Me.ItemContainer41 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer42 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer20 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem21 = New DevComponents.DotNetBar.LabelItem()
        Me.RibbonControl1 = New DevComponents.DotNetBar.RibbonControl()
        Me.RibbonPanel5 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar10 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem24 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel2 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar3 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem6 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar5 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem25 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem12 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar2 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem14 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel3 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar9 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem10 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar11 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem33 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem34 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem35 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar7 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem18 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem19 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem32 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar4 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem11 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem8 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem9 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel1 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar8 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem20 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar6 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem13 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem15 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem16 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem17 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonTabItem3 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.D = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem8 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.RibbonTabItem10 = New DevComponents.DotNetBar.RibbonTabItem()
        Me.QatCustomizeItem1 = New DevComponents.DotNetBar.QatCustomizeItem()
        Me.ApplicationButton1 = New DevComponents.DotNetBar.ApplicationButton()
        Me.ItemContainer1 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer43 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer44 = New DevComponents.DotNetBar.ItemContainer()
        Me.GalleryContainer21 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem22 = New DevComponents.DotNetBar.LabelItem()
        Me.ApplicationButton2 = New DevComponents.DotNetBar.ApplicationButton()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RibbonControl1.SuspendLayout()
        Me.RibbonPanel5.SuspendLayout()
        Me.RibbonPanel2.SuspendLayout()
        Me.RibbonPanel3.SuspendLayout()
        Me.RibbonPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelItem1
        '
        Me.LabelItem1.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem1.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem1.CanCustomize = False
        Me.LabelItem1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem1.Name = "LabelItem1"
        '
        'ItemContainer2
        '
        '
        '
        '
        Me.ItemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer2.ItemSpacing = 0
        Me.ItemContainer2.Name = "ItemContainer2"
        Me.ItemContainer2.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer3, Me.GalleryContainer1})
        '
        '
        '
        Me.ItemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer3
        '
        '
        '
        '
        Me.ItemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer3.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer3.Name = "ItemContainer3"
        Me.ItemContainer3.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem27, Me.ButtonItem28, Me.ButtonItem29, Me.ButtonItem30, Me.ButtonItem31})
        '
        '
        '
        Me.ItemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem27
        '
        Me.ButtonItem27.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem27.Image = CType(resources.GetObject("ButtonItem27.Image"), System.Drawing.Image)
        Me.ButtonItem27.Name = "ButtonItem27"
        Me.ButtonItem27.SubItemsExpandWidth = 24
        Me.ButtonItem27.Text = "&Open..."
        '
        'ButtonItem28
        '
        Me.ButtonItem28.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem28.Image = CType(resources.GetObject("ButtonItem28.Image"), System.Drawing.Image)
        Me.ButtonItem28.Name = "ButtonItem28"
        Me.ButtonItem28.SubItemsExpandWidth = 24
        Me.ButtonItem28.Text = "&Save..."
        '
        'ButtonItem29
        '
        Me.ButtonItem29.BeginGroup = True
        Me.ButtonItem29.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem29.Image = CType(resources.GetObject("ButtonItem29.Image"), System.Drawing.Image)
        Me.ButtonItem29.Name = "ButtonItem29"
        Me.ButtonItem29.SubItemsExpandWidth = 24
        Me.ButtonItem29.Text = "S&hare..."
        '
        'ButtonItem30
        '
        Me.ButtonItem30.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem30.Image = CType(resources.GetObject("ButtonItem30.Image"), System.Drawing.Image)
        Me.ButtonItem30.Name = "ButtonItem30"
        Me.ButtonItem30.SubItemsExpandWidth = 24
        Me.ButtonItem30.Text = "&Print..."
        '
        'ButtonItem31
        '
        Me.ButtonItem31.BeginGroup = True
        Me.ButtonItem31.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem31.Image = CType(resources.GetObject("ButtonItem31.Image"), System.Drawing.Image)
        Me.ButtonItem31.Name = "ButtonItem31"
        Me.ButtonItem31.SubItemsExpandWidth = 24
        Me.ButtonItem31.Text = "&Close"
        '
        'GalleryContainer1
        '
        '
        '
        '
        Me.GalleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer1.EnableGalleryPopup = False
        Me.GalleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer1.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer1.MultiLine = False
        Me.GalleryContainer1.Name = "GalleryContainer1"
        Me.GalleryContainer1.PopupUsesStandardScrollbars = False
        Me.GalleryContainer1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.labelItem8, Me.ButtonItem36, Me.ButtonItem37, Me.ButtonItem38, Me.ButtonItem39})
        '
        '
        '
        Me.GalleryContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'labelItem8
        '
        Me.labelItem8.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.labelItem8.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.labelItem8.CanCustomize = False
        Me.labelItem8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelItem8.Name = "labelItem8"
        Me.labelItem8.PaddingBottom = 2
        Me.labelItem8.PaddingTop = 2
        Me.labelItem8.Stretch = True
        Me.labelItem8.Text = "Recent Documents"
        '
        'ButtonItem36
        '
        Me.ButtonItem36.Name = "ButtonItem36"
        Me.ButtonItem36.Text = "&1. Short News 5-7.rtf"
        '
        'ButtonItem37
        '
        Me.ButtonItem37.Name = "ButtonItem37"
        Me.ButtonItem37.Text = "&2. Prospect Email.rtf"
        '
        'ButtonItem38
        '
        Me.ButtonItem38.Name = "ButtonItem38"
        Me.ButtonItem38.Text = "&3. Customer Email.rtf"
        '
        'ButtonItem39
        '
        Me.ButtonItem39.Name = "ButtonItem39"
        Me.ButtonItem39.Text = "&4. example.rtf"
        '
        'ItemContainer4
        '
        '
        '
        '
        Me.ItemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer"
        Me.ItemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right
        Me.ItemContainer4.Name = "ItemContainer4"
        Me.ItemContainer4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem40, Me.ButtonItem41})
        '
        '
        '
        Me.ItemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem40
        '
        Me.ButtonItem40.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem40.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonItem40.Image = CType(resources.GetObject("ButtonItem40.Image"), System.Drawing.Image)
        Me.ButtonItem40.Name = "ButtonItem40"
        Me.ButtonItem40.SubItemsExpandWidth = 24
        Me.ButtonItem40.Text = "Opt&ions"
        '
        'ButtonItem41
        '
        Me.ButtonItem41.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem41.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonItem41.Image = CType(resources.GetObject("ButtonItem41.Image"), System.Drawing.Image)
        Me.ButtonItem41.Name = "ButtonItem41"
        Me.ButtonItem41.SubItemsExpandWidth = 24
        Me.ButtonItem41.Text = "E&xit"
        '
        'RibbonTabItem6
        '
        Me.RibbonTabItem6.Name = "RibbonTabItem6"
        Me.RibbonTabItem6.Text = "Report Liner"
        '
        'RibbonTabItem5
        '
        Me.RibbonTabItem5.Name = "RibbonTabItem5"
        Me.RibbonTabItem5.Text = "Gate In/Out"
        '
        'RibbonTabItem7
        '
        Me.RibbonTabItem7.Name = "RibbonTabItem7"
        Me.RibbonTabItem7.Text = "Consignee Management"
        '
        'RibbonTabItem2
        '
        Me.RibbonTabItem2.Name = "RibbonTabItem2"
        Me.RibbonTabItem2.Text = "Container Management"
        '
        'RibbonTabItem1
        '
        Me.RibbonTabItem1.Checked = True
        Me.RibbonTabItem1.Name = "RibbonTabItem1"
        Me.RibbonTabItem1.Text = "Voyage"
        '
        'LayoutGroup1
        '
        Me.LayoutGroup1.Height = 100
        Me.LayoutGroup1.MinSize = New System.Drawing.Size(120, 32)
        Me.LayoutGroup1.Name = "LayoutGroup1"
        Me.LayoutGroup1.TextPosition = DevComponents.DotNetBar.Layout.eLayoutPosition.Top
        Me.LayoutGroup1.Width = 200
        '
        'ItemContainer5
        '
        '
        '
        '
        Me.ItemContainer5.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer5.ItemSpacing = 0
        Me.ItemContainer5.Name = "ItemContainer5"
        Me.ItemContainer5.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer6})
        '
        '
        '
        Me.ItemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer6
        '
        '
        '
        '
        Me.ItemContainer6.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer6.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer6.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer6.Name = "ItemContainer6"
        '
        '
        '
        Me.ItemContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer2
        '
        '
        '
        '
        Me.GalleryContainer2.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer2.EnableGalleryPopup = False
        Me.GalleryContainer2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer2.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer2.MultiLine = False
        Me.GalleryContainer2.Name = "GalleryContainer2"
        Me.GalleryContainer2.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem2
        '
        Me.LabelItem2.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem2.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem2.CanCustomize = False
        Me.LabelItem2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem2.Name = "LabelItem2"
        '
        'ItemContainer7
        '
        '
        '
        '
        Me.ItemContainer7.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer7.ItemSpacing = 0
        Me.ItemContainer7.Name = "ItemContainer7"
        Me.ItemContainer7.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer8})
        '
        '
        '
        Me.ItemContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer8
        '
        '
        '
        '
        Me.ItemContainer8.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer8.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer8.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer8.Name = "ItemContainer8"
        '
        '
        '
        Me.ItemContainer8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer3
        '
        '
        '
        '
        Me.GalleryContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer3.EnableGalleryPopup = False
        Me.GalleryContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer3.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer3.MultiLine = False
        Me.GalleryContainer3.Name = "GalleryContainer3"
        Me.GalleryContainer3.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem3
        '
        Me.LabelItem3.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem3.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem3.CanCustomize = False
        Me.LabelItem3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem3.Name = "LabelItem3"
        '
        'ItemContainer9
        '
        '
        '
        '
        Me.ItemContainer9.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer9.ItemSpacing = 0
        Me.ItemContainer9.Name = "ItemContainer9"
        Me.ItemContainer9.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer10})
        '
        '
        '
        Me.ItemContainer9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer10
        '
        '
        '
        '
        Me.ItemContainer10.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer10.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer10.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer10.Name = "ItemContainer10"
        '
        '
        '
        Me.ItemContainer10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer4
        '
        '
        '
        '
        Me.GalleryContainer4.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer4.EnableGalleryPopup = False
        Me.GalleryContainer4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer4.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer4.MultiLine = False
        Me.GalleryContainer4.Name = "GalleryContainer4"
        Me.GalleryContainer4.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem4
        '
        Me.LabelItem4.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem4.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem4.CanCustomize = False
        Me.LabelItem4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem4.Name = "LabelItem4"
        '
        'DockContainerItem1
        '
        Me.DockContainerItem1.Name = "DockContainerItem1"
        Me.DockContainerItem1.Text = "DockContainerItem1"
        '
        'ItemContainer11
        '
        '
        '
        '
        Me.ItemContainer11.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer11.ItemSpacing = 0
        Me.ItemContainer11.Name = "ItemContainer11"
        Me.ItemContainer11.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer12})
        '
        '
        '
        Me.ItemContainer11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer12
        '
        '
        '
        '
        Me.ItemContainer12.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer12.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer12.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer12.Name = "ItemContainer12"
        Me.ItemContainer12.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem4})
        '
        '
        '
        Me.ItemContainer12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem4
        '
        Me.ButtonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem4.Image = CType(resources.GetObject("ButtonItem4.Image"), System.Drawing.Image)
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.SubItemsExpandWidth = 24
        Me.ButtonItem4.Text = "&Save..."
        '
        'GalleryContainer5
        '
        '
        '
        '
        Me.GalleryContainer5.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer5.EnableGalleryPopup = False
        Me.GalleryContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer5.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer5.MultiLine = False
        Me.GalleryContainer5.Name = "GalleryContainer5"
        Me.GalleryContainer5.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem6
        '
        Me.LabelItem6.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem6.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem6.CanCustomize = False
        Me.LabelItem6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem6.Name = "LabelItem6"
        '
        'ItemContainer13
        '
        '
        '
        '
        Me.ItemContainer13.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer13.ItemSpacing = 0
        Me.ItemContainer13.Name = "ItemContainer13"
        Me.ItemContainer13.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer14})
        '
        '
        '
        Me.ItemContainer13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer14
        '
        '
        '
        '
        Me.ItemContainer14.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer14.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer14.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer14.Name = "ItemContainer14"
        '
        '
        '
        Me.ItemContainer14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer6
        '
        '
        '
        '
        Me.GalleryContainer6.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer6.EnableGalleryPopup = False
        Me.GalleryContainer6.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer6.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer6.MultiLine = False
        Me.GalleryContainer6.Name = "GalleryContainer6"
        Me.GalleryContainer6.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem7
        '
        Me.LabelItem7.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem7.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem7.CanCustomize = False
        Me.LabelItem7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem7.Name = "LabelItem7"
        '
        'ItemContainer15
        '
        '
        '
        '
        Me.ItemContainer15.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer15.ItemSpacing = 0
        Me.ItemContainer15.Name = "ItemContainer15"
        Me.ItemContainer15.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer16})
        '
        '
        '
        Me.ItemContainer15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer16
        '
        '
        '
        '
        Me.ItemContainer16.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer16.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer16.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer16.Name = "ItemContainer16"
        '
        '
        '
        Me.ItemContainer16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer7
        '
        '
        '
        '
        Me.GalleryContainer7.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer7.EnableGalleryPopup = False
        Me.GalleryContainer7.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer7.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer7.MultiLine = False
        Me.GalleryContainer7.Name = "GalleryContainer7"
        Me.GalleryContainer7.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem9
        '
        Me.LabelItem9.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem9.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem9.CanCustomize = False
        Me.LabelItem9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem9.Name = "LabelItem9"
        '
        'ItemContainer17
        '
        '
        '
        '
        Me.ItemContainer17.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer17.ItemSpacing = 0
        Me.ItemContainer17.Name = "ItemContainer17"
        Me.ItemContainer17.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer18})
        '
        '
        '
        Me.ItemContainer17.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer18
        '
        '
        '
        '
        Me.ItemContainer18.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer18.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer18.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer18.Name = "ItemContainer18"
        '
        '
        '
        Me.ItemContainer18.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer8
        '
        '
        '
        '
        Me.GalleryContainer8.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer8.EnableGalleryPopup = False
        Me.GalleryContainer8.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer8.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer8.MultiLine = False
        Me.GalleryContainer8.Name = "GalleryContainer8"
        Me.GalleryContainer8.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem10
        '
        Me.LabelItem10.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem10.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem10.CanCustomize = False
        Me.LabelItem10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem10.Name = "LabelItem10"
        '
        'ItemContainer19
        '
        '
        '
        '
        Me.ItemContainer19.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer19.ItemSpacing = 0
        Me.ItemContainer19.Name = "ItemContainer19"
        Me.ItemContainer19.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer20})
        '
        '
        '
        Me.ItemContainer19.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer20
        '
        '
        '
        '
        Me.ItemContainer20.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer20.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer20.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer20.Name = "ItemContainer20"
        '
        '
        '
        Me.ItemContainer20.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer9
        '
        '
        '
        '
        Me.GalleryContainer9.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer9.EnableGalleryPopup = False
        Me.GalleryContainer9.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer9.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer9.MultiLine = False
        Me.GalleryContainer9.Name = "GalleryContainer9"
        Me.GalleryContainer9.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem11
        '
        Me.LabelItem11.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem11.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem11.CanCustomize = False
        Me.LabelItem11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem11.Name = "LabelItem11"
        '
        'ItemContainer21
        '
        '
        '
        '
        Me.ItemContainer21.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer21.ItemSpacing = 0
        Me.ItemContainer21.Name = "ItemContainer21"
        Me.ItemContainer21.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer22})
        '
        '
        '
        Me.ItemContainer21.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer22
        '
        '
        '
        '
        Me.ItemContainer22.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer22.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer22.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer22.Name = "ItemContainer22"
        '
        '
        '
        Me.ItemContainer22.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer10
        '
        '
        '
        '
        Me.GalleryContainer10.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer10.EnableGalleryPopup = False
        Me.GalleryContainer10.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer10.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer10.MultiLine = False
        Me.GalleryContainer10.Name = "GalleryContainer10"
        Me.GalleryContainer10.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem12
        '
        Me.LabelItem12.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem12.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem12.CanCustomize = False
        Me.LabelItem12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem12.Name = "LabelItem12"
        '
        'ItemContainer23
        '
        '
        '
        '
        Me.ItemContainer23.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer23.ItemSpacing = 0
        Me.ItemContainer23.Name = "ItemContainer23"
        Me.ItemContainer23.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer24})
        '
        '
        '
        Me.ItemContainer23.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer24
        '
        '
        '
        '
        Me.ItemContainer24.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer24.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer24.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer24.Name = "ItemContainer24"
        '
        '
        '
        Me.ItemContainer24.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer11
        '
        '
        '
        '
        Me.GalleryContainer11.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer11.EnableGalleryPopup = False
        Me.GalleryContainer11.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer11.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer11.MultiLine = False
        Me.GalleryContainer11.Name = "GalleryContainer11"
        Me.GalleryContainer11.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem13
        '
        Me.LabelItem13.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem13.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem13.CanCustomize = False
        Me.LabelItem13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem13.Name = "LabelItem13"
        '
        'ItemContainer25
        '
        '
        '
        '
        Me.ItemContainer25.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer25.ItemSpacing = 0
        Me.ItemContainer25.Name = "ItemContainer25"
        Me.ItemContainer25.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer26})
        '
        '
        '
        Me.ItemContainer25.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer26
        '
        '
        '
        '
        Me.ItemContainer26.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer26.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer26.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer26.Name = "ItemContainer26"
        '
        '
        '
        Me.ItemContainer26.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer12
        '
        '
        '
        '
        Me.GalleryContainer12.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer12.EnableGalleryPopup = False
        Me.GalleryContainer12.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer12.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer12.MultiLine = False
        Me.GalleryContainer12.Name = "GalleryContainer12"
        Me.GalleryContainer12.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer12.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem14
        '
        Me.LabelItem14.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem14.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem14.CanCustomize = False
        Me.LabelItem14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem14.Name = "LabelItem14"
        '
        'ItemContainer27
        '
        '
        '
        '
        Me.ItemContainer27.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer27.ItemSpacing = 0
        Me.ItemContainer27.Name = "ItemContainer27"
        Me.ItemContainer27.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer28})
        '
        '
        '
        Me.ItemContainer27.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer28
        '
        '
        '
        '
        Me.ItemContainer28.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer28.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer28.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer28.Name = "ItemContainer28"
        '
        '
        '
        Me.ItemContainer28.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer13
        '
        '
        '
        '
        Me.GalleryContainer13.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer13.EnableGalleryPopup = False
        Me.GalleryContainer13.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer13.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer13.MultiLine = False
        Me.GalleryContainer13.Name = "GalleryContainer13"
        Me.GalleryContainer13.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer13.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem15
        '
        Me.LabelItem15.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem15.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem15.CanCustomize = False
        Me.LabelItem15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem15.Name = "LabelItem15"
        '
        'ItemContainer29
        '
        '
        '
        '
        Me.ItemContainer29.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer29.ItemSpacing = 0
        Me.ItemContainer29.Name = "ItemContainer29"
        Me.ItemContainer29.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer30})
        '
        '
        '
        Me.ItemContainer29.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer30
        '
        '
        '
        '
        Me.ItemContainer30.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer30.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer30.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer30.Name = "ItemContainer30"
        '
        '
        '
        Me.ItemContainer30.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer14
        '
        '
        '
        '
        Me.GalleryContainer14.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer14.EnableGalleryPopup = False
        Me.GalleryContainer14.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer14.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer14.MultiLine = False
        Me.GalleryContainer14.Name = "GalleryContainer14"
        Me.GalleryContainer14.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer14.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem5
        '
        Me.LabelItem5.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem5.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem5.CanCustomize = False
        Me.LabelItem5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem5.Name = "LabelItem5"
        '
        'ItemContainer31
        '
        '
        '
        '
        Me.ItemContainer31.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer31.ItemSpacing = 0
        Me.ItemContainer31.Name = "ItemContainer31"
        Me.ItemContainer31.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer32})
        '
        '
        '
        Me.ItemContainer31.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer32
        '
        '
        '
        '
        Me.ItemContainer32.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer32.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer32.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer32.Name = "ItemContainer32"
        '
        '
        '
        Me.ItemContainer32.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer15
        '
        '
        '
        '
        Me.GalleryContainer15.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer15.EnableGalleryPopup = False
        Me.GalleryContainer15.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer15.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer15.MultiLine = False
        Me.GalleryContainer15.Name = "GalleryContainer15"
        Me.GalleryContainer15.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer15.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem16
        '
        Me.LabelItem16.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem16.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem16.CanCustomize = False
        Me.LabelItem16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem16.Name = "LabelItem16"
        '
        'ItemContainer33
        '
        '
        '
        '
        Me.ItemContainer33.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer33.ItemSpacing = 0
        Me.ItemContainer33.Name = "ItemContainer33"
        Me.ItemContainer33.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer34})
        '
        '
        '
        Me.ItemContainer33.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer34
        '
        '
        '
        '
        Me.ItemContainer34.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer34.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer34.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer34.Name = "ItemContainer34"
        '
        '
        '
        Me.ItemContainer34.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer16
        '
        '
        '
        '
        Me.GalleryContainer16.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer16.EnableGalleryPopup = False
        Me.GalleryContainer16.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer16.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer16.MultiLine = False
        Me.GalleryContainer16.Name = "GalleryContainer16"
        Me.GalleryContainer16.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer16.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem17
        '
        Me.LabelItem17.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem17.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem17.CanCustomize = False
        Me.LabelItem17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem17.Name = "LabelItem17"
        '
        'ItemContainer35
        '
        '
        '
        '
        Me.ItemContainer35.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer35.ItemSpacing = 0
        Me.ItemContainer35.Name = "ItemContainer35"
        Me.ItemContainer35.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer36})
        '
        '
        '
        Me.ItemContainer35.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer36
        '
        '
        '
        '
        Me.ItemContainer36.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer36.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer36.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer36.Name = "ItemContainer36"
        '
        '
        '
        Me.ItemContainer36.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer17
        '
        '
        '
        '
        Me.GalleryContainer17.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer17.EnableGalleryPopup = False
        Me.GalleryContainer17.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer17.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer17.MultiLine = False
        Me.GalleryContainer17.Name = "GalleryContainer17"
        Me.GalleryContainer17.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer17.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem18
        '
        Me.LabelItem18.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem18.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem18.CanCustomize = False
        Me.LabelItem18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem18.Name = "LabelItem18"
        '
        'ItemContainer37
        '
        '
        '
        '
        Me.ItemContainer37.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer37.ItemSpacing = 0
        Me.ItemContainer37.Name = "ItemContainer37"
        Me.ItemContainer37.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer38})
        '
        '
        '
        Me.ItemContainer37.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer38
        '
        '
        '
        '
        Me.ItemContainer38.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer38.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer38.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer38.Name = "ItemContainer38"
        '
        '
        '
        Me.ItemContainer38.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer18
        '
        '
        '
        '
        Me.GalleryContainer18.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer18.EnableGalleryPopup = False
        Me.GalleryContainer18.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer18.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer18.MultiLine = False
        Me.GalleryContainer18.Name = "GalleryContainer18"
        Me.GalleryContainer18.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer18.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem19
        '
        Me.LabelItem19.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem19.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem19.CanCustomize = False
        Me.LabelItem19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem19.Name = "LabelItem19"
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2013
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(154, Byte), Integer)))
        '
        'ItemContainer39
        '
        '
        '
        '
        Me.ItemContainer39.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer39.ItemSpacing = 0
        Me.ItemContainer39.Name = "ItemContainer39"
        Me.ItemContainer39.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer40})
        '
        '
        '
        Me.ItemContainer39.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer40
        '
        '
        '
        '
        Me.ItemContainer40.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer40.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer40.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer40.Name = "ItemContainer40"
        '
        '
        '
        Me.ItemContainer40.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer19
        '
        '
        '
        '
        Me.GalleryContainer19.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer19.EnableGalleryPopup = False
        Me.GalleryContainer19.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer19.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer19.MultiLine = False
        Me.GalleryContainer19.Name = "GalleryContainer19"
        Me.GalleryContainer19.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer19.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem20
        '
        Me.LabelItem20.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem20.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem20.CanCustomize = False
        Me.LabelItem20.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem20.Name = "LabelItem20"
        '
        'ItemContainer41
        '
        '
        '
        '
        Me.ItemContainer41.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer41.ItemSpacing = 0
        Me.ItemContainer41.Name = "ItemContainer41"
        Me.ItemContainer41.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer42})
        '
        '
        '
        Me.ItemContainer41.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer42
        '
        '
        '
        '
        Me.ItemContainer42.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer42.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer42.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer42.Name = "ItemContainer42"
        '
        '
        '
        Me.ItemContainer42.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer20
        '
        '
        '
        '
        Me.GalleryContainer20.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer20.EnableGalleryPopup = False
        Me.GalleryContainer20.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer20.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer20.MultiLine = False
        Me.GalleryContainer20.Name = "GalleryContainer20"
        Me.GalleryContainer20.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer20.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem21
        '
        Me.LabelItem21.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem21.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem21.CanCustomize = False
        Me.LabelItem21.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem21.Name = "LabelItem21"
        '
        'RibbonControl1
        '
        Me.RibbonControl1.AllowDrop = True
        Me.RibbonControl1.AutoExpand = False
        Me.RibbonControl1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.RibbonControl1.BackgroundImage = Global.YKP_SYSTEM.My.Resources.Resources.RibbonCircuit
        '
        '
        '
        Me.RibbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonControl1.CaptionVisible = True
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel2)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel5)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel3)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel1)
        Me.RibbonControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RibbonControl1.ForeColor = System.Drawing.Color.Black
        Me.RibbonControl1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.RibbonTabItem3, Me.D, Me.RibbonTabItem8, Me.RibbonTabItem10})
        Me.RibbonControl1.KeyTipsFont = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RibbonControl1.Location = New System.Drawing.Point(5, 1)
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.QuickToolbarItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.QatCustomizeItem1})
        Me.RibbonControl1.Size = New System.Drawing.Size(1376, 155)
        Me.RibbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon"
        Me.RibbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon"
        Me.RibbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>"
        Me.RibbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar..."
        Me.RibbonControl1.SystemText.QatDialogAddButton = "&Add >>"
        Me.RibbonControl1.SystemText.QatDialogCancelButton = "Cancel"
        Me.RibbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:"
        Me.RibbonControl1.SystemText.QatDialogOkButton = "OK"
        Me.RibbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatDialogRemoveButton = "&Remove"
        Me.RibbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon"
        Me.RibbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar"
        Me.RibbonControl1.TabGroupHeight = 14
        Me.RibbonControl1.TabIndex = 3
        Me.RibbonControl1.Text = "RibbonControl1"
        '
        'RibbonPanel5
        '
        Me.RibbonPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonPanel5.Controls.Add(Me.RibbonBar10)
        Me.RibbonPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel5.Location = New System.Drawing.Point(0, 53)
        Me.RibbonPanel5.Name = "RibbonPanel5"
        Me.RibbonPanel5.Padding = New System.Windows.Forms.Padding(3, 0, 3, 2)
        Me.RibbonPanel5.Size = New System.Drawing.Size(1376, 102)
        '
        '
        '
        Me.RibbonPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel5.TabIndex = 5
        Me.RibbonPanel5.Visible = False
        '
        'RibbonBar10
        '
        Me.RibbonBar10.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar10.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar10.ContainerControlProcessDialogKey = True
        Me.RibbonBar10.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar10.DragDropSupport = True
        Me.RibbonBar10.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem24})
        Me.RibbonBar10.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar10.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar10.Name = "RibbonBar10"
        Me.RibbonBar10.Size = New System.Drawing.Size(100, 100)
        Me.RibbonBar10.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar10.TabIndex = 1
        Me.RibbonBar10.Text = "Report Liner"
        '
        '
        '
        Me.RibbonBar10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar10.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem24
        '
        Me.ButtonItem24.ImagePaddingHorizontal = 35
        Me.ButtonItem24.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem24.Name = "ButtonItem24"
        Me.ButtonItem24.SubItemsExpandWidth = 14
        Me.ButtonItem24.Symbol = ""
        Me.ButtonItem24.SymbolSize = 40.0!
        Me.ButtonItem24.Text = "REPORT"
        '
        'RibbonPanel2
        '
        Me.RibbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar3)
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar5)
        Me.RibbonPanel2.Controls.Add(Me.RibbonBar2)
        Me.RibbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel2.Location = New System.Drawing.Point(0, 53)
        Me.RibbonPanel2.Name = "RibbonPanel2"
        Me.RibbonPanel2.Padding = New System.Windows.Forms.Padding(3, 0, 3, 2)
        Me.RibbonPanel2.Size = New System.Drawing.Size(1376, 102)
        '
        '
        '
        Me.RibbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel2.TabIndex = 2
        '
        'RibbonBar3
        '
        Me.RibbonBar3.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar3.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar3.ContainerControlProcessDialogKey = True
        Me.RibbonBar3.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar3.DragDropSupport = True
        Me.RibbonBar3.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem6})
        Me.RibbonBar3.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar3.Location = New System.Drawing.Point(262, 0)
        Me.RibbonBar3.Name = "RibbonBar3"
        Me.RibbonBar3.Size = New System.Drawing.Size(95, 100)
        Me.RibbonBar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar3.TabIndex = 6
        Me.RibbonBar3.Text = "จัดการข้อมูล Depot"
        '
        '
        '
        Me.RibbonBar3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar3.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar3.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem6
        '
        Me.ButtonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem6.Image = Global.YKP_SYSTEM.My.Resources.Resources.depot_image
        Me.ButtonItem6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem6.Name = "ButtonItem6"
        Me.ButtonItem6.SubItemsExpandWidth = 14
        Me.ButtonItem6.Text = "Depot Main"
        '
        'RibbonBar5
        '
        Me.RibbonBar5.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar5.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar5.ContainerControlProcessDialogKey = True
        Me.RibbonBar5.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar5.DragDropSupport = True
        Me.RibbonBar5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar5.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem25, Me.ButtonItem12})
        Me.RibbonBar5.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar5.Location = New System.Drawing.Point(114, 0)
        Me.RibbonBar5.Name = "RibbonBar5"
        Me.RibbonBar5.Size = New System.Drawing.Size(148, 100)
        Me.RibbonBar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar5.TabIndex = 5
        Me.RibbonBar5.Text = "รายงานตู้เสีย"
        '
        '
        '
        Me.RibbonBar5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar5.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar5.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem25
        '
        Me.ButtonItem25.Image = Global.YKP_SYSTEM.My.Resources.Resources.report
        Me.ButtonItem25.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem25.Name = "ButtonItem25"
        Me.ButtonItem25.SubItemsExpandWidth = 14
        Me.ButtonItem25.Text = "Report จำนวนการซ่อม"
        '
        'ButtonItem12
        '
        Me.ButtonItem12.Image = Global.YKP_SYSTEM.My.Resources.Resources.report
        Me.ButtonItem12.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem12.Name = "ButtonItem12"
        Me.ButtonItem12.SubItemsExpandWidth = 14
        Me.ButtonItem12.Text = "Report Damage"
        '
        'RibbonBar2
        '
        Me.RibbonBar2.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.ContainerControlProcessDialogKey = True
        Me.RibbonBar2.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar2.DragDropSupport = True
        Me.RibbonBar2.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem3, Me.ButtonItem14})
        Me.RibbonBar2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar2.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar2.Name = "RibbonBar2"
        Me.RibbonBar2.Size = New System.Drawing.Size(111, 100)
        Me.RibbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar2.TabIndex = 3
        Me.RibbonBar2.Text = "Container Excel"
        '
        '
        '
        Me.RibbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem3.Image = Global.YKP_SYSTEM.My.Resources.Resources.images1
        Me.ButtonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.SubItemsExpandWidth = 14
        Me.ButtonItem3.Text = "นำเข้า Container"
        '
        'ButtonItem14
        '
        Me.ButtonItem14.Image = Global.YKP_SYSTEM.My.Resources.Resources.addcontainer
        Me.ButtonItem14.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem14.Name = "ButtonItem14"
        Me.ButtonItem14.SubItemsExpandWidth = 14
        Me.ButtonItem14.Text = "เพิ่มตู้"
        '
        'RibbonPanel3
        '
        Me.RibbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar9)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar11)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar7)
        Me.RibbonPanel3.Controls.Add(Me.RibbonBar4)
        Me.RibbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel3.Location = New System.Drawing.Point(0, 53)
        Me.RibbonPanel3.Name = "RibbonPanel3"
        Me.RibbonPanel3.Padding = New System.Windows.Forms.Padding(3, 0, 3, 2)
        Me.RibbonPanel3.Size = New System.Drawing.Size(1376, 102)
        '
        '
        '
        Me.RibbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel3.TabIndex = 3
        Me.RibbonPanel3.Visible = False
        '
        'RibbonBar9
        '
        Me.RibbonBar9.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar9.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar9.ContainerControlProcessDialogKey = True
        Me.RibbonBar9.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar9.DragDropSupport = True
        Me.RibbonBar9.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar9.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem5, Me.ButtonItem7, Me.ButtonItem10})
        Me.RibbonBar9.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar9.Location = New System.Drawing.Point(541, 0)
        Me.RibbonBar9.Name = "RibbonBar9"
        Me.RibbonBar9.Size = New System.Drawing.Size(139, 100)
        Me.RibbonBar9.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar9.TabIndex = 9
        Me.RibbonBar9.Text = "โค้ดซ่อม"
        '
        '
        '
        Me.RibbonBar9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar9.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar9.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem5
        '
        Me.ButtonItem5.BeginGroup = True
        Me.ButtonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem5.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_add
        Me.ButtonItem5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.SubItemsExpandWidth = 14
        Me.ButtonItem5.Symbol = ""
        Me.ButtonItem5.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItem5.SymbolSize = 30.0!
        Me.ButtonItem5.Text = "เพิ่มชื่อ โค้ช"
        '
        'ButtonItem7
        '
        Me.ButtonItem7.BeginGroup = True
        Me.ButtonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem7.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_edit
        Me.ButtonItem7.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.SubItemsExpandWidth = 14
        Me.ButtonItem7.Symbol = ""
        Me.ButtonItem7.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ButtonItem7.SymbolSize = 30.0!
        Me.ButtonItem7.Text = "แก้ไข โค๊ช"
        '
        'ButtonItem10
        '
        Me.ButtonItem10.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_delete
        Me.ButtonItem10.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem10.Name = "ButtonItem10"
        Me.ButtonItem10.SubItemsExpandWidth = 14
        Me.ButtonItem10.Symbol = ""
        Me.ButtonItem10.SymbolSize = 30.0!
        Me.ButtonItem10.Text = "ลบ โค๊ช"
        '
        'RibbonBar11
        '
        Me.RibbonBar11.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar11.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar11.ContainerControlProcessDialogKey = True
        Me.RibbonBar11.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar11.DragDropSupport = True
        Me.RibbonBar11.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar11.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem33, Me.ButtonItem34, Me.ButtonItem35})
        Me.RibbonBar11.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar11.Location = New System.Drawing.Point(334, 0)
        Me.RibbonBar11.Name = "RibbonBar11"
        Me.RibbonBar11.Size = New System.Drawing.Size(207, 100)
        Me.RibbonBar11.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar11.TabIndex = 8
        Me.RibbonBar11.Text = "Consignee"
        '
        '
        '
        Me.RibbonBar11.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar11.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar11.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem33
        '
        Me.ButtonItem33.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_add
        Me.ButtonItem33.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem33.Name = "ButtonItem33"
        Me.ButtonItem33.SubItemsExpandWidth = 14
        Me.ButtonItem33.Text = "เพิ่มชื่อ Consignee"
        '
        'ButtonItem34
        '
        Me.ButtonItem34.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_edit
        Me.ButtonItem34.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem34.Name = "ButtonItem34"
        Me.ButtonItem34.SubItemsExpandWidth = 14
        Me.ButtonItem34.Text = "แก้ไข Consignee"
        '
        'ButtonItem35
        '
        Me.ButtonItem35.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_delete
        Me.ButtonItem35.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem35.Name = "ButtonItem35"
        Me.ButtonItem35.SubItemsExpandWidth = 14
        Me.ButtonItem35.Text = "ลบ  Consignee"
        '
        'RibbonBar7
        '
        Me.RibbonBar7.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar7.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar7.ContainerControlProcessDialogKey = True
        Me.RibbonBar7.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar7.DragDropSupport = True
        Me.RibbonBar7.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar7.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem18, Me.ButtonItem19, Me.ButtonItem32})
        Me.RibbonBar7.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar7.Location = New System.Drawing.Point(169, 0)
        Me.RibbonBar7.Name = "RibbonBar7"
        Me.RibbonBar7.Size = New System.Drawing.Size(165, 100)
        Me.RibbonBar7.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar7.TabIndex = 7
        Me.RibbonBar7.Text = "Vessel"
        '
        '
        '
        Me.RibbonBar7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar7.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar7.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem18
        '
        Me.ButtonItem18.Image = Global.YKP_SYSTEM.My.Resources.Resources.vessel
        Me.ButtonItem18.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem18.Name = "ButtonItem18"
        Me.ButtonItem18.SubItemsExpandWidth = 14
        Me.ButtonItem18.Symbol = ""
        Me.ButtonItem18.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItem18.SymbolSize = 30.0!
        Me.ButtonItem18.Text = "เพิ่ม ชื่อเรือ"
        '
        'ButtonItem19
        '
        Me.ButtonItem19.Image = Global.YKP_SYSTEM.My.Resources.Resources.vessel
        Me.ButtonItem19.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem19.Name = "ButtonItem19"
        Me.ButtonItem19.SubItemsExpandWidth = 14
        Me.ButtonItem19.Symbol = ""
        Me.ButtonItem19.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ButtonItem19.SymbolSize = 30.0!
        Me.ButtonItem19.Text = "แก้ไข รายชื่อเรือ"
        '
        'ButtonItem32
        '
        Me.ButtonItem32.Image = Global.YKP_SYSTEM.My.Resources.Resources.vessel
        Me.ButtonItem32.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem32.Name = "ButtonItem32"
        Me.ButtonItem32.SubItemsExpandWidth = 14
        Me.ButtonItem32.Symbol = ""
        Me.ButtonItem32.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ButtonItem32.SymbolSize = 30.0!
        Me.ButtonItem32.Text = "ลบข้อ มูลเรือ"
        '
        'RibbonBar4
        '
        Me.RibbonBar4.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar4.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar4.ContainerControlProcessDialogKey = True
        Me.RibbonBar4.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar4.DragDropSupport = True
        Me.RibbonBar4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar4.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem11, Me.ButtonItem8, Me.ButtonItem9})
        Me.RibbonBar4.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar4.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar4.Name = "RibbonBar4"
        Me.RibbonBar4.Size = New System.Drawing.Size(166, 100)
        Me.RibbonBar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar4.TabIndex = 6
        Me.RibbonBar4.Text = "Shipper / Forwarder"
        '
        '
        '
        Me.RibbonBar4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar4.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar4.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem11
        '
        Me.ButtonItem11.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_add
        Me.ButtonItem11.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem11.Name = "ButtonItem11"
        Me.ButtonItem11.SubItemsExpandWidth = 14
        Me.ButtonItem11.Text = "เพิ่ม ผู้ส่งสินค้า"
        '
        'ButtonItem8
        '
        Me.ButtonItem8.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_edit
        Me.ButtonItem8.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem8.Name = "ButtonItem8"
        Me.ButtonItem8.SubItemsExpandWidth = 14
        Me.ButtonItem8.Text = "แก้ไข ผู้ส่งสินค้า"
        '
        'ButtonItem9
        '
        Me.ButtonItem9.Image = Global.YKP_SYSTEM.My.Resources.Resources.group_delete
        Me.ButtonItem9.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem9.Name = "ButtonItem9"
        Me.ButtonItem9.SubItemsExpandWidth = 14
        Me.ButtonItem9.Text = "ลบข้อมูล ผู้ส่งสินค้า"
        '
        'RibbonPanel1
        '
        Me.RibbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonPanel1.Controls.Add(Me.RibbonBar8)
        Me.RibbonPanel1.Controls.Add(Me.RibbonBar6)
        Me.RibbonPanel1.Controls.Add(Me.RibbonBar1)
        Me.RibbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel1.Location = New System.Drawing.Point(0, 53)
        Me.RibbonPanel1.Name = "RibbonPanel1"
        Me.RibbonPanel1.Padding = New System.Windows.Forms.Padding(3, 0, 3, 2)
        Me.RibbonPanel1.Size = New System.Drawing.Size(1376, 102)
        '
        '
        '
        Me.RibbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel1.TabIndex = 1
        Me.RibbonPanel1.Visible = False
        '
        'RibbonBar8
        '
        Me.RibbonBar8.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar8.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar8.ContainerControlProcessDialogKey = True
        Me.RibbonBar8.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar8.DragDropSupport = True
        Me.RibbonBar8.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem20})
        Me.RibbonBar8.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar8.Location = New System.Drawing.Point(326, 0)
        Me.RibbonBar8.Name = "RibbonBar8"
        Me.RibbonBar8.Size = New System.Drawing.Size(102, 100)
        Me.RibbonBar8.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar8.TabIndex = 6
        Me.RibbonBar8.Text = "VOYAGE EMPTY"
        '
        '
        '
        Me.RibbonBar8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar8.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem20
        '
        Me.ButtonItem20.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem20.Name = "ButtonItem20"
        Me.ButtonItem20.SubItemsExpandWidth = 14
        Me.ButtonItem20.Symbol = ""
        Me.ButtonItem20.SymbolColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ButtonItem20.SymbolSize = 35.0!
        Me.ButtonItem20.Text = "VOYAGE"
        '
        'RibbonBar6
        '
        Me.RibbonBar6.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar6.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar6.ContainerControlProcessDialogKey = True
        Me.RibbonBar6.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar6.DragDropSupport = True
        Me.RibbonBar6.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar6.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem13, Me.ButtonItem15, Me.ButtonItem16})
        Me.RibbonBar6.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar6.Location = New System.Drawing.Point(169, 0)
        Me.RibbonBar6.Name = "RibbonBar6"
        Me.RibbonBar6.Size = New System.Drawing.Size(157, 100)
        Me.RibbonBar6.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar6.TabIndex = 5
        Me.RibbonBar6.Text = "Report"
        '
        '
        '
        Me.RibbonBar6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar6.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar6.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem13
        '
        Me.ButtonItem13.Image = Global.YKP_SYSTEM.My.Resources.Resources.report_go
        Me.ButtonItem13.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem13.Name = "ButtonItem13"
        Me.ButtonItem13.SubItemsExpandWidth = 14
        Me.ButtonItem13.SymbolSize = 45.0!
        Me.ButtonItem13.Text = "Container Movement"
        '
        'ButtonItem15
        '
        Me.ButtonItem15.Name = "ButtonItem15"
        Me.ButtonItem15.SubItemsExpandWidth = 14
        '
        'ButtonItem16
        '
        Me.ButtonItem16.Image = Global.YKP_SYSTEM.My.Resources.Resources.document_comment_above
        Me.ButtonItem16.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem16.Name = "ButtonItem16"
        Me.ButtonItem16.SubItemsExpandWidth = 14
        Me.ButtonItem16.SymbolSize = 45.0!
        Me.ButtonItem16.Text = "จำนวนตู้คงเหลือ"
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar1.DragDropSupport = True
        Me.RibbonBar1.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem1, Me.ButtonItem2, Me.ButtonItem17})
        Me.RibbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar1.Location = New System.Drawing.Point(3, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(166, 100)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.RibbonBar1.TabIndex = 4
        Me.RibbonBar1.Text = "Voyage"
        '
        '
        '
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem1
        '
        Me.ButtonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem1.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.SubItemsExpandWidth = 14
        Me.ButtonItem1.SymbolSize = 42.0!
        Me.ButtonItem1.Text = "<div align=""center"">สร้าง Voyage </div>"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.Image = Global.YKP_SYSTEM.My.Resources.Resources.page_edit
        Me.ButtonItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.SubItemsExpandWidth = 14
        Me.ButtonItem2.SymbolSize = 40.0!
        Me.ButtonItem2.Text = "<div align=""center"">แก้ไข Voyage </div>"
        '
        'ButtonItem17
        '
        Me.ButtonItem17.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem17.Name = "ButtonItem17"
        Me.ButtonItem17.SubItemsExpandWidth = 14
        Me.ButtonItem17.Symbol = ""
        Me.ButtonItem17.SymbolSize = 35.0!
        Me.ButtonItem17.Text = "จัดการ Yoyage"
        '
        'RibbonTabItem3
        '
        Me.RibbonTabItem3.Name = "RibbonTabItem3"
        Me.RibbonTabItem3.Panel = Me.RibbonPanel1
        Me.RibbonTabItem3.Text = "Voyage"
        '
        'D
        '
        Me.D.Checked = True
        Me.D.Name = "D"
        Me.D.Panel = Me.RibbonPanel2
        Me.D.Text = "Container Management"
        '
        'RibbonTabItem8
        '
        Me.RibbonTabItem8.Name = "RibbonTabItem8"
        Me.RibbonTabItem8.Panel = Me.RibbonPanel3
        Me.RibbonTabItem8.Text = "Management"
        '
        'RibbonTabItem10
        '
        Me.RibbonTabItem10.Name = "RibbonTabItem10"
        Me.RibbonTabItem10.Panel = Me.RibbonPanel5
        Me.RibbonTabItem10.Text = "Report Liner"
        '
        'QatCustomizeItem1
        '
        Me.QatCustomizeItem1.Name = "QatCustomizeItem1"
        '
        'ApplicationButton1
        '
        Me.ApplicationButton1.AutoExpandOnClick = True
        Me.ApplicationButton1.CanCustomize = False
        Me.ApplicationButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image
        Me.ApplicationButton1.Image = CType(resources.GetObject("ApplicationButton1.Image"), System.Drawing.Image)
        Me.ApplicationButton1.ImagePaddingHorizontal = 2
        Me.ApplicationButton1.ImagePaddingVertical = 2
        Me.ApplicationButton1.Name = "ApplicationButton1"
        Me.ApplicationButton1.ShowSubItems = False
        Me.ApplicationButton1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer1})
        Me.ApplicationButton1.Text = "&File"
        '
        'ItemContainer1
        '
        '
        '
        '
        Me.ItemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer"
        Me.ItemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer1.Name = "ItemContainer1"
        Me.ItemContainer1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer5})
        '
        '
        '
        Me.ItemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer43
        '
        '
        '
        '
        Me.ItemContainer43.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer43.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer43.ItemSpacing = 0
        Me.ItemContainer43.Name = "ItemContainer43"
        Me.ItemContainer43.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer44})
        '
        '
        '
        Me.ItemContainer43.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer44
        '
        '
        '
        '
        Me.ItemContainer44.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer44.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer44.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer44.Name = "ItemContainer44"
        '
        '
        '
        Me.ItemContainer44.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'GalleryContainer21
        '
        '
        '
        '
        Me.GalleryContainer21.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer21.EnableGalleryPopup = False
        Me.GalleryContainer21.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer21.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer21.MultiLine = False
        Me.GalleryContainer21.Name = "GalleryContainer21"
        Me.GalleryContainer21.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer21.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem22
        '
        Me.LabelItem22.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem22.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem22.CanCustomize = False
        Me.LabelItem22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem22.Name = "LabelItem22"
        '
        'ApplicationButton2
        '
        Me.ApplicationButton2.AutoExpandOnClick = True
        Me.ApplicationButton2.CanCustomize = False
        Me.ApplicationButton2.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image
        Me.ApplicationButton2.Image = CType(resources.GetObject("ApplicationButton2.Image"), System.Drawing.Image)
        Me.ApplicationButton2.ImagePaddingHorizontal = 2
        Me.ApplicationButton2.ImagePaddingVertical = 2
        Me.ApplicationButton2.Name = "ApplicationButton2"
        Me.ApplicationButton2.ShowSubItems = False
        Me.ApplicationButton2.Text = "&File"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'main_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1386, 772)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MaximumSize = New System.Drawing.Size(1400, 800)
        Me.MinimumSize = New System.Drawing.Size(1364, 726)
        Me.Name = "main_form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "YKP Management System"
        Me.RibbonControl1.ResumeLayout(False)
        Me.RibbonControl1.PerformLayout()
        Me.RibbonPanel5.ResumeLayout(False)
        Me.RibbonPanel2.ResumeLayout(False)
        Me.RibbonPanel3.ResumeLayout(False)
        Me.RibbonPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer2 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer3 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem27 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem28 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem29 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem30 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem31 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer1 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents labelItem8 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem36 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem37 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem38 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem39 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemContainer4 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem40 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem41 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ApplicationButton2 As DevComponents.DotNetBar.ApplicationButton
    Private WithEvents RibbonTabItem6 As DevComponents.DotNetBar.RibbonTabItem
    Private WithEvents RibbonTabItem5 As DevComponents.DotNetBar.RibbonTabItem
    Private WithEvents RibbonTabItem7 As DevComponents.DotNetBar.RibbonTabItem
    Private WithEvents RibbonTabItem2 As DevComponents.DotNetBar.RibbonTabItem
    Private WithEvents RibbonTabItem1 As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents RibbonControl1 As DevComponents.DotNetBar.RibbonControl
    Friend WithEvents RibbonPanel2 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonPanel1 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonTabItem3 As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents D As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents QatCustomizeItem1 As DevComponents.DotNetBar.QatCustomizeItem
    Private WithEvents RibbonBar6 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem13 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem15 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem16 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem17 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents RibbonBar5 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem25 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem12 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents RibbonBar2 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel3 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents RibbonTabItem8 As DevComponents.DotNetBar.RibbonTabItem
    Private WithEvents RibbonBar11 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem33 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem34 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem35 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents RibbonBar7 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem18 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem19 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem32 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents RibbonBar4 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem11 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem8 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem9 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel5 As DevComponents.DotNetBar.RibbonPanel
    Private WithEvents RibbonBar10 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem24 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonTabItem10 As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents ApplicationButton1 As DevComponents.DotNetBar.ApplicationButton
    Friend WithEvents ItemContainer1 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer5 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer6 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer2 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer7 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer8 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer3 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer9 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer10 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer4 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents DockContainerItem1 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents ItemContainer11 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer12 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer5 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem6 As DevComponents.DotNetBar.LabelItem
    Private WithEvents RibbonBar9 As DevComponents.DotNetBar.RibbonBar
    Private WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem10 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemContainer13 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer14 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer6 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem7 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents RibbonBar3 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem6 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemContainer15 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer16 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer7 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem9 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer17 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer18 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer8 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem10 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer19 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer20 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer9 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem11 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer21 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer22 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer10 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem12 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer23 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer24 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer11 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem13 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer25 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer26 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer12 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem14 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer27 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer28 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer13 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem15 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem14 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents LayoutGroup1 As DevComponents.DotNetBar.Layout.LayoutGroup
    Friend WithEvents ItemContainer29 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer30 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer14 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer31 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer32 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer15 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem16 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer33 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer34 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer16 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem17 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer35 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer36 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer17 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem18 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer37 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer38 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer18 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem19 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents ItemContainer39 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer40 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer19 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem20 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents RibbonBar8 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem20 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemContainer41 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer42 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer20 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem21 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ItemContainer43 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer44 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents GalleryContainer21 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem22 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
End Class
