﻿Imports MySql.Data.MySqlClient
Public Class frmedit_booking
    Dim mysql As MySqlConnection = main_form.mysqlconection
    Dim mySqlCommand As New MySqlCommand
    Dim mySqlCommand1 As New MySqlCommand
    Dim mySqlAdaptor As New MySqlDataAdapter
    Dim mySqlReader As MySqlDataReader
    Dim respone As Object
    Dim consig As String
    Dim shipper As String
    Dim forwarder As String

    Dim consigneeid As String
    Dim shippingid As String
    Dim forwarderid As String

    Dim fedstr As String
    Dim fedboo As String

    Dim mothstr As String
    Dim mothboo As String

    Dim ptranshipstr As String
    Dim ptranshipboo As String

    Dim ptranshipstr2 As String
    Dim ptranshipboo2 As String

    Dim dischargestr As String
    Dim dischargeboo As String

    Dim pdeliverlystr As String
    Dim pdeliverlyboo As String

    Dim fdestinationstr As String
    Dim fdestinationboo As String


    Dim nicknameforward As String
    Dim nicknameshipname As String
    Private Sub frmedit_booking_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        mysql.Close()
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If
        mySqlCommand.CommandText = "Select * from booking join consignee join shipper on booking.BFORWARDER = shipper.SHIPPERID where  BOOKINGID ='" & frmview_voyage.idbooking & "' ;"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                txt_booking_no.Text = mySqlReader("BNO")

                TextBox3.Text = mySqlReader("BLANDNO")
                txt_type.Text = mySqlReader("BCTNTYPE")

                txt_notify.Text = mySqlReader("BNOTIFY")
                txt_scn.Text = mySqlReader("BSCN")
                txt_comodity.Text = mySqlReader("BCOM")
                txt_descript.Text = mySqlReader("BDESCRIPT")
                If mySqlReader("BTYPEMOVE") = 1 Then
                    RadioButton1.Checked = True
                ElseIf mySqlReader("BTYPEMOVE") = 2 Then
                    RadioButton2.Checked = True
                Else
                End If

                TextBox2.Text = mySqlReader("SHIPADD")
                'TextBox1.Text = mySqlReader("SHIPADD")
                txt_TUG.Text = mySqlReader("BTUG")
                txt_Gross.Text = mySqlReader("BGROSS")

                TextBox4.Text = mySqlReader("BCTNNO")


                txt_localforwared.Text = mySqlReader("BLOCALFOR")
              
                If mySqlReader("BSHIPNAME") Is DBNull.Value Then
                Else
                    ComboBox2.Text = mySqlReader("BSHIPNAME")
                End If
                If mySqlReader("BFORWARDERNAME") Is DBNull.Value Then
                Else
                    ComboBox3.Text = mySqlReader("BFORWARDERNAME")
                End If
                If mySqlReader("POL") Is DBNull.Value Then
                Else
                    txt_pol.Text = mySqlReader("POL")
                End If
                If mySqlReader("TSPORT") Is DBNull.Value Then
                Else
                    txt_tsport.Text = mySqlReader("TSPORT")
                End If
                If mySqlReader("REMARK") Is DBNull.Value Then
                Else
                    txt_remark.Text = mySqlReader("REMARK")
                End If
                If mySqlReader("BCONSI") Is DBNull.Value Then
                Else
                    consig = mySqlReader("BCONSI")
                End If
                If mySqlReader("BSHIP") Is DBNull.Value Then
                Else
                    shipper = mySqlReader("BSHIP")
                End If
                If mySqlReader("BFORWARDER") Is DBNull.Value Then
                Else
                    forwarder = mySqlReader("BFORWARDER")
                End If

                If mySqlReader("BFEDS") = "1" Then
                    FEDTXT.ReadOnly = False
                    FEDTXT.Text = mySqlReader("BFED")
                    FED.Checked = True
                Else
                    FEDTXT.ReadOnly = True
                    FEDTXT.Text = ""
                    FED.Checked = False
                End If



                If mySqlReader("BMOTHS") = "1" Then
                    MOTHTXT.ReadOnly = False
                    MOTHTXT.Text = mySqlReader("BMOTH")
                    MOTH.Checked = True
                Else
                    MOTHTXT.ReadOnly = True
                    MOTHTXT.Text = ""
                    MOTH.Checked = False
                End If


                If mySqlReader("BTRANSHIPS") = "1" Then
                    TRANSHIPTXT.ReadOnly = False
                    TRANSHIP.Checked = True
                    TRANSHIPTXT.Text = mySqlReader("BTRANSHIP")
                Else
                    TRANSHIPTXT.ReadOnly = True
                    TRANSHIP.Checked = False
                    TRANSHIPTXT.Text = ""
                End If

                If mySqlReader("BTRANSHIP2S") = "1" Then
                    TRANSHIP2TXT.Text = mySqlReader("BTRANSHIP2")
                    TRANSHIP2.Checked = True
                    TRANSHIP2TXT.ReadOnly = False
                Else
                    TRANSHIP2TXT.Text = ""
                    TRANSHIP2.Checked = False
                    TRANSHIP2TXT.ReadOnly = True
                End If


                If mySqlReader("BPORTDISS") = "1" Then
                    txt_portdischarge.Text = mySqlReader("BPORTDIS")
                    DISCHARGE.Checked = True
                    txt_portdischarge.ReadOnly = False
                Else
                    txt_portdischarge.Text = ""
                    DISCHARGE.Checked = False
                    txt_portdischarge.ReadOnly = True
                End If


                If mySqlReader("BDELIVERYS") Then
                    DELIVERY.Checked = True
                    DELIVERYTXT.Text = mySqlReader("BDELIVERYS")
                    DELIVERYTXT.ReadOnly = False
                Else
                    DELIVERY.Checked = False
                    DELIVERYTXT.Text = ""
                    DELIVERYTXT.ReadOnly = True
                End If

                If mySqlReader("BFINALDESTS") = "1" Then
                    txt_finaldes.Text = mySqlReader("BFINALDEST")
                    txt_finaldes.ReadOnly = False
                    DESTINATION.Checked = True
                Else
                    txt_finaldes.Text = ""
                    txt_finaldes.ReadOnly = True
                    DESTINATION.Checked = False
                End If




            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from booking join consignee join shipper on booking.BCONSI = consignee.CONSIGNEEID and booking.BFORWARDER = shipper.SHIPPERID where  BOOKINGID ='" & frmview_voyage.idbooking & "' ;"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            While (mySqlReader.Read())
                TextBox1.Text = mySqlReader("SHIPADD")
            End While
        Catch ex As Exception

        End Try
        mysql.Close()


        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If


        mySqlCommand.CommandText = "Select * from shipper;"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                ComboBox2.Items.Add(mySqlReader("SHIPNAME"))

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()


     



        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If


        mySqlCommand.CommandText = "Select * from shipper;"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                ComboBox3.Items.Add(mySqlReader("SHIPNAME"))

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If FED.Checked = True Then
            fedstr = FEDTXT.Text
            fedboo = "1"
        Else
            fedboo = "0"
            fedstr = ""
        End If


        If MOTH.Checked = True Then
            mothstr = MOTHTXT.Text
            mothboo = "1"
        Else
            mothstr = ""
            mothboo = "0"
        End If

        If TRANSHIP.Checked = True Then
            ptranshipstr = TRANSHIPTXT.Text
            ptranshipboo = "1"
        Else
            ptranshipstr = ""
            ptranshipboo = "0"
        End If


        If TRANSHIP2.Checked = True Then
            ptranshipstr2 = TRANSHIP2TXT.Text
            ptranshipboo2 = "1"
        Else
            ptranshipstr2 = ""
            ptranshipboo2 = "0"
        End If

        If DISCHARGE.Checked = True Then
            dischargestr = txt_portdischarge.Text
            dischargeboo = "1"
        Else
            dischargestr = ""
            dischargeboo = "0"
        End If


        If DELIVERY.Checked = True Then
            pdeliverlystr = DELIVERYTXT.Text
            pdeliverlyboo = "1"
        Else
            pdeliverlystr = ""
            pdeliverlyboo = "0"
        End If



        If DESTINATION.Checked = True Then
            fdestinationstr = txt_finaldes.Text
            fdestinationboo = "1"
        Else
            fdestinationstr = ""
            fdestinationboo = "0"
        End If

     

        mysql.Close()
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If
        Try

            Dim checkradio2 As String
            If RadioButton1.Checked = True Then
                checkradio2 = "1"

            ElseIf RadioButton2.Checked = True Then
                checkradio2 = "2"
            End If

            mySqlCommand1.CommandText = "UPDATE booking SET BLOCALFOR ='" & txt_localforwared.Text & "' , BNO = '" & MySqlHelper.EscapeString(txt_booking_no.Text) & "' , BCTNNO = '" & TextBox4.Text & "', BLANDNO = '" & TextBox3.Text & "' ,BCTNTYPE ='" & MySqlHelper.EscapeString(txt_type.Text) & "',BLANDNO ='" & MySqlHelper.EscapeString(TextBox3.Text) & "',BNOTIFY = '" & MySqlHelper.EscapeString(txt_notify.Text) & "', BSCN = '" & MySqlHelper.EscapeString(txt_scn.Text) & "',BCOM ='" & MySqlHelper.EscapeString(txt_comodity.Text) & "',BDESCRIPT = '" & MySqlHelper.EscapeString(txt_descript.Text) & "' ,POL = '" & MySqlHelper.EscapeString(txt_pol.Text) & "',TSPORT = '" & MySqlHelper.EscapeString(txt_tsport.Text) & "',BCONSI = '" & MySqlHelper.EscapeString(consig) & "'  ,BSHIP = '" & MySqlHelper.EscapeString(shipper) & "' ,BCONSINAME = '' ,BSHIPNAME = '" & MySqlHelper.EscapeString(ComboBox2.Text) & "' ,REMARK = '" & MySqlHelper.EscapeString(txt_remark.Text) & "' , BFED ='" & fedstr & "' ,BFEDS ='" & fedboo & "' , BMOTH ='" & mothstr & "',BMOTHS ='" & mothboo & "',BTRANSHIP= '" & ptranshipstr & "',BTRANSHIPS='" & ptranshipboo & "', BTRANSHIP2 ='" & ptranshipstr2 & "' , BTRANSHIP2S ='" & ptranshipboo2 & "', BDELIVERY ='" & pdeliverlystr & "', BDELIVERYS = '" & pdeliverlyboo & "',BFINALDESTS ='" & fdestinationboo & "', BFINALDEST ='" & fdestinationstr & "', BNSHIPNAME = '" & nicknameshipname & "', BNFORWARDERNAME ='" & nicknameforward & "' , BGROSS ='" & txt_Gross.Text & "' , BTUG ='" & txt_TUG.Text & "', BPORTDIS ='" & dischargestr & "' , BPORTDISS = '" & dischargeboo & "' WHERE BOOKINGID = '" & frmview_voyage.idbooking & "'; "

            mySqlCommand1.CommandType = CommandType.Text
            mySqlCommand1.Connection = mysql

            mySqlCommand1.ExecuteNonQuery()
            mysql.Close()

            MsgBox("แก้ไขข้อเรียบร้อย")
            Dim cf As New frmview_voyage

            cf.MdiParent = Me.MdiParent
            Me.Close()
            cf.Dock = DockStyle.Fill
            cf.Show()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try




    End Sub

     

    Private Sub ComboBox2_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedValueChanged
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from  shipper where SHIPNAME = '" & Trim(ComboBox2.Text) & "';"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                shipper = mySqlReader("SHIPPERID")
                ComboBox2.Text = mySqlReader("SHIPNAME")
                TextBox2.Text = mySqlReader("SHIPADD")
                nicknameshipname = mySqlReader("SHIPNICKNAME")
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub

    Private Sub ComboBox3_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedValueChanged
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from  shipper where SHIPNAME = '" & Trim(ComboBox3.Text) & "';"
        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                forwarder = mySqlReader("SHIPPERID")
                ComboBox3.Text = mySqlReader("SHIPNAME")
                TextBox1.Text = mySqlReader("SHIPADD")
                nicknameforward = mySqlReader("SHIPNICKNAME")
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub

    Private Sub GroupBox5_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox5.Enter

    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        Dim cf As New frmview_voyage

        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub
    Private Sub FED_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FED.CheckedChanged
        If FED.Checked = True Then
            FEDTXT.ReadOnly = False
        Else
            FEDTXT.ReadOnly = True
            FEDTXT.Text = ""
        End If
    End Sub

    Private Sub MOTH_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MOTH.CheckedChanged
        If MOTH.Checked = True Then
            MOTHTXT.ReadOnly = False
        Else
            MOTHTXT.ReadOnly = True
            MOTHTXT.Text = ""
        End If
    End Sub

    Private Sub TRANSHIP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TRANSHIP.CheckedChanged
        If TRANSHIP.Checked = True Then
            TRANSHIPTXT.ReadOnly = False
        Else
            TRANSHIPTXT.ReadOnly = True
            TRANSHIPTXT.Text = ""
        End If
    End Sub

    Private Sub TRANSHIP2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TRANSHIP2.CheckedChanged
        If TRANSHIP2.Checked = True Then
            TRANSHIP2TXT.ReadOnly = False
        Else
            TRANSHIP2TXT.ReadOnly = True
            TRANSHIP2TXT.Text = ""
        End If
    End Sub

    Private Sub DISCHARGE_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DISCHARGE.CheckedChanged
        If DISCHARGE.Checked = True Then
            txt_portdischarge.ReadOnly = False
        Else
            txt_portdischarge.ReadOnly = True
            txt_portdischarge.Text = ""
        End If
    End Sub

    Private Sub DELIVERY_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DELIVERY.CheckedChanged
        If DELIVERY.Checked = True Then
            DELIVERYTXT.ReadOnly = False
        Else
            DELIVERYTXT.ReadOnly = True
            DELIVERYTXT.Text = ""
        End If
    End Sub

    Private Sub DESTINATION_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESTINATION.CheckedChanged
        If DESTINATION.Checked = True Then
            txt_finaldes.ReadOnly = False
        Else
            txt_finaldes.ReadOnly = True
            txt_finaldes.Text = ""
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged

    End Sub
End Class