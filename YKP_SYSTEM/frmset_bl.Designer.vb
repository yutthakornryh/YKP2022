﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmset_bl
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.dateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.txtMeasurement = New DevExpress.XtraEditors.TextEdit()
        Me.RadioGroup5 = New DevExpress.XtraEditors.RadioGroup()
        Me.RadioPageGroup = New DevExpress.XtraEditors.RadioGroup()
        Me.RadioGroup3 = New DevExpress.XtraEditors.RadioGroup()
        Me.RadioInv = New DevExpress.XtraEditors.RadioGroup()
        Me.RadioNM = New DevExpress.XtraEditors.RadioGroup()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBOOKINGID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCloseDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFCL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBINVOICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtNM = New DevExpress.XtraEditors.MemoEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.RadioMarks = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.RadioPage12 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMeasurement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioPageGroup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioInv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioNM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioMarks, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioPage12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.BackColor = System.Drawing.Color.White
        Me.LayoutControl1.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.dateEdit1)
        Me.LayoutControl1.Controls.Add(Me.txtMeasurement)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup5)
        Me.LayoutControl1.Controls.Add(Me.RadioPageGroup)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup3)
        Me.LayoutControl1.Controls.Add(Me.RadioInv)
        Me.LayoutControl1.Controls.Add(Me.RadioNM)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.txtNM)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.ForeColor = System.Drawing.Color.Black
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1268, 576)
        Me.LayoutControl1.TabIndex = 173
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.ForeColor = System.Drawing.Color.Black
        Me.SimpleButton4.Appearance.Options.UseForeColor = True
        Me.SimpleButton4.Location = New System.Drawing.Point(767, 352)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(489, 22)
        Me.SimpleButton4.StyleController = Me.LayoutControl1
        Me.SimpleButton4.TabIndex = 187
        Me.SimpleButton4.Text = "Print BL SPH"
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(434, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.slcVoyage.Properties.Appearance.Options.UseBackColor = True
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Appearance.Options.UseForeColor = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.voyageBindingSource
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(329, 22)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 186
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset4
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 1
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 2
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.ForeColor = System.Drawing.Color.Black
        Me.SimpleButton3.Appearance.Options.UseForeColor = True
        Me.SimpleButton3.Location = New System.Drawing.Point(1136, 378)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(120, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 185
        Me.SimpleButton3.Text = "YKP Marine Line"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.ForeColor = System.Drawing.Color.Black
        Me.SimpleButton2.Appearance.Options.UseForeColor = True
        Me.SimpleButton2.Location = New System.Drawing.Point(955, 378)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(177, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 184
        Me.SimpleButton2.Text = "YKP Ocean Line"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.ForeColor = System.Drawing.Color.Black
        Me.SimpleButton1.Appearance.Options.UseForeColor = True
        Me.SimpleButton1.Location = New System.Drawing.Point(767, 378)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(184, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 183
        Me.SimpleButton1.Text = "YKP Marine Service"
        '
        'dateEdit1
        '
        Me.dateEdit1.EditValue = Nothing
        Me.dateEdit1.Location = New System.Drawing.Point(1130, 311)
        Me.dateEdit1.Name = "dateEdit1"
        Me.dateEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.dateEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.dateEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.dateEdit1.Properties.Appearance.Options.UseForeColor = True
        Me.dateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateEdit1.Size = New System.Drawing.Size(114, 20)
        Me.dateEdit1.StyleController = Me.LayoutControl1
        Me.dateEdit1.TabIndex = 182
        '
        'txtMeasurement
        '
        Me.txtMeasurement.Location = New System.Drawing.Point(989, 311)
        Me.txtMeasurement.Name = "txtMeasurement"
        Me.txtMeasurement.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtMeasurement.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txtMeasurement.Properties.Appearance.Options.UseBackColor = True
        Me.txtMeasurement.Properties.Appearance.Options.UseForeColor = True
        Me.txtMeasurement.Size = New System.Drawing.Size(137, 20)
        Me.txtMeasurement.StyleController = Me.LayoutControl1
        Me.txtMeasurement.TabIndex = 181
        '
        'RadioGroup5
        '
        Me.RadioGroup5.Location = New System.Drawing.Point(779, 311)
        Me.RadioGroup5.Name = "RadioGroup5"
        Me.RadioGroup5.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.RadioGroup5.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.RadioGroup5.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup5.Properties.Appearance.Options.UseForeColor = True
        Me.RadioGroup5.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Dont Show"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Show")})
        Me.RadioGroup5.Size = New System.Drawing.Size(206, 25)
        Me.RadioGroup5.StyleController = Me.LayoutControl1
        Me.RadioGroup5.TabIndex = 4
        '
        'RadioPageGroup
        '
        Me.RadioPageGroup.Location = New System.Drawing.Point(1066, 42)
        Me.RadioPageGroup.Name = "RadioPageGroup"
        Me.RadioPageGroup.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.RadioPageGroup.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.RadioPageGroup.Properties.Appearance.Options.UseBackColor = True
        Me.RadioPageGroup.Properties.Appearance.Options.UseForeColor = True
        Me.RadioPageGroup.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "Page 1"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(2, Short), "Page 2")})
        Me.RadioPageGroup.Size = New System.Drawing.Size(178, 40)
        Me.RadioPageGroup.StyleController = Me.LayoutControl1
        Me.RadioPageGroup.TabIndex = 180
        '
        'RadioGroup3
        '
        Me.RadioGroup3.Location = New System.Drawing.Point(922, 42)
        Me.RadioGroup3.Name = "RadioGroup3"
        Me.RadioGroup3.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.RadioGroup3.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.RadioGroup3.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup3.Properties.Appearance.Options.UseForeColor = True
        Me.RadioGroup3.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "Marks & Nos."), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(2, Short), "Container / Seal No.")})
        Me.RadioGroup3.Size = New System.Drawing.Size(116, 40)
        Me.RadioGroup3.StyleController = Me.LayoutControl1
        Me.RadioGroup3.TabIndex = 178
        '
        'RadioInv
        '
        Me.RadioInv.Location = New System.Drawing.Point(779, 42)
        Me.RadioInv.Name = "RadioInv"
        Me.RadioInv.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.RadioInv.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.RadioInv.Properties.Appearance.Options.UseBackColor = True
        Me.RadioInv.Properties.Appearance.Options.UseForeColor = True
        Me.RadioInv.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "Show"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(2, Short), "Not Show")})
        Me.RadioInv.Size = New System.Drawing.Size(115, 40)
        Me.RadioInv.StyleController = Me.LayoutControl1
        Me.RadioInv.TabIndex = 177
        '
        'RadioNM
        '
        Me.RadioNM.Location = New System.Drawing.Point(779, 128)
        Me.RadioNM.Name = "RadioNM"
        Me.RadioNM.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.RadioNM.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.RadioNM.Properties.Appearance.Options.UseBackColor = True
        Me.RadioNM.Properties.Appearance.Options.UseForeColor = True
        Me.RadioNM.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Show"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Not Show"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "FLEXIONE"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "PROGYPS")})
        Me.RadioNM.Size = New System.Drawing.Size(110, 137)
        Me.RadioNM.StyleController = Me.LayoutControl1
        Me.RadioNM.TabIndex = 176
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "booking"
        Me.GridControl2.DataSource = Me.Ykpdtset2
        Me.GridControl2.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White
        Me.GridControl2.EmbeddedNavigator.Appearance.ForeColor = System.Drawing.Color.Black
        Me.GridControl2.EmbeddedNavigator.Appearance.Options.UseBackColor = True
        Me.GridControl2.EmbeddedNavigator.Appearance.Options.UseForeColor = True
        Me.GridControl2.Location = New System.Drawing.Point(12, 38)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(751, 526)
        Me.GridControl2.TabIndex = 0
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBOOKINGID, Me.colBNO, Me.colBNSHIPNAME, Me.colBFORWARDERNAME, Me.colCloseDate, Me.colFCL, Me.colBINVOICE})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colBOOKINGID
        '
        Me.colBOOKINGID.FieldName = "BOOKINGID"
        Me.colBOOKINGID.Name = "colBOOKINGID"
        Me.colBOOKINGID.OptionsColumn.AllowEdit = False
        Me.colBOOKINGID.OptionsColumn.ReadOnly = True
        Me.colBOOKINGID.Width = 81
        '
        'colBNO
        '
        Me.colBNO.Caption = "Booking No."
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Visible = True
        Me.colBNO.VisibleIndex = 0
        Me.colBNO.Width = 89
        '
        'colBNSHIPNAME
        '
        Me.colBNSHIPNAME.Caption = "Shipper Name"
        Me.colBNSHIPNAME.FieldName = "BNSHIPNAME"
        Me.colBNSHIPNAME.Name = "colBNSHIPNAME"
        Me.colBNSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colBNSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colBNSHIPNAME.Visible = True
        Me.colBNSHIPNAME.VisibleIndex = 1
        Me.colBNSHIPNAME.Width = 60
        '
        'colBFORWARDERNAME
        '
        Me.colBFORWARDERNAME.Caption = "Forwarder Name"
        Me.colBFORWARDERNAME.FieldName = "BFORWARDERNAME"
        Me.colBFORWARDERNAME.Name = "colBFORWARDERNAME"
        Me.colBFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBFORWARDERNAME.Visible = True
        Me.colBFORWARDERNAME.VisibleIndex = 2
        Me.colBFORWARDERNAME.Width = 60
        '
        'colCloseDate
        '
        Me.colCloseDate.FieldName = "CloseDate"
        Me.colCloseDate.Name = "colCloseDate"
        Me.colCloseDate.OptionsColumn.AllowEdit = False
        Me.colCloseDate.OptionsColumn.ReadOnly = True
        Me.colCloseDate.Visible = True
        Me.colCloseDate.VisibleIndex = 3
        Me.colCloseDate.Width = 60
        '
        'colFCL
        '
        Me.colFCL.Caption = "FCL"
        Me.colFCL.FieldName = "FCL"
        Me.colFCL.Name = "colFCL"
        Me.colFCL.OptionsColumn.AllowEdit = False
        Me.colFCL.OptionsColumn.ReadOnly = True
        Me.colFCL.Visible = True
        Me.colFCL.VisibleIndex = 4
        Me.colFCL.Width = 60
        '
        'colBINVOICE
        '
        Me.colBINVOICE.Caption = "Invoice"
        Me.colBINVOICE.FieldName = "BINVOICE"
        Me.colBINVOICE.Name = "colBINVOICE"
        Me.colBINVOICE.OptionsColumn.AllowEdit = False
        Me.colBINVOICE.OptionsColumn.ReadOnly = True
        Me.colBINVOICE.Visible = True
        Me.colBINVOICE.VisibleIndex = 5
        Me.colBINVOICE.Width = 66
        '
        'txtNM
        '
        Me.txtNM.Location = New System.Drawing.Point(893, 128)
        Me.txtNM.Name = "txtNM"
        Me.txtNM.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.txtNM.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.txtNM.Properties.Appearance.Options.UseBackColor = True
        Me.txtNM.Properties.Appearance.Options.UseForeColor = True
        Me.txtNM.Size = New System.Drawing.Size(351, 137)
        Me.txtNM.StyleController = Me.LayoutControl1
        Me.txtNM.TabIndex = 179
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlGroup3, Me.RadioMarks, Me.LayoutControlGroup2, Me.RadioPage12, Me.LayoutControlGroup6, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem11, Me.EmptySpaceItem1, Me.LayoutControlItem9, Me.EmptySpaceItem2, Me.LayoutControlItem10})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1268, 576)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.GridControl2
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(755, 530)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(755, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(143, 86)
        Me.LayoutControlGroup3.Text = "INV"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.RadioInv
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(119, 44)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'RadioMarks
        '
        Me.RadioMarks.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6})
        Me.RadioMarks.Location = New System.Drawing.Point(898, 0)
        Me.RadioMarks.Name = "RadioMarks"
        Me.RadioMarks.Size = New System.Drawing.Size(144, 86)
        Me.RadioMarks.Text = "INV"
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.RadioGroup3
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(120, 44)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem7})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(755, 86)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(493, 183)
        Me.LayoutControlGroup2.Text = "N/M"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.RadioNM
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(114, 141)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtNM
        Me.LayoutControlItem7.Location = New System.Drawing.Point(114, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(355, 141)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'RadioPage12
        '
        Me.RadioPage12.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8})
        Me.RadioPage12.Location = New System.Drawing.Point(1042, 0)
        Me.RadioPage12.Name = "RadioPage12"
        Me.RadioPage12.Size = New System.Drawing.Size(206, 86)
        Me.RadioPage12.Text = "Page"
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.RadioPageGroup
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(182, 44)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(755, 269)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(493, 71)
        Me.LayoutControlGroup6.Text = "Measurement"
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.RadioGroup5
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(210, 29)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.txtMeasurement
        Me.LayoutControlItem13.Location = New System.Drawing.Point(210, 0)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(141, 29)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.dateEdit1
        Me.LayoutControlItem14.Location = New System.Drawing.Point(351, 0)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(118, 29)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.SimpleButton1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(755, 366)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(188, 26)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        Me.LayoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton2
        Me.LayoutControlItem4.Location = New System.Drawing.Point(943, 366)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(181, 26)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        Me.LayoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.SimpleButton3
        Me.LayoutControlItem11.Location = New System.Drawing.Point(1124, 366)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(124, 26)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        Me.LayoutControlItem11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(755, 392)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(493, 164)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.slcVoyage
        Me.LayoutControlItem9.Location = New System.Drawing.Point(377, 0)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(378, 26)
        Me.LayoutControlItem9.Text = "Voayge"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(42, 16)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(377, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.SimpleButton4
        Me.LayoutControlItem10.Location = New System.Drawing.Point(755, 340)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(493, 26)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmset_bl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1268, 576)
        Me.Controls.Add(Me.LayoutControl1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Black
        Me.Name = "frmset_bl"
        Me.Text = "MetroForm"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMeasurement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioPageGroup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioInv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioNM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioMarks, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioPage12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colBOOKINGID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents RadioNM As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioGroup3 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents RadioInv As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents colBNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCloseDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFCL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBINVOICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtNM As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioMarks As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioPageGroup As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents RadioPage12 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioGroup5 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents dateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txtMeasurement As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
End Class
