Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraReports.UI
Imports MySql.Data.MySqlClient
Public Class frmset_bl

    Public Shared idbooking As String

    Public Shared invoice As String

    Public Shared invoice1 As String

    Public Shared nm As String

    Public Shared mark As String

    Public Shared page1 As String

    Public Shared checkinv As String

    Public Shared datetimebl As String

    Public Shared textmsg As String

    Public Shared meastext As String

    Public Shared meast As String

    Private idvoyage As String

    Dim voyageid As String
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Dim ykpset As New ykpdtset
    Dim objCLASS As New ContainerClass(ykpset)

    Public Sub New(ByVal idvoyage As String)

        ' This call is required by the designer.
        InitializeComponent()
        voyageid = idvoyage
        dateEdit1.EditValue = Date.Now
        ' Add any initialization after the InitializeComponent() call.

        loadDatavoyage()
        slcVoyage.EditValue = idvoyage
    End Sub
    Private Sub frmset_bl_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture



        '   LoadBooking()

    End Sub
    Public Sub LoadBooking()
        Dim sql As String
        ' sql = "Select * from booking_bl where booking_id ='" & frmview_voyage.idbooking & "'"
        sql = "Select * ,  BNO As 'Booking No.',BSHIPNAME As 'Shipper',BFORWARDERNAME As 'Forwarder', CONCAT_WS('@',BCONFIRMDAY,BCONFIRMTIME) AS 'CloseDate', CONCAT_WS(' ',CONCAT_WS('X',BCTNNO,BCTNTYPE) ,BNICKAGENT ) AS CTNNO  from booking left join booking_bl on booking_bl.idbooking_bl =  booking.BINV where BVOYAGE ='" & slcVoyage.EditValue & "' order by  BCTNTYPE ASC , FIELD( BFORWARDERNAME ,'v.i.m','psk','queen','3gshipping' ,'baolidor','kvm','j.mac','topline') DESC;"

        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
        GridControl2.DataSource = ykpset.Tables("booking")
    End Sub
    Public Sub LoadBooking_BL()
        'Dim sql As String
        'sql = "Select * from booking_bl where booking_id ='" & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString() & "'"
        'ykpset.Tables("booking_bl").Clear()
        'connectDB.GetTable(sql, ykpset.Tables("booking_bl"))
        'GridControl1.DataSource = ykpset.Tables("booking_bl")
    End Sub



    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)




    End Sub
    Public Sub checkfill()
        If RadioNM.SelectedIndex = 0 Then
            nm = "0"
        ElseIf RadioNM.SelectedIndex = 1 Then

            nm = "1"
        ElseIf RadioNM.SelectedIndex = 2 Then
            nm = "2"
        ElseIf RadioNM.SelectedIndex = 3 Then
            nm = "3"

        End If
        If RadioInv.SelectedIndex = 0 Then
            frmset_bl.checkinv = "1"

        ElseIf RadioInv.SelectedIndex = 1 Then
            frmset_bl.checkinv = "0"

        End If
        If RadioPageGroup.SelectedIndex = 0 Then
            page1 = "0"

        ElseIf RadioPageGroup.SelectedIndex = 1 Then
            page1 = "1"

        End If


        If RadioGroup3.SelectedIndex = 0 Then
            mark = "0"
        ElseIf RadioGroup3.SelectedIndex = 1 Then

            mark = "1"

        End If

        If RadioGroup5.SelectedIndex = 0 Then
            meast = "0"

        ElseIf RadioGroup5.SelectedIndex = 1 Then
            meast = "1"
            meastext = Me.txtMeasurement.Text
        End If


        'frmset_bl.datetimebl = DateTimeInput1.ToString("dd-MM-yyyy")
        'frmset_bl.textmsg = TextBox1.EditValue


    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        checkfill()
        Dim nextform As frmrpt_bl1 = New frmrpt_bl1
        nextform.Show()



    End Sub

    Private Sub GridView2_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView2.RowCellClick
        idbooking = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString()
        invoice = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BINVOICE").ToString()

        LoadBooking_BL()
    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs)
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        checkfill()
        If invoice Is Nothing Then
            MsgBox("��س����͡ Bill")
            Exit Sub
        End If

        rpt_bl()


        ' Dim nextform As frmrpt_bl = New frmrpt_bl
        '    nextform.Show()

    End Sub
    Public Sub rpt_bl_SPH()
        Dim container As String = ""

        Dim rptBL As rptBL_SPH = New rptBL_SPH
        Dim rptBL1 As rptBL_SPH2 = New rptBL_SPH2

        'rptBL.Parameters("DateString").Value = dateStart.ToString("MMMM �.�. yyyy") ''��˹����������� ��� dat_print �form report1
        'rptBL.Parameters("ProductName").Value = SearchLookUpEdit1.Text

        If page1 = "0" Then
            Dim sql As String
            Dim dt As New DataTable



            If mark = "0" Then
                rptBL.Parameters("menuctn").Value = "Marks & Nos." + Environment.NewLine + "Container / Seal No."
            Else
                rptBL.Parameters("menuctn").Value = "Container / Seal No."

            End If

            dt = objCLASS.LoadBookingBLReport(idbooking)

            'Try
            '    mySqlReader = mySqlCommand.ExecuteReader
            If dt.Rows.Count > 0 Then
                rptBL.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                rptBL.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                rptBL.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                rptBL.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                rptBL.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                rptBL.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                rptBL.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                rptBL.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                rptBL.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                rptBL.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                rptBL.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                If dt.Rows(0)("BTYPEMOVE") = "0" Then
                    rptBL.Parameters("typemove").Value = "X"
                    rptBL.Parameters("typemove1").Value = ""
                Else
                    rptBL.Parameters("typemove1").Value = "X"
                    rptBL.Parameters("typemove").Value = ""
                End If

                rptBL.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                rptBL.Parameters("voyage").Value = dt.Rows(0)("VOYNAME").ToString

                If dt.Rows(0)("BPORTDIS") Is DBNull.Value Then
                    rptBL.Parameters("portdischarge").Value = ""

                Else
                    rptBL.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")

                End If


                If dt.Rows(0)("BFINALDEST") Is DBNull.Value Then
                    rptBL.Parameters("placerecieve").Value = ""
                Else
                    rptBL.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST").ToString
                End If

                rptBL.Parameters("description").Value = dt.Rows(0)("BDESCRIPT")
                rptBL.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                rptBL.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                rptBL.Parameters("number_font").Value = dt.Rows(0)("number_word").ToString
                rptBL.Parameters("number").Value = dt.Rows(0)("number").ToString
                rptBL.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE").ToString
                rptBL.Parameters("invoicenum").Value = frmset_bl.invoice
                rptBL.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
            End If




            If RadioNM.SelectedIndex = 0 Then
                container = "N/M " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 1 Then
                container = txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 2 Then
                container = "FLEXIONE " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 3 Then
                container = "PROGYPS " & txtNM.EditValue & Environment.NewLine

            Else

            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL.Parameters("measurement").Value = txtMeasurement.EditValue

            End If


            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)
            If dt.Rows.Count > 0 Then

                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") & Environment.NewLine
                Next

            End If
            rptBL.Parameters("container").Value = container
            rptBL.CreateDocument()
            rptBL.ShowRibbonPreviewDialog()


        Else
            'CrystalReportViewer1.ReportSource = rpt2

            'CrystalReportViewer1.Refresh()

            If frmset_bl.mark = "0" Then
                rptBL1.Parameters("menuctn").Value = "Marks & Nos." & Environment.NewLine + "Container / Seal No."
            Else
                rptBL1.Parameters("menuctn").Value = "Container / Seal No."

            End If

            Dim sql As String
            Dim dt As New DataTable



            dt = objCLASS.LoadBookingBLReport(idbooking)
            Try

                If dt.Rows.Count > 0 Then
                    rptBL1.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                    rptBL1.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                    rptBL1.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                    rptBL1.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                    rptBL1.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                    rptBL1.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                    rptBL1.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                    rptBL1.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                    rptBL1.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                    rptBL1.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                    rptBL1.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                    If dt.Rows(0)("BTYPEMOVE") = "0" Then
                        rptBL1.Parameters("typemove").Value = "X"
                        rptBL1.Parameters("typemove1").Value = ""
                    Else
                        rptBL1.Parameters("typemove1").Value = "X"
                        rptBL1.Parameters("typemove").Value = ""
                    End If

                    rptBL1.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                    rptBL1.Parameters("voyage").Value = dt.Rows(0)("VOYNAME")

                    rptBL1.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")
                    rptBL1.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                    rptBL1.Parameters("description").Value = "*** B/L Attached List ***" & Environment.NewLine & dt.Rows(0)("BDESCRIPT")
                    rptBL1.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                    rptBL1.Parameters("grossweight").Value = dt.Rows(0)("BGROSS").ToString
                    rptBL1.Parameters("number_font").Value = dt.Rows(0)("number_word").ToString
                    rptBL1.Parameters("number").Value = dt.Rows(0)("number").ToString
                    rptBL1.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                    rptBL1.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
                    rptBL1.Parameters("invoicenum").Value = frmset_bl.invoice
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            If RadioNM.SelectedIndex = 0 Then
                rptBL1.Parameters("nm").Value = "N/M " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 1 Then
                rptBL1.Parameters("nm").Value = txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 2 Then
                rptBL1.Parameters("nm").Value = "FLEXIONE " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 3 Then
                rptBL1.Parameters("nm").Value = "PROGYPS " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            Else

            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL1.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL1.Parameters("measurement").Value = txtMeasurement.EditValue

            End If




            Dim count_line As Integer = 1
            Dim countstring As String = ""


            ' mySqlCommand.CommandText = 
            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)

            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") + Environment.NewLine
                    countstring += count_line.ToString & "." + Environment.NewLine
                    count_line += 1
                Next
            End If



            rptBL1.Parameters("container").Value = container
            rptBL1.Parameters("count").Value = countstring

            rptBL1.CreateDocument()
            rptBL1.ShowRibbonPreviewDialog()

        End If

    End Sub



    Public Sub rpt_bl()
        Dim container As String = ""

        Dim rptBL As rptBL = New rptBL
        Dim rptBL1 As rpbBL_2 = New rpbBL_2

        'rptBL.Parameters("DateString").Value = dateStart.ToString("MMMM �.�. yyyy") ''��˹����������� ��� dat_print �form report1
        'rptBL.Parameters("ProductName").Value = SearchLookUpEdit1.Text

        If page1 = "0" Then
            Dim sql As String
            Dim dt As New DataTable



            If mark = "0" Then
                rptBL.Parameters("menuctn").Value = "Marks & Nos." + Environment.NewLine + "Container / Seal No."
            Else
                rptBL.Parameters("menuctn").Value = "Container / Seal No."

            End If



            dt = objCLASS.LoadBookingBLReport(idbooking)

            'Try
            '    mySqlReader = mySqlCommand.ExecuteReader
            If dt.Rows.Count > 0 Then
                rptBL.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                rptBL.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                rptBL.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                rptBL.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                rptBL.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                rptBL.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                rptBL.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                rptBL.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                rptBL.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                rptBL.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                rptBL.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                If dt.Rows(0)("BTYPEMOVE") = "0" Then
                    rptBL.Parameters("typemove").Value = "X"
                    rptBL.Parameters("typemove1").Value = ""
                Else
                    rptBL.Parameters("typemove1").Value = "X"
                    rptBL.Parameters("typemove").Value = ""
                End If

                rptBL.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                rptBL.Parameters("voyage").Value = dt.Rows(0)("VOYNAME")

                If dt.Rows(0)("BPORTDIS") Is DBNull.Value Then
                    rptBL.Parameters("portdischarge").Value = ""

                Else
                    rptBL.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")

                End If


                If dt.Rows(0)("BFINALDEST") Is DBNull.Value Then
                    rptBL.Parameters("placerecieve").Value = ""
                Else
                    rptBL.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                End If

                rptBL.Parameters("description").Value = dt.Rows(0)("BDESCRIPT")
                rptBL.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                rptBL.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                rptBL.Parameters("number_font").Value = dt.Rows(0)("number_word").ToString
                rptBL.Parameters("number").Value = dt.Rows(0)("number").ToString
                rptBL.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                rptBL.Parameters("invoicenum").Value = frmset_bl.invoice
                rptBL.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
            End If





            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)
            If RadioNM.SelectedIndex = 0 Then
                container = "N/M " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 1 Then
                container = txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 2 Then
                container = "FLEXIONE " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 3 Then
                container = "PROGYPS " & txtNM.EditValue & Environment.NewLine

            Else

            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL.Parameters("measurement").Value = txtMeasurement.EditValue

            End If

            If dt.Rows.Count > 0 Then

                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") & Environment.NewLine
                Next

            End If
            rptBL.Parameters("container").Value = container
            rptBL.CreateDocument()
            rptBL.ShowRibbonPreviewDialog()


        Else
            'CrystalReportViewer1.ReportSource = rpt2

            'CrystalReportViewer1.Refresh()

            If frmset_bl.mark = "0" Then
                rptBL1.Parameters("menuctn").Value = "Marks & Nos." & Environment.NewLine + "Container / Seal No."
            Else
                rptBL1.Parameters("menuctn").Value = "Container / Seal No."

            End If

            Dim sql As String
            Dim dt As New DataTable




            dt = objCLASS.LoadBookingBLReport(idbooking)
            Try

                If dt.Rows.Count > 0 Then
                    rptBL1.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                    rptBL1.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                    rptBL1.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                    rptBL1.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                    rptBL1.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                    rptBL1.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                    rptBL1.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                    rptBL1.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                    rptBL1.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                    rptBL1.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                    rptBL1.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                    If dt.Rows(0)("BTYPEMOVE") = "0" Then
                        rptBL1.Parameters("typemove").Value = "X"
                        rptBL1.Parameters("typemove1").Value = ""
                    Else
                        rptBL1.Parameters("typemove1").Value = "X"
                        rptBL1.Parameters("typemove").Value = ""
                    End If

                    rptBL1.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                    rptBL1.Parameters("voyage").Value = dt.Rows(0)("VOYNAME").ToString
                    rptBL1.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")
                    rptBL1.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                    rptBL1.Parameters("description").Value = "*** B/L Attached List ***" & Environment.NewLine & dt.Rows(0)("BDESCRIPT")
                    rptBL1.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                    rptBL1.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                    rptBL1.Parameters("number_font").Value = dt.Rows(0)("number_word")
                    rptBL1.Parameters("number").Value = dt.Rows(0)("number")
                    rptBL1.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                    rptBL1.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
                    rptBL1.Parameters("invoicenum").Value = frmset_bl.invoice
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            If RadioNM.SelectedIndex = 0 Then
                rptBL1.Parameters("nm").Value = "N/M " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 1 Then
                rptBL1.Parameters("nm").Value = txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 2 Then
                rptBL1.Parameters("nm").Value = "FLEXIONE " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 3 Then
                rptBL1.Parameters("nm").Value = "PROGYPS " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            Else

            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL1.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL1.Parameters("measurement").Value = txtMeasurement.EditValue

            End If




            Dim count_line As Integer = 1
            Dim countstring As String = ""


            ' mySqlCommand.CommandText = 
            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)

            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") + Environment.NewLine
                    countstring += count_line.ToString & "." + Environment.NewLine
                    count_line += 1
                Next
            End If



            rptBL1.Parameters("container").Value = container
            rptBL1.Parameters("count").Value = countstring

            rptBL1.CreateDocument()
            rptBL1.ShowRibbonPreviewDialog()

        End If

    End Sub
    Public Sub rpt_bl_ocean()
        Dim container As String = ""

        Dim rptBL As rptBL1 = New rptBL1
        Dim rptBL1 As rptBL1_2 = New rptBL1_2

        'rptBL.Parameters("DateString").Value = dateStart.ToString("MMMM �.�. yyyy") ''��˹����������� ��� dat_print �form report1
        'rptBL.Parameters("ProductName").Value = SearchLookUpEdit1.Text

        If page1 = "0" Then
            Dim sql As String
            Dim dt As New DataTable



            If mark = "0" Then
                rptBL.Parameters("menuctn").Value = "Marks & Nos." + Environment.NewLine + "Container / Seal No."
            Else
                rptBL.Parameters("menuctn").Value = "Container / Seal No."

            End If

            dt = objCLASS.LoadBookingBLReport(idbooking)

            'Try
            '    mySqlReader = mySqlCommand.ExecuteReader
            If dt.Rows.Count > 0 Then
                rptBL.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                rptBL.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                rptBL.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                rptBL.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                rptBL.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                rptBL.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                rptBL.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                rptBL.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                rptBL.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                rptBL.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                rptBL.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                If dt.Rows(0)("BTYPEMOVE") = "0" Then
                    rptBL.Parameters("typemove").Value = "X"
                    rptBL.Parameters("typemove1").Value = ""
                Else
                    rptBL.Parameters("typemove1").Value = "X"
                    rptBL.Parameters("typemove").Value = ""
                End If

                rptBL.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                rptBL.Parameters("voyage").Value = dt.Rows(0)("VOYNAME")

                If dt.Rows(0)("BPORTDIS") Is DBNull.Value Then
                    rptBL.Parameters("portdischarge").Value = ""

                Else
                    rptBL.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")

                End If


                If dt.Rows(0)("BFINALDEST") Is DBNull.Value Then
                    rptBL.Parameters("placerecieve").Value = ""
                Else
                    rptBL.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                End If

                rptBL.Parameters("description").Value = "*** B/L Attached List ***" & Environment.NewLine & dt.Rows(0)("BDESCRIPT")
                rptBL.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                rptBL.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                rptBL.Parameters("number_font").Value = dt.Rows(0)("number_word")
                rptBL.Parameters("number").Value = dt.Rows(0)("number")
                rptBL.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                rptBL.Parameters("invoicenum").Value = frmset_bl.invoice
                rptBL.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
            End If




            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)
            If RadioNM.SelectedIndex = 0 Then
                container = "N/M " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 1 Then
                container = txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 2 Then
                container = "FLEXIONE " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 3 Then
                container = "PROGYPS " & txtNM.EditValue & Environment.NewLine

            Else

            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL.Parameters("measurement").Value = txtMeasurement.EditValue

            End If


            If dt.Rows.Count > 0 Then

                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") & Environment.NewLine
                Next

            End If
            rptBL.Parameters("container").Value = container
            rptBL.CreateDocument()
            rptBL.ShowRibbonPreviewDialog()


        Else
            'CrystalReportViewer1.ReportSource = rpt2

            'CrystalReportViewer1.Refresh()

            If frmset_bl.mark = "0" Then
                rptBL1.Parameters("menuctn").Value = "Marks & Nos." & Environment.NewLine + "Container / Seal No."
            Else
                rptBL1.Parameters("menuctn").Value = "Container / Seal No."

            End If

            Dim sql As String
            Dim dt As New DataTable





            dt = objCLASS.LoadBookingBLReport(idbooking)
            Try

                If dt.Rows.Count > 0 Then
                    rptBL1.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                    rptBL1.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                    rptBL1.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                    rptBL1.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                    rptBL1.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                    rptBL1.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                    rptBL1.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                    rptBL1.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                    rptBL1.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                    rptBL1.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                    rptBL1.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                    If dt.Rows(0)("BTYPEMOVE") = "0" Then
                        rptBL1.Parameters("typemove").Value = "X"
                        rptBL1.Parameters("typemove1").Value = ""
                    Else
                        rptBL1.Parameters("typemove1").Value = "X"
                        rptBL1.Parameters("typemove").Value = ""
                    End If

                    rptBL1.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                    rptBL1.Parameters("voyage").Value = dt.Rows(0)("VOYNAME")
                    rptBL1.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")
                    rptBL1.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                    rptBL1.Parameters("description").Value = "*** B/L Attached List ***" & Environment.NewLine & dt.Rows(0)("BDESCRIPT")
                    rptBL1.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                    rptBL1.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                    rptBL1.Parameters("number_font").Value = dt.Rows(0)("number_word")
                    rptBL1.Parameters("number").Value = dt.Rows(0)("number")
                    rptBL1.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                    rptBL1.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
                    rptBL1.Parameters("invoicenum").Value = frmset_bl.invoice
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            If RadioNM.SelectedIndex = 0 Then
                rptBL1.Parameters("nm").Value = "N/M " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 1 Then
                rptBL1.Parameters("nm").Value = txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 2 Then
                rptBL1.Parameters("nm").Value = "FLEXIONE " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 3 Then
                rptBL1.Parameters("nm").Value = "PROGYPS " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            Else
            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL1.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL1.Parameters("measurement").Value = txtMeasurement.EditValue

            End If

            Dim count_line As Integer = 1
            Dim countstring As String = ""

            ' mySqlCommand.CommandText = 
            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)

            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") + Environment.NewLine
                    countstring += count_line.ToString & "." + Environment.NewLine
                    count_line += 1
                Next
            End If



            rptBL1.Parameters("container").Value = container
            rptBL1.Parameters("count").Value = countstring

            rptBL1.CreateDocument()
            rptBL1.ShowRibbonPreviewDialog()

        End If

    End Sub


    Public Sub rpt_bl_marine()
        Dim container As String = ""

        Dim rptBL As rptBL2 = New rptBL2
        Dim rptBL1 As rptBL2_2 = New rptBL2_2

        'rptBL.Parameters("DateString").Value = dateStart.ToString("MMMM �.�. yyyy") ''��˹����������� ��� dat_print �form report1
        'rptBL.Parameters("ProductName").Value = SearchLookUpEdit1.Text

        If page1 = "0" Then
            Dim sql As String
            Dim dt As New DataTable



            If mark = "0" Then
                rptBL.Parameters("menuctn").Value = "Marks & Nos." + Environment.NewLine + "Container / Seal No."
            Else
                rptBL.Parameters("menuctn").Value = "Container / Seal No."

            End If


            dt = objCLASS.LoadBookingBLReport(idbooking)

            'Try
            '    mySqlReader = mySqlCommand.ExecuteReader
            If dt.Rows.Count > 0 Then
                rptBL.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                rptBL.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                rptBL.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                rptBL.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                rptBL.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                rptBL.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                rptBL.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                rptBL.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                rptBL.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                rptBL.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                rptBL.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                If dt.Rows(0)("BTYPEMOVE") = "0" Then
                    rptBL.Parameters("typemove").Value = "X"
                    rptBL.Parameters("typemove1").Value = ""
                Else
                    rptBL.Parameters("typemove1").Value = "X"
                    rptBL.Parameters("typemove").Value = ""
                End If

                rptBL.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                rptBL.Parameters("voyage").Value = dt.Rows(0)("VOYNAME")

                If dt.Rows(0)("BPORTDIS") Is DBNull.Value Then
                    rptBL.Parameters("portdischarge").Value = ""

                Else
                    rptBL.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")

                End If


                If dt.Rows(0)("BFINALDEST") Is DBNull.Value Then
                    rptBL.Parameters("placerecieve").Value = ""
                Else
                    rptBL.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                End If

                rptBL.Parameters("description").Value = "*** B/L Attached List ***" & Environment.NewLine & dt.Rows(0)("BDESCRIPT")
                rptBL.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                rptBL.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                rptBL.Parameters("number_font").Value = dt.Rows(0)("number_word")
                rptBL.Parameters("number").Value = dt.Rows(0)("number")
                rptBL.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                rptBL.Parameters("invoicenum").Value = frmset_bl.invoice
                rptBL.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
            End If





            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)
            If RadioNM.SelectedIndex = 0 Then
                container = "N/M " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 1 Then
                container = txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 2 Then
                container = "FLEXIONE " & txtNM.EditValue & Environment.NewLine

            ElseIf RadioNM.SelectedIndex = 3 Then
                container = "PROGYPS " & txtNM.EditValue & Environment.NewLine

            Else

            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL.Parameters("measurement").Value = txtMeasurement.EditValue

            End If


            If dt.Rows.Count > 0 Then

                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") & Environment.NewLine
                Next

            End If
            rptBL.Parameters("container").Value = container
            rptBL.CreateDocument()
            rptBL.ShowRibbonPreviewDialog()


        Else
            'CrystalReportViewer1.ReportSource = rpt2

            'CrystalReportViewer1.Refresh()

            If frmset_bl.mark = "0" Then
                rptBL1.Parameters("menuctn").Value = "Marks & Nos." & Environment.NewLine + "Container / Seal No."
            Else
                rptBL1.Parameters("menuctn").Value = "Container / Seal No."

            End If

            Dim sql As String
            Dim dt As New DataTable



            dt = objCLASS.LoadBookingBLReport(idbooking)
            Try

                If dt.Rows.Count > 0 Then
                    rptBL1.Parameters("bookingno").Value = dt.Rows(0)("BNO")
                    rptBL1.Parameters("billoflanding").Value = dt.Rows(0)("BLANDNO")
                    rptBL1.Parameters("notify").Value = dt.Rows(0)("BNOTIFY")
                    rptBL1.Parameters("shipname").Value = dt.Rows(0)("BSHIPNAME")
                    rptBL1.Parameters("shipaddress").Value = dt.Rows(0)("SHIPADD")
                    rptBL1.Parameters("shiptell").Value = dt.Rows(0)("SHIPTELL")
                    rptBL1.Parameters("shipfax").Value = dt.Rows(0)("SHIPFAX")
                    rptBL1.Parameters("conname").Value = dt.Rows(0)("CONNAME")
                    rptBL1.Parameters("conaddress").Value = dt.Rows(0)("CONADD")
                    rptBL1.Parameters("contell").Value = dt.Rows(0)("CONTELL")
                    rptBL1.Parameters("confax").Value = dt.Rows(0)("CONFAX")
                    If dt.Rows(0)("BTYPEMOVE") = "0" Then
                        rptBL1.Parameters("typemove").Value = "X"
                        rptBL1.Parameters("typemove1").Value = ""
                    Else
                        rptBL1.Parameters("typemove1").Value = "X"
                        rptBL1.Parameters("typemove").Value = ""
                    End If

                    rptBL1.Parameters("vesselname").Value = dt.Rows(0)("VOYVESNAMES")
                    rptBL1.Parameters("voyage").Value = dt.Rows(0)("VOYNAME")
                    rptBL1.Parameters("portdischarge").Value = dt.Rows(0)("BPORTDIS")
                    rptBL1.Parameters("placerecieve").Value = dt.Rows(0)("BFINALDEST")
                    rptBL1.Parameters("description").Value = "*** B/L Attached List ***" & Environment.NewLine & dt.Rows(0)("BDESCRIPT")
                    rptBL1.Parameters("containerno").Value = dt.Rows(0)("BCTNNO")
                    rptBL1.Parameters("grossweight").Value = dt.Rows(0)("BGROSS")
                    rptBL1.Parameters("number_font").Value = dt.Rows(0)("number_word")
                    rptBL1.Parameters("number").Value = dt.Rows(0)("number")
                    rptBL1.Parameters("typectn").Value = dt.Rows(0)("BCTNTYPE")
                    rptBL1.Parameters("date_commit").Value = Convert.ToDateTime(dateEdit1.EditValue).ToString("dd-MM-yyyy")
                    rptBL1.Parameters("invoicenum").Value = frmset_bl.invoice
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            If RadioNM.SelectedIndex = 0 Then
                rptBL1.Parameters("nm").Value = "N/M " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 1 Then
                rptBL1.Parameters("nm").Value = txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 2 Then
                rptBL1.Parameters("nm").Value = "FLEXIONE " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            ElseIf RadioNM.SelectedIndex = 3 Then
                rptBL1.Parameters("nm").Value = "PROGYPS " & txtNM.EditValue & Environment.NewLine & "TO BE CONTINUED ON ATTACHED LIST"

            Else
            End If
            If RadioGroup5.SelectedIndex <= 0 Then
                rptBL1.Parameters("measurement").Value = "Don't Show"
            Else
                rptBL1.Parameters("measurement").Value = txtMeasurement.EditValue

            End If

            Dim count_line As Integer = 1
            Dim countstring As String = ""


            ' mySqlCommand.CommandText = 
            dt = objCLASS.LoadbookingBLReportCountCTN(idbooking)

            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    container += dt.Rows(i)("CTNSTRING") & "/" + dt.Rows(i)("CTNSEALID") + Environment.NewLine
                    countstring += count_line.ToString & "." + Environment.NewLine
                    count_line += 1
                Next
            End If



            rptBL1.Parameters("container").Value = container
            rptBL1.Parameters("count").Value = countstring

            rptBL1.CreateDocument()
            rptBL1.ShowRibbonPreviewDialog()

        End If

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click

        checkfill()
        rpt_bl_ocean()
        ' Dim nextform As frmrpt_bl1 = New frmrpt_bl1
        'nextform.Show()

    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        'Me.checkfill()
        'Dim nextform As frmrpt_bl2 = New frmrpt_bl2()
        'nextform.Show()

        checkfill()
        rpt_bl_marine()
    End Sub

    Private Sub slcVoayge_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        LoadBooking()
    End Sub
    Public Sub loadDatavoyage()
        Dim sql As String = "Select * from voyage;"

        ykpset.Tables("voyage").Clear()

        connectDB.GetTable(sql, ykpset.Tables("voyage"))


        slcVoyage.Properties.DataSource = ykpset.Tables("voyage")
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        checkfill()
        If invoice Is Nothing Then
            MsgBox("��س����͡ Bill")
            Exit Sub
        End If

        rpt_bl_SPH()

    End Sub
End Class