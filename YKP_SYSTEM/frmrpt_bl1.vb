﻿Imports MySql.Data.MySqlClient
Public Class frmrpt_bl1

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Dim container As String = ""
    Dim rpt1 As New rpt_BL1
    Dim rpt2 As New rpt_bl1_2

    Private Sub frmrpt_bl1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        If frmset_bl.page1 = "0" Then
            Dim sql As String
            Dim dt As New DataTable
            CrystalReportViewer1.ReportSource = rpt1

            CrystalReportViewer1.Refresh()

            If frmset_bl.mark = "0" Then
                rpt1.SetParameterValue("menuctn", "Marks & Nos." + Environment.NewLine + "Container / Seal No.")
            Else
                rpt1.SetParameterValue("menuctn", "Container / Seal No.")

            End If

            sql = "Select * from booking_bl join booking on booking_bl.booking_id = booking.BOOKINGID join voyage on voyage.VOYAGEID = booking.BVOYAGE   join consignee on  consignee.consigneeid = booking_bl.conid join shipper on shipper.SHIPPERID = booking.BSHIP  where BOOKINGID ='" & frmset_bl.idbooking & "' ;"
            ' mySqlCommand.CommandText = 
            dt = connectDB.GetTable(sql)


            Try

                If dt.Rows.Count > 0 Then
                    rpt1.SetParameterValue("bookingno", dt.Rows(0)("BNO"))
                    rpt1.SetParameterValue("billoflanding", dt.Rows(0)("BLANDNO"))
                    rpt1.SetParameterValue("notify", dt.Rows(0)("BNOTIFY"))
                    rpt1.SetParameterValue("shipname", dt.Rows(0)("BSHIPNAME"))
                    rpt1.SetParameterValue("shipaddress", dt.Rows(0)("SHIPADD"))
                    rpt1.SetParameterValue("shiptell", dt.Rows(0)("SHIPTELL"))
                    rpt1.SetParameterValue("shipfax", dt.Rows(0)("SHIPFAX"))
                    rpt1.SetParameterValue("conname", dt.Rows(0)("CONNAME"))
                    rpt1.SetParameterValue("conaddress", dt.Rows(0)("CONADD"))
                    rpt1.SetParameterValue("contell", dt.Rows(0)("CONTELL"))
                    rpt1.SetParameterValue("confax", dt.Rows(0)("CONFAX"))
                    If dt.Rows(0)("BTYPEMOVE") = "0" Then
                        rpt1.SetParameterValue("typemove", "X")
                        rpt1.SetParameterValue("typemove1", "")
                    Else
                        rpt1.SetParameterValue("typemove1", "X")
                        rpt1.SetParameterValue("typemove", "")
                    End If

                    rpt1.SetParameterValue("vesselname", dt.Rows(0)("VOYVESNAMES"))
                    rpt1.SetParameterValue("voyage", "S" + Format(dt.Rows(0)("VOYAGEID"), "000"))

                    If dt.Rows(0)("BPORTDIS") Is DBNull.Value Then
                        rpt1.SetParameterValue("portdischarge", "")

                    Else
                        rpt1.SetParameterValue("portdischarge", dt.Rows(0)("BPORTDIS"))
                        'rpt1.SetParameterValue("portdischarge", "test")

                    End If


                    If dt.Rows(0)("BFINALDEST") Is DBNull.Value Then
                        rpt1.SetParameterValue("placerecieve", "")

                    Else

                        rpt1.SetParameterValue("placerecieve", dt.Rows(0)("BFINALDEST"))
                        'rpt1.SetParameterValue("placerecieve", "test1")


                    End If

                    rpt1.SetParameterValue("description", dt.Rows(0)("BDESCRIPT"))
                    rpt1.SetParameterValue("containerno", dt.Rows(0)("BCTNNO"))
                    rpt1.SetParameterValue("grossweight", dt.Rows(0)("BGROSS"))
                    rpt1.SetParameterValue("number_font", dt.Rows(0)("number_word"))
                    rpt1.SetParameterValue("number", dt.Rows(0)("number"))
                    rpt1.SetParameterValue("typectn", dt.Rows(0)("BCTNTYPE"))
                    rpt1.SetParameterValue("invoicenum", frmset_bl.invoice)
                    rpt1.SetParameterValue("date_commit", Date.Now.Date.ToString("dd-MM-yyyy"))

                End If




            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
            If frmset_bl.nm = "0" Then
                container = "N/M" + Environment.NewLine
            Else
                container = ""
            End If


            sql = "Select * from booking join borrow on booking.BOOKINGID = borrow.BOOKID join ctnmain on ctnmain.CTNMAINID = borrow.CTNID    where BOOKINGID ='" & frmset_bl.idbooking & "' ;"

            dt = connectDB.GetTable(sql)


            Try
                If dt.Rows.Count > 0 Then

                    For i As Integer = 0 To dt.Rows.Count - 1
                        container += dt.Rows(i)("CTNSTRING") + "/" + dt.Rows(i)("CTNSEALID") + Environment.NewLine
                    Next

                End If
            Catch ex As Exception

            End Try
            rpt1.SetParameterValue("container", container)

        Else


            CrystalReportViewer1.ReportSource = rpt2

            CrystalReportViewer1.Refresh()

            If frmset_bl.mark = "0" Then
                rpt2.SetParameterValue("menuctn", "Marks & Nos." + Environment.NewLine + "Container / Seal No.")
            Else
                rpt2.SetParameterValue("menuctn", "Container / Seal No.")

            End If
            Dim sql As String
            Dim dt As New DataTable


            sql = "Select * from booking_bl join booking on booking_bl.booking_id = booking.BOOKINGID join voyage on voyage.VOYAGEID = booking.BVOYAGE   join consignee on  consignee.consigneeid = booking_bl.conid join shipper on shipper.SHIPPERID = booking.BSHIP  where BOOKINGID ='" & frmset_bl.idbooking & "'   ;"



            dt = connectDB.GetTable(sql)


            If dt.Rows.Count > 0 Then
                rpt2.SetParameterValue("bookingno", dt.Rows(0)("BNO"))
                rpt2.SetParameterValue("billoflanding", dt.Rows(0)("BLANDNO"))
                rpt2.SetParameterValue("notify", dt.Rows(0)("BNOTIFY"))
                rpt2.SetParameterValue("shipname", dt.Rows(0)("BSHIPNAME"))
                rpt2.SetParameterValue("shipaddress", dt.Rows(0)("SHIPADD"))
                rpt2.SetParameterValue("shiptell", dt.Rows(0)("SHIPTELL"))
                rpt2.SetParameterValue("shipfax", dt.Rows(0)("SHIPFAX"))
                rpt2.SetParameterValue("conname", dt.Rows(0)("CONNAME"))
                rpt2.SetParameterValue("conaddress", dt.Rows(0)("CONADD"))
                rpt2.SetParameterValue("contell", dt.Rows(0)("CONTELL"))
                rpt2.SetParameterValue("confax", dt.Rows(0)("CONFAX"))
                If dt.Rows(0)("BTYPEMOVE") = "0" Then
                    rpt2.SetParameterValue("typemove", "X")
                    rpt2.SetParameterValue("typemove1", "")
                Else
                    rpt2.SetParameterValue("typemove1", "X")
                    rpt2.SetParameterValue("typemove", "")
                End If

                rpt2.SetParameterValue("vesselname", dt.Rows(0)("VOYVESNAMES"))
                rpt2.SetParameterValue("voyage", "S" + Format(dt.Rows(0)("VOYAGEID"), "000"))
                rpt2.SetParameterValue("portdischarge", dt.Rows(0)("BPORTDIS"))
                rpt2.SetParameterValue("placerecieve", dt.Rows(0)("BFINALDEST"))
                rpt2.SetParameterValue("description", dt.Rows(0)("BDESCRIPT"))
                rpt2.SetParameterValue("containerno", dt.Rows(0)("BCTNNO"))
                rpt2.SetParameterValue("grossweight", dt.Rows(0)("BGROSS"))
                rpt2.SetParameterValue("number_font", dt.Rows(0)("number_word"))
                rpt2.SetParameterValue("number", dt.Rows(0)("number"))
                rpt2.SetParameterValue("typectn", dt.Rows(0)("BCTNTYPE"))
                rpt2.SetParameterValue("date_commit", Date.Now.Date.ToString("dd-MM-yyyy"))
                rpt2.SetParameterValue("invoicenum", frmset_bl.invoice)
            End If


            If frmset_bl.nm = "0" Then
                    rpt2.SetParameterValue("nm", "N/M" + Environment.NewLine + "TO BE CONTINUED ON ATTACHED LIST")

                Else
                    rpt2.SetParameterValue("nm", "TO BE CONTINUED ON ATTACHED LIST")

                End If
                Dim count_line As Integer = 1
                Dim countstring As String = ""



            sql = "Select * from booking join borrow on booking.BOOKINGID = borrow.BOOKID join ctnmain on ctnmain.CTNMAINID = borrow.CTNID    where BOOKINGID ='" & frmset_bl.idbooking & "'  ;"
            ' mySqlCommand.CommandText = 



            dt = connectDB.GetTable(sql)


            Try
                If dt.Rows.Count > 0 Then

                    For i As Integer = 0 To dt.Rows.Count - 1
                        container += dt.Rows(i)("CTNSTRING") + "/" + dt.Rows(i)("CTNSEALID") + Environment.NewLine
                        countstring += count_line.ToString + "." + Environment.NewLine
                        count_line += 1
                    Next

                End If
            Catch ex As Exception

            End Try


            rpt2.SetParameterValue("container", container)
            rpt2.SetParameterValue("count", countstring)

        End If


    End Sub
End Class