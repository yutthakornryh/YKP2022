﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmmain_voyempty
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.id_customer = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.agent = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.condition = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ButtonX3 = New DevComponents.DotNetBar.ButtonX()
        Me.TextBoxX1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX2 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX4 = New DevComponents.DotNetBar.ButtonX()
        Me.txt_voyage = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.date_eta_penang = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.date_etd_penang = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.date_krabi_etd = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.date_krabi_eta = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.date_eta_penang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_etd_penang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.date_krabi_etd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_krabi_eta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ListView1
        '
        Me.ListView1.BackColor = System.Drawing.Color.White
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.id_customer, Me.agent, Me.condition, Me.ColumnHeader2, Me.ColumnHeader5, Me.ColumnHeader6})
        Me.ListView1.Font = New System.Drawing.Font("Cordia New", 14.25!)
        Me.ListView1.FullRowSelect = True
        Me.ListView1.GridLines = True
        Me.ListView1.HideSelection = False
        Me.ListView1.Location = New System.Drawing.Point(12, 78)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(681, 320)
        Me.ListView1.TabIndex = 151
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Width = 0
        '
        'id_customer
        '
        Me.id_customer.Text = "ID"
        Me.id_customer.Width = 90
        '
        'agent
        '
        Me.agent.Text = "ETA"
        Me.agent.Width = 115
        '
        'condition
        '
        Me.condition.Text = "ETD"
        Me.condition.Width = 113
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "ID"
        Me.ColumnHeader2.Width = 84
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ETA"
        Me.ColumnHeader5.Width = 138
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "ETD"
        Me.ColumnHeader6.Width = 134
        '
        'ButtonX3
        '
        Me.ButtonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX3.Location = New System.Drawing.Point(613, 28)
        Me.ButtonX3.Name = "ButtonX3"
        Me.ButtonX3.Size = New System.Drawing.Size(80, 34)
        Me.ButtonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX3.Symbol = ""
        Me.ButtonX3.TabIndex = 213
        Me.ButtonX3.Text = "Search"
        '
        'TextBoxX1
        '
        Me.TextBoxX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX1.Border.Class = "TextBoxBorder"
        Me.TextBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX1.Font = New System.Drawing.Font("Cordia New", 14.25!)
        Me.TextBoxX1.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX1.Location = New System.Drawing.Point(87, 28)
        Me.TextBoxX1.Name = "TextBoxX1"
        Me.TextBoxX1.PreventEnterBeep = True
        Me.TextBoxX1.Size = New System.Drawing.Size(519, 34)
        Me.TextBoxX1.TabIndex = 212
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(29, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 26)
        Me.Label2.TabIndex = 211
        Me.Label2.Text = "Search"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(612, 404)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(80, 34)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.TabIndex = 214
        Me.ButtonX1.Text = "VOY S"
        '
        'ButtonX2
        '
        Me.ButtonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX2.Location = New System.Drawing.Point(526, 404)
        Me.ButtonX2.Name = "ButtonX2"
        Me.ButtonX2.Size = New System.Drawing.Size(80, 34)
        Me.ButtonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX2.Symbol = ""
        Me.ButtonX2.TabIndex = 215
        Me.ButtonX2.Text = "VOY N"
        '
        'ButtonX4
        '
        Me.ButtonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX4.Location = New System.Drawing.Point(1135, 212)
        Me.ButtonX4.Name = "ButtonX4"
        Me.ButtonX4.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(12)
        Me.ButtonX4.Size = New System.Drawing.Size(111, 43)
        Me.ButtonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX4.Symbol = ""
        Me.ButtonX4.SymbolSize = 25.0!
        Me.ButtonX4.TabIndex = 220
        Me.ButtonX4.Text = "Save"
        '
        'txt_voyage
        '
        Me.txt_voyage.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_voyage.Location = New System.Drawing.Point(1093, 43)
        Me.txt_voyage.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.Size = New System.Drawing.Size(153, 37)
        Me.txt_voyage.TabIndex = 217
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.date_eta_penang)
        Me.GroupBox1.Controls.Add(Me.date_etd_penang)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Location = New System.Drawing.Point(707, 91)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(539, 53)
        Me.GroupBox1.TabIndex = 218
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PENANG PORT TYPE N"
        '
        'date_eta_penang
        '
        '
        '
        '
        Me.date_eta_penang.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_eta_penang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_eta_penang.ButtonDropDown.Visible = True
        Me.date_eta_penang.CustomFormat = "ddd-dd-MM-yyyy"
        Me.date_eta_penang.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_eta_penang.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_eta_penang.IsPopupCalendarOpen = False
        Me.date_eta_penang.Location = New System.Drawing.Point(57, 23)
        Me.date_eta_penang.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_eta_penang.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_eta_penang.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_eta_penang.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_eta_penang.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_eta_penang.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.MonthCalendar.TodayButtonVisible = True
        Me.date_eta_penang.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_eta_penang.Name = "date_eta_penang"
        Me.date_eta_penang.Size = New System.Drawing.Size(200, 22)
        Me.date_eta_penang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_eta_penang.TabIndex = 180
        '
        'date_etd_penang
        '
        '
        '
        '
        Me.date_etd_penang.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_etd_penang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_etd_penang.ButtonDropDown.Visible = True
        Me.date_etd_penang.CustomFormat = "ddd-dd-MM-yyyy"
        Me.date_etd_penang.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_etd_penang.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_etd_penang.IsPopupCalendarOpen = False
        Me.date_etd_penang.Location = New System.Drawing.Point(313, 22)
        Me.date_etd_penang.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_etd_penang.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_etd_penang.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_etd_penang.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_etd_penang.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_etd_penang.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.MonthCalendar.TodayButtonVisible = True
        Me.date_etd_penang.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_etd_penang.Name = "date_etd_penang"
        Me.date_etd_penang.Size = New System.Drawing.Size(200, 22)
        Me.date_etd_penang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_etd_penang.TabIndex = 179
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(9, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 26)
        Me.Label19.TabIndex = 144
        Me.Label19.Text = "ETD :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(267, 20)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(40, 26)
        Me.Label18.TabIndex = 143
        Me.Label18.Text = "ETA :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.date_krabi_etd)
        Me.GroupBox2.Controls.Add(Me.date_krabi_eta)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Location = New System.Drawing.Point(707, 150)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(539, 56)
        Me.GroupBox2.TabIndex = 219
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "KANTANG PORT YKP TYPE S"
        '
        'date_krabi_etd
        '
        '
        '
        '
        Me.date_krabi_etd.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_krabi_etd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_krabi_etd.ButtonDropDown.Visible = True
        Me.date_krabi_etd.CustomFormat = "ddd-dd-MM-yyyy"
        Me.date_krabi_etd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_krabi_etd.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_krabi_etd.IsPopupCalendarOpen = False
        Me.date_krabi_etd.Location = New System.Drawing.Point(313, 20)
        Me.date_krabi_etd.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_krabi_etd.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_krabi_etd.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_krabi_etd.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_krabi_etd.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_krabi_etd.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.MonthCalendar.TodayButtonVisible = True
        Me.date_krabi_etd.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_krabi_etd.Name = "date_krabi_etd"
        Me.date_krabi_etd.Size = New System.Drawing.Size(200, 22)
        Me.date_krabi_etd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_krabi_etd.TabIndex = 181
        '
        'date_krabi_eta
        '
        '
        '
        '
        Me.date_krabi_eta.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_krabi_eta.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_krabi_eta.ButtonDropDown.Visible = True
        Me.date_krabi_eta.CustomFormat = "ddd-dd-MM-yyyy"
        Me.date_krabi_eta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_krabi_eta.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_krabi_eta.IsPopupCalendarOpen = False
        Me.date_krabi_eta.Location = New System.Drawing.Point(57, 19)
        Me.date_krabi_eta.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_krabi_eta.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_krabi_eta.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_krabi_eta.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_krabi_eta.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_krabi_eta.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.MonthCalendar.TodayButtonVisible = True
        Me.date_krabi_eta.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_krabi_eta.Name = "date_krabi_eta"
        Me.date_krabi_eta.Size = New System.Drawing.Size(200, 22)
        Me.date_krabi_eta.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_krabi_eta.TabIndex = 180
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(9, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 26)
        Me.Label12.TabIndex = 144
        Me.Label12.Text = "ETD :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(267, 16)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 26)
        Me.Label20.TabIndex = 143
        Me.Label20.Text = "ETA :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1000, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 26)
        Me.Label5.TabIndex = 216
        Me.Label5.Text = "VOYAGE  NO"
        '
        'frmmain_voyempty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1280, 628)
        Me.Controls.Add(Me.ButtonX4)
        Me.Controls.Add(Me.txt_voyage)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ButtonX2)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.ButtonX3)
        Me.Controls.Add(Me.TextBoxX1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ListView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmmain_voyempty"
        Me.Text = "frmmain_voyempty"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.date_eta_penang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_etd_penang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.date_krabi_etd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_krabi_eta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents id_customer As System.Windows.Forms.ColumnHeader
    Friend WithEvents agent As System.Windows.Forms.ColumnHeader
    Friend WithEvents condition As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ButtonX3 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TextBoxX1 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX2 As DevComponents.DotNetBar.ButtonX
    Private WithEvents ButtonX4 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txt_voyage As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents date_eta_penang As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents date_etd_penang As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents date_krabi_etd As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents date_krabi_eta As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
End Class
