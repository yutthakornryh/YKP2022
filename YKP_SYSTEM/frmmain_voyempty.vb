﻿Imports MySql.Data.MySqlClient
Public Class frmmain_voyempty
    Dim mysql As MySqlConnection = main_form.mysqlconection
    Dim mySqlCommand As New MySqlCommand
    Dim mySqlAdaptor As New MySqlDataAdapter
    Dim mySqlReader As MySqlDataReader
    Public Shared idvoyage As Integer
    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        If ListView1.SelectedItems.Count > 0 Then
            idvoyage = ListView1.SelectedItems(0).SubItems(0).Text
            Dim cf As New frmview_voyempty

            cf.MdiParent = Me.MdiParent
            Me.Close()
            cf.Dock = DockStyle.Fill
            cf.Show()
        Else
            Dim cf As New frmview_voyempty

            cf.MdiParent = Me.MdiParent
            Me.Close()
            cf.Dock = DockStyle.Fill
            cf.Show()
        End If
    End Sub

    Private Sub frmmain_voyempty_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture

        mysql.Close()

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from voyage_empty;"
        ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql

        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())

                With ListView1.Items.Add(mySqlReader("idvoyage_empty"))
                    .SubItems.Add((mySqlReader("idvoyage") + "N"))
                    .SubItems.Add(mySqlReader("etdn"))
                    .SubItems.Add(mySqlReader("etan"))
                    .SubItems.Add((mySqlReader("idvoyage") + "S"))
                    .SubItems.Add(mySqlReader("etds"))
                    .SubItems.Add(mySqlReader("etas"))
                End With

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub
    Public Sub searchData()
        mysql.Close()

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from voyage_empty where idvoyage like '%" & TextBoxX1.Text & "%';"
        ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql

        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())

                With ListView1.Items.Add(mySqlReader("idvoyage_empty"))
                    .SubItems.Add(Format(mySqlReader("idvoyage"), "000") + "N")
                    .SubItems.Add(mySqlReader("etdn"))
                    .SubItems.Add(mySqlReader("etan"))
                    .SubItems.Add(Format(mySqlReader("idvoyage"), "000") + "S")
                    .SubItems.Add(mySqlReader("etds"))
                    .SubItems.Add(mySqlReader("etas"))
                End With

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub

    Private Sub TextBoxX1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxX1.KeyDown
        If e.KeyCode = Keys.Enter Then
            searchData()

        End If
    End Sub

    Private Sub ButtonX3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX3.Click
        searchData()
    End Sub

    Private Sub ListView1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.Click
        idvoyage = ListView1.SelectedItems(0).SubItems(0).Text
    End Sub

    Private Sub ButtonX4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX4.Click

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If
        Try
            mySqlCommand.Parameters.Clear()
            mySqlCommand.CommandText = "insert into voyage_empty ( idvoyage, etdn, etan,etds,etas) values (@idvoyage,@etdn,@etan,@etds,@etas)"
            mySqlCommand.Connection = mysql


            mySqlCommand.Parameters.AddWithValue("@idvoyage", txt_voyage.Text)
            mySqlCommand.Parameters.AddWithValue("@etdn", date_eta_penang.Text)
            mySqlCommand.Parameters.AddWithValue("@etan", date_etd_penang.Text)
            mySqlCommand.Parameters.AddWithValue("@etds", date_krabi_eta.Text)
            mySqlCommand.Parameters.AddWithValue("@etas", date_krabi_etd.Text)

            mySqlCommand.ExecuteNonQuery()
            mysql.Close()
            MsgBox("บันทึกข้อมูลเรียบร้อย")
        Catch ex As Exception

        End Try


        searchData()

    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        Dim cf As New frmview_voyemptyn

        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub
End Class