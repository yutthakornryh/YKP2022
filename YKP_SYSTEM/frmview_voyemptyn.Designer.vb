﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmview_voyemptyn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmview_voyemptyn))
        Me.txt_voyage = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.date_etd_penang = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.date_eta_penang = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.date_krabi_etd = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.date_krabi_eta = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DataGridViewX1 = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SAVE = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.EDIT = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.DELETE = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ButtonX3 = New DevComponents.DotNetBar.ButtonX()
        Me.MonthCalendarAdv1 = New DevComponents.Editors.DateTimeAdv.MonthCalendarAdv()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_voyage
        '
        Me.txt_voyage.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_voyage.Location = New System.Drawing.Point(127, 14)
        Me.txt_voyage.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.Size = New System.Drawing.Size(153, 37)
        Me.txt_voyage.TabIndex = 225
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.date_etd_penang)
        Me.GroupBox1.Controls.Add(Me.date_eta_penang)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Location = New System.Drawing.Point(31, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(383, 53)
        Me.GroupBox1.TabIndex = 226
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PENANG PORT TYPE N"
        '
        'date_etd_penang
        '
        '
        '
        '
        Me.date_etd_penang.BackgroundStyle.Class = "TextBoxBorder"
        Me.date_etd_penang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.ButtonClear.Visible = True
        Me.date_etd_penang.Location = New System.Drawing.Point(240, 24)
        Me.date_etd_penang.Mask = "00-00-0000"
        Me.date_etd_penang.Name = "date_etd_penang"
        Me.date_etd_penang.Size = New System.Drawing.Size(137, 19)
        Me.date_etd_penang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_etd_penang.TabIndex = 225
        Me.date_etd_penang.Text = ""
        '
        'date_eta_penang
        '
        '
        '
        '
        Me.date_eta_penang.BackgroundStyle.Class = "TextBoxBorder"
        Me.date_eta_penang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.ButtonClear.Visible = True
        Me.date_eta_penang.Location = New System.Drawing.Point(55, 24)
        Me.date_eta_penang.Mask = "00-00-0000"
        Me.date_eta_penang.Name = "date_eta_penang"
        Me.date_eta_penang.Size = New System.Drawing.Size(137, 19)
        Me.date_eta_penang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_eta_penang.TabIndex = 224
        Me.date_eta_penang.Text = ""
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(9, 20)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 26)
        Me.Label19.TabIndex = 144
        Me.Label19.Text = "ETD :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(198, 20)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(40, 26)
        Me.Label18.TabIndex = 143
        Me.Label18.Text = "ETA :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.date_krabi_etd)
        Me.GroupBox2.Controls.Add(Me.date_krabi_eta)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Location = New System.Drawing.Point(420, 57)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(395, 51)
        Me.GroupBox2.TabIndex = 227
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "KANTANG PORT YKP TYPE S"
        '
        'date_krabi_etd
        '
        '
        '
        '
        Me.date_krabi_etd.BackgroundStyle.Class = "TextBoxBorder"
        Me.date_krabi_etd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.ButtonClear.Visible = True
        Me.date_krabi_etd.Location = New System.Drawing.Point(245, 22)
        Me.date_krabi_etd.Mask = "00-00-0000"
        Me.date_krabi_etd.Name = "date_krabi_etd"
        Me.date_krabi_etd.Size = New System.Drawing.Size(137, 19)
        Me.date_krabi_etd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_krabi_etd.TabIndex = 226
        Me.date_krabi_etd.Text = ""
        '
        'date_krabi_eta
        '
        '
        '
        '
        Me.date_krabi_eta.BackgroundStyle.Class = "TextBoxBorder"
        Me.date_krabi_eta.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.ButtonClear.Visible = True
        Me.date_krabi_eta.Location = New System.Drawing.Point(57, 22)
        Me.date_krabi_eta.Mask = "00-00-0000"
        Me.date_krabi_eta.Name = "date_krabi_eta"
        Me.date_krabi_eta.Size = New System.Drawing.Size(137, 19)
        Me.date_krabi_eta.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.date_krabi_eta.TabIndex = 225
        Me.date_krabi_eta.Text = ""
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(9, 19)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 26)
        Me.Label12.TabIndex = 144
        Me.Label12.Text = "ETD :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(204, 18)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 26)
        Me.Label20.TabIndex = 143
        Me.Label20.Text = "ETA :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(34, 19)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 26)
        Me.Label5.TabIndex = 224
        Me.Label5.Text = "VOYAGE  NO"
        '
        'DataGridViewX1
        '
        Me.DataGridViewX1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewX1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewX1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column4, Me.Column5, Me.Column3, Me.Column11, Me.Column1, Me.Column10, Me.SAVE, Me.EDIT, Me.DELETE, Me.Column6})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewX1.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewX1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.DataGridViewX1.EnableHeadersVisualStyles = False
        Me.DataGridViewX1.GridColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.DataGridViewX1.Location = New System.Drawing.Point(31, 147)
        Me.DataGridViewX1.MultiSelect = False
        Me.DataGridViewX1.Name = "DataGridViewX1"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewX1.RowTemplate.Height = 21
        Me.DataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridViewX1.Size = New System.Drawing.Size(928, 272)
        Me.DataGridViewX1.TabIndex = 228
        '
        'Column2
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column2.HeaderText = "OPERATOR"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 120
        '
        'Column4
        '
        Me.Column4.HeaderText = "NO"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 55
        '
        'Column5
        '
        Me.Column5.DisplayMember = "Text"
        Me.Column5.DropDownHeight = 106
        Me.Column5.DropDownWidth = 121
        Me.Column5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Column5.HeaderText = "TYPE"
        Me.Column5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Column5.IntegralHeight = False
        Me.Column5.ItemHeight = 15
        Me.Column5.Items.AddRange(New Object() {"40 HC", "20 GP"})
        Me.Column5.Name = "Column5"
        Me.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Column5.Width = 55
        '
        'Column3
        '
        Me.Column3.HeaderText = "BOOKING"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 120
        '
        'Column11
        '
        Me.Column11.HeaderText = "SCN"
        Me.Column11.Name = "Column11"
        '
        'Column1
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column1.HeaderText = "BY"
        Me.Column1.Name = "Column1"
        '
        'Column10
        '
        Me.Column10.HeaderText = "Remark"
        Me.Column10.Name = "Column10"
        Me.Column10.Width = 250
        '
        'SAVE
        '
        Me.SAVE.HeaderText = ""
        Me.SAVE.Image = CType(resources.GetObject("SAVE.Image"), System.Drawing.Image)
        Me.SAVE.MinimumWidth = 25
        Me.SAVE.Name = "SAVE"
        Me.SAVE.Text = Nothing
        Me.SAVE.Width = 25
        '
        'EDIT
        '
        Me.EDIT.HeaderText = ""
        Me.EDIT.Image = CType(resources.GetObject("EDIT.Image"), System.Drawing.Image)
        Me.EDIT.Name = "EDIT"
        Me.EDIT.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.EDIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.EDIT.Text = Nothing
        Me.EDIT.Width = 25
        '
        'DELETE
        '
        Me.DELETE.HeaderText = ""
        Me.DELETE.Image = CType(resources.GetObject("DELETE.Image"), System.Drawing.Image)
        Me.DELETE.Name = "DELETE"
        Me.DELETE.Text = Nothing
        Me.DELETE.Width = 25
        '
        'Column6
        '
        Me.Column6.HeaderText = "id"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 5
        '
        'ButtonX3
        '
        Me.ButtonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX3.Location = New System.Drawing.Point(31, 425)
        Me.ButtonX3.Name = "ButtonX3"
        Me.ButtonX3.Size = New System.Drawing.Size(106, 41)
        Me.ButtonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX3.Symbol = ""
        Me.ButtonX3.TabIndex = 233
        Me.ButtonX3.Text = "Back"
        '
        'MonthCalendarAdv1
        '
        Me.MonthCalendarAdv1.AnnuallyMarkedDates = New Date(-1) {}
        Me.MonthCalendarAdv1.AutoSize = True
        '
        '
        '
        Me.MonthCalendarAdv1.BackgroundStyle.Class = "MonthCalendarAdv"
        Me.MonthCalendarAdv1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.MonthCalendarAdv1.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MonthCalendarAdv1.ContainerControlProcessDialogKey = True
        Me.MonthCalendarAdv1.DisplayMonth = New Date(2014, 2, 1, 0, 0, 0, 0)
        Me.MonthCalendarAdv1.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.MonthCalendarAdv1.Location = New System.Drawing.Point(821, 12)
        Me.MonthCalendarAdv1.MarkedDates = New Date(-1) {}
        Me.MonthCalendarAdv1.MonthlyMarkedDates = New Date(-1) {}
        Me.MonthCalendarAdv1.Name = "MonthCalendarAdv1"
        '
        '
        '
        Me.MonthCalendarAdv1.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.MonthCalendarAdv1.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.MonthCalendarAdv1.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.MonthCalendarAdv1.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MonthCalendarAdv1.Size = New System.Drawing.Size(170, 131)
        Me.MonthCalendarAdv1.TabIndex = 234
        Me.MonthCalendarAdv1.Text = "MonthCalendarAdv1"
        Me.MonthCalendarAdv1.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        '
        'frmview_voyemptyn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1364, 653)
        Me.Controls.Add(Me.MonthCalendarAdv1)
        Me.Controls.Add(Me.ButtonX3)
        Me.Controls.Add(Me.DataGridViewX1)
        Me.Controls.Add(Me.txt_voyage)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmview_voyemptyn"
        Me.Text = "frmview_voyemptyn"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_voyage As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents date_etd_penang As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents date_eta_penang As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents date_krabi_etd As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents date_krabi_eta As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewX1 As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SAVE As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents EDIT As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents DELETE As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ButtonX3 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents MonthCalendarAdv1 As DevComponents.Editors.DateTimeAdv.MonthCalendarAdv
End Class
