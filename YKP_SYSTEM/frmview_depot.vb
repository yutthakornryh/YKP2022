﻿Option Explicit On

Imports MySql.Data.MySqlClient
Imports Microsoft.Office.Interop
Imports System.Threading
Public Class frmview_depot
    Dim mysql As MySqlConnection = main_form.mysqlconection
    Dim mysql1 As MySqlConnection = main_form.mysqlconection1
    Dim count_day As Integer = 0
    Dim mySqlCommand As New MySqlCommand
    Dim mySqlAdaptor As New MySqlDataAdapter
    Dim mySqlReader As MySqlDataReader
    Dim iddepot As String
    Dim idcontainer As String
    Dim voyagelist As String
    Dim containerlist As String
    Dim sizelist As String
    Dim agenlist As String
    Dim pathExcel As String
    Public Delegate Sub DelegateSub(ByVal x As Integer)
    Private Sub frmview_depot_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListView1.Items.Clear()
        ListView3.Items.Clear()
        ListView4.Items.Clear()

        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        mysql.Close()


        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from voyage join ctnmain on voyage.VOYAGEID = ctnmain.CTNVOYN join ctndepot on ctnmain.CTNMAINID = ctndepot.CTNID where  CTNSTATDEPOT ='1';"        ' mySqlCommand.CommandText = 
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader
            ListView2.Items.Clear()
            While (mySqlReader.Read())

                With ListView2.Items.Add(Format(mySqlReader("VOYAGEID"), "000"))
                    .SubItems.Add(mySqlReader("CTNSTRING"))
                    .SubItems.Add(mySqlReader("CTNSIZE"))
                    .SubItems.Add(mySqlReader("CTNAGENT"))
                    If mySqlReader("CTNSTATDEPOT") = 1 Then

                        .SubItems.Add("รอตรวจสอบ")
                    ElseIf mySqlReader("CTNSTATDEPOT") = 2 Then

                        .SubItems.Add("ทำความสะอาด")
                    ElseIf mySqlReader("CTNSTATDEPOT") = 3 Then

                        .SubItems.Add("รอซ่อม")
                    Else
                        .SubItems.Add(mySqlReader("CTNAGENT"))
                    End If
           
                    .SubItems.Add(mySqlReader("CTNDEPOTID"))
                    .SubItems.Add(mySqlReader("CTNMAINID"))
                End With


            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()


      
    End Sub

    Private Sub ButtonItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)



    End Sub

    Private Sub ListView2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView2.Click


        iddepot = ListView2.SelectedItems(0).SubItems(5).Text
        idcontainer = ListView2.SelectedItems(0).SubItems(6).Text
        voyagelist = ListView2.SelectedItems(0).SubItems(0).Text
        containerlist = ListView2.SelectedItems(0).SubItems(1).Text
        sizelist = ListView2.SelectedItems(0).SubItems(2).Text
        agenlist = ListView2.SelectedItems(0).SubItems(3).Text


    End Sub

    Private Sub ButtonItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem4.Click
        Dim cf As New frmview_depotclean
        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()

    End Sub

    Private Sub ListView2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView2.DoubleClick

    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub
    Public Sub addlistview() Handles ListView2.DoubleClick

        iddepot = ListView2.SelectedItems(0).SubItems(5).Text
        idcontainer = ListView2.SelectedItems(0).SubItems(6).Text
        voyagelist = ListView2.SelectedItems(0).SubItems(0).Text
        containerlist = ListView2.SelectedItems(0).SubItems(1).Text
        sizelist = ListView2.SelectedItems(0).SubItems(2).Text
        agenlist = ListView2.SelectedItems(0).SubItems(3).Text

        If RadioButton1.Checked = True Then
            With ListView1.Items.Add(voyagelist)
                .SubItems.Add(containerlist)
                .SubItems.Add(sizelist)
                .SubItems.Add(agenlist)
                .SubItems.Add("พร้อมใช้งาน")
                .SubItems.Add(MaskedTextBoxAdv1.Value.ToString)
                .SubItems.Add(iddepot)
                .SubItems.Add(idcontainer)
            End With


        ElseIf RadioButton2.Checked = True Then
            With ListView3.Items.Add(voyagelist)
                .SubItems.Add(containerlist)
                .SubItems.Add(sizelist)
                .SubItems.Add(agenlist)
                .SubItems.Add("ทำความสะอาด")
                .SubItems.Add(MaskedTextBoxAdv1.Value.ToString)
                .SubItems.Add(iddepot)
                .SubItems.Add(idcontainer)
            End With


        ElseIf RadioButton3.Checked = True Then

            With ListView4.Items.Add(voyagelist)
                .SubItems.Add(containerlist)
                .SubItems.Add(sizelist)
                .SubItems.Add(agenlist)
                .SubItems.Add("รอซ่อม")
                .SubItems.Add(MaskedTextBoxAdv1.Value.ToString)
                .SubItems.Add(iddepot)
                .SubItems.Add(idcontainer)
            End With

        End If
        Dim Listview3_Index As Integer
        Listview3_Index = ListView2.SelectedIndices(0)
        'ListView3.Items.RemoveAt(Listview3_Index)
        'SelectedEmployee = ListView3.SelectedItems(0).Text
        'MsgBox(SelectedEmployee)
        ListView2.Items.RemoveAt(Listview3_Index)
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click

        Dim commandText2 As String
        If ListView1.Items.Count > 0 Then
            For i = 0 To ListView1.Items.Count - 1
                mysql.Close()
                If mysql.State = ConnectionState.Closed Then
                    mysql.Open()
                End If
                Try

                    commandText2 = "UPDATE ctndepot SET CTNSTATDEPOT = '0' , CTNDAYRE = '" & ListView1.Items(i).SubItems(5).Text & "'  ,CTNDATEIN ='" & Date.Now.Date.ToString("dd-MM-yyyy") & "' WHERE CTNDEPOTID = '" & ListView1.Items(i).SubItems(6).Text & "';"
                    mySqlCommand.CommandText = commandText2

                    mySqlCommand.Connection = mysql

                    mySqlCommand.ExecuteNonQuery()
                    mysql.Close()


                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                mysql.Close()
                If mysql.State = ConnectionState.Closed Then
                    mysql.Open()
                End If
                Try

                    commandText2 = "UPDATE ctnmain SET CTNSTAT = '1'WHERE CTNMAINID = " & ListView1.Items(i).SubItems(7).Text & "; "
                    mySqlCommand.CommandText = commandText2

                    mySqlCommand.Connection = mysql

                    mySqlCommand.ExecuteNonQuery()
                    mysql.Close()


                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
            Next
        End If
        If ListView3.Items.Count > 0 Then
            For i = 0 To ListView3.Items.Count - 1
                mysql.Close()
                If mysql.State = ConnectionState.Closed Then
                    mysql.Open()
                End If
                Try

                    commandText2 = "UPDATE ctndepot SET CTNSTATDEPOT = '2' ,  CTNSTATCHECK ='0', CTNDAYRE = '" & ListView3.Items(i).SubItems(5).Text & "'  ,CTNDATEIN ='" & Date.Now.Date.ToString("dd-MM-yyyy") & "' WHERE CTNDEPOTID = '" & ListView3.Items(i).SubItems(6).Text & "';"
                    mySqlCommand.CommandText = commandText2

                    mySqlCommand.Connection = mysql

                    mySqlCommand.ExecuteNonQuery()
                    mysql.Close()


                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
            Next
        End If
        If ListView4.Items.Count > 0 Then
            For i = 0 To ListView4.Items.Count - 1
                mysql.Close()
                If mysql.State = ConnectionState.Closed Then
                    mysql.Open()
                End If
                Try

                    commandText2 = "UPDATE ctndepot SET CTNSTATDEPOT = '3' ,  CTNSTATCHECK ='0'  ,CTNDAYRE = '" & ListView4.Items(i).SubItems(5).Text & "'  ,CTNDATEIN ='" & Date.Now.Date.ToString("dd-MM-yyyy") & "' WHERE CTNDEPOTID = '" & ListView4.Items(i).SubItems(6).Text & "';"
                    mySqlCommand.CommandText = commandText2

                    mySqlCommand.Connection = mysql

                    mySqlCommand.ExecuteNonQuery()
                    mysql.Close()


                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
            Next
        End If
        ListView1.Items.Clear()
        ListView3.Items.Clear()
        ListView4.Items.Clear()
    End Sub

    Private Sub PanelEx1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PanelEx1.Click

    End Sub

    Private Sub ButtonItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem5.Click

        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try
                Dim t = New Thread(New ThreadStart(AddressOf excelReport))
                t.Start()

            Catch ex As Exception

            End Try
        End If


    End Sub
    Private Sub excelReport()
        Dim count_Row As Integer = 5
        Dim count_Row2 As Integer = 9

        pathExcel = FolderBrowserDialog1.SelectedPath

        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add
        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)
        excelsheets.Rows("3:3").rowheight = 20


        With (excelsheets)
            .Range("A1:Q900").Font.Name = "Angsana New"

            .Range("A2:Q900").Font.Size = 14
            Dim CheckIndex As Integer
            Dim i As Integer
            Dim CheckData As Boolean
            CheckData = False
            CheckIndex = ListView1.Items.Count
            Dim J As Integer
            For J = 7 To 10
                .Range("A4").Borders(J).Weight = 2 ' xlThin
                .Range("B4").Borders(J).Weight = 2 ' xlThin
                .Range("C4").Borders(J).Weight = 2 ' xlThin
                .Range("D4").Borders(J).Weight = 2 ' xlThin
                .Range("E4").Borders(J).Weight = 2 ' xlThin
                .Range("F4").Borders(J).Weight = 2 ' xlThin
                .Range("G4").Borders(J).Weight = 2 ' xlThin
                .Range("H4").Borders(J).Weight = 2 ' xlThin


            Next
            With .Range("A2:F2")
                .Merge()


                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "YKP PORT CONTAINER DAMAGE"
                .Font.Size = 16
                ''.ColumnWidth = 20
            End With
            With .Range("A3:F3")
                .Merge()
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "จำนวนตู้รอตรวจสอบ"
                .Font.Size = 14
                ''.ColumnWidth = 20
            End With
            With .Range("A4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "Container No."
                .ColumnWidth = 15
            End With
            With .Range("B4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "Agent Line"
                .ColumnWidth = 12
            End With
     
            With .Range("C4")


                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "Date Recieve"
                .Font.Size = 14
                .ColumnWidth = 14
            End With
            With .Range("D4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "Size"
                .Font.Size = 14
                ''.ColumnWidth = 12
            End With
            With .Range("E4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "VOYN"
                .Font.Size = 14
                ''.ColumnWidth = 20
            End With
            With .Range("F4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "ใช้งานได้"
                .Font.Size = 14
                ''.ColumnWidth = 20
            End With
            With .Range("G4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "ทำความสะอาด"
                .Font.Size = 14
                ''.ColumnWidth = 20
            End With
            With .Range("H4")

                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.Bold = True
                .Value = "ซ่่อม"
                .Font.Size = 14
                ''.ColumnWidth = 20
            End With
            If mysql.State = ConnectionState.Closed Then
                mysql.Open()
            End If

            mySqlCommand.CommandText = "Select  * from ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where CTNSTAT= '0' order by CTNMAINID ASC;"
            mySqlCommand.Connection = mysql
            mySqlAdaptor.SelectCommand = mySqlCommand

            Try
                mySqlReader = mySqlCommand.ExecuteReader

                While (mySqlReader.Read())

                    With .Range("A" + count_Row.ToString)
                        .Value = mySqlReader("CTNSTRING")
                    End With

                    With .Range("B" + count_Row.ToString)
                        .Value = mySqlReader("CTNAGENT")
                    End With
                    With .Range("C" + count_Row.ToString)
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                        .Value = mySqlReader("VOYDATEEN")
                    End With
                    With .Range("D" + count_Row.ToString)
                        .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                        .Value = mySqlReader("CTNSIZE")
                    End With
                    With .Range("E" + count_Row.ToString)
                        .Value = Format(mySqlReader("VOYAGEID"), "000") + "N"
                    End With
                    count_Row += 1
                End While
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

         
            For i = 5 To count_Row
                .Range("B" & i.ToString() & ":B" & i.ToString()).Borders(7).Weight = 2
                .Range("A" & i.ToString() & ":A" & i.ToString()).Borders(7).Weight = 2
                .Range("C" & i.ToString() & ":C" & i.ToString()).Borders(7).Weight = 2
                .Range("D" & i.ToString() & ":D" & i.ToString()).Borders(7).Weight = 2
                .Range("E" & i.ToString() & ":E" & i.ToString()).Borders(7).Weight = 2
                .Range("F" & i.ToString() & ":F" & i.ToString()).Borders(7).Weight = 2
                .Range("G" & i.ToString() & ":G" & i.ToString()).Borders(7).Weight = 2
                .Range("H" & i.ToString() & ":H" & i.ToString()).Borders(7).Weight = 2
                .Range("I" & i.ToString() & ":I" & i.ToString()).Borders(7).Weight = 2
            Next
            count_Row += 1
            With .Range("A" + count_Row.ToString + ":H" + count_Row.ToString)
                .Borders(8).Weight = 2
            End With

        End With



        Try
            excelbooks.SaveAs(pathExcel.ToString + "\" + "ReportContainerDamage" + Date.Now.Day.ToString + "-" + Date.Now.Month.ToString + "-" + Date.Now.Year.ToString + ".xlsx")
            MsgBox("Report Complete", MsgBoxStyle.Information, "Complete Report")
            excelsheets = Nothing
            excelbooks.Close()


            excelapp.Quit()

            excelbooks = Nothing


            excelapp = Nothing
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


    End Sub
    Private Sub showResult(ByVal Num As Integer)
        If Label10.InvokeRequired Then
            Dim dlg As New DelegateSub(AddressOf showResult)
            Me.Invoke(dlg, Num)

        Else


        End If
    End Sub

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem1.Click
        Dim nextform As frmrpt_voyagedamage = New frmrpt_voyagedamage
        nextform.Show()
    End Sub

    Private Sub ButtonItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim cf As New frmadd_ctndamage
        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()

    End Sub

    Private Sub ButtonItem3_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem3.Click

        Dim cf As New frmadd_ctndamage
        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub

    Private Sub ButtonItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ButtonItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem7.Click

        Dim cf As New frmrpt_depotdetail
        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub
End Class