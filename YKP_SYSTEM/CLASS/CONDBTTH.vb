﻿Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Drawing
Imports System.Windows.Forms
Imports MySql.Data.MySqlClient
Imports System.Data
Public Class CONDBTTH
    Implements IDisposable

    'deterministic แก้  SET GLOBAL log_bin_trust_function_creators = 1;
    'Private Shared ReadOnly strCommon As String = "server=172.30.10.132;port = 3305;user id=" + "root" + ";password=" + "software" + ";database=myfriendsdb;default command timeout=100;"

    'Private Shared ReadOnly strCommon As String = "server=10.88.88.210;port = 3306;persistsecurityinfo=True;user id=" + "tth" + ";password=" + "tpxFXUpt206854" + ";database=ykp_db;Character Set =utf8 ;default command timeout=600;"
    Private Shared ReadOnly strCommon As String = "server=159.138.93.177;port = 3306;persistsecurityinfo=True;user id=" + "TTHadmin" + ";password=" + "1Dx54&ac8" + ";database=sph_db;Character Set =utf8 ;default command timeout=600;"

    ' Private Shared ReadOnly strCommon As String = "server=159.138.93.177;port = 3306;persistsecurityinfo=True;user id=" + "TDSadmin" + ";password=" + "~Jhof4592" + ";database=TDS_DB;Character Set =utf8 ;default command timeout=600;"

    '    Private Shared ReadOnly strCommon As String = "server=127.0.0.1;port = 3306;persistsecurityinfo=True;user id=" + "root" + ";password=" + "tpxFXUpt206854" + ";database=ykp_db;Character Set =utf8 ;default command timeout=600;"
    'Private Shared strCommon As String = "server=192.168.10.222;port = 3306;user id=" + "myfriends" + ";password=" + "mftpxFXUpt206854" + ";database=myfriendsdb_chumpho;Character Set =utf8 ;default command timeout=3600;"



    Public Shared Function NewConnection() As CONDBTTH

        Return New CONDBTTH(strCommon)
    End Function
    Public Sub CheckConnect()

        If mConnection.State = ConnectionState.Closed Then
            mConnection.Open()
        End If
        Dim sql As String
        sql = "SET NAMES UTF8;"
        Try
            Using cmd As New MySqlCommand(sql, Me.mConnection)
                cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            mConnection.Close()
            mConnection.Open()
        End Try



    End Sub

    Public mConnection As MySqlConnection
    Private mTransaction As MySqlTransaction
    Private mIsInTransaction As Boolean

    Public Sub New(ByVal connectionString As String)
        mConnection = New MySqlConnection(connectionString)
        mIsInTransaction = False
        Try
            mConnection.Close()

            mConnection.Open()
        Catch ex As MySqlException


        End Try

    End Sub

    Public Sub BeginTrans()
        'CheckConnect()
        'If (Not mIsInTransaction) Then
        '    mTransaction = mConnection.BeginTransaction()
        '    mIsInTransaction = True
        'End If
    End Sub

    Public Sub RollbackTrans()
        'If (mIsInTransaction) Then
        '    mTransaction.Rollback()
        '    mTransaction.Dispose()
        '    mIsInTransaction = False
        'End If
    End Sub

    Public Sub CommitTrans()
        'If (mIsInTransaction) Then
        '    mTransaction.Commit()
        '    mTransaction.Dispose()
        '    mIsInTransaction = False
        'End If
    End Sub

    Public Function ExecuteNonQuery(ByVal sql As String) As Integer
        CheckConnect()
        Dim result As Integer
        If (Me.mIsInTransaction) Then
            Using cmd As New MySqlCommand(sql, Me.mConnection, Me.mTransaction)
                result = cmd.ExecuteNonQuery()
                cmd.Dispose()

            End Using
        Else
            Using cmd As New MySqlCommand(sql, Me.mConnection)
                result = cmd.ExecuteNonQuery()

                cmd.Dispose()
            End Using

        End If
        Return result

    End Function


    Public Function ExecuteNonQuery(ByVal sql As String, ByVal mParameter() As MySqlParameter) As Integer
        'for BLOB object
        CheckConnect()

        Dim result As Integer
        If (Me.mIsInTransaction) Then

            Dim cmd As New MySqlCommand(sql, Me.mConnection, Me.mTransaction)

            For i = 0 To mParameter.Length() - 2
                cmd.Parameters.Add(mParameter(i))
            Next

            result = cmd.ExecuteNonQuery()
            cmd.Dispose()

        Else

            Dim cmd As New MySqlCommand(sql, Me.mConnection)

            For i = 0 To mParameter.Length() - 2
                cmd.Parameters.Add(mParameter(i))
            Next

            result = cmd.ExecuteNonQuery()
            cmd.Dispose()

        End If
        Return result

    End Function

    Public Function ExecuteReader(ByVal sql As String) As MySqlDataReader
        CheckConnect()
        Dim mysqlReader As MySqlDataReader
        If (Me.mIsInTransaction) Then
            Using cmd As New MySqlCommand(sql, Me.mConnection, Me.mTransaction)
                mysqlReader = cmd.ExecuteReader()
                cmd.Dispose()

            End Using
        Else
            Using cmd As New MySqlCommand(sql, Me.mConnection)
                mysqlReader = cmd.ExecuteReader()
                cmd.Dispose()

            End Using
        End If

        Return mysqlReader
    End Function
    Public Function ExecuteScalar(ByVal sql As String) As Object
        CheckConnect()
        Dim result As Object
        If (Me.mIsInTransaction) Then
            Using cmd As New MySqlCommand(sql, Me.mConnection, Me.mTransaction)
                result = cmd.ExecuteScalar()
                cmd.Dispose()

            End Using
        Else
            Using cmd As New MySqlCommand(sql, Me.mConnection)
                result = cmd.ExecuteScalar()
                cmd.Dispose()

            End Using
        End If
        Return result
    End Function

    Public Function GetTable(ByVal sql As String) As DataTable
        CheckConnect()
        Dim result As New DataTable()
        If (Me.mIsInTransaction) Then
            Using selectCommand As New MySqlCommand(sql, Me.mConnection, Me.mTransaction)
                Using adapter As New MySqlDataAdapter(selectCommand)
                    adapter.Fill(result)
                    adapter.Dispose()
                End Using
                selectCommand.Dispose()
            End Using
        Else
            Using adapter As New MySqlDataAdapter(sql, Me.mConnection)
                adapter.Fill(result)
                adapter.Dispose()

            End Using
        End If
        Return result
    End Function

    Public Function GetTable(ByVal sql As String, ByRef DTTABLE As DataTable)
        CheckConnect()
        '  Dim result As New DataTable()
        If (Me.mIsInTransaction) Then
            Using selectCommand As New MySqlCommand(sql, Me.mConnection, Me.mTransaction)
                Using adapter As New MySqlDataAdapter(selectCommand)
                    adapter.Fill(DTTABLE)
                    adapter.Dispose()
                End Using
                selectCommand.Dispose()
            End Using
        Else
            Using selectCommand As New MySqlCommand(sql, Me.mConnection)
                Using adapter As New MySqlDataAdapter(selectCommand)
                    adapter.Fill(DTTABLE)
                    adapter.Dispose()
                End Using
                selectCommand.Dispose()
            End Using

        End If
        '   Return DTTABLE
    End Function

    Public Function ExecuteScalar_Parameter(ByVal msCmd As MySqlCommand) As Integer
        CheckConnect()
        Dim result As Integer
        If (Me.mIsInTransaction) Then
            msCmd.Connection = Me.mConnection
            msCmd.Transaction = Me.mTransaction
            result = msCmd.ExecuteScalar
        Else
            msCmd.Connection = Me.mConnection
            result = msCmd.ExecuteScalar
        End If
        Return result
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean

    Protected Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If

            If (mIsInTransaction) Then
                mTransaction.Rollback()
                mTransaction.Dispose()
            End If
            ' mConnection.ClearAllPoolsAsync()

            mConnection.Close()
            mConnection.Dispose()

        End If
        Me.disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        'Dispose(True)
        'GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class