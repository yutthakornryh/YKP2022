﻿Public Class frmMainlayout

    Dim strLayout As layoutClass.layoutStruct
    Dim layoutClass As layoutClass
    Dim dtset As New layoutSet


    Public Sub New(ByVal tag As String, ByVal layout As System.IO.Stream)

        ' This call is required by the designer.
        InitializeComponent()
        layoutClass = New layoutClass(dtset)
        strLayout.prgtag = tag
        strLayout.Layout = layout
        layoutClass.strLayout = strLayout
        layoutClass.getTempleteUser()
        GridControl1.DataSource = dtset


    End Sub
    Public Sub New(ByVal tag As String, ByVal layout As System.IO.Stream, ByVal idmasprgtemplate As String, ByVal masprgtempname As String)

        ' This call is required by the designer.
        InitializeComponent()
        layoutClass = New layoutClass(dtset)
        txtprgname.Tag = idmasprgtemplate
        txtprgname.EditValue = masprgtempname
        chkStatus.EditValue = True
        strLayout.prgtag = tag
        strLayout.Layout = layout
        layoutClass.strLayout = strLayout
        layoutClass.getTempleteUser()
        GridControl1.DataSource = dtset
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub frmMainLayout_Load(sender As Object, e As EventArgs) Handles MyBase.Load


    End Sub
    Public Sub clearForm()
        strLayout = New layoutClass.layoutStruct
        txtprgname.Tag = Nothing

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtprgname.EditValue Is Nothing Then
            MsgBox("กรุณาระบุ ชื่อ template")
            Exit Sub
        End If
        strLayout.prgtemp_name = txtprgname.EditValue
        strLayout.status = chkStatus.EditValue

        If txtprgname.Tag Is Nothing Then
            layoutClass.strLayout = strLayout

            layoutClass.insertTemplateUsmkclass()
        Else
            strLayout.idmasprgtemplate = txtprgname.Tag

            layoutClass.strLayout = strLayout
            layoutClass.UpdateTemplateClass()

        End If
        'layoutClass.getTempleteUser()
        Me.Close()

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        If GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "f_default") = True Then
            MsgBox("ไม่สามารถ แก้ Template Default ได้ ")
        Else
            showdata()

        End If

    End Sub

    Public Sub showdata()
        txtprgname.Tag = GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "idmasprgtemplate").ToString
        txtprgname.EditValue = GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "prgtemp_name").ToString
        chkStatus.EditValue = GridView1.GetRowCellValue(GridView1.FocusedRowHandle, "status")
        btnSave.Text = "แก้ไข"
    End Sub

End Class