﻿Public Class ContainerClass
    Dim ykpset As ykpdtset
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Public Sub New(ByRef dataset As ykpdtset)
        ykpset = dataset

    End Sub
    Public Sub loadVoyage()
        Dim sql As String
        sql = "SELECT * FROM voyage"
        ykpset.Tables("voyage").Clear()
        connectDB.GetTable(sql, ykpset.Tables("voyage"))
    End Sub
    Public Sub LoadBookingByvoyage(ByVal voyageStart As String, ByVal voyageEnd As String)
        Dim sql As String
        sql = "Select * ,  BNO As 'Booking No.',BSHIPNAME As 'Shipper',BFORWARDERNAME As 'Forwarder', CONCAT_WS('@',BCONFIRMDAY,BCONFIRMTIME) AS 'CloseDate', CONCAT_WS(' ',CONCAT_WS('X',BCTNNO,BCTNTYPE) ,BNICKAGENT ) AS CTNNO  from booking LEFT JOIN voyage ON booking.BVOYAGE = voyage.VOYAGEID  left join booking_bl on booking_bl.idbooking_bl =  booking.BINV where BVOYAGE  >= '" & voyageStart & "' AND  BVOYAGE <= '" & voyageEnd & "' order by  BCTNTYPE ASC , FIELD( BFORWARDERNAME ,'v.i.m','psk','queen','3gshipping' ,'baolidor','kvm','j.mac','topline') DESC;"

        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
    End Sub
    Public Sub loadGroupAgentByCTN()
        Dim sql As String
        sql = "SELECT CTNAGENT FROM ctnmain GROUP by CTNAGENT"
        ykpset.Tables("Agent").Clear()
        connectDB.GetTable(sql, ykpset.Tables("Agent"))
    End Sub
    Public Sub loadMascmrunning()
        Dim sql As String
        sql = "SELECt * FROM mascmrunning"
        ykpset.Tables("mascmrunning").Clear()
        connectDB.GetTable(sql, ykpset.Tables("mascmrunning"))
    End Sub
    Public Sub LoadCTNByVoyn(ByVal slcvoyage As String)
        Dim sql As String
        sql = "SELECT * FROM ctnmain WHERE CTNSTAT = 1 AND CTNVOYN = '" & slcvoyage & "';"
        ykpset.Tables("ctnmain").Clear()

        connectDB.GetTable(sql, ykpset.Tables("ctnmain"))
    End Sub
    Public Sub loadBookingAll(ByVal idvoyage As String)
        Dim sql As String
        sql = "Select * from booking  left join shipper on booking.BSHIP = shipper.SHIPPERID where BVOYAGE='" & idvoyage & "';"
        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
    End Sub
    Public Sub LoadBookingDetail(ByVal idbooking As String)
        Dim sql As String
        sql = "Select * from booking  left join shipper on booking.BSHIP = shipper.SHIPPERID where bookingid ='" & idbooking & "';"
        ykpset.Tables("bookingdetail").Clear()
        connectDB.GetTable(sql, ykpset.Tables("bookingdetail"))
    End Sub
    Public Sub loadContainerByStatus(ByVal Status As String)
        Dim sql As String
        sql = "SELECT *  FROM ctnmain  join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID LEFT JOIN mascmrunning On mascmrunning.idmascmrunning = ctnmain.idmascmrunning  where CTNSTAT = '" & Status & "' ;"
        ykpset.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset.Tables("ctnmain"))
    End Sub
    Public Sub LoadConsinee()
        Dim sql As String
        sql = "SELECt * FROM consignee"
        ykpset.Tables("consignee").Clear()
        connectDB.GetTable(sql, ykpset.Tables("consignee"))
    End Sub
    Public Sub LoadShipper()
        Dim sql As String

        sql = "Select * from shipper ;"
        ykpset.Tables("shipper").Clear()
        connectDB.GetTable(sql, ykpset.Tables("shipper"))
    End Sub
    Public Sub loadmasterbl_ctn()
        Dim sql As String
        sql = "SELECT * FROM masterbl_ctn"
        ykpset.Tables("masterbl_ctn").Clear()
        connectDB.GetTable(sql, ykpset.Tables("masterbl_ctn"))
    End Sub
    Public Sub Loadbooking_bl_ctn(ByVal idbooking As String)
        Dim sql As String
        sql = "SELECT * FROM booking_bl_ctn WHERE bookid = '" & idbooking & "';"
        ykpset.Tables("booking_bl_ctn").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking_bl_ctn"))
    End Sub
    Public Sub Loadbooking_bl(ByVal idbooking As String)
        Dim sql As String
        sql = "SELECT * FROM booking_bl WHERE booking_id = '" & idbooking & "';"
        ykpset.Tables("booking_bl").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking_bl"))
    End Sub
    Public Sub deletebooking_bl(ByVal idbooking As String)
        Dim sql As String
        sql = "DELETE FROM booking_bl where booking_id = '" & idbooking & "';"
        connectDB.ExecuteNonQuery(sql)
    End Sub
    Public Sub deletebooking_bl_ctn(ByVal idbooking As String)
        Dim sql As String

        sql = "DELETE FROM booking_bl_ctn where bookid = '" & idbooking & "';"
        connectDB.ExecuteNonQuery(sql)
    End Sub
    Public Function LoadBookingBLReport(ByVal idbooking As String) As DataTable
        Dim sql As String
        Dim dt As New DataTable
        sql = "Select * from borrow join (SELECT * FROM booking WHERE bookingid = '" & idbooking & "' ) booking on borrow.BOOKID = booking.BOOKINGID join voyage on voyage.VOYAGEID = booking.BVOYAGE join booking_bl on booking_bl.booking_id = booking.BOOKINGID  LEFT join consignee on  consignee.consigneeid = booking_bl.conid LEFT join shipper on shipper.SHIPPERID = booking.BSHIP  where BOOKINGID ='" & idbooking & "' ;"

        dt = connectDB.GetTable(sql)

        Return dt
    End Function
    Public Function LoadbookingBLReportCountCTN(ByVal idbooking As String) As DataTable
        Dim dt As New DataTable
        Dim sql As String
        sql = "Select * from booking join (SELECT * FROM borrow WHERE BOOKID = '" & idbooking & "') AS borrow on booking.BOOKINGID = borrow.BOOKID join ctnmain on ctnmain.CTNMAINID = borrow.CTNID    where BOOKINGID ='" & idbooking & "' ;"

        dt = connectDB.GetTable(sql)

        Return dt

    End Function
End Class
