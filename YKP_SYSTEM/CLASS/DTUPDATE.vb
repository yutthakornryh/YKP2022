﻿Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Globalization
Imports System.Data.OleDb
Imports System.Data.Odbc
Imports System.Drawing
Imports System.Windows.Forms

Imports System.Data
Module DTMODULE

    <System.Runtime.CompilerServices.Extension>
    Public Function Delete(table As DataTable, filter As String) As DataTable
        table.[Select](filter).Delete()
        Return table
    End Function
    <System.Runtime.CompilerServices.Extension>
    Public Sub Delete(rows As IEnumerable(Of DataRow))
        For Each row As DataRow In rows
            row.Delete()
        Next
    End Sub
    <System.Runtime.CompilerServices.Extension>
    Public Function Update(table As DataTable, filter As String, colname As String, value As String) As DataTable
        table.[Select](filter).Update(colname, value)
        Return table
    End Function
    <System.Runtime.CompilerServices.Extension>
    Public Sub Update(rows As IEnumerable(Of DataRow), col As String, value As String)
        For Each row As DataRow In rows
            row(col) = value
        Next
    End Sub
    <System.Runtime.CompilerServices.Extension>
    Public Function GetTotal(table As DataTable, colname As String, filter As String) As Decimal
        Dim dv = table.DefaultView
        Dim sumX As Decimal = 0
        dv.RowFilter = filter
        Dim dt As New DataTable
        dt = dv.ToTable
        For i As Integer = 0 To dt.Rows.Count - 1
            Try
                sumX += Convert.ToDecimal(dt.Rows(i)(colname))
            Catch ex As Exception
            End Try
        Next i
        Return sumX
    End Function
    <System.Runtime.CompilerServices.Extension>
    Public Function SelectDistinct(table As DataTable, colname As String, filter As String) As DataRow
        Dim row() As DataRow
        row = table.Select(colname & " = '" & filter & "'")
        Return row(0)

    End Function
    'ในดาต้า Table  ใส่เงื่อนไขเพื่อเช็คว่า ซ้ำหรือไม่
    <System.Runtime.CompilerServices.Extension>
    Public Function SelectCount(table As DataTable, colname As String, filter As String) As Integer
        Dim number As Integer
        Dim row() As DataRow
        row = table.Select(colname & " = '" & filter & "'")

        Return row.ToArray.Length

    End Function

    ' Textbox GET ค่า ใน TAG ชื่อtextbox.GetTagToString
    <System.Runtime.CompilerServices.Extension>
    Public Function GetTagToString(textbox As TextBox) As String
        Return Convert.ToString(textbox.Tag).Trim
    End Function


End Module