﻿Imports System.IO

Public Class layoutClass
    Dim condb As CONDBTTH = CONDBTTH.NewConnection

    Dim dtset As layoutSet
    Private usmk As String
    Public Sub New(ByRef dt As layoutSet, ByVal tagFrom As String)
        dtset = dt
        getTemplate(tagFrom)
    End Sub

    Public Sub New()

    End Sub
    Public Sub New(ByRef dt As layoutSet)
        dtset = dt
    End Sub
    Public Sub getTemplate(ByVal prgcode As String)
        Dim sql As String
        getprgid(prgcode)
        dtset.Tables("masprgtemplate").Clear()
        sql = "SELECT prgtemp_name,idmasprgtemplate,layout FROM masprgtemplate JOIN masprg ON masprg.prgid = masprgtemplate.prgid  WHERE program = '" & prgcode & "';"
        condb.GetTable(sql, dtset.Tables("masprgtemplate"))
    End Sub
    Public Property setusmk As String
        Get
            Return usmk
        End Get
        Set(value As String)
            usmk = value
        End Set
    End Property
    Public Sub getTempleteUser()
        dtset.Tables("masprgtemplate").Clear()
        If _strLayout.prgid Is Nothing Then
            getprgid(_strLayout.prgtag)
        End If
        Dim sql As String
        sql = "SELECT prgtemp_name,idmasprgtemplate,layout,status,f_default FROM masprgtemplate WHERE prgid = '" & _strLayout.prgid & "' AND f_default = 1"
        condb.GetTable(sql, dtset.Tables("masprgtemplate"))
        sql = "SELECT prgtemp_name,idmasprgtemplate,layout,status,f_default FROM masprgtemplate WHERE prgid = '" & _strLayout.prgid & "' AND usmk = '" & frmMain.ID.TagUser & "';"
        condb.GetTable(sql, dtset.Tables("masprgtemplate"))



    End Sub
    Public Sub getprgid(ByVal prgcode As String)
        Dim sql As String
        sql = "SELECT prgid FROM masprg WHERE prgcode = '" & prgcode & "';"

        Dim dt As New DataTable
        dt = condb.GetTable(sql)
        If dt.Rows.Count > 0 Then
            _strLayout.prgid = dt.Rows(0)("prgid")
        End If
    End Sub

    Structure layoutStruct
        Dim idmasprgtemplate As String
        Dim prgtag As String
        Dim prgid As String
        Dim prgtemp_name As String
        Dim status As Boolean
        Dim usmk As String
        Dim Layout As System.IO.Stream
    End Structure
    Private _strLayout As New layoutStruct
    Property strLayout As layoutStruct
        Get
            Return _strLayout
        End Get
        Set(value As layoutStruct)
            _strLayout = value
        End Set
    End Property
    Public Sub insertTemplateUsmkclass()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        If _strLayout.prgid Is Nothing Then
            getprgid(_strLayout.prgtag)
        End If
        sql1 = "prgid , "
        sql2 = " '" & _strLayout.prgid & "' , "


        sql1 += " prgtemp_name , "
        sql2 += " '" & _strLayout.prgtemp_name & "' , "

        sql1 += " usmk , "
        sql2 += " '" & frmMain.ID.TagUser & "' , "

        sql1 += " status , "
        sql2 += " '" & If(_strLayout.status = True, "1", "0") & "' , "




        sql1 += " f_default , "
        sql2 += " '0' , "
        sql1 += "Layout  "

        Dim reader As New StreamReader(_strLayout.Layout)
        sql2 += " '" & reader.ReadToEnd & "' "


        sql = "INSERT INTO masprgtemplate ( " & sql1 & ") VALUES ( " & sql2 & " );"
        condb.ExecuteNonQuery(sql)
    End Sub
    Public Sub insertTemplateClass()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        If _strLayout.prgid Is Nothing Then
            getprgid(_strLayout.prgtag)
        End If
        sql1 = "prgid , "
        sql2 = " '" & _strLayout.prgid & "' , "


        sql1 += " prgtemp_name , "
        sql2 += " '" & _strLayout.prgtemp_name & "' , "

        sql1 += "Layout  "

        Dim reader As New StreamReader(_strLayout.Layout)
        sql2 += " '" & reader.ReadToEnd & "' "


        sql = "INSERT INTO masprgtemplate ( " & sql1 & ") VALUES ( " & sql2 & " );"
        condb.ExecuteNonQuery(sql)
    End Sub
    Public Sub UpdateTemplateClass()
        Dim sql As String
        Dim sql1 As String
        sql1 = " prgtemp_name = "
        sql1 += " '" & _strLayout.prgtemp_name & "' , "

        sql1 += " status = "
        sql1 += " '" & If(_strLayout.status = True, "1", "0") & "' , "
        sql1 += "Layout = "
        Dim reader As New StreamReader(_strLayout.Layout)

        sql1 += " '" & reader.ReadToEnd & "' "
        sql = "UPDATE masprgtemplate SET " & sql1 & " WHERE idmasprgtemplate = '" & _strLayout.idmasprgtemplate & "' ;"
        condb.ExecuteNonQuery(sql)

    End Sub
    Public Function loadLayout(ByVal idmasprgtemplate As String) As System.IO.Stream
        Dim sql As String
        sql = "SELEC * FROM  masprgtemplate WHERE idmasprgtemplate = '" & idmasprgtemplate & "';"
        Dim dt As DataTable
        dt = condb.GetTable(sql)
        Return GenerateStreamFromString(dt.Rows(0)("Layout").ToString)


    End Function
    Public Function GenerateStreamFromString(s As String) As Stream
        Dim stream As New MemoryStream()
        Dim writer As New StreamWriter(stream)
        writer.Write(s)
        writer.Flush()
        stream.Position = 0
        Return stream
    End Function
End Class
