﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmview_depotclean
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ListViewGroup1 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left)
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmview_depotclean))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.TextBoxX1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.idctn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBoxX9 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBoxX10 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBoxX11 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Depot_Detail = New DevComponents.DotNetBar.RibbonBarMergeContainer()
        Me.RibbonBar2 = New DevComponents.DotNetBar.RibbonBar()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.CircularProgress1 = New DevComponents.DotNetBar.Controls.CircularProgress()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem6 = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ComboItem1 = New DevComponents.Editors.ComboItem()
        Me.ComboItem2 = New DevComponents.Editors.ComboItem()
        Me.ComboItem3 = New DevComponents.Editors.ComboItem()
        Me.ComboItem4 = New DevComponents.Editors.ComboItem()
        Me.ComboItem5 = New DevComponents.Editors.ComboItem()
        Me.ComboItem6 = New DevComponents.Editors.ComboItem()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.ButtonX3 = New DevComponents.DotNetBar.ButtonX()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBoxItem1 = New DevComponents.DotNetBar.ComboBoxItem()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.DataGridViewX1 = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn()
        Me.Column3 = New DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn()
        Me.KeyCode = New DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn()
        Me.Column = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.Column9 = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.Column10 = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewButtonXColumn1 = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.DataGridViewButtonXColumn2 = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.DataGridViewButtonXColumn3 = New DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Depot_Detail.SuspendLayout()
        Me.RibbonBar1.SuspendLayout()
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ButtonX1)
        Me.GroupBox2.Controls.Add(Me.TextBoxX1)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.ListView2)
        Me.GroupBox2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 36)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(648, 230)
        Me.GroupBox2.TabIndex = 178
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Container"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(444, 22)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 34)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.TabIndex = 65
        '
        'TextBoxX1
        '
        Me.TextBoxX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX1.Border.Class = "TextBoxBorder"
        Me.TextBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX1.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX1.Location = New System.Drawing.Point(60, 22)
        Me.TextBoxX1.Name = "TextBoxX1"
        Me.TextBoxX1.PreventEnterBeep = True
        Me.TextBoxX1.Size = New System.Drawing.Size(377, 31)
        Me.TextBoxX1.TabIndex = 64
        Me.TextBoxX1.WatermarkText = "ค้นหาข้อมูลตู้ Container "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 24)
        Me.Label1.TabIndex = 63
        Me.Label1.Text = "ค้นหา"
        '
        'ListView2
        '
        Me.ListView2.BackColor = System.Drawing.Color.White
        Me.ListView2.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader9, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader21, Me.ColumnHeader10, Me.idctn})
        Me.ListView2.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.ListView2.FullRowSelect = True
        Me.ListView2.GridLines = True
        ListViewGroup1.Header = "ListViewGroup"
        ListViewGroup1.Name = "ListViewGroup1"
        Me.ListView2.Groups.AddRange(New System.Windows.Forms.ListViewGroup() {ListViewGroup1})
        Me.ListView2.HideSelection = False
        Me.ListView2.Location = New System.Drawing.Point(8, 61)
        Me.ListView2.MultiSelect = False
        Me.ListView2.Name = "ListView2"
        Me.ListView2.ShowGroups = False
        Me.ListView2.Size = New System.Drawing.Size(632, 159)
        Me.ListView2.TabIndex = 62
        Me.ListView2.UseCompatibleStateImageBehavior = False
        Me.ListView2.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "VOY"
        Me.ColumnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader1.Width = 52
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ID Container"
        Me.ColumnHeader5.Width = 141
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "SIZE"
        Me.ColumnHeader6.Width = 64
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "AGENT LINE"
        Me.ColumnHeader7.Width = 102
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "สถานะ"
        Me.ColumnHeader9.Width = 82
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "วันที่เริ่ม"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "สิ้นสุด"
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "จำนวนวัน"
        Me.ColumnHeader21.Width = 63
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "iddepot"
        Me.ColumnHeader10.Width = 0
        '
        'idctn
        '
        Me.idctn.Width = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1209, 33)
        Me.Panel1.TabIndex = 179
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Cordia New", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(4, -5)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(251, 45)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Depot Cleaning / Repair"
        '
        'TextBoxX9
        '
        Me.TextBoxX9.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX9.Border.Class = "TextBoxBorder"
        Me.TextBoxX9.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX9.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxX9.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX9.Location = New System.Drawing.Point(204, 495)
        Me.TextBoxX9.Name = "TextBoxX9"
        Me.TextBoxX9.PreventEnterBeep = True
        Me.TextBoxX9.Size = New System.Drawing.Size(58, 28)
        Me.TextBoxX9.TabIndex = 209
        Me.TextBoxX9.WatermarkText = "ค่าแรง"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(162, 497)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(36, 20)
        Me.Label11.TabIndex = 208
        Me.Label11.Text = "ค่าแรง"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(285, 499)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(37, 20)
        Me.Label12.TabIndex = 210
        Me.Label12.Text = "ค่าวัสดุ"
        '
        'TextBoxX10
        '
        Me.TextBoxX10.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX10.Border.Class = "TextBoxBorder"
        Me.TextBoxX10.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX10.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxX10.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX10.Location = New System.Drawing.Point(327, 496)
        Me.TextBoxX10.Name = "TextBoxX10"
        Me.TextBoxX10.PreventEnterBeep = True
        Me.TextBoxX10.Size = New System.Drawing.Size(58, 28)
        Me.TextBoxX10.TabIndex = 211
        Me.TextBoxX10.WatermarkText = "ค่าวัสดุ"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(403, 498)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(26, 20)
        Me.Label13.TabIndex = 212
        Me.Label13.Text = "รวม"
        '
        'TextBoxX11
        '
        Me.TextBoxX11.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX11.Border.Class = "TextBoxBorder"
        Me.TextBoxX11.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX11.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxX11.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX11.Location = New System.Drawing.Point(444, 495)
        Me.TextBoxX11.Name = "TextBoxX11"
        Me.TextBoxX11.PreventEnterBeep = True
        Me.TextBoxX11.Size = New System.Drawing.Size(58, 28)
        Me.TextBoxX11.TabIndex = 213
        Me.TextBoxX11.WatermarkText = "รวม"
        '
        'Depot_Detail
        '
        Me.Depot_Detail.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.Depot_Detail.Controls.Add(Me.RibbonBar2)
        Me.Depot_Detail.Controls.Add(Me.RibbonBar1)
        Me.Depot_Detail.Location = New System.Drawing.Point(8, 529)
        Me.Depot_Detail.Name = "Depot_Detail"
        Me.Depot_Detail.Size = New System.Drawing.Size(957, 100)
        '
        '
        '
        Me.Depot_Detail.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.Depot_Detail.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.Depot_Detail.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Depot_Detail.TabIndex = 216
        Me.Depot_Detail.Visible = False
        '
        'RibbonBar2
        '
        Me.RibbonBar2.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar2.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.ContainerControlProcessDialogKey = True
        Me.RibbonBar2.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar2.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem5})
        Me.RibbonBar2.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar2.Location = New System.Drawing.Point(377, 0)
        Me.RibbonBar2.Name = "RibbonBar2"
        Me.RibbonBar2.Size = New System.Drawing.Size(116, 100)
        Me.RibbonBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar2.TabIndex = 11
        Me.RibbonBar2.Text = "Confirm"
        '
        '
        '
        Me.RibbonBar2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar2.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar2.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'ButtonItem5
        '
        Me.ButtonItem5.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.SubItemsExpandWidth = 14
        Me.ButtonItem5.Symbol = ""
        Me.ButtonItem5.SymbolSize = 45.0!
        Me.ButtonItem5.Text = "ตู้พร้อมใช้งาน"
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Controls.Add(Me.CircularProgress1)
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar1.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Center
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem4, Me.ButtonItem4, Me.ButtonItem1, Me.ButtonItem2, Me.ButtonItem3, Me.ButtonItem6, Me.ControlContainerItem1})
        Me.RibbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(377, 100)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar1.TabIndex = 10
        Me.RibbonBar1.Text = "Report"
        '
        '
        '
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.VerticalItemAlignment = DevComponents.DotNetBar.eVerticalItemsAlignment.Middle
        '
        'CircularProgress1
        '
        Me.CircularProgress1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.CircularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CircularProgress1.Location = New System.Drawing.Point(281, 7)
        Me.CircularProgress1.Name = "CircularProgress1"
        Me.CircularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Spoke
        Me.CircularProgress1.ProgressColor = System.Drawing.Color.MediumSeaGreen
        Me.CircularProgress1.Size = New System.Drawing.Size(72, 74)
        Me.CircularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP
        Me.CircularProgress1.TabIndex = 180
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        '
        'ButtonItem4
        '
        Me.ButtonItem4.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem4.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.SubItemsExpandWidth = 14
        Me.ButtonItem4.Text = "เอกสาร Checker"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.SubItemsExpandWidth = 14
        Me.ButtonItem1.Text = "เอกสาร ช่างซ่อม"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.SubItemsExpandWidth = 14
        Me.ButtonItem2.Text = "เอกสาร ส่งมาเลย์"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem3.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.SubItemsExpandWidth = 14
        Me.ButtonItem3.Text = "เอกสาร สายเรืออื่น"
        '
        'ButtonItem6
        '
        Me.ButtonItem6.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.ButtonItem6.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem6.Name = "ButtonItem6"
        Me.ButtonItem6.SubItemsExpandWidth = 14
        Me.ButtonItem6.Text = "ออกเอกสาร รวมทั้งหมด"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.CircularProgress1
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'ComboItem1
        '
        Me.ComboItem1.Text = "10"
        '
        'ComboItem2
        '
        Me.ComboItem2.Text = "11"
        '
        'ComboItem3
        '
        Me.ComboItem3.Text = "12"
        '
        'ComboItem4
        '
        Me.ComboItem4.Text = "13"
        '
        'ComboItem5
        '
        Me.ComboItem5.Text = "14"
        '
        'ComboItem6
        '
        Me.ComboItem6.Text = "15"
        '
        'ButtonX3
        '
        Me.ButtonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX3.Location = New System.Drawing.Point(12, 482)
        Me.ButtonX3.Name = "ButtonX3"
        Me.ButtonX3.Size = New System.Drawing.Size(127, 41)
        Me.ButtonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX3.Symbol = ""
        Me.ButtonX3.TabIndex = 217
        Me.ButtonX3.Text = "กลับหน้า Depot Main"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(57, 33)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(61, 21)
        Me.ComboBox1.TabIndex = 220
        '
        'ComboBoxItem1
        '
        Me.ComboBoxItem1.DropDownHeight = 106
        Me.ComboBoxItem1.Items.AddRange(New Object() {Me.ComboItem1, Me.ComboItem2, Me.ComboItem3, Me.ComboItem4, Me.ComboItem5, Me.ComboItem6})
        Me.ComboBoxItem1.Name = "ComboBoxItem1"
        Me.ComboBoxItem1.Text = "12"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(1044, 555)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(0, 13)
        Me.Label10.TabIndex = 0
        '
        'DataGridViewX1
        '
        Me.DataGridViewX1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewX1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewX1.ColumnHeadersHeight = 25
        Me.DataGridViewX1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column8, Me.Column2, Me.Column3, Me.KeyCode, Me.Column, Me.Column11, Me.Column5, Me.Column6, Me.Column7, Me.Column9, Me.Column10, Me.Column4})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewX1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewX1.EnableHeadersVisualStyles = False
        Me.DataGridViewX1.GridColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.DataGridViewX1.Location = New System.Drawing.Point(7, 19)
        Me.DataGridViewX1.MultiSelect = False
        Me.DataGridViewX1.Name = "DataGridViewX1"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewX1.RowHeadersWidth = 35
        Me.DataGridViewX1.RowTemplate.Height = 21
        Me.DataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.DataGridViewX1.Size = New System.Drawing.Size(1208, 178)
        Me.DataGridViewX1.TabIndex = 218
        '
        'Column1
        '
        Me.Column1.HeaderText = "ลักษณะการซ่อม"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 300
        '
        'Column8
        '
        Me.Column8.HeaderText = "ลักษณะการซ่อม ENG"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 300
        '
        'Column2
        '
        Me.Column2.DisplayMember = "Text"
        Me.Column2.DropDownHeight = 106
        Me.Column2.DropDownWidth = 121
        Me.Column2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Column2.HeaderText = "โค๊ซซ่อม"
        Me.Column2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Column2.IntegralHeight = False
        Me.Column2.ItemHeight = 15
        Me.Column2.Items.AddRange(New Object() {"WLD", "STR", "JAK", "S&W", "PTH", "SEC", "RPL", "INS", "RFT", "RMV", "FRE"})
        Me.Column2.Name = "Column2"
        Me.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Column2.Width = 70
        '
        'Column3
        '
        Me.Column3.DisplayMember = "Text"
        Me.Column3.DropDownHeight = 106
        Me.Column3.DropDownWidth = 121
        Me.Column3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Column3.HeaderText = "โค๊ซ"
        Me.Column3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Column3.IntegralHeight = False
        Me.Column3.ItemHeight = 15
        Me.Column3.Items.AddRange(New Object() {"DM", "WT", "CN"})
        Me.Column3.Name = "Column3"
        Me.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Column3.Width = 50
        '
        'KeyCode
        '
        Me.KeyCode.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.KeyCode.BackgroundStyle.Class = "DataGridViewBorder"
        Me.KeyCode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.KeyCode.Culture = New System.Globalization.CultureInfo("th-TH")
        Me.KeyCode.ForeColor = System.Drawing.Color.Black
        Me.KeyCode.HeaderText = "ลักษณะ"
        Me.KeyCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.KeyCode.Mask = ""
        Me.KeyCode.Name = "KeyCode"
        Me.KeyCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.KeyCode.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.KeyCode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.KeyCode.Text = ""
        Me.KeyCode.Width = 80
        '
        'Column
        '
        Me.Column.HeaderText = "ชั่วโมงการซ่อม"
        Me.Column.Name = "Column"
        Me.Column.Width = 95
        '
        'Column11
        '
        Me.Column11.DisplayMember = "Text"
        Me.Column11.DropDownHeight = 106
        Me.Column11.DropDownWidth = 121
        Me.Column11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Column11.HeaderText = "Rate"
        Me.Column11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Column11.IntegralHeight = False
        Me.Column11.ItemHeight = 15
        Me.Column11.Items.AddRange(New Object() {"10", "11", "12", "13", "14", "15", "16"})
        Me.Column11.MinimumWidth = 40
        Me.Column11.Name = "Column11"
        Me.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column11.Text = "12"
        Me.Column11.Width = 40
        '
        'Column5
        '
        Me.Column5.HeaderText = "ค่าแรง"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 80
        '
        'Column6
        '
        Me.Column6.HeaderText = "ราคาวัสดุ"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 80
        '
        'Column7
        '
        Me.Column7.HeaderText = ""
        Me.Column7.Image = CType(resources.GetObject("Column7.Image"), System.Drawing.Image)
        Me.Column7.Name = "Column7"
        Me.Column7.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.Column7.Text = Nothing
        Me.Column7.Width = 25
        '
        'Column9
        '
        Me.Column9.HeaderText = ""
        Me.Column9.Image = CType(resources.GetObject("Column9.Image"), System.Drawing.Image)
        Me.Column9.Name = "Column9"
        Me.Column9.Text = Nothing
        Me.Column9.Width = 25
        '
        'Column10
        '
        Me.Column10.HeaderText = ""
        Me.Column10.Image = CType(resources.GetObject("Column10.Image"), System.Drawing.Image)
        Me.Column10.Name = "Column10"
        Me.Column10.Text = Nothing
        Me.Column10.Width = 25
        '
        'Column4
        '
        Me.Column4.HeaderText = "Column4"
        Me.Column4.MinimumWidth = 2
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 2
        '
        'DataGridViewButtonXColumn1
        '
        Me.DataGridViewButtonXColumn1.HeaderText = "บันทึก"
        Me.DataGridViewButtonXColumn1.Image = CType(resources.GetObject("DataGridViewButtonXColumn1.Image"), System.Drawing.Image)
        Me.DataGridViewButtonXColumn1.Name = "DataGridViewButtonXColumn1"
        Me.DataGridViewButtonXColumn1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.DataGridViewButtonXColumn1.Text = Nothing
        Me.DataGridViewButtonXColumn1.Width = 35
        '
        'DataGridViewButtonXColumn2
        '
        Me.DataGridViewButtonXColumn2.HeaderText = ""
        Me.DataGridViewButtonXColumn2.Image = CType(resources.GetObject("DataGridViewButtonXColumn2.Image"), System.Drawing.Image)
        Me.DataGridViewButtonXColumn2.Name = "DataGridViewButtonXColumn2"
        Me.DataGridViewButtonXColumn2.Text = Nothing
        Me.DataGridViewButtonXColumn2.Width = 25
        '
        'DataGridViewButtonXColumn3
        '
        Me.DataGridViewButtonXColumn3.HeaderText = "Column10"
        Me.DataGridViewButtonXColumn3.Image = CType(resources.GetObject("DataGridViewButtonXColumn3.Image"), System.Drawing.Image)
        Me.DataGridViewButtonXColumn3.Name = "DataGridViewButtonXColumn3"
        Me.DataGridViewButtonXColumn3.Text = Nothing
        Me.DataGridViewButtonXColumn3.Width = 25
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DataGridViewX1)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 273)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1238, 203)
        Me.GroupBox1.TabIndex = 221
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'frmview_depotclean
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1329, 620)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TextBoxX11)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.TextBoxX9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.TextBoxX10)
        Me.Controls.Add(Me.ButtonX3)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Depot_Detail)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmview_depotclean"
        Me.Text = "frmview_depotclean"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Depot_Detail.ResumeLayout(False)
        Me.RibbonBar1.ResumeLayout(False)
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents idctn As System.Windows.Forms.ColumnHeader
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TextBoxX1 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBoxX9 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBoxX10 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBoxX11 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Depot_Detail As DevComponents.DotNetBar.RibbonBarMergeContainer
    Friend WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents CircularProgress1 As DevComponents.DotNetBar.Controls.CircularProgress
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents RibbonBar2 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonX3 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ComboItem1 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem2 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem3 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem4 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem5 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem6 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxItem1 As DevComponents.DotNetBar.ComboBoxItem
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewX1 As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents DataGridViewButtonXColumn1 As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents DataGridViewButtonXColumn2 As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents DataGridViewButtonXColumn3 As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn
    Friend WithEvents Column3 As DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn
    Friend WithEvents KeyCode As DevComponents.DotNetBar.Controls.DataGridViewMaskedTextBoxAdvColumn
    Friend WithEvents Column As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DevComponents.DotNetBar.Controls.DataGridViewComboBoxExColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents Column9 As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents Column10 As DevComponents.DotNetBar.Controls.DataGridViewButtonXColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ButtonItem6 As DevComponents.DotNetBar.ButtonItem
End Class
