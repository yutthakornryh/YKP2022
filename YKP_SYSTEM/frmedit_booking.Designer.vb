﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmedit_booking
    Inherits DevComponents.DotNetBar.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_TUG = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_Gross = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_localforwared = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_remark = New System.Windows.Forms.TextBox()
        Me.txt_comodity = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_scn = New System.Windows.Forms.TextBox()
        Me.txt_type = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_notify = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_booking_no = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_finaldes = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txt_tsport = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt_pol = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_portdischarge = New System.Windows.Forms.TextBox()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_descript = New System.Windows.Forms.TextBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.DELIVERY = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.DELIVERYTXT = New System.Windows.Forms.TextBox()
        Me.TRANSHIP2 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TRANSHIP2TXT = New System.Windows.Forms.TextBox()
        Me.TRANSHIP = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TRANSHIPTXT = New System.Windows.Forms.TextBox()
        Me.MOTH = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.MOTHTXT = New System.Windows.Forms.TextBox()
        Me.FED = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.FEDTXT = New System.Windows.Forms.TextBox()
        Me.DESTINATION = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.DISCHARGE = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_TUG
        '
        Me.txt_TUG.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_TUG.Location = New System.Drawing.Point(151, 14)
        Me.txt_TUG.Name = "txt_TUG"
        Me.txt_TUG.Size = New System.Drawing.Size(322, 28)
        Me.txt_TUG.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(59, 22)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(79, 20)
        Me.Label12.TabIndex = 136
        Me.Label12.Text = "TUG / BARGE :"
        '
        'txt_Gross
        '
        Me.txt_Gross.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_Gross.Location = New System.Drawing.Point(151, 48)
        Me.txt_Gross.Name = "txt_Gross"
        Me.txt_Gross.Size = New System.Drawing.Size(322, 28)
        Me.txt_Gross.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(65, 48)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(73, 20)
        Me.Label11.TabIndex = 134
        Me.Label11.Text = "Gross Weight :"
        '
        'txt_localforwared
        '
        Me.txt_localforwared.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_localforwared.Location = New System.Drawing.Point(151, 80)
        Me.txt_localforwared.Name = "txt_localforwared"
        Me.txt_localforwared.Size = New System.Drawing.Size(322, 28)
        Me.txt_localforwared.TabIndex = 12
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox4)
        Me.GroupBox5.Controls.Add(Me.Label3)
        Me.GroupBox5.Controls.Add(Me.TextBox3)
        Me.GroupBox5.Controls.Add(Me.Label2)
        Me.GroupBox5.Controls.Add(Me.Label18)
        Me.GroupBox5.Controls.Add(Me.txt_remark)
        Me.GroupBox5.Controls.Add(Me.txt_comodity)
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Controls.Add(Me.txt_scn)
        Me.GroupBox5.Controls.Add(Me.txt_type)
        Me.GroupBox5.Controls.Add(Me.Label13)
        Me.GroupBox5.Controls.Add(Me.txt_notify)
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Controls.Add(Me.txt_booking_no)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Location = New System.Drawing.Point(20, 68)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(474, 284)
        Me.GroupBox5.TabIndex = 187
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Booking"
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(401, 13)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(58, 28)
        Me.TextBox4.TabIndex = 196
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(343, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 22)
        Me.Label3.TabIndex = 195
        Me.Label3.Text = "No. CTN"
        '
        'TextBox3
        '
        Me.TextBox3.Font = New System.Drawing.Font("Cordia New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(109, 49)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(239, 26)
        Me.TextBox3.TabIndex = 194
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 22)
        Me.Label2.TabIndex = 193
        Me.Label2.Text = "Bill land No,"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(43, 254)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 20)
        Me.Label18.TabIndex = 190
        Me.Label18.Text = "REMARK"
        '
        'txt_remark
        '
        Me.txt_remark.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_remark.Location = New System.Drawing.Point(109, 251)
        Me.txt_remark.Name = "txt_remark"
        Me.txt_remark.Size = New System.Drawing.Size(350, 28)
        Me.txt_remark.TabIndex = 189
        '
        'txt_comodity
        '
        Me.txt_comodity.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_comodity.Location = New System.Drawing.Point(109, 217)
        Me.txt_comodity.Name = "txt_comodity"
        Me.txt_comodity.Size = New System.Drawing.Size(350, 28)
        Me.txt_comodity.TabIndex = 7
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(29, 220)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(67, 20)
        Me.Label15.TabIndex = 185
        Me.Label15.Text = "COMMODITY"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(64, 189)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 20)
        Me.Label14.TabIndex = 184
        Me.Label14.Text = "SCN"
        '
        'txt_scn
        '
        Me.txt_scn.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_scn.Location = New System.Drawing.Point(109, 186)
        Me.txt_scn.Name = "txt_scn"
        Me.txt_scn.Size = New System.Drawing.Size(350, 28)
        Me.txt_scn.TabIndex = 6
        '
        'txt_type
        '
        Me.txt_type.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.txt_type.FormattingEnabled = True
        Me.txt_type.Items.AddRange(New Object() {"20'GP", "40'HC"})
        Me.txt_type.Location = New System.Drawing.Point(401, 47)
        Me.txt_type.Name = "txt_type"
        Me.txt_type.Size = New System.Drawing.Size(58, 28)
        Me.txt_type.TabIndex = 182
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(367, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(28, 20)
        Me.Label13.TabIndex = 146
        Me.Label13.Text = "Type"
        '
        'txt_notify
        '
        Me.txt_notify.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_notify.Location = New System.Drawing.Point(109, 84)
        Me.txt_notify.Multiline = True
        Me.txt_notify.Name = "txt_notify"
        Me.txt_notify.Size = New System.Drawing.Size(350, 98)
        Me.txt_notify.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(31, 87)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 22)
        Me.Label1.TabIndex = 144
        Me.Label1.Text = "Notify Party"
        '
        'txt_booking_no
        '
        Me.txt_booking_no.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_booking_no.Location = New System.Drawing.Point(109, 16)
        Me.txt_booking_no.Name = "txt_booking_no"
        Me.txt_booking_no.Size = New System.Drawing.Size(214, 28)
        Me.txt_booking_no.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(19, 16)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(78, 20)
        Me.Label19.TabIndex = 140
        Me.Label19.Text = "BOOKING NO."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(20, 83)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(118, 20)
        Me.Label7.TabIndex = 132
        Me.Label7.Text = "LOCAL FORWARDING :"
        '
        'txt_finaldes
        '
        Me.txt_finaldes.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_finaldes.Location = New System.Drawing.Point(174, 550)
        Me.txt_finaldes.Name = "txt_finaldes"
        Me.txt_finaldes.ReadOnly = True
        Me.txt_finaldes.Size = New System.Drawing.Size(367, 28)
        Me.txt_finaldes.TabIndex = 11
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txt_tsport)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.txt_pol)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.txt_TUG)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.txt_Gross)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txt_localforwared)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Location = New System.Drawing.Point(662, 315)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(479, 188)
        Me.GroupBox3.TabIndex = 185
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "CONSIGNORS"
        '
        'txt_tsport
        '
        Me.txt_tsport.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_tsport.Location = New System.Drawing.Point(151, 148)
        Me.txt_tsport.Name = "txt_tsport"
        Me.txt_tsport.Size = New System.Drawing.Size(322, 28)
        Me.txt_tsport.TabIndex = 145
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(80, 146)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(58, 20)
        Me.Label21.TabIndex = 144
        Me.Label21.Text = "T/S PORT :"
        '
        'txt_pol
        '
        Me.txt_pol.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pol.Location = New System.Drawing.Point(151, 114)
        Me.txt_pol.Name = "txt_pol"
        Me.txt_pol.Size = New System.Drawing.Size(322, 28)
        Me.txt_pol.TabIndex = 143
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(33, 117)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(105, 20)
        Me.Label20.TabIndex = 142
        Me.Label20.Text = "PORT OF LOADING :"
        '
        'txt_portdischarge
        '
        Me.txt_portdischarge.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_portdischarge.Location = New System.Drawing.Point(172, 484)
        Me.txt_portdischarge.Name = "txt_portdischarge"
        Me.txt_portdischarge.ReadOnly = True
        Me.txt_portdischarge.Size = New System.Drawing.Size(369, 28)
        Me.txt_portdischarge.TabIndex = 10
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(87, 20)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(56, 17)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "CY-CY"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'ComboBox2
        '
        Me.ComboBox2.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(88, 19)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(236, 28)
        Me.ComboBox2.TabIndex = 181
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.TextBox2.Location = New System.Drawing.Point(88, 50)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(236, 59)
        Me.TextBox2.TabIndex = 178
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.Label5.Location = New System.Drawing.Point(38, 50)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 20)
        Me.Label5.TabIndex = 180
        Me.Label5.Text = "Address"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.ComboBox2)
        Me.GroupBox6.Controls.Add(Me.TextBox2)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Location = New System.Drawing.Point(496, 189)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(330, 115)
        Me.GroupBox6.TabIndex = 190
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Shipping"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.Label10.Location = New System.Drawing.Point(6, 23)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(76, 20)
        Me.Label10.TabIndex = 179
        Me.Label10.Text = "Company Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_descript)
        Me.GroupBox1.Location = New System.Drawing.Point(832, 68)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(327, 245)
        Me.GroupBox1.TabIndex = 186
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Description of Goods"
        '
        'txt_descript
        '
        Me.txt_descript.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_descript.Location = New System.Drawing.Point(10, 14)
        Me.txt_descript.Multiline = True
        Me.txt_descript.Name = "txt_descript"
        Me.txt_descript.Size = New System.Drawing.Size(303, 222)
        Me.txt_descript.TabIndex = 127
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(13, 20)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(68, 17)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "FCL/FCL"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.RadioButton2)
        Me.GroupBox4.Controls.Add(Me.RadioButton1)
        Me.GroupBox4.Location = New System.Drawing.Point(502, 315)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(154, 48)
        Me.GroupBox4.TabIndex = 189
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Type Of Movement"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Cordia New", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 1)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(268, 45)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Edit Customer To Booking"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(1048, 508)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(93, 42)
        Me.Button1.TabIndex = 188
        Me.Button1.Text = "Save"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Location = New System.Drawing.Point(13, 11)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1141, 51)
        Me.Panel1.TabIndex = 183
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.ComboBox3)
        Me.GroupBox7.Controls.Add(Me.TextBox1)
        Me.GroupBox7.Controls.Add(Me.Label23)
        Me.GroupBox7.Controls.Add(Me.Label24)
        Me.GroupBox7.Location = New System.Drawing.Point(500, 68)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(326, 115)
        Me.GroupBox7.TabIndex = 191
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Forwarder"
        '
        'ComboBox3
        '
        Me.ComboBox3.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(88, 19)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(232, 28)
        Me.ComboBox3.TabIndex = 181
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.TextBox1.Location = New System.Drawing.Point(88, 50)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(232, 59)
        Me.TextBox1.TabIndex = 178
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.Label23.Location = New System.Drawing.Point(38, 50)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(44, 20)
        Me.Label23.TabIndex = 180
        Me.Label23.Text = "Address"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Cordia New", 11.25!)
        Me.Label24.Location = New System.Drawing.Point(6, 23)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(76, 20)
        Me.Label24.TabIndex = 179
        Me.Label24.Text = "Company Name"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(565, 509)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(127, 41)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.TabIndex = 192
        Me.ButtonX1.Text = "กลับหน้า Voyage"
        '
        'DELIVERY
        '
        '
        '
        '
        Me.DELIVERY.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DELIVERY.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.DELIVERY.Location = New System.Drawing.Point(20, 517)
        Me.DELIVERY.Name = "DELIVERY"
        Me.DELIVERY.Size = New System.Drawing.Size(148, 23)
        Me.DELIVERY.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DELIVERY.TabIndex = 210
        Me.DELIVERY.Text = "PORT OF DELIVERLY :"
        '
        'DELIVERYTXT
        '
        Me.DELIVERYTXT.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DELIVERYTXT.Location = New System.Drawing.Point(174, 517)
        Me.DELIVERYTXT.Name = "DELIVERYTXT"
        Me.DELIVERYTXT.ReadOnly = True
        Me.DELIVERYTXT.Size = New System.Drawing.Size(367, 28)
        Me.DELIVERYTXT.TabIndex = 209
        '
        'TRANSHIP2
        '
        '
        '
        '
        Me.TRANSHIP2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TRANSHIP2.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.TRANSHIP2.Location = New System.Drawing.Point(20, 449)
        Me.TRANSHIP2.Name = "TRANSHIP2"
        Me.TRANSHIP2.Size = New System.Drawing.Size(148, 23)
        Me.TRANSHIP2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.TRANSHIP2.TabIndex = 208
        Me.TRANSHIP2.Text = "PORT TRANSSHIP 2 :"
        '
        'TRANSHIP2TXT
        '
        Me.TRANSHIP2TXT.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TRANSHIP2TXT.Location = New System.Drawing.Point(171, 451)
        Me.TRANSHIP2TXT.Name = "TRANSHIP2TXT"
        Me.TRANSHIP2TXT.ReadOnly = True
        Me.TRANSHIP2TXT.Size = New System.Drawing.Size(370, 28)
        Me.TRANSHIP2TXT.TabIndex = 207
        '
        'TRANSHIP
        '
        '
        '
        '
        Me.TRANSHIP.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TRANSHIP.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.TRANSHIP.Location = New System.Drawing.Point(20, 419)
        Me.TRANSHIP.Name = "TRANSHIP"
        Me.TRANSHIP.Size = New System.Drawing.Size(148, 23)
        Me.TRANSHIP.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.TRANSHIP.TabIndex = 206
        Me.TRANSHIP.Text = "PORT TRANSSHIP :"
        '
        'TRANSHIPTXT
        '
        Me.TRANSHIPTXT.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TRANSHIPTXT.Location = New System.Drawing.Point(171, 419)
        Me.TRANSHIPTXT.Name = "TRANSHIPTXT"
        Me.TRANSHIPTXT.ReadOnly = True
        Me.TRANSHIPTXT.Size = New System.Drawing.Size(370, 28)
        Me.TRANSHIPTXT.TabIndex = 205
        '
        'MOTH
        '
        '
        '
        '
        Me.MOTH.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MOTH.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.MOTH.Location = New System.Drawing.Point(20, 386)
        Me.MOTH.Name = "MOTH"
        Me.MOTH.Size = New System.Drawing.Size(148, 23)
        Me.MOTH.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.MOTH.TabIndex = 204
        Me.MOTH.Text = "MOTHER VESL :"
        '
        'MOTHTXT
        '
        Me.MOTHTXT.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MOTHTXT.Location = New System.Drawing.Point(171, 386)
        Me.MOTHTXT.Name = "MOTHTXT"
        Me.MOTHTXT.ReadOnly = True
        Me.MOTHTXT.Size = New System.Drawing.Size(370, 28)
        Me.MOTHTXT.TabIndex = 203
        '
        'FED
        '
        '
        '
        '
        Me.FED.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.FED.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.FED.Location = New System.Drawing.Point(20, 355)
        Me.FED.Name = "FED"
        Me.FED.Size = New System.Drawing.Size(145, 23)
        Me.FED.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.FED.TabIndex = 202
        Me.FED.Text = "FEEDER VESL :"
        '
        'FEDTXT
        '
        Me.FEDTXT.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FEDTXT.Location = New System.Drawing.Point(171, 355)
        Me.FEDTXT.Name = "FEDTXT"
        Me.FEDTXT.ReadOnly = True
        Me.FEDTXT.Size = New System.Drawing.Size(370, 28)
        Me.FEDTXT.TabIndex = 201
        '
        'DESTINATION
        '
        '
        '
        '
        Me.DESTINATION.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DESTINATION.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.DESTINATION.Location = New System.Drawing.Point(20, 550)
        Me.DESTINATION.Name = "DESTINATION"
        Me.DESTINATION.Size = New System.Drawing.Size(148, 23)
        Me.DESTINATION.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DESTINATION.TabIndex = 200
        Me.DESTINATION.Text = "FINAL DESTINATION :"
        '
        'DISCHARGE
        '
        '
        '
        '
        Me.DISCHARGE.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DISCHARGE.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.DISCHARGE.Location = New System.Drawing.Point(20, 484)
        Me.DISCHARGE.Name = "DISCHARGE"
        Me.DISCHARGE.Size = New System.Drawing.Size(148, 23)
        Me.DISCHARGE.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DISCHARGE.TabIndex = 199
        Me.DISCHARGE.Text = "PORT OF DISCHARGE :"
        '
        'frmedit_booking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1189, 625)
        Me.Controls.Add(Me.DELIVERY)
        Me.Controls.Add(Me.DELIVERYTXT)
        Me.Controls.Add(Me.TRANSHIP2)
        Me.Controls.Add(Me.TRANSHIP2TXT)
        Me.Controls.Add(Me.TRANSHIP)
        Me.Controls.Add(Me.TRANSHIPTXT)
        Me.Controls.Add(Me.MOTH)
        Me.Controls.Add(Me.MOTHTXT)
        Me.Controls.Add(Me.FED)
        Me.Controls.Add(Me.FEDTXT)
        Me.Controls.Add(Me.txt_finaldes)
        Me.Controls.Add(Me.DESTINATION)
        Me.Controls.Add(Me.DISCHARGE)
        Me.Controls.Add(Me.txt_portdischarge)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmedit_booking"
        Me.Text = "frmedit_booking"
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_TUG As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_Gross As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_localforwared As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_comodity As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_scn As System.Windows.Forms.TextBox
    Friend WithEvents txt_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt_notify As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_booking_no As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_finaldes As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_portdischarge As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_descript As System.Windows.Forms.TextBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_tsport As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txt_pol As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_remark As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents DELIVERY As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents DELIVERYTXT As System.Windows.Forms.TextBox
    Friend WithEvents TRANSHIP2 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TRANSHIP2TXT As System.Windows.Forms.TextBox
    Friend WithEvents TRANSHIP As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TRANSHIPTXT As System.Windows.Forms.TextBox
    Friend WithEvents MOTH As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents MOTHTXT As System.Windows.Forms.TextBox
    Friend WithEvents FED As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents FEDTXT As System.Windows.Forms.TextBox
    Friend WithEvents DESTINATION As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents DISCHARGE As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
