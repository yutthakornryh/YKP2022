﻿Imports MySql.Data.MySqlClient
Public Class frmadd_booking
    Dim mySqlCommand As New MySqlCommand
    Dim mySqlAdaptor As New MySqlDataAdapter
    Dim mySqlReader As MySqlDataReader
    Dim respone As Object
    Dim mysql As MySqlConnection = main_form.mysqlconection
    Dim consigneeid As String
    Dim shippingid As String
    Dim forwarderid As String

    Dim fedstr As String
    Dim fedboo As String

    Dim mothstr As String
    Dim mothboo As String

    Dim ptranshipstr As String
    Dim ptranshipboo As String

    Dim ptranshipstr2 As String
    Dim ptranshipboo2 As String

    Dim dischargestr As String
    Dim dischargeboo As String

    Dim pdeliverlystr As String
    Dim pdeliverlyboo As String

    Dim fdestinationstr As String
    Dim fdestinationboo As String


    Dim nicknameforward As String
    Dim nicknameshipname As String


    Private Sub frmadd_booking_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        mysql.Close()
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture


        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If


        mySqlCommand.CommandText = "Select * from shipper;"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                ComboBox2.Items.Add(mySqlReader("SHIPNAME"))

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()


     

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If


        mySqlCommand.CommandText = "Select * from shipper;"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                ComboBox3.Items.Add(mySqlReader("SHIPNAME"))

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()

        txt_booking_no.Focus()
        txt_booking_no.Select()


    End Sub

 
    Sub ChangFocus(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_booking_no.KeyPress, txt_notify.KeyPress, txt_scn.KeyPress, txt_comodity.KeyPress, txt_TUG.KeyPress, txt_Gross.KeyPress, txt_portdischarge.KeyPress, txt_finaldes.KeyPress, txt_localforwared.KeyPress, txt_remark.KeyPress, txt_TUG.KeyPress, txt_type.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub



    Private Sub ComboBox2_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedValueChanged
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from  shipper where SHIPNAME = '" & Trim(ComboBox2.Text) & "';"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                shippingid = mySqlReader("SHIPPERID")
                ComboBox2.Text = mySqlReader("SHIPNAME")
                TextBox2.Text = mySqlReader("SHIPADD")
                nicknameshipname = mySqlReader("SHIPNICKNAME")
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub

    Private Sub ComboBox3_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedValueChanged
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from  shipper where SHIPNAME = '" & Trim(ComboBox3.Text) & "';"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                forwarderid = mySqlReader("SHIPPERID")
                ComboBox3.Text = mySqlReader("SHIPNAME")
                TextBox1.Text = mySqlReader("SHIPADD")
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        Dim cf As New frmview_voyage
        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub

    Private Sub FED_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FED.CheckedChanged
        If FED.Checked = True Then
            FEDTXT.ReadOnly = False
        Else
            FEDTXT.ReadOnly = True
            FEDTXT.Text = ""
        End If
    End Sub

    Private Sub MOTH_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MOTH.CheckedChanged
        If MOTH.Checked = True Then
            MOTHTXT.ReadOnly = False
        Else
            MOTHTXT.ReadOnly = True
            MOTHTXT.Text = ""
        End If
    End Sub

    Private Sub TRANSHIP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TRANSHIP.CheckedChanged
        If TRANSHIP.Checked = True Then
            TRANSHIPTXT.ReadOnly = False
        Else
            TRANSHIPTXT.ReadOnly = True
            TRANSHIPTXT.Text = ""
        End If
    End Sub

    Private Sub TRANSHIP2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TRANSHIP2.CheckedChanged
        If TRANSHIP2.Checked = True Then
            TRANSHIP2TXT.ReadOnly = False
        Else
            TRANSHIP2TXT.ReadOnly = True
            TRANSHIP2TXT.Text = ""
        End If
    End Sub

    Private Sub DISCHARGE_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DISCHARGE.CheckedChanged
        If DISCHARGE.Checked = True Then
            txt_portdischarge.ReadOnly = False
        Else
            txt_portdischarge.ReadOnly = True
            txt_portdischarge.Text = ""
        End If
    End Sub

    Private Sub DELIVERY_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DELIVERY.CheckedChanged
        If DELIVERY.Checked = True Then
            DELIVERYTXT.ReadOnly = False
        Else
            DELIVERYTXT.ReadOnly = True
            DELIVERYTXT.Text = ""
        End If
    End Sub

    Private Sub DESTINATION_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DESTINATION.CheckedChanged
        If DESTINATION.Checked = True Then
            txt_finaldes.ReadOnly = False
        Else
            txt_finaldes.ReadOnly = True
            txt_finaldes.Text = ""
        End If
    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click

        If FED.Checked = True Then
            fedstr = FEDTXT.Text
            fedboo = "1"
        Else
            fedboo = "0"
            fedstr = ""
        End If


        If MOTH.Checked = True Then
            mothstr = MOTHTXT.Text
            mothboo = "1"
        Else
            mothstr = ""
            mothboo = "0"
        End If

        If TRANSHIP.Checked = True Then
            ptranshipstr = TRANSHIPTXT.Text
            ptranshipboo = "1"
        Else
            ptranshipstr = ""
            ptranshipboo = "0"
        End If


        If TRANSHIP2.Checked = True Then
            ptranshipstr2 = TRANSHIP2TXT.Text
            ptranshipboo2 = "1"
        Else
            ptranshipstr2 = ""
            ptranshipboo2 = "0"
        End If

        If DISCHARGE.Checked = True Then
            dischargestr = txt_portdischarge.Text
            dischargeboo = "1"
        Else
            dischargestr = ""
            dischargeboo = "0"
        End If


        If DELIVERY.Checked = True Then
            pdeliverlystr = DELIVERYTXT.Text
            pdeliverlyboo = "1"
        Else
            pdeliverlystr = ""
            pdeliverlyboo = "0"
        End If



        If DESTINATION.Checked = True Then
            fdestinationstr = txt_finaldes.Text
            fdestinationboo = "1"
        Else
            fdestinationstr = ""
            fdestinationboo = "0"
        End If



        mysql.Close()
        mySqlCommand = New MySqlCommand
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            Try
                mySqlCommand.Parameters.Clear()
                mySqlCommand.CommandText = "insert into booking (BNO, BLANDNO, BCTNNO,BCTNTYPE,BCTNWORD, BNOTIFY, BSCN, BCOM, BDESCRIPT, BTYPEMOVE, BCONSI, BSHIP, BTUG, BGROSS,BLOCALFOR,BVOYAGE,BSHIPNAME,BCONSINAME,POL,TSPORT,REMARK,BFORWARDER,BFORWARDERNAME, BPORTDIS,BPORTDISS, BFINALDEST,BFINALDESTS,BFED,BFEDS,BMOTH,BMOTHS,BTRANSHIP,BTRANSHIPS,BTRANSHIP2,BTRANSHIP2S,BDELIVERY,BDELIVERYS,BNSHIPNAME, BNFORWARDERNAME) values (@BNO,@BLANDNO,@BCTNNO,@BCTNTYPE,@BCTNWORD,@BNOTIFY,@BSCN,@BCOM,@BDESCRIPT,@BTYPEMOVE,@BCONSI,@BSHIP,@BTUG,@BGROSS,@BLOCALFOR,@BVOYAGE,@BSHIPNAME,@BCONSINAME,@POL,@TSPORT,@REMARK,@BFORWARDER,@BFORWARDERNAME,@BPORTDIS,@BPORTDISS,@BFINALDEST,@BFINALDESTS,@BFED,@BFEDS,@BMOTH,@BMOTHS,@BTRANSHIP,@BTRANSHIPS,@BTRANSHIP2,@BTRANSHIP2S,@BDELIVERY,@BDELIVERYS,@BNSHIPNAME,@BNFORWARDERNAME )"
                mySqlCommand.Connection = mysql


                'mySqlCommand.Parameters.AddWithValue("@BNO", Trim(txt_booking_no.Text))
                'mySqlCommand.Parameters.AddWithValue("@BLANDNO", Trim(TextBox3.Text))
                'mySqlCommand.Parameters.AddWithValue("@BCTNNO", TextBox4.Text)
                'mySqlCommand.Parameters.AddWithValue("@BCTNTYPE", Trim(txt_type.Text))
                'mySqlCommand.Parameters.AddWithValue("@BCTNWORD", "")
                'mySqlCommand.Parameters.AddWithValue("@BNOTIFY", Trim(txt_notify.Text))
                'mySqlCommand.Parameters.AddWithValue("@BSCN", Trim(txt_scn.Text))
                'mySqlCommand.Parameters.AddWithValue("@BCOM", Trim(txt_comodity.Text))
                'mySqlCommand.Parameters.AddWithValue("@BDESCRIPT", Trim(txt_descript.Text))
                'mySqlCommand.Parameters.AddWithValue("@POL", Trim(txt_pol.Text))
                'mySqlCommand.Parameters.AddWithValue("@TSPORT", Trim(txt_tsport.Text))
                'mySqlCommand.Parameters.AddWithValue("@REMARK", Trim(txt_remark.Text))


                'mySqlCommand.Parameters.AddWithValue("@BVOYAGE", Trim(frmsearch_voyage1.idvoyage))

                'If RadioButton1.Checked = True Then
                '    mySqlCommand.Parameters.AddWithValue("@BTYPEMOVE", "1")
                'ElseIf RadioButton2.Checked = True Then
                '    mySqlCommand.Parameters.AddWithValue("@BTYPEMOVE", "2")
                'Else
                '    mySqlCommand.Parameters.AddWithValue("@BTYPEMOVE", "0")
                'End If


                mySqlCommand.Parameters.AddWithValue("@BCONSI", Trim(consigneeid))
                mySqlCommand.Parameters.AddWithValue("@BSHIP", Trim(shippingid))
                mySqlCommand.Parameters.AddWithValue("@BFORWARDER", Trim(forwarderid))


                mySqlCommand.Parameters.AddWithValue("@BTUG", Trim(txt_TUG.Text))
                mySqlCommand.Parameters.AddWithValue("@BGROSS", Trim(txt_Gross.Text))

                mySqlCommand.Parameters.AddWithValue("@BLOCALFOR", Trim(txt_localforwared.Text))
                mySqlCommand.Parameters.AddWithValue("@BCONSINAME", Trim(""))
                mySqlCommand.Parameters.AddWithValue("@BSHIPNAME", Trim(ComboBox2.Text))
                mySqlCommand.Parameters.AddWithValue("@BFORWARDERNAME", Trim(ComboBox3.Text))

                mySqlCommand.Parameters.AddWithValue("@BPORTDIS", dischargestr)
                mySqlCommand.Parameters.AddWithValue("@BPORTDISS", dischargeboo)

                mySqlCommand.Parameters.AddWithValue("@BFINALDEST", fdestinationstr)
                mySqlCommand.Parameters.AddWithValue("@BFINALDESTS", fdestinationboo)

                mySqlCommand.Parameters.AddWithValue("@BFED", fedstr)
                mySqlCommand.Parameters.AddWithValue("@BFEDS", fedboo)

                mySqlCommand.Parameters.AddWithValue("@BMOTH", mothstr)
                mySqlCommand.Parameters.AddWithValue("@BMOTHS", mothboo)

                mySqlCommand.Parameters.AddWithValue("@BTRANSHIP", ptranshipstr)
                mySqlCommand.Parameters.AddWithValue("@BTRANSHIPS", ptranshipboo)

                mySqlCommand.Parameters.AddWithValue("@BTRANSHIP2", ptranshipstr2)
                mySqlCommand.Parameters.AddWithValue("@BTRANSHIP2S", ptranshipboo2)

                mySqlCommand.Parameters.AddWithValue("@BDELIVERY", pdeliverlystr)
                mySqlCommand.Parameters.AddWithValue("@BDELIVERYS", pdeliverlyboo)
                mySqlCommand.Parameters.AddWithValue("@BNSHIPNAME", nicknameshipname)
                mySqlCommand.Parameters.AddWithValue("@BNFORWARDERNAME", nicknameforward)






                mySqlCommand.ExecuteNonQuery()
                mysql.Close()


                MsgBox("Save Complete")
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        mysql.Close()
        Dim cf As New frmview_voyage
        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from  shipper where SHIPNAME = '" & ComboBox3.Text & "';"
        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand

        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())
                forwarderid = mySqlReader("SHIPPERID")
                ComboBox3.Text = mySqlReader("SHIPNAME")
                TextBox1.Text = mySqlReader("SHIPADD")
                nicknameforward = mySqlReader("SHIPNICKNAME")
            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

    End Sub
End Class