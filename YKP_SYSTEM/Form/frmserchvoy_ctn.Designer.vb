﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmserchvoy_ctn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression1 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule2 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression2 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Me.colSTATCTN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBOOKINGID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRCHECK = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.colBSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRCHECK1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colctnid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCARID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidborrow = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEDATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEHHMM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEHHMMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRBILLNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colSTATCTN
        '
        Me.colSTATCTN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSTATCTN.AppearanceCell.Options.UseFont = True
        Me.colSTATCTN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSTATCTN.AppearanceHeader.Options.UseFont = True
        Me.colSTATCTN.Caption = "Status"
        Me.colSTATCTN.FieldName = "STATCTN"
        Me.colSTATCTN.Name = "colSTATCTN"
        Me.colSTATCTN.OptionsColumn.AllowEdit = False
        Me.colSTATCTN.OptionsColumn.ReadOnly = True
        Me.colSTATCTN.Visible = True
        Me.colSTATCTN.VisibleIndex = 16
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1156, 646)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(67, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(508, 30)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 5
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 2
        Me.colVOYVESNAMEN.Width = 696
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 1
        Me.colVOYVESNAMES.Width = 474
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        Me.colVOYNAME.Width = 417
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl1
        Me.SearchControl1.Location = New System.Drawing.Point(634, 12)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl1
        Me.SearchControl1.Size = New System.Drawing.Size(510, 26)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 6
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "CTNVIEW"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.Location = New System.Drawing.Point(12, 46)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1, Me.RepositoryItemCheckEdit2})
        Me.GridControl1.Size = New System.Drawing.Size(1132, 588)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBNO, Me.colBOOKINGID, Me.colBFORWARDERNAME, Me.colBRCHECK, Me.colBSHIPNAME, Me.colCTNSTRING, Me.colCTNSEALID, Me.colBRCHECK1, Me.colCTNAGENT, Me.colCOMNAME, Me.colctnid, Me.colCARID, Me.colCTNWEIGHT, Me.colidborrow, Me.colTIMEDATE, Me.colTIMEHHMM, Me.colTIMEDATEIN, Me.colTIMEHHMMIN, Me.colSTATCTN, Me.colBRBILLNAME})
        GridFormatRule1.Column = Me.colSTATCTN
        GridFormatRule1.ColumnApplyTo = Me.colSTATCTN
        GridFormatRule1.Name = "Format0"
        FormatConditionRuleExpression1.Appearance.BackColor = System.Drawing.Color.Blue
        FormatConditionRuleExpression1.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression1.Expression = "[STATCTN] = 'FL'"
        GridFormatRule1.Rule = FormatConditionRuleExpression1
        GridFormatRule2.Column = Me.colSTATCTN
        GridFormatRule2.ColumnApplyTo = Me.colSTATCTN
        GridFormatRule2.Name = "Format1"
        FormatConditionRuleExpression2.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        FormatConditionRuleExpression2.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression2.Expression = "[STATCTN] = 'MS'"
        GridFormatRule2.Rule = FormatConditionRuleExpression2
        Me.GridView1.FormatRules.Add(GridFormatRule1)
        Me.GridView1.FormatRules.Add(GridFormatRule2)
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colBNO
        '
        Me.colBNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNO.AppearanceCell.Options.UseFont = True
        Me.colBNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNO.AppearanceHeader.Options.UseFont = True
        Me.colBNO.Caption = "Booking"
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Visible = True
        Me.colBNO.VisibleIndex = 0
        '
        'colBOOKINGID
        '
        Me.colBOOKINGID.FieldName = "BOOKINGID"
        Me.colBOOKINGID.Name = "colBOOKINGID"
        '
        'colBFORWARDERNAME
        '
        Me.colBFORWARDERNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDERNAME.AppearanceCell.Options.UseFont = True
        Me.colBFORWARDERNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDERNAME.AppearanceHeader.Options.UseFont = True
        Me.colBFORWARDERNAME.Caption = "Forwarder"
        Me.colBFORWARDERNAME.FieldName = "BFORWARDERNAME"
        Me.colBFORWARDERNAME.Name = "colBFORWARDERNAME"
        Me.colBFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBFORWARDERNAME.Visible = True
        Me.colBFORWARDERNAME.VisibleIndex = 1
        '
        'colBRCHECK
        '
        Me.colBRCHECK.ColumnEdit = Me.RepositoryItemCheckEdit2
        Me.colBRCHECK.FieldName = "BRCHECK"
        Me.colBRCHECK.Name = "colBRCHECK"
        Me.colBRCHECK.Visible = True
        Me.colBRCHECK.VisibleIndex = 2
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        '
        'colBSHIPNAME
        '
        Me.colBSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colBSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colBSHIPNAME.Caption = "Shipper"
        Me.colBSHIPNAME.FieldName = "BSHIPNAME"
        Me.colBSHIPNAME.Name = "colBSHIPNAME"
        Me.colBSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colBSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colBSHIPNAME.Visible = True
        Me.colBSHIPNAME.VisibleIndex = 3
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.Caption = "Container ID"
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.OptionsColumn.AllowEdit = False
        Me.colCTNSTRING.OptionsColumn.ReadOnly = True
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 4
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSEALID.AppearanceCell.Options.UseFont = True
        Me.colCTNSEALID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSEALID.AppearanceHeader.Options.UseFont = True
        Me.colCTNSEALID.Caption = "SEAL ID"
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.OptionsColumn.AllowEdit = False
        Me.colCTNSEALID.OptionsColumn.ReadOnly = True
        Me.colCTNSEALID.Visible = True
        Me.colCTNSEALID.VisibleIndex = 5
        '
        'colBRCHECK1
        '
        Me.colBRCHECK1.ColumnEdit = Me.RepositoryItemCheckEdit1
        Me.colBRCHECK1.FieldName = "BRCHECK1"
        Me.colBRCHECK1.Name = "colBRCHECK1"
        Me.colBRCHECK1.Visible = True
        Me.colBRCHECK1.VisibleIndex = 6
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.Caption = "AGENT"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.OptionsColumn.AllowEdit = False
        Me.colCTNAGENT.OptionsColumn.ReadOnly = True
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 7
        '
        'colCOMNAME
        '
        Me.colCOMNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCOMNAME.AppearanceCell.Options.UseFont = True
        Me.colCOMNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCOMNAME.AppearanceHeader.Options.UseFont = True
        Me.colCOMNAME.Caption = "Transport"
        Me.colCOMNAME.FieldName = "COMNAME"
        Me.colCOMNAME.Name = "colCOMNAME"
        Me.colCOMNAME.OptionsColumn.AllowEdit = False
        Me.colCOMNAME.OptionsColumn.ReadOnly = True
        Me.colCOMNAME.Visible = True
        Me.colCOMNAME.VisibleIndex = 8
        '
        'colctnid
        '
        Me.colctnid.FieldName = "ctnid"
        Me.colctnid.Name = "colctnid"
        Me.colctnid.Visible = True
        Me.colctnid.VisibleIndex = 9
        '
        'colCARID
        '
        Me.colCARID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCARID.AppearanceCell.Options.UseFont = True
        Me.colCARID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCARID.AppearanceHeader.Options.UseFont = True
        Me.colCARID.Caption = "Car ID"
        Me.colCARID.FieldName = "CARID"
        Me.colCARID.Name = "colCARID"
        Me.colCARID.OptionsColumn.AllowEdit = False
        Me.colCARID.OptionsColumn.ReadOnly = True
        Me.colCARID.Visible = True
        Me.colCARID.VisibleIndex = 10
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNWEIGHT.Caption = "Weight"
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNWEIGHT.OptionsColumn.ReadOnly = True
        Me.colCTNWEIGHT.Visible = True
        Me.colCTNWEIGHT.VisibleIndex = 11
        '
        'colidborrow
        '
        Me.colidborrow.FieldName = "idborrow"
        Me.colidborrow.Name = "colidborrow"
        '
        'colTIMEDATE
        '
        Me.colTIMEDATE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEDATE.AppearanceCell.Options.UseFont = True
        Me.colTIMEDATE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEDATE.AppearanceHeader.Options.UseFont = True
        Me.colTIMEDATE.FieldName = "TIMEDATE"
        Me.colTIMEDATE.Name = "colTIMEDATE"
        Me.colTIMEDATE.OptionsColumn.AllowEdit = False
        Me.colTIMEDATE.OptionsColumn.ReadOnly = True
        Me.colTIMEDATE.Visible = True
        Me.colTIMEDATE.VisibleIndex = 12
        '
        'colTIMEHHMM
        '
        Me.colTIMEHHMM.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEHHMM.AppearanceCell.Options.UseFont = True
        Me.colTIMEHHMM.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEHHMM.AppearanceHeader.Options.UseFont = True
        Me.colTIMEHHMM.FieldName = "TIMEHHMM"
        Me.colTIMEHHMM.Name = "colTIMEHHMM"
        Me.colTIMEHHMM.OptionsColumn.AllowEdit = False
        Me.colTIMEHHMM.OptionsColumn.ReadOnly = True
        Me.colTIMEHHMM.Visible = True
        Me.colTIMEHHMM.VisibleIndex = 13
        '
        'colTIMEDATEIN
        '
        Me.colTIMEDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEDATEIN.AppearanceCell.Options.UseFont = True
        Me.colTIMEDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colTIMEDATEIN.FieldName = "TIMEDATEIN"
        Me.colTIMEDATEIN.Name = "colTIMEDATEIN"
        Me.colTIMEDATEIN.OptionsColumn.AllowEdit = False
        Me.colTIMEDATEIN.OptionsColumn.ReadOnly = True
        Me.colTIMEDATEIN.Visible = True
        Me.colTIMEDATEIN.VisibleIndex = 14
        '
        'colTIMEHHMMIN
        '
        Me.colTIMEHHMMIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEHHMMIN.AppearanceCell.Options.UseFont = True
        Me.colTIMEHHMMIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTIMEHHMMIN.AppearanceHeader.Options.UseFont = True
        Me.colTIMEHHMMIN.FieldName = "TIMEHHMMIN"
        Me.colTIMEHHMMIN.Name = "colTIMEHHMMIN"
        Me.colTIMEHHMMIN.OptionsColumn.AllowEdit = False
        Me.colTIMEHHMMIN.OptionsColumn.ReadOnly = True
        Me.colTIMEHHMMIN.Visible = True
        Me.colTIMEHHMMIN.VisibleIndex = 15
        '
        'colBRBILLNAME
        '
        Me.colBRBILLNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBRBILLNAME.AppearanceCell.Options.UseFont = True
        Me.colBRBILLNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBRBILLNAME.AppearanceHeader.Options.UseFont = True
        Me.colBRBILLNAME.Caption = "Invoice"
        Me.colBRBILLNAME.FieldName = "BRBILLNAME"
        Me.colBRBILLNAME.Name = "colBRBILLNAME"
        Me.colBRBILLNAME.OptionsColumn.AllowEdit = False
        Me.colBRBILLNAME.OptionsColumn.ReadOnly = True
        Me.colBRBILLNAME.Visible = True
        Me.colBRBILLNAME.VisibleIndex = 17
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1156, 646)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 34)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1136, 592)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.SearchControl1
        Me.LayoutControlItem3.CustomizationFormText = "ค้นหา"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(567, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(569, 34)
        Me.LayoutControlItem3.Text = "ค้นหา"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(52, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.slcVoyage
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(567, 34)
        Me.LayoutControlItem2.Text = "Voyage"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(52, 19)
        '
        'frmserchvoy_ctn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1156, 646)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmserchvoy_ctn"
        Me.Text = "Serach Container By Voyage"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCARID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEDATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEHHMM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEHHMMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTATCTN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRBILLNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBOOKINGID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRCHECK As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRCHECK1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colctnid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidborrow As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
End Class
