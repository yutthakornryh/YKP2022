﻿Imports DevExpress.XtraGrid.Views.Grid
Imports MySql.Data.MySqlClient

Public Class frmAddBooking



    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Public Shared idvoyage As String
    Dim idbooking As String = Nothing

    Dim ykpset As New ykpdtset
    Dim objCLASS As New ContainerClass(ykpset)
    Public Sub New(ByVal voyageid As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = voyageid

        ' Add any initialization after the InitializeComponent() call.
        loadBooking()
        LoadConsinee()
        LoadShipper()
        loadmasterbl_ctn()
    End Sub
    Private Sub frmAddBooking_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Public Sub loadBooking()



        objCLASS.loadBookingAll(idvoyage)
        GridControl2.DataSource = ykpset.Tables("booking")
    End Sub

    Public Sub LoadConsinee()
        objCLASS.LoadConsinee()
        txtconsignee.Properties.DataSource = ykpset.Tables("consignee")
    End Sub
    Public Sub LoadShipper()

        objCLASS.LoadShipper()
        txtForwarder.Properties.DataSource = ykpset.Tables("shipper")
        txtShipping.Properties.DataSource = ykpset.Tables("shipper")
    End Sub
    Public Sub loadmasterbl_ctn()
        objCLASS.loadmasterbl_ctn()
        slcmasterbl_ctn.DataSource = ykpset.Tables("masterbl_ctn")
    End Sub
    Private Sub txtForwarder_EditValueChanged(sender As Object, e As EventArgs) Handles txtForwarder.EditValueChanged
        If txtForwarder.EditValue Is Nothing Then
            txtForwarderAddress.EditValue = Nothing
            Exit Sub
        End If
        Dim sql As String
        Dim dt As New DataTable
        sql = "SELECt * FROM shipper WHERE shipperid = '" & txtForwarder.EditValue & "';"
        connectDB.GetTable(sql, dt)
        If dt.Rows.Count > 0 Then
            txtForwarderAddress.EditValue = dt.Rows(0)("SHIPADD").ToString
            txtForwarder.Tag = dt.Rows(0)("SHIPNICKNAME").ToString
        End If
    End Sub

    Private Sub GridView2_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView2.RowCellClick




        ClearData()
        idbooking = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString

        LoadBooking(GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString)




    End Sub


    Public Sub ClearData()
        idbooking = Nothing
        txt_booking_no.EditValue = ""
        TextBox3.EditValue = ""
        TextBox3.EditValue = ""
        txt_notify.EditValue = "SAME AS CONSIGNEE"
        txt_scn.EditValue = ""
        txt_comodity.EditValue = ""
        BDESCRIPT.EditValue = ""
        txtconsignee.EditValue = ""
        txtShipping.EditValue = ""
        txtForwarder.EditValue = ""
        txt_TUG.EditValue = ""
        txt_Gross.EditValue = ""
        txt_localforwared.EditValue = ""
        BNICKAGENT.EditValue = ""
        BDESCRIPT.EditValue = ""
        BINVOICE.EditValue = ""
        BINCOTERM.EditValue = ""
        idbooking = Nothing
        TextBox4.EditValue = ""
        txt_remark.EditValue = ""
        BCHECKLOAD.EditValue = 1
        billboking.EditValue = ""
        txtNumber.EditValue = ""
        txtNumberWord.EditValue = ""
        txtPlace.EditValue = ""
        txt_pol.EditValue = ""
        BTYPEMOVE.EditValue = 0
        RadioGroup3.EditValue = 0
        txtShippingAddress.EditValue = ""
        txtconsigneeAddress.EditValue = ""
        txtForwarderAddress.EditValue = ""
        ''ยังไม่รู้บันทึกตรงไหน
        BPENANG.EditValue = Nothing
        ykpset.Tables("booking_bl_ctn").Clear()

        GridControl1.DataSource = ykpset.Tables("booking_bl_ctn")

    End Sub
    Public Sub addbooking_bl(ByVal idbooking As String)

        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        objCLASS.deletebooking_bl(idbooking)


        sql1 += "     `billbooking`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(billboking.EditValue Is Nothing, "", billboking.EditValue)) & "',"

        sql1 += "       `number`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtNumber.EditValue Is Nothing, "", txtNumber.EditValue)) & "',"

        sql1 += "     `place`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtPlace.Text Is Nothing, "", txtPlace.Text)) & "',"

        sql1 += "     `conid`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtconsignee.EditValue Is Nothing, "", txtconsignee.EditValue)) & "',"

        sql1 += "       `conname`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtconsignee.Text Is Nothing, "", txtconsignee.Text)) & "',"

        sql1 += "    `conaddress`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtconsigneeAddress.EditValue Is Nothing, "", txtconsigneeAddress.EditValue)) & "',"



        sql1 += "     `booking_id`,"
        sql2 += "'" & idbooking & "',"

        sql1 += "     `number_word`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtNumberWord.EditValue Is Nothing, "", txtNumberWord.EditValue)) & "',"

        sql1 += "    `unitctn` "
        sql2 += "'" & MySqlHelper.EscapeString(If(txtUnitCtn.Text Is Nothing, "", txtUnitCtn.Text)) & "' "


        sql = "INSERT INTO booking_bl ( " & sql1 & ") VALUES ( " & sql2 & " );  SELECT LAST_INSERT_ID() "
        Dim lasid As String
        lasid = connectDB.ExecuteScalar(sql)

        sql = "UPDATE booking SET BINV='" & lasid & "'   WHERE BOOKINGID = '" & idbooking & "'; "
        connectDB.ExecuteNonQuery(sql)




    End Sub
    Public Sub addbooking_bl_ctn(ByVal idbooking As String
                                 )
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        objCLASS.deletebooking_bl_ctn(idbooking)

        For i As Integer = 0 To GridView1.RowCount - 1
            sql1 = "insert into booking_bl_ctn (idnumber, bookid, name_1, name_2) VALUES "
            sql1 += "( '" & i & "' ,'" & idbooking & "' , '" & MySqlHelper.EscapeString(GridView1.GetDataRow(i)("name_1").ToString) & "' , '" & MySqlHelper.EscapeString(GridView1.GetDataRow(i)("name_2").ToString) & "' );"
            connectDB.ExecuteNonQuery(sql1)


        Next


    End Sub

    Private Sub DataRow()
        Throw New NotImplementedException()
    End Sub

    Public Sub addBooking()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql1 += "     `BNO`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_booking_no.EditValue Is Nothing, "", txt_booking_no.EditValue)) & "',"

        sql1 += "       `BLANDNO`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(TextBox3.EditValue Is Nothing, "", TextBox3.EditValue)) & "',"

        sql1 += "     `BCTNTYPE`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_type.Text Is Nothing, "", txt_type.Text)) & "',"

        sql1 += "     `BCTNWORD`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(TextBox3.EditValue Is Nothing, "", TextBox3.EditValue)) & "',"

        sql1 += "       `BNOTIFY`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_notify.EditValue Is Nothing, "", txt_notify.EditValue)) & "',"

        sql1 += "    `BSCN`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_scn.EditValue Is Nothing, "", txt_scn.EditValue)) & "',"

        sql1 += "     `BCOM`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_comodity.EditValue Is Nothing, "", txt_comodity.EditValue)) & "',"

        sql1 += "     `BDESCRIPT`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BDESCRIPT.EditValue Is Nothing, "", BDESCRIPT.EditValue)) & "',"

        sql1 += "    `BTYPEMOVE`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BTYPEMOVE.EditValue Is Nothing, "0", BTYPEMOVE.EditValue)) & "',"

        sql1 += "     `BCONSI`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtconsignee.EditValue Is Nothing, "", txtconsignee.EditValue)) & "',"

        sql1 += "     `BSHIP`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtShipping.EditValue Is Nothing, "", txtShipping.EditValue)) & "',"

        sql1 += "      `BFORWARDER`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtForwarder.EditValue Is Nothing, "", txtShipping.EditValue)) & "',"

        sql1 += "     `BSHIPNAME`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtShipping.Text Is Nothing, "", txtShipping.Text)) & "',"

        sql1 += "     `BCONSINAME`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtconsignee.Text Is Nothing, "", txtconsignee.Text)) & "',"

        sql1 += "    `BFORWARDERNAME`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtForwarder.Text Is Nothing, "", txtForwarder.Text)) & "',"


        sql1 += "    `BTUG`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_TUG.EditValue Is Nothing, "", txt_TUG.EditValue)) & "',"

        sql1 += "   `BGROSS`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_Gross.EditValue Is Nothing, "", txt_Gross.EditValue)) & "',"

        sql1 += "    `BLOCALFOR`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_localforwared.EditValue Is Nothing, "", txt_localforwared.EditValue)) & "',"

        sql1 += "    `BCTNNO`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(TextBox4.EditValue Is Nothing, "", TextBox4.EditValue)) & "',"


        sql1 += "      `BVOYAGE`,"
        sql2 += "'" & idvoyage & "',"



        sql1 += "     `POL`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_pol.EditValue Is Nothing, "", txt_pol.EditValue)) & "',"

        sql1 += "     `TSPORT`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_tsport.EditValue Is Nothing, "", txt_tsport.EditValue)) & "',"

        sql1 += "     `REMARK`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_remark.EditValue Is Nothing, "", txt_remark.EditValue)) & "',"


        sql1 += "    `BNSHIPNAME`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtShipping.Text Is Nothing, "", txtShipping.Text)) & "',"

        sql1 += "     `BNFORWARDERNAME`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(txtForwarder.Text Is Nothing, "", txtForwarder.Text)) & "',"

        sql1 += "     `BNICKAGENT`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BNICKAGENT.EditValue Is Nothing, "", BNICKAGENT.EditValue)) & "',"

        sql1 += "     `BMOTH`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BMOTH.EditValue Is Nothing, "", BMOTH.EditValue)) & "',"




        sql1 += "     `BDIS`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BDESCRIPT.EditValue Is Nothing, "", BDESCRIPT.EditValue)) & "',"

        sql1 += "     `BCHECKLOAD`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BCHECKLOAD.EditValue Is Nothing, "", BCHECKLOAD.EditValue)) & "',"

        sql1 += "    `BINVOICE`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BINVOICE.EditValue Is Nothing, "", BINVOICE.EditValue)) & "',"

        sql1 += "   `BINCOTERM`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BINCOTERM.EditValue Is Nothing, "", BINCOTERM.EditValue)) & "',"

        sql1 += "     `BREMARK`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BREMARK.EditValue Is Nothing, "", BREMARK.EditValue)) & "',"
        sql1 += "     `BPENANG`,"
        sql2 += "'" & MySqlHelper.EscapeString(If(BPENANG.EditValue Is Nothing, "", BPENANG.EditValue)) & "',"
        sql1 += "     `BNETGROSS`"
        sql2 += "'" & MySqlHelper.EscapeString(If(txt_Gross.EditValue Is Nothing, "", txt_Gross.EditValue)) & "' "



        sql = "INSERT INTO booking ( " & sql1 & ") VALUES ( " & sql2 & " );  SELECT LAST_INSERT_ID() "
        Dim lastid As String
        lastid = connectDB.ExecuteScalar(sql)


        addbooking_bl_ctn(lastid)
        addbooking_bl(lastid)
    End Sub

    Public Sub UpdateBooking()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql1 += "     `BNO` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_booking_no.EditValue Is Nothing, "", txt_booking_no.EditValue)) & "',"

        sql1 += "       `BLANDNO`="
        sql1 += "'" & MySqlHelper.EscapeString(If(TextBox3.EditValue Is Nothing, "", TextBox3.EditValue)) & "',"

        sql1 += "     `BCTNTYPE` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_type.Text Is Nothing, "", txt_type.Text)) & "',"

        sql1 += "     `BCTNWORD` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(TextBox3.EditValue Is Nothing, "", TextBox3.EditValue)) & "',"

        sql1 += "       `BNOTIFY` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_notify.EditValue Is Nothing, "", txt_notify.EditValue)) & "',"

        sql1 += "    `BSCN` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_scn.EditValue Is Nothing, "", txt_scn.EditValue)) & "',"

        sql1 += "     `BCOM` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_comodity.EditValue Is Nothing, "", txt_comodity.EditValue)) & "',"

        sql1 += "     `BDESCRIPT` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BDESCRIPT.EditValue Is Nothing, "", BDESCRIPT.EditValue)) & "',"

        sql1 += "    `BTYPEMOVE` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BTYPEMOVE.EditValue Is Nothing, "0", BTYPEMOVE.EditValue)) & "',"

        sql1 += "     `BCONSI` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtconsignee.EditValue Is Nothing, "", txtconsignee.EditValue)) & "',"

        sql1 += "     `BSHIP` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtShipping.EditValue Is Nothing, "", txtShipping.EditValue)) & "',"

        sql1 += "      `BFORWARDER` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtForwarder.EditValue Is Nothing, "", txtForwarder.EditValue)) & "',"

        sql1 += "     `BSHIPNAME` ="
        sql1 += "'" & MySqlHelper.EscapeString(If(txtShipping.Text Is Nothing, "", txtShipping.Text)) & "',"

        sql1 += "     `BCONSINAME`  = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtconsignee.EditValue Is Nothing, "", txtconsignee.EditValue)) & "',"

        sql1 += "    `BFORWARDERNAME` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtForwarder.Text Is Nothing, "", txtForwarder.Text)) & "',"


        sql1 += "    `BTUG` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_TUG.EditValue Is Nothing, "", txt_TUG.EditValue)) & "',"

        sql1 += "   `BGROSS` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_Gross.EditValue Is Nothing, "", txt_Gross.EditValue)) & "',"

        sql1 += "    `BLOCALFOR`  = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_localforwared.EditValue Is Nothing, "", txt_localforwared.EditValue)) & "',"

        sql1 += "    `BCTNNO` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(TextBox4.EditValue Is Nothing, "", TextBox4.EditValue)) & "',"


        sql1 += "      `BVOYAGE` = "
        sql1 += "'" & idvoyage & "',"



        sql1 += "     `POL` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_pol.EditValue Is Nothing, "", txt_pol.EditValue)) & "',"

        sql1 += "     `TSPORT` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_tsport.EditValue Is Nothing, "", txt_tsport.EditValue)) & "',"

        sql1 += "     `REMARK` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txt_remark.EditValue Is Nothing, "", txt_remark.EditValue)) & "',"


        sql1 += "    `BNSHIPNAME` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtShipping.EditValue Is Nothing, "", txtShipping.EditValue)) & "',"

        sql1 += "     `BNFORWARDERNAME` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(txtForwarder.EditValue Is Nothing, "", txtForwarder.EditValue)) & "',"

        sql1 += "     `BNICKAGENT`  = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BNICKAGENT.EditValue Is Nothing, "", BNICKAGENT.EditValue)) & "',"


        sql1 += "     `BMOTH`  = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BMOTH.EditValue Is Nothing, "", BMOTH.EditValue)) & "',"



        sql1 += "     `BDIS`  = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BDESCRIPT.EditValue Is Nothing, "", BDESCRIPT.EditValue)) & "',"

        sql1 += "     `BCHECKLOAD` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BCHECKLOAD.EditValue Is Nothing, "", BCHECKLOAD.EditValue)) & "',"

        sql1 += "    `BINVOICE`  = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BINVOICE.EditValue Is Nothing, "", BINVOICE.EditValue)) & "',"



        sql1 += "   `BINCOTERM` ="
        sql1 += "'" & MySqlHelper.EscapeString(If(BINCOTERM.EditValue Is Nothing, "", BINCOTERM.EditValue)) & "',"

        sql1 += "     `BREMARK` = "
        sql1 += "'" & MySqlHelper.EscapeString(If(BREMARK.EditValue Is Nothing, "", BREMARK.EditValue)) & "',"

        sql1 += "     `BPENANG` ="
        sql1 += "'" & MySqlHelper.EscapeString(If(BPENANG.EditValue Is Nothing, "", BPENANG.EditValue)) & "',"

        sql1 += "     `BNETGROSS` = "
        sql1 += "''  "
        Try
            sql = "UPDATE booking SET " & sql1 & " WHERE BOOKINGID = '" & idbooking & "'"
            connectDB.ExecuteNonQuery(sql)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        addbooking_bl_ctn(idbooking)
        addbooking_bl(idbooking)



    End Sub

    Private Sub txtShipping_EditValueChanged(sender As Object, e As EventArgs) Handles txtShipping.EditValueChanged
        If txtShipping.EditValue Is Nothing Then
            txtShippingAddress.EditValue = Nothing
            Exit Sub
        End If
        Dim sql As String
        Dim dt As New DataTable
        sql = "SELECt * FROM shipper WHERE shipperid = '" & txtShipping.EditValue & "';"
        connectDB.GetTable(sql, dt)
        If dt.Rows.Count > 0 Then
            txtShippingAddress.EditValue = dt.Rows(0)("SHIPADD").ToString
            txtShipping.Tag = dt.Rows(0)("SHIPNICKNAME").ToString
        End If
    End Sub

    Private Sub txtconsignee_EditValueChanged(sender As Object, e As EventArgs) Handles txtconsignee.EditValueChanged
        If txtconsignee.EditValue Is Nothing Then
            txtconsigneeAddress.EditValue = Nothing
            Exit Sub
        End If
        Dim sql As String
        Dim dt As New DataTable
        sql = "SELECt * FROM consignee WHERE CONSIGNEEID = '" & txtconsignee.EditValue & "';"
        connectDB.GetTable(sql, dt)
        If dt.Rows.Count > 0 Then
            txtconsigneeAddress.EditValue = dt.Rows(0)("CONADD").ToString

        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If idbooking Is Nothing Then
            addBooking()
        Else
            UpdateBooking()
        End If
        ClearData()
        loadBooking()
    End Sub


    Public Sub LoadBooking(ByVal idbooking As String)
        objCLASS.LoadBookingDetail(idbooking)
        If ykpset.Tables("bookingdetail").Rows.Count > 0 Then
            txt_booking_no.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BNO")

            TextBox3.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BLANDNO")
            txt_type.Text = ykpset.Tables("bookingdetail").Rows(0)("BCTNTYPE")
            txt_notify.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BNOTIFY")
            txt_scn.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BSCN")
            txt_comodity.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BCOM")
            BDESCRIPT.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BDESCRIPT")
            BTYPEMOVE.SelectedIndex = Convert.ToDecimal(ykpset.Tables("booking").Rows(0)("BTYPEMOVE"))
            txt_TUG.Text = ykpset.Tables("bookingdetail").Rows(0)("BTUG")
            txt_Gross.Text = ykpset.Tables("bookingdetail").Rows(0)("BGROSS")
            TextBox4.Text = ykpset.Tables("bookingdetail").Rows(0)("BCTNNO")
            BNICKAGENT.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BNICKAGENT")
            BPENANG.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BPENANG")
            txt_localforwared.Text = ykpset.Tables("bookingdetail").Rows(0)("BLOCALFOR")
            Try
                If ykpset.Tables("bookingdetail").Rows(0)("BCHECKLOAD").ToString = "False" Then
                    BCHECKLOAD.SelectedIndex = 1

                ElseIf ykpset.Tables("bookingdetail").Rows(0)("BCHECKLOAD") = "True" Then

                    BCHECKLOAD.SelectedIndex = 0
                Else

                    BCHECKLOAD.SelectedIndex = -1

                End If

            Catch ex As Exception

            End Try

            If ykpset.Tables("bookingdetail").Rows(0)("POL") Is DBNull.Value Then
            Else
                txt_pol.Text = ykpset.Tables("booking").Rows(0)("POL")
            End If
            If ykpset.Tables("bookingdetail").Rows(0)("TSPORT") Is DBNull.Value Then
            Else
                txt_tsport.Text = ykpset.Tables("bookingdetail").Rows(0)("TSPORT")
            End If
            If ykpset.Tables("bookingdetail").Rows(0)("REMARK") Is DBNull.Value Then
            Else
                txt_remark.Text = ykpset.Tables("bookingdetail").Rows(0)("REMARK")
            End If

            If ykpset.Tables("bookingdetail").Rows(0)("BCONSI") Is DBNull.Value Then
            Else
                txtconsignee.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BCONSI")
            End If
            If ykpset.Tables("bookingdetail").Rows(0)("BSHIP") Is DBNull.Value Then
            Else
                txtShipping.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BSHIP")
            End If
            If ykpset.Tables("bookingdetail").Rows(0)("BFORWARDER") Is DBNull.Value Then
            Else
                txtForwarder.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BFORWARDER")
            End If

            If ykpset.Tables("bookingdetail").Rows(0)("BMOTH") Is DBNull.Value Then
            Else
                BMOTH.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BMOTH")
            End If

            If ykpset.Tables("bookingdetail").Rows(0)("BINVOICE") Is DBNull.Value Then
            Else
                BINVOICE.EditValue = ykpset.Tables("booking").Rows(0)("BINVOICE")
            End If
            If ykpset.Tables("bookingdetail").Rows(0)("BINCOTERM") Is DBNull.Value Then
            Else
                BINCOTERM.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BINCOTERM")
            End If

            If ykpset.Tables("bookingdetail").Rows(0)("BREMARK") Is DBNull.Value Then
            Else
                BREMARK.EditValue = ykpset.Tables("bookingdetail").Rows(0)("BREMARK")
            End If

            Loadbooking_bl_ctn(idbooking)
            loadbooking_bl(idbooking)

        End If

    End Sub

    Public Sub Loadbooking_bl_ctn(ByVal idbooking As String)
        objCLASS.Loadbooking_bl_ctn(idbooking)
        GridControl1.DataSource = ykpset.Tables("booking_bl_ctn")
    End Sub
    Public Sub loadbooking_bl(ByVal idbooking As String)
        objCLASS.Loadbooking_bl(idbooking)
        If ykpset.Tables("booking_bl").Rows.Count > 0 Then
            Me.billboking.EditValue = Convert.ToString(ykpset.Tables("booking_bl").Rows(0)("billbooking"))

            Me.txtNumber.EditValue = Convert.ToString(ykpset.Tables("booking_bl").Rows(0)("number"))
            Me.txtPlace.Text = Convert.ToString(ykpset.Tables("booking_bl").Rows(0)("place"))
            Me.txtconsignee.EditValue = Convert.ToString(ykpset.Tables("booking_bl").Rows(0)("conid"))
            Me.txtUnitCtn.Text = Convert.ToString(ykpset.Tables("booking_bl").Rows(0)("UNITCTN"))
        End If

    End Sub

    Private Sub btnAddRow_Click(sender As Object, e As EventArgs) Handles btnAddRow.Click
        '  GridView1.AddNewRow()
        Dim number As Integer = ykpset.Tables("booking_bl_ctn").Rows.Count
        Dim dr As DataRow = ykpset.Tables("booking_bl_ctn").NewRow()
        dr("idnumber") = number + 1
        ykpset.Tables("booking_bl_ctn").Rows.Add(dr)
        GridControl1.DataSource = ykpset.Tables("booking_bl_ctn")
        GridView1.RefreshData()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        GridView1.DeleteRow(GridView1.FocusedRowHandle)
        GridView1.RefreshData()
        addbooking_bl_ctn(idbooking)
        Loadbooking_bl_ctn(idbooking)
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        loadBooking()
        LoadConsinee()
        LoadShipper()
        loadmasterbl_ctn()
        ClearData()
    End Sub
    Dim convertNumtoWord As New clsConversion

    Private Sub txtNumber_EditValueChanged(sender As Object, e As EventArgs) Handles txtNumber.EditValueChanged
        If txtNumber.EditValue Is Nothing Then
            Exit Sub
        End If
        txtNumberWord.EditValue = convertNumtoWord.ConvertNumberToWords(txtNumber.EditValue)
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        addbooking_bl_ctn(idbooking)
        Loadbooking_bl_ctn(idbooking)
    End Sub
End Class