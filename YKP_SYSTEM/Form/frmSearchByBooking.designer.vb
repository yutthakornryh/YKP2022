﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchByBooking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYNAME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnBookingDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONFIRMTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONFIRMDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBINVOICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.slcVoyageStart = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource4 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset6 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.slcVoyageEnd = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource5 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset7 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.voyageBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.voyageBindingSource3 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset5 = New YKP_SYSTEM.ykpdtset()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.voyageBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnBookingDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcVoyageStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcVoyageEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.slcVoyageStart)
        Me.LayoutControl1.Controls.Add(Me.slcVoyageEnd)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1156, 646)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "booking"
        Me.GridControl2.DataSource = Me.Ykpdtset1
        Me.GridControl2.Location = New System.Drawing.Point(12, 64)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnBookingDelete})
        Me.GridControl2.Size = New System.Drawing.Size(1132, 570)
        Me.GridControl2.TabIndex = 209
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYNAME1, Me.GridColumn7, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.colBNFORWARDERNAME, Me.colBFORWARDER, Me.colCTNNO, Me.colBCONFIRMTIME, Me.colBCONFIRMDAY, Me.GridColumn8, Me.colBINVOICE})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.IndicatorWidth = 25
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsImageLoad.DesiredThumbnailSize = New System.Drawing.Size(10, 0)
        Me.GridView2.OptionsSelection.MultiSelect = True
        Me.GridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colVOYNAME1
        '
        Me.colVOYNAME1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME1.AppearanceCell.Options.UseFont = True
        Me.colVOYNAME1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME1.AppearanceHeader.Options.UseFont = True
        Me.colVOYNAME1.FieldName = "VOYNAME"
        Me.colVOYNAME1.Name = "colVOYNAME1"
        Me.colVOYNAME1.Visible = True
        Me.colVOYNAME1.VisibleIndex = 0
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn7.AppearanceHeader.Options.UseFont = True
        Me.GridColumn7.Caption = "Delete"
        Me.GridColumn7.ColumnEdit = Me.btnBookingDelete
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Width = 54
        '
        'btnBookingDelete
        '
        Me.btnBookingDelete.AutoHeight = False
        Me.btnBookingDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)})
        Me.btnBookingDelete.Name = "btnBookingDelete"
        Me.btnBookingDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.FieldName = "BOOKINGID"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn2.AppearanceHeader.Options.UseFont = True
        Me.GridColumn2.Caption = "Booking No."
        Me.GridColumn2.FieldName = "BNO"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.ReadOnly = True
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 89
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn3.AppearanceHeader.Options.UseFont = True
        Me.GridColumn3.FieldName = "BLANDNO"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.ReadOnly = True
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn4.AppearanceHeader.Options.UseFont = True
        Me.GridColumn4.FieldName = "BCTNTYPE"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.ReadOnly = True
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn5.AppearanceHeader.Options.UseFont = True
        Me.GridColumn5.FieldName = "BCOM"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.OptionsColumn.ReadOnly = True
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        Me.GridColumn5.Width = 87
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn6.AppearanceHeader.Options.UseFont = True
        Me.GridColumn6.Caption = "Shipper"
        Me.GridColumn6.FieldName = "BSHIPNAME"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.ReadOnly = True
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        Me.GridColumn6.Width = 97
        '
        'colBNFORWARDERNAME
        '
        Me.colBNFORWARDERNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNFORWARDERNAME.AppearanceCell.Options.UseFont = True
        Me.colBNFORWARDERNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNFORWARDERNAME.AppearanceHeader.Options.UseFont = True
        Me.colBNFORWARDERNAME.FieldName = "BNFORWARDERNAME"
        Me.colBNFORWARDERNAME.Name = "colBNFORWARDERNAME"
        Me.colBNFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBNFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBNFORWARDERNAME.Visible = True
        Me.colBNFORWARDERNAME.VisibleIndex = 4
        Me.colBNFORWARDERNAME.Width = 104
        '
        'colBFORWARDER
        '
        Me.colBFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colBFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colBFORWARDER.Caption = "Forwarder"
        Me.colBFORWARDER.FieldName = "BFORWARDER"
        Me.colBFORWARDER.Name = "colBFORWARDER"
        Me.colBFORWARDER.OptionsColumn.AllowEdit = False
        Me.colBFORWARDER.OptionsColumn.ReadOnly = True
        Me.colBFORWARDER.Width = 183
        '
        'colCTNNO
        '
        Me.colCTNNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNNO.AppearanceCell.Options.UseFont = True
        Me.colCTNNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNNO.AppearanceHeader.Options.UseFont = True
        Me.colCTNNO.Caption = "FCL"
        Me.colCTNNO.FieldName = "CTNNO"
        Me.colCTNNO.Name = "colCTNNO"
        Me.colCTNNO.OptionsColumn.AllowEdit = False
        Me.colCTNNO.OptionsColumn.ReadOnly = True
        Me.colCTNNO.Visible = True
        Me.colCTNNO.VisibleIndex = 6
        Me.colCTNNO.Width = 92
        '
        'colBCONFIRMTIME
        '
        Me.colBCONFIRMTIME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMTIME.AppearanceCell.Options.UseFont = True
        Me.colBCONFIRMTIME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMTIME.AppearanceHeader.Options.UseFont = True
        Me.colBCONFIRMTIME.FieldName = "BCONFIRMTIME"
        Me.colBCONFIRMTIME.Name = "colBCONFIRMTIME"
        Me.colBCONFIRMTIME.OptionsColumn.AllowEdit = False
        Me.colBCONFIRMTIME.OptionsColumn.ReadOnly = True
        Me.colBCONFIRMTIME.Width = 153
        '
        'colBCONFIRMDAY
        '
        Me.colBCONFIRMDAY.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMDAY.AppearanceCell.Options.UseFont = True
        Me.colBCONFIRMDAY.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMDAY.AppearanceHeader.Options.UseFont = True
        Me.colBCONFIRMDAY.FieldName = "BCONFIRMDAY"
        Me.colBCONFIRMDAY.Name = "colBCONFIRMDAY"
        Me.colBCONFIRMDAY.OptionsColumn.AllowEdit = False
        Me.colBCONFIRMDAY.OptionsColumn.ReadOnly = True
        Me.colBCONFIRMDAY.Width = 164
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn8.AppearanceHeader.Options.UseFont = True
        Me.GridColumn8.Caption = "Close Date"
        Me.GridColumn8.FieldName = "GridColumn8"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.OptionsColumn.AllowEdit = False
        Me.GridColumn8.OptionsColumn.ReadOnly = True
        Me.GridColumn8.UnboundExpression = "Concat([BCONFIRMDAY], ' ', [BCONFIRMTIME])"
        Me.GridColumn8.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 5
        Me.GridColumn8.Width = 68
        '
        'colBINVOICE
        '
        Me.colBINVOICE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINVOICE.AppearanceCell.Options.UseFont = True
        Me.colBINVOICE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINVOICE.AppearanceHeader.Options.UseFont = True
        Me.colBINVOICE.Caption = "Invoice"
        Me.colBINVOICE.FieldName = "BINVOICE"
        Me.colBINVOICE.Name = "colBINVOICE"
        Me.colBINVOICE.OptionsColumn.AllowEdit = False
        Me.colBINVOICE.OptionsColumn.ReadOnly = True
        Me.colBINVOICE.Width = 88
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(970, 38)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(174, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 10
        Me.SimpleButton2.Text = "Excel Export Booking"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(574, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(158, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 9
        Me.SimpleButton1.Text = "Load"
        '
        'slcVoyageStart
        '
        Me.slcVoyageStart.Location = New System.Drawing.Point(89, 12)
        Me.slcVoyageStart.Name = "slcVoyageStart"
        Me.slcVoyageStart.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.slcVoyageStart.Properties.Appearance.Options.UseFont = True
        Me.slcVoyageStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyageStart.Properties.DataSource = Me.voyageBindingSource4
        Me.slcVoyageStart.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyageStart.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyageStart.Properties.View = Me.SearchLookUpEdit2View
        Me.slcVoyageStart.Size = New System.Drawing.Size(188, 22)
        Me.slcVoyageStart.StyleController = Me.LayoutControl1
        Me.slcVoyageStart.TabIndex = 8
        '
        'voyageBindingSource4
        '
        Me.voyageBindingSource4.DataMember = "voyage"
        Me.voyageBindingSource4.DataSource = Me.Ykpdtset6
        Me.voyageBindingSource4.Sort = ""
        '
        'Ykpdtset6
        '
        Me.Ykpdtset6.DataSetName = "ykpdtset"
        Me.Ykpdtset6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        Me.SearchLookUpEdit2View.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colVOYNAME, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        Me.colVOYDATEES.Visible = True
        Me.colVOYDATEES.VisibleIndex = 2
        Me.colVOYDATEES.Width = 612
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 1
        Me.colVOYVESNAMES.Width = 608
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Width = 403
        '
        'colVOYNAME
        '
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        Me.colVOYNAME.Width = 402
        '
        'slcVoyageEnd
        '
        Me.slcVoyageEnd.Location = New System.Drawing.Point(358, 12)
        Me.slcVoyageEnd.Name = "slcVoyageEnd"
        Me.slcVoyageEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.slcVoyageEnd.Properties.Appearance.Options.UseFont = True
        Me.slcVoyageEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyageEnd.Properties.DataSource = Me.voyageBindingSource5
        Me.slcVoyageEnd.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyageEnd.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyageEnd.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyageEnd.Size = New System.Drawing.Size(212, 22)
        Me.slcVoyageEnd.StyleController = Me.LayoutControl1
        Me.slcVoyageEnd.TabIndex = 7
        '
        'voyageBindingSource5
        '
        Me.voyageBindingSource5.DataMember = "voyage"
        Me.voyageBindingSource5.DataSource = Me.Ykpdtset7
        Me.voyageBindingSource5.Sort = ""
        '
        'Ykpdtset7
        '
        Me.Ykpdtset7.DataSetName = "ykpdtset"
        Me.Ykpdtset7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn15, Me.GridColumn16, Me.GridColumn17, Me.GridColumn18, Me.GridColumn19})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn9
        '
        Me.GridColumn9.FieldName = "VOYAGEID"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'GridColumn10
        '
        Me.GridColumn10.FieldName = "VOYVESIDN"
        Me.GridColumn10.Name = "GridColumn10"
        '
        'GridColumn11
        '
        Me.GridColumn11.FieldName = "VOYVESIDS"
        Me.GridColumn11.Name = "GridColumn11"
        '
        'GridColumn12
        '
        Me.GridColumn12.FieldName = "VOYDATESN"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'GridColumn13
        '
        Me.GridColumn13.FieldName = "VOYDATESS"
        Me.GridColumn13.Name = "GridColumn13"
        '
        'GridColumn14
        '
        Me.GridColumn14.FieldName = "VOYDATEEN"
        Me.GridColumn14.Name = "GridColumn14"
        '
        'GridColumn15
        '
        Me.GridColumn15.FieldName = "VOYVESNAMEN"
        Me.GridColumn15.Name = "GridColumn15"
        '
        'GridColumn16
        '
        Me.GridColumn16.FieldName = "VOYDATEES"
        Me.GridColumn16.Name = "GridColumn16"
        '
        'GridColumn17
        '
        Me.GridColumn17.FieldName = "VOYVESNAMES"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 2
        '
        'GridColumn18
        '
        Me.GridColumn18.FieldName = "VOYTIMEHHMMNN"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 1
        '
        'GridColumn19
        '
        Me.GridColumn19.FieldName = "VOYNAME"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 0
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl2
        Me.SearchControl1.Location = New System.Drawing.Point(633, 38)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl2
        Me.SearchControl1.Size = New System.Drawing.Size(333, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 6
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.LayoutControlItem7, Me.LayoutControlItem6, Me.EmptySpaceItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1156, 646)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.SearchControl1
        Me.LayoutControlItem3.CustomizationFormText = "ค้นหา"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(544, 26)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(414, 26)
        Me.LayoutControlItem3.Text = "ค้นหา"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(74, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.slcVoyageEnd
        Me.LayoutControlItem2.Location = New System.Drawing.Point(269, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(293, 26)
        Me.LayoutControlItem2.Text = "Voyage End"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(74, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.slcVoyageStart
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(269, 26)
        Me.LayoutControlItem4.Text = "Voyage Start"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(74, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.SimpleButton1
        Me.LayoutControlItem5.Location = New System.Drawing.Point(562, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(162, 26)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(724, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(412, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.GridControl2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(1136, 574)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.SimpleButton2
        Me.LayoutControlItem6.Location = New System.Drawing.Point(958, 26)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(178, 26)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 26)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(544, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'voyageBindingSource2
        '
        Me.voyageBindingSource2.DataMember = "voyage"
        Me.voyageBindingSource2.DataSource = Me.Ykpdtset4
        Me.voyageBindingSource2.Sort = ""
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'voyageBindingSource3
        '
        Me.voyageBindingSource3.DataMember = "voyage"
        Me.voyageBindingSource3.DataSource = Me.Ykpdtset5
        Me.voyageBindingSource3.Sort = ""
        '
        'Ykpdtset5
        '
        Me.Ykpdtset5.DataSetName = "ykpdtset"
        Me.Ykpdtset5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset2
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'voyageBindingSource1
        '
        Me.voyageBindingSource1.DataMember = "voyage"
        Me.voyageBindingSource1.DataSource = Me.Ykpdtset3
        Me.voyageBindingSource1.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmSearchByBooking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1156, 646)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmSearchByBooking"
        Me.Text = "Serach Container By Voyage"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnBookingDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcVoyageStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcVoyageEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents slcVoyageStart As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents slcVoyageEnd As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnBookingDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONFIRMTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONFIRMDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBINVOICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents voyageBindingSource1 As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents voyageBindingSource2 As BindingSource
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents voyageBindingSource3 As BindingSource
    Friend WithEvents Ykpdtset5 As ykpdtset
    Friend WithEvents voyageBindingSource4 As BindingSource
    Friend WithEvents Ykpdtset6 As ykpdtset
    Friend WithEvents voyageBindingSource5 As BindingSource
    Friend WithEvents Ykpdtset7 As ykpdtset
    Friend WithEvents colVOYNAME1 As DevExpress.XtraGrid.Columns.GridColumn
End Class
