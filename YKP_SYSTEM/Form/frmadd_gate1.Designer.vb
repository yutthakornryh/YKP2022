<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmadd_gate1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.txt_no_car = New System.Windows.Forms.TextBox()
        Me.txt_name_car = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New DevExpress.XtraEditors.TextEdit()
        Me.port_loading = New DevExpress.XtraEditors.TextEdit()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONDI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNBOOKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNYARD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNUPGRADE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTRBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEALBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEINBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.TextBox4 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_insurance = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox1 = New DevExpress.XtraEditors.TextEdit()
        Me.port_discharge = New DevExpress.XtraEditors.TextEdit()
        Me.txt_sealid = New DevExpress.XtraEditors.TextEdit()
        Me.txt_customer = New DevExpress.XtraEditors.TextEdit()
        Me.txt_container = New DevExpress.XtraEditors.TextEdit()
        Me.txt_voyage = New DevExpress.XtraEditors.TextEdit()
        Me.txt_booking_no = New DevExpress.XtraEditors.TextEdit()
        Me.DateTimePicker3 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.DateTimePicker1 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.DateTimePicker2 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.DateTimePicker4 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBLANDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNWORD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNOTIFY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBSCN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCOM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBDESCRIPT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBTYPEMOVE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBSHIP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBOOKINGID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONSINAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBTUG = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBGROSS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBLOCALFOR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBVOYAGE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONFIRMDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONFIRMTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPOL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTSPORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colREMARK = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFEDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBMOTH = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBMOTHS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBTRANSHIP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBTRANSHIPS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBPORTDIS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBPORTDISS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBTRANSHIP2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBTRANSHIP2S = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBDELIVERY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBDELIVERYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFINALDESTS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFINALDEST = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNICKAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBINV = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBDIS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCHECKLOAD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBINVOICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBINCOTERM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBREMARK = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBPENANG = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNETGROSS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.TextBox3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.port_loading.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_insurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.port_discharge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_sealid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_container.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_booking_no.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTimePicker3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTimePicker2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txt_no_car)
        Me.LayoutControl1.Controls.Add(Me.txt_name_car)
        Me.LayoutControl1.Controls.Add(Me.TextBox3)
        Me.LayoutControl1.Controls.Add(Me.port_loading)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup1)
        Me.LayoutControl1.Controls.Add(Me.TextBox4)
        Me.LayoutControl1.Controls.Add(Me.txt_insurance)
        Me.LayoutControl1.Controls.Add(Me.TextBox1)
        Me.LayoutControl1.Controls.Add(Me.port_discharge)
        Me.LayoutControl1.Controls.Add(Me.txt_sealid)
        Me.LayoutControl1.Controls.Add(Me.txt_customer)
        Me.LayoutControl1.Controls.Add(Me.txt_container)
        Me.LayoutControl1.Controls.Add(Me.txt_voyage)
        Me.LayoutControl1.Controls.Add(Me.txt_booking_no)
        Me.LayoutControl1.Controls.Add(Me.DateTimePicker3)
        Me.LayoutControl1.Controls.Add(Me.DateTimePicker1)
        Me.LayoutControl1.Controls.Add(Me.DateTimePicker2)
        Me.LayoutControl1.Controls.Add(Me.DateTimePicker4)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(807, 338, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(997, 586)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'txt_no_car
        '
        Me.txt_no_car.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_no_car.Location = New System.Drawing.Point(885, 56)
        Me.txt_no_car.Name = "txt_no_car"
        Me.txt_no_car.Size = New System.Drawing.Size(100, 20)
        Me.txt_no_car.TabIndex = 205
        '
        'txt_name_car
        '
        Me.txt_name_car.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_name_car.Location = New System.Drawing.Point(613, 56)
        Me.txt_name_car.Name = "txt_name_car"
        Me.txt_name_car.Size = New System.Drawing.Size(158, 20)
        Me.txt_name_car.TabIndex = 204
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(885, 212)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox3.Properties.Appearance.Options.UseFont = True
        Me.TextBox3.Size = New System.Drawing.Size(100, 22)
        Me.TextBox3.StyleController = Me.LayoutControl1
        Me.TextBox3.TabIndex = 203
        '
        'port_loading
        '
        Me.port_loading.EditValue = "PENANG"
        Me.port_loading.Location = New System.Drawing.Point(613, 212)
        Me.port_loading.Name = "port_loading"
        Me.port_loading.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.port_loading.Properties.Appearance.Options.UseFont = True
        Me.port_loading.Size = New System.Drawing.Size(158, 22)
        Me.port_loading.StyleController = Me.LayoutControl1
        Me.port_loading.TabIndex = 202
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl3
        Me.SearchControl1.Location = New System.Drawing.Point(122, 230)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl3
        Me.SearchControl1.Size = New System.Drawing.Size(377, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 201
        '
        'GridControl3
        '
        Me.GridControl3.DataMember = "ctnmain"
        Me.GridControl3.DataSource = Me.Ykpdtset2
        Me.GridControl3.Location = New System.Drawing.Point(12, 256)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(487, 318)
        Me.GridControl3.TabIndex = 200
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNMAINID, Me.colCTNSTRING, Me.colCTNSEALID, Me.colCTNAGENT, Me.colCTNSIZE, Me.colCTNCONDI, Me.colCTNSTAT, Me.colCTNCONSI, Me.colCTNVOYN, Me.colCTNVOYS, Me.colCTNWEIGHT, Me.colCTNINS, Me.colCTNSHIPNAME, Me.colCTNDATEOUT, Me.colCTNBOOKING, Me.colCTNFORWARDER, Me.colCTNYARD, Me.colCTNUPGRADE, Me.colCTNMARKSTR, Me.colCTNMARKSEAL, Me.colCTNMARKSTRBOO, Me.colCTNMARKSEALBOO, Me.colCTNMARKDATEIN, Me.colCTNMARKDATEINBOO, Me.colCTNMARKDATEOUT, Me.colCTNMARKDATEOUTBOO, Me.colCTNMARKWEIGHT, Me.colCTNMARKWEIGHTBOO, Me.colCTNMARKTRANSPORT, Me.colCTNMARKTRANSPORTBOO, Me.colmenuname})
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowGroupPanel = False
        Me.GridView3.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCTNAGENT, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colCTNMAINID
        '
        Me.colCTNMAINID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMAINID.AppearanceCell.Options.UseFont = True
        Me.colCTNMAINID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMAINID.AppearanceHeader.Options.UseFont = True
        Me.colCTNMAINID.FieldName = "CTNMAINID"
        Me.colCTNMAINID.Name = "colCTNMAINID"
        Me.colCTNMAINID.OptionsColumn.AllowEdit = False
        Me.colCTNMAINID.OptionsColumn.ReadOnly = True
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.Caption = "Container No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.OptionsColumn.AllowEdit = False
        Me.colCTNSTRING.OptionsColumn.ReadOnly = True
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 0
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSEALID.AppearanceCell.Options.UseFont = True
        Me.colCTNSEALID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSEALID.AppearanceHeader.Options.UseFont = True
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.OptionsColumn.AllowEdit = False
        Me.colCTNSEALID.OptionsColumn.ReadOnly = True
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.Caption = "Agent Line"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.OptionsColumn.AllowEdit = False
        Me.colCTNAGENT.OptionsColumn.ReadOnly = True
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 1
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSIZE.AppearanceCell.Options.UseFont = True
        Me.colCTNSIZE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSIZE.AppearanceHeader.Options.UseFont = True
        Me.colCTNSIZE.Caption = "Size"
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.OptionsColumn.AllowEdit = False
        Me.colCTNSIZE.OptionsColumn.ReadOnly = True
        Me.colCTNSIZE.Visible = True
        Me.colCTNSIZE.VisibleIndex = 2
        '
        'colCTNCONDI
        '
        Me.colCTNCONDI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONDI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONDI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONDI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONDI.FieldName = "CTNCONDI"
        Me.colCTNCONDI.Name = "colCTNCONDI"
        Me.colCTNCONDI.OptionsColumn.AllowEdit = False
        Me.colCTNCONDI.OptionsColumn.ReadOnly = True
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTAT.AppearanceCell.Options.UseFont = True
        Me.colCTNSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTAT.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        Me.colCTNSTAT.OptionsColumn.AllowEdit = False
        Me.colCTNSTAT.OptionsColumn.ReadOnly = True
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONSI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONSI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        Me.colCTNCONSI.OptionsColumn.AllowEdit = False
        Me.colCTNCONSI.OptionsColumn.ReadOnly = True
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYN.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYN.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYN.Caption = "Voy N"
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        Me.colCTNVOYN.OptionsColumn.AllowEdit = False
        Me.colCTNVOYN.OptionsColumn.ReadOnly = True
        Me.colCTNVOYN.Visible = True
        Me.colCTNVOYN.VisibleIndex = 3
        '
        'colCTNVOYS
        '
        Me.colCTNVOYS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYS.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYS.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYS.FieldName = "CTNVOYS"
        Me.colCTNVOYS.Name = "colCTNVOYS"
        Me.colCTNVOYS.OptionsColumn.AllowEdit = False
        Me.colCTNVOYS.OptionsColumn.ReadOnly = True
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNWEIGHT.OptionsColumn.ReadOnly = True
        '
        'colCTNINS
        '
        Me.colCTNINS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNINS.AppearanceCell.Options.UseFont = True
        Me.colCTNINS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNINS.AppearanceHeader.Options.UseFont = True
        Me.colCTNINS.FieldName = "CTNINS"
        Me.colCTNINS.Name = "colCTNINS"
        Me.colCTNINS.OptionsColumn.AllowEdit = False
        Me.colCTNINS.OptionsColumn.ReadOnly = True
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colCTNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colCTNSHIPNAME.Caption = "Shipper"
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        Me.colCTNSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colCTNSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colCTNSHIPNAME.Visible = True
        Me.colCTNSHIPNAME.VisibleIndex = 5
        '
        'colCTNDATEOUT
        '
        Me.colCTNDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNDATEOUT.FieldName = "CTNDATEOUT"
        Me.colCTNDATEOUT.Name = "colCTNDATEOUT"
        Me.colCTNDATEOUT.OptionsColumn.AllowEdit = False
        Me.colCTNDATEOUT.OptionsColumn.ReadOnly = True
        '
        'colCTNBOOKING
        '
        Me.colCTNBOOKING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNBOOKING.AppearanceCell.Options.UseFont = True
        Me.colCTNBOOKING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNBOOKING.AppearanceHeader.Options.UseFont = True
        Me.colCTNBOOKING.Caption = "Booking"
        Me.colCTNBOOKING.FieldName = "CTNBOOKING"
        Me.colCTNBOOKING.Name = "colCTNBOOKING"
        Me.colCTNBOOKING.OptionsColumn.AllowEdit = False
        Me.colCTNBOOKING.OptionsColumn.ReadOnly = True
        Me.colCTNBOOKING.Visible = True
        Me.colCTNBOOKING.VisibleIndex = 4
        '
        'colCTNFORWARDER
        '
        Me.colCTNFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colCTNFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colCTNFORWARDER.FieldName = "CTNFORWARDER"
        Me.colCTNFORWARDER.Name = "colCTNFORWARDER"
        Me.colCTNFORWARDER.OptionsColumn.AllowEdit = False
        Me.colCTNFORWARDER.OptionsColumn.ReadOnly = True
        '
        'colCTNYARD
        '
        Me.colCTNYARD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNYARD.AppearanceCell.Options.UseFont = True
        Me.colCTNYARD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNYARD.AppearanceHeader.Options.UseFont = True
        Me.colCTNYARD.Caption = "CTNYARD"
        Me.colCTNYARD.FieldName = "CTNYARD"
        Me.colCTNYARD.Name = "colCTNYARD"
        Me.colCTNYARD.OptionsColumn.AllowEdit = False
        Me.colCTNYARD.OptionsColumn.ReadOnly = True
        '
        'colCTNUPGRADE
        '
        Me.colCTNUPGRADE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNUPGRADE.AppearanceCell.Options.UseFont = True
        Me.colCTNUPGRADE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNUPGRADE.AppearanceHeader.Options.UseFont = True
        Me.colCTNUPGRADE.FieldName = "CTNUPGRADE"
        Me.colCTNUPGRADE.Name = "colCTNUPGRADE"
        Me.colCTNUPGRADE.OptionsColumn.AllowEdit = False
        Me.colCTNUPGRADE.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSTR
        '
        Me.colCTNMARKSTR.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTR.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTR.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTR.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTR.FieldName = "CTNMARKSTR"
        Me.colCTNMARKSTR.Name = "colCTNMARKSTR"
        Me.colCTNMARKSTR.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSTR.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSEAL
        '
        Me.colCTNMARKSEAL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEAL.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEAL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEAL.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEAL.FieldName = "CTNMARKSEAL"
        Me.colCTNMARKSEAL.Name = "colCTNMARKSEAL"
        Me.colCTNMARKSEAL.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSEAL.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSTRBOO
        '
        Me.colCTNMARKSTRBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTRBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTRBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTRBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTRBOO.FieldName = "CTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.Name = "colCTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSTRBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSEALBOO
        '
        Me.colCTNMARKSEALBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEALBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEALBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEALBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEALBOO.FieldName = "CTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.Name = "colCTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSEALBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEIN
        '
        Me.colCTNMARKDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEIN.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEIN.FieldName = "CTNMARKDATEIN"
        Me.colCTNMARKDATEIN.Name = "colCTNMARKDATEIN"
        Me.colCTNMARKDATEIN.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEIN.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEINBOO
        '
        Me.colCTNMARKDATEINBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEINBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.FieldName = "CTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.Name = "colCTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEINBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEOUT
        '
        Me.colCTNMARKDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUT.FieldName = "CTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.Name = "colCTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEOUT.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEOUTBOO
        '
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.FieldName = "CTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.Name = "colCTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEOUTBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKWEIGHT
        '
        Me.colCTNMARKWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHT.FieldName = "CTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.Name = "colCTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKWEIGHT.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKWEIGHTBOO
        '
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.FieldName = "CTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.Name = "colCTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKWEIGHTBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKTRANSPORT
        '
        Me.colCTNMARKTRANSPORT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.FieldName = "CTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.Name = "colCTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKTRANSPORT.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKTRANSPORTBOO
        '
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.FieldName = "CTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.Name = "colCTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKTRANSPORTBOO.OptionsColumn.ReadOnly = True
        '
        'colmenuname
        '
        Me.colmenuname.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmenuname.AppearanceCell.Options.UseFont = True
        Me.colmenuname.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmenuname.AppearanceHeader.Options.UseFont = True
        Me.colmenuname.Caption = "DEPOT"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 6
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(855, 315)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(130, 23)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 199
        Me.SimpleButton1.Text = "�ѹ�֡"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(503, 264)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Normal"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Upgrade")})
        Me.RadioGroup1.Size = New System.Drawing.Size(115, 47)
        Me.RadioGroup1.StyleController = Me.LayoutControl1
        Me.RadioGroup1.TabIndex = 198
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(718, 238)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox4.Properties.Appearance.Options.UseFont = True
        Me.TextBox4.Size = New System.Drawing.Size(53, 22)
        Me.TextBox4.StyleController = Me.LayoutControl1
        Me.TextBox4.TabIndex = 197
        '
        'txt_insurance
        '
        Me.txt_insurance.Location = New System.Drawing.Point(885, 238)
        Me.txt_insurance.Name = "txt_insurance"
        Me.txt_insurance.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_insurance.Properties.Appearance.Options.UseFont = True
        Me.txt_insurance.Size = New System.Drawing.Size(100, 22)
        Me.txt_insurance.StyleController = Me.LayoutControl1
        Me.txt_insurance.TabIndex = 196
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(613, 238)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox1.Properties.Appearance.Options.UseFont = True
        Me.TextBox1.Size = New System.Drawing.Size(101, 22)
        Me.TextBox1.StyleController = Me.LayoutControl1
        Me.TextBox1.TabIndex = 195
        '
        'port_discharge
        '
        Me.port_discharge.EditValue = "SPH"
        Me.port_discharge.Location = New System.Drawing.Point(885, 186)
        Me.port_discharge.Name = "port_discharge"
        Me.port_discharge.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.port_discharge.Properties.Appearance.Options.UseFont = True
        Me.port_discharge.Size = New System.Drawing.Size(100, 22)
        Me.port_discharge.StyleController = Me.LayoutControl1
        Me.port_discharge.TabIndex = 194
        '
        'txt_sealid
        '
        Me.txt_sealid.Location = New System.Drawing.Point(613, 186)
        Me.txt_sealid.Name = "txt_sealid"
        Me.txt_sealid.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_sealid.Properties.Appearance.Options.UseFont = True
        Me.txt_sealid.Size = New System.Drawing.Size(158, 22)
        Me.txt_sealid.StyleController = Me.LayoutControl1
        Me.txt_sealid.TabIndex = 193
        '
        'txt_customer
        '
        Me.txt_customer.Location = New System.Drawing.Point(885, 160)
        Me.txt_customer.Name = "txt_customer"
        Me.txt_customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_customer.Properties.Appearance.Options.UseFont = True
        Me.txt_customer.Size = New System.Drawing.Size(100, 22)
        Me.txt_customer.StyleController = Me.LayoutControl1
        Me.txt_customer.TabIndex = 192
        '
        'txt_container
        '
        Me.txt_container.Location = New System.Drawing.Point(613, 160)
        Me.txt_container.Name = "txt_container"
        Me.txt_container.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_container.Properties.Appearance.Options.UseFont = True
        Me.txt_container.Size = New System.Drawing.Size(158, 22)
        Me.txt_container.StyleController = Me.LayoutControl1
        Me.txt_container.TabIndex = 191
        '
        'txt_voyage
        '
        Me.txt_voyage.Location = New System.Drawing.Point(613, 134)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_voyage.Properties.Appearance.Options.UseFont = True
        Me.txt_voyage.Size = New System.Drawing.Size(158, 22)
        Me.txt_voyage.StyleController = Me.LayoutControl1
        Me.txt_voyage.TabIndex = 190
        '
        'txt_booking_no
        '
        Me.txt_booking_no.Location = New System.Drawing.Point(885, 134)
        Me.txt_booking_no.Name = "txt_booking_no"
        Me.txt_booking_no.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_booking_no.Properties.Appearance.Options.UseFont = True
        Me.txt_booking_no.Size = New System.Drawing.Size(100, 22)
        Me.txt_booking_no.StyleController = Me.LayoutControl1
        Me.txt_booking_no.TabIndex = 189
        '
        'DateTimePicker3
        '
        '
        '
        '
        Me.DateTimePicker3.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateTimePicker3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker3.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateTimePicker3.ButtonDropDown.Visible = True
        Me.DateTimePicker3.CustomFormat = "dd-MM-yyyy"
        Me.DateTimePicker3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DateTimePicker3.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateTimePicker3.IsPopupCalendarOpen = False
        Me.DateTimePicker3.Location = New System.Drawing.Point(885, 107)
        Me.DateTimePicker3.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.DateTimePicker3.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateTimePicker3.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker3.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateTimePicker3.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateTimePicker3.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker3.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.DateTimePicker3.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.DateTimePicker3.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateTimePicker3.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker3.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateTimePicker3.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateTimePicker3.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateTimePicker3.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateTimePicker3.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker3.MonthCalendar.TodayButtonVisible = True
        Me.DateTimePicker3.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(100, 22)
        Me.DateTimePicker3.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.DateTimePicker3.TabIndex = 188
        '
        'DateTimePicker1
        '
        '
        '
        '
        Me.DateTimePicker1.BackgroundStyle.Class = "TextBoxBorder"
        Me.DateTimePicker1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker1.ButtonClear.Visible = True
        Me.DateTimePicker1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DateTimePicker1.Location = New System.Drawing.Point(613, 107)
        Me.DateTimePicker1.Mask = "90:00"
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(158, 23)
        Me.DateTimePicker1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.DateTimePicker1.TabIndex = 187
        Me.DateTimePicker1.Text = "0000"
        Me.DateTimePicker1.ValidatingType = GetType(Date)
        '
        'DateTimePicker2
        '
        '
        '
        '
        Me.DateTimePicker2.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateTimePicker2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker2.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateTimePicker2.ButtonDropDown.Visible = True
        Me.DateTimePicker2.CustomFormat = "dd-MM-yyyy"
        Me.DateTimePicker2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DateTimePicker2.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateTimePicker2.IsPopupCalendarOpen = False
        Me.DateTimePicker2.Location = New System.Drawing.Point(885, 80)
        Me.DateTimePicker2.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.DateTimePicker2.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateTimePicker2.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker2.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateTimePicker2.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateTimePicker2.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker2.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.DateTimePicker2.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.DateTimePicker2.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateTimePicker2.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.DateTimePicker2.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateTimePicker2.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateTimePicker2.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateTimePicker2.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateTimePicker2.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker2.MonthCalendar.TodayButtonVisible = True
        Me.DateTimePicker2.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(100, 22)
        Me.DateTimePicker2.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.DateTimePicker2.TabIndex = 186
        '
        'DateTimePicker4
        '
        '
        '
        '
        Me.DateTimePicker4.BackgroundStyle.Class = "TextBoxBorder"
        Me.DateTimePicker4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker4.ButtonClear.Visible = True
        Me.DateTimePicker4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.DateTimePicker4.Location = New System.Drawing.Point(613, 80)
        Me.DateTimePicker4.Mask = "90:00"
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(158, 23)
        Me.DateTimePicker4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.DateTimePicker4.TabIndex = 184
        Me.DateTimePicker4.Text = "0000"
        Me.DateTimePicker4.ValidatingType = GetType(Date)
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "booking"
        Me.GridControl2.DataSource = Me.Ykpdtset1
        Me.GridControl2.Location = New System.Drawing.Point(12, 12)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(487, 214)
        Me.GridControl2.TabIndex = 6
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBNO, Me.colBLANDNO, Me.colBCTNTYPE, Me.colBCTNWORD, Me.colBNOTIFY, Me.colBSCN, Me.colBCOM, Me.colBDESCRIPT, Me.colBCONSI, Me.colBTYPEMOVE, Me.colBSHIP, Me.colBOOKINGID, Me.colBFORWARDER, Me.colBSHIPNAME, Me.colBCONSINAME, Me.colBFORWARDERNAME, Me.colBTUG, Me.colBGROSS, Me.colBLOCALFOR, Me.colBCTNNO, Me.colBVOYAGE, Me.colBCONFIRMDAY, Me.colBCONFIRMTIME, Me.colPOL, Me.colTSPORT, Me.colREMARK, Me.colBFEDS, Me.colBFED, Me.colBMOTH, Me.colBMOTHS, Me.colBTRANSHIP, Me.colBTRANSHIPS, Me.colBPORTDIS, Me.colBPORTDISS, Me.colBTRANSHIP2, Me.colBTRANSHIP2S, Me.colBDELIVERY, Me.colBDELIVERYS, Me.colBFINALDESTS, Me.colBFINALDEST, Me.colBNSHIPNAME, Me.colBNFORWARDERNAME, Me.colBNICKAGENT, Me.colBINV, Me.colBDIS, Me.colBCHECKLOAD, Me.colBINVOICE, Me.colBINCOTERM, Me.colBREMARK, Me.colBPENANG, Me.colBNETGROSS, Me.GridColumn1})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colBNO
        '
        Me.colBNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNO.AppearanceCell.Options.UseFont = True
        Me.colBNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNO.AppearanceHeader.Options.UseFont = True
        Me.colBNO.Caption = "Booking No."
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Visible = True
        Me.colBNO.VisibleIndex = 0
        Me.colBNO.Width = 268
        '
        'colBLANDNO
        '
        Me.colBLANDNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBLANDNO.AppearanceCell.Options.UseFont = True
        Me.colBLANDNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBLANDNO.AppearanceHeader.Options.UseFont = True
        Me.colBLANDNO.FieldName = "BLANDNO"
        Me.colBLANDNO.Name = "colBLANDNO"
        Me.colBLANDNO.OptionsColumn.AllowEdit = False
        Me.colBLANDNO.OptionsColumn.ReadOnly = True
        Me.colBLANDNO.Width = 44
        '
        'colBCTNTYPE
        '
        Me.colBCTNTYPE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCTNTYPE.AppearanceCell.Options.UseFont = True
        Me.colBCTNTYPE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCTNTYPE.AppearanceHeader.Options.UseFont = True
        Me.colBCTNTYPE.FieldName = "BCTNTYPE"
        Me.colBCTNTYPE.Name = "colBCTNTYPE"
        Me.colBCTNTYPE.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPE.OptionsColumn.ReadOnly = True
        Me.colBCTNTYPE.Width = 44
        '
        'colBCTNWORD
        '
        Me.colBCTNWORD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCTNWORD.AppearanceCell.Options.UseFont = True
        Me.colBCTNWORD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCTNWORD.AppearanceHeader.Options.UseFont = True
        Me.colBCTNWORD.FieldName = "BCTNWORD"
        Me.colBCTNWORD.Name = "colBCTNWORD"
        Me.colBCTNWORD.OptionsColumn.AllowEdit = False
        Me.colBCTNWORD.OptionsColumn.ReadOnly = True
        Me.colBCTNWORD.Width = 44
        '
        'colBNOTIFY
        '
        Me.colBNOTIFY.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNOTIFY.AppearanceCell.Options.UseFont = True
        Me.colBNOTIFY.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNOTIFY.AppearanceHeader.Options.UseFont = True
        Me.colBNOTIFY.FieldName = "BNOTIFY"
        Me.colBNOTIFY.Name = "colBNOTIFY"
        Me.colBNOTIFY.OptionsColumn.AllowEdit = False
        Me.colBNOTIFY.OptionsColumn.ReadOnly = True
        Me.colBNOTIFY.Width = 44
        '
        'colBSCN
        '
        Me.colBSCN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSCN.AppearanceCell.Options.UseFont = True
        Me.colBSCN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSCN.AppearanceHeader.Options.UseFont = True
        Me.colBSCN.FieldName = "BSCN"
        Me.colBSCN.Name = "colBSCN"
        Me.colBSCN.OptionsColumn.AllowEdit = False
        Me.colBSCN.OptionsColumn.ReadOnly = True
        '
        'colBCOM
        '
        Me.colBCOM.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCOM.AppearanceCell.Options.UseFont = True
        Me.colBCOM.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCOM.AppearanceHeader.Options.UseFont = True
        Me.colBCOM.FieldName = "BCOM"
        Me.colBCOM.Name = "colBCOM"
        Me.colBCOM.OptionsColumn.AllowEdit = False
        Me.colBCOM.OptionsColumn.ReadOnly = True
        Me.colBCOM.Width = 44
        '
        'colBDESCRIPT
        '
        Me.colBDESCRIPT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDESCRIPT.AppearanceCell.Options.UseFont = True
        Me.colBDESCRIPT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDESCRIPT.AppearanceHeader.Options.UseFont = True
        Me.colBDESCRIPT.FieldName = "BDESCRIPT"
        Me.colBDESCRIPT.Name = "colBDESCRIPT"
        Me.colBDESCRIPT.OptionsColumn.AllowEdit = False
        Me.colBDESCRIPT.OptionsColumn.ReadOnly = True
        Me.colBDESCRIPT.Width = 44
        '
        'colBCONSI
        '
        Me.colBCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONSI.AppearanceCell.Options.UseFont = True
        Me.colBCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONSI.AppearanceHeader.Options.UseFont = True
        Me.colBCONSI.FieldName = "BCONSI"
        Me.colBCONSI.Name = "colBCONSI"
        Me.colBCONSI.OptionsColumn.AllowEdit = False
        Me.colBCONSI.OptionsColumn.ReadOnly = True
        '
        'colBTYPEMOVE
        '
        Me.colBTYPEMOVE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTYPEMOVE.AppearanceCell.Options.UseFont = True
        Me.colBTYPEMOVE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTYPEMOVE.AppearanceHeader.Options.UseFont = True
        Me.colBTYPEMOVE.FieldName = "BTYPEMOVE"
        Me.colBTYPEMOVE.Name = "colBTYPEMOVE"
        Me.colBTYPEMOVE.OptionsColumn.AllowEdit = False
        Me.colBTYPEMOVE.OptionsColumn.ReadOnly = True
        Me.colBTYPEMOVE.Width = 44
        '
        'colBSHIP
        '
        Me.colBSHIP.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSHIP.AppearanceCell.Options.UseFont = True
        Me.colBSHIP.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSHIP.AppearanceHeader.Options.UseFont = True
        Me.colBSHIP.FieldName = "BSHIP"
        Me.colBSHIP.Name = "colBSHIP"
        Me.colBSHIP.OptionsColumn.AllowEdit = False
        Me.colBSHIP.OptionsColumn.ReadOnly = True
        Me.colBSHIP.Width = 44
        '
        'colBOOKINGID
        '
        Me.colBOOKINGID.FieldName = "BOOKINGID"
        Me.colBOOKINGID.Name = "colBOOKINGID"
        '
        'colBFORWARDER
        '
        Me.colBFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colBFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colBFORWARDER.FieldName = "BFORWARDER"
        Me.colBFORWARDER.Name = "colBFORWARDER"
        Me.colBFORWARDER.OptionsColumn.AllowEdit = False
        Me.colBFORWARDER.OptionsColumn.ReadOnly = True
        Me.colBFORWARDER.Width = 44
        '
        'colBSHIPNAME
        '
        Me.colBSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colBSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colBSHIPNAME.Caption = "Shipper"
        Me.colBSHIPNAME.FieldName = "BSHIPNAME"
        Me.colBSHIPNAME.Name = "colBSHIPNAME"
        Me.colBSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colBSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colBSHIPNAME.Visible = True
        Me.colBSHIPNAME.VisibleIndex = 1
        Me.colBSHIPNAME.Width = 456
        '
        'colBCONSINAME
        '
        Me.colBCONSINAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONSINAME.AppearanceCell.Options.UseFont = True
        Me.colBCONSINAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONSINAME.AppearanceHeader.Options.UseFont = True
        Me.colBCONSINAME.FieldName = "BCONSINAME"
        Me.colBCONSINAME.Name = "colBCONSINAME"
        Me.colBCONSINAME.OptionsColumn.AllowEdit = False
        Me.colBCONSINAME.OptionsColumn.ReadOnly = True
        '
        'colBFORWARDERNAME
        '
        Me.colBFORWARDERNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDERNAME.AppearanceCell.Options.UseFont = True
        Me.colBFORWARDERNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFORWARDERNAME.AppearanceHeader.Options.UseFont = True
        Me.colBFORWARDERNAME.FieldName = "BFORWARDERNAME"
        Me.colBFORWARDERNAME.Name = "colBFORWARDERNAME"
        Me.colBFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBFORWARDERNAME.Width = 44
        '
        'colBTUG
        '
        Me.colBTUG.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTUG.AppearanceCell.Options.UseFont = True
        Me.colBTUG.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTUG.AppearanceHeader.Options.UseFont = True
        Me.colBTUG.FieldName = "BTUG"
        Me.colBTUG.Name = "colBTUG"
        Me.colBTUG.OptionsColumn.AllowEdit = False
        Me.colBTUG.OptionsColumn.ReadOnly = True
        Me.colBTUG.Width = 44
        '
        'colBGROSS
        '
        Me.colBGROSS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBGROSS.AppearanceCell.Options.UseFont = True
        Me.colBGROSS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBGROSS.AppearanceHeader.Options.UseFont = True
        Me.colBGROSS.FieldName = "BGROSS"
        Me.colBGROSS.Name = "colBGROSS"
        Me.colBGROSS.OptionsColumn.AllowEdit = False
        Me.colBGROSS.OptionsColumn.ReadOnly = True
        Me.colBGROSS.Width = 44
        '
        'colBLOCALFOR
        '
        Me.colBLOCALFOR.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBLOCALFOR.AppearanceCell.Options.UseFont = True
        Me.colBLOCALFOR.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBLOCALFOR.AppearanceHeader.Options.UseFont = True
        Me.colBLOCALFOR.FieldName = "BLOCALFOR"
        Me.colBLOCALFOR.Name = "colBLOCALFOR"
        Me.colBLOCALFOR.OptionsColumn.AllowEdit = False
        Me.colBLOCALFOR.OptionsColumn.ReadOnly = True
        Me.colBLOCALFOR.Width = 44
        '
        'colBCTNNO
        '
        Me.colBCTNNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCTNNO.AppearanceCell.Options.UseFont = True
        Me.colBCTNNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCTNNO.AppearanceHeader.Options.UseFont = True
        Me.colBCTNNO.FieldName = "BCTNNO"
        Me.colBCTNNO.Name = "colBCTNNO"
        Me.colBCTNNO.OptionsColumn.AllowEdit = False
        Me.colBCTNNO.OptionsColumn.ReadOnly = True
        Me.colBCTNNO.Width = 476
        '
        'colBVOYAGE
        '
        Me.colBVOYAGE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBVOYAGE.AppearanceCell.Options.UseFont = True
        Me.colBVOYAGE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBVOYAGE.AppearanceHeader.Options.UseFont = True
        Me.colBVOYAGE.FieldName = "BVOYAGE"
        Me.colBVOYAGE.Name = "colBVOYAGE"
        Me.colBVOYAGE.OptionsColumn.AllowEdit = False
        Me.colBVOYAGE.OptionsColumn.ReadOnly = True
        Me.colBVOYAGE.Width = 44
        '
        'colBCONFIRMDAY
        '
        Me.colBCONFIRMDAY.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMDAY.AppearanceCell.Options.UseFont = True
        Me.colBCONFIRMDAY.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMDAY.AppearanceHeader.Options.UseFont = True
        Me.colBCONFIRMDAY.FieldName = "BCONFIRMDAY"
        Me.colBCONFIRMDAY.Name = "colBCONFIRMDAY"
        Me.colBCONFIRMDAY.OptionsColumn.AllowEdit = False
        Me.colBCONFIRMDAY.OptionsColumn.ReadOnly = True
        Me.colBCONFIRMDAY.Width = 44
        '
        'colBCONFIRMTIME
        '
        Me.colBCONFIRMTIME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMTIME.AppearanceCell.Options.UseFont = True
        Me.colBCONFIRMTIME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCONFIRMTIME.AppearanceHeader.Options.UseFont = True
        Me.colBCONFIRMTIME.FieldName = "BCONFIRMTIME"
        Me.colBCONFIRMTIME.Name = "colBCONFIRMTIME"
        Me.colBCONFIRMTIME.OptionsColumn.AllowEdit = False
        Me.colBCONFIRMTIME.OptionsColumn.ReadOnly = True
        Me.colBCONFIRMTIME.Width = 44
        '
        'colPOL
        '
        Me.colPOL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colPOL.AppearanceCell.Options.UseFont = True
        Me.colPOL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colPOL.AppearanceHeader.Options.UseFont = True
        Me.colPOL.FieldName = "POL"
        Me.colPOL.Name = "colPOL"
        Me.colPOL.OptionsColumn.AllowEdit = False
        Me.colPOL.OptionsColumn.ReadOnly = True
        Me.colPOL.Width = 44
        '
        'colTSPORT
        '
        Me.colTSPORT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTSPORT.AppearanceCell.Options.UseFont = True
        Me.colTSPORT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colTSPORT.AppearanceHeader.Options.UseFont = True
        Me.colTSPORT.FieldName = "TSPORT"
        Me.colTSPORT.Name = "colTSPORT"
        Me.colTSPORT.OptionsColumn.AllowEdit = False
        Me.colTSPORT.OptionsColumn.ReadOnly = True
        '
        'colREMARK
        '
        Me.colREMARK.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colREMARK.AppearanceCell.Options.UseFont = True
        Me.colREMARK.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colREMARK.AppearanceHeader.Options.UseFont = True
        Me.colREMARK.FieldName = "REMARK"
        Me.colREMARK.Name = "colREMARK"
        Me.colREMARK.OptionsColumn.AllowEdit = False
        Me.colREMARK.OptionsColumn.ReadOnly = True
        '
        'colBFEDS
        '
        Me.colBFEDS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFEDS.AppearanceCell.Options.UseFont = True
        Me.colBFEDS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFEDS.AppearanceHeader.Options.UseFont = True
        Me.colBFEDS.FieldName = "BFEDS"
        Me.colBFEDS.Name = "colBFEDS"
        Me.colBFEDS.OptionsColumn.AllowEdit = False
        Me.colBFEDS.OptionsColumn.ReadOnly = True
        '
        'colBFED
        '
        Me.colBFED.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFED.AppearanceCell.Options.UseFont = True
        Me.colBFED.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFED.AppearanceHeader.Options.UseFont = True
        Me.colBFED.FieldName = "BFED"
        Me.colBFED.Name = "colBFED"
        Me.colBFED.OptionsColumn.AllowEdit = False
        Me.colBFED.OptionsColumn.ReadOnly = True
        '
        'colBMOTH
        '
        Me.colBMOTH.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBMOTH.AppearanceCell.Options.UseFont = True
        Me.colBMOTH.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBMOTH.AppearanceHeader.Options.UseFont = True
        Me.colBMOTH.FieldName = "BMOTH"
        Me.colBMOTH.Name = "colBMOTH"
        Me.colBMOTH.OptionsColumn.AllowEdit = False
        Me.colBMOTH.OptionsColumn.ReadOnly = True
        '
        'colBMOTHS
        '
        Me.colBMOTHS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBMOTHS.AppearanceCell.Options.UseFont = True
        Me.colBMOTHS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBMOTHS.AppearanceHeader.Options.UseFont = True
        Me.colBMOTHS.FieldName = "BMOTHS"
        Me.colBMOTHS.Name = "colBMOTHS"
        Me.colBMOTHS.OptionsColumn.AllowEdit = False
        Me.colBMOTHS.OptionsColumn.ReadOnly = True
        '
        'colBTRANSHIP
        '
        Me.colBTRANSHIP.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIP.AppearanceCell.Options.UseFont = True
        Me.colBTRANSHIP.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIP.AppearanceHeader.Options.UseFont = True
        Me.colBTRANSHIP.FieldName = "BTRANSHIP"
        Me.colBTRANSHIP.Name = "colBTRANSHIP"
        Me.colBTRANSHIP.OptionsColumn.AllowEdit = False
        Me.colBTRANSHIP.OptionsColumn.ReadOnly = True
        Me.colBTRANSHIP.Width = 44
        '
        'colBTRANSHIPS
        '
        Me.colBTRANSHIPS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIPS.AppearanceCell.Options.UseFont = True
        Me.colBTRANSHIPS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIPS.AppearanceHeader.Options.UseFont = True
        Me.colBTRANSHIPS.FieldName = "BTRANSHIPS"
        Me.colBTRANSHIPS.Name = "colBTRANSHIPS"
        Me.colBTRANSHIPS.OptionsColumn.AllowEdit = False
        Me.colBTRANSHIPS.OptionsColumn.ReadOnly = True
        '
        'colBPORTDIS
        '
        Me.colBPORTDIS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBPORTDIS.AppearanceCell.Options.UseFont = True
        Me.colBPORTDIS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBPORTDIS.AppearanceHeader.Options.UseFont = True
        Me.colBPORTDIS.FieldName = "BPORTDIS"
        Me.colBPORTDIS.Name = "colBPORTDIS"
        Me.colBPORTDIS.OptionsColumn.AllowEdit = False
        Me.colBPORTDIS.OptionsColumn.ReadOnly = True
        '
        'colBPORTDISS
        '
        Me.colBPORTDISS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBPORTDISS.AppearanceCell.Options.UseFont = True
        Me.colBPORTDISS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBPORTDISS.AppearanceHeader.Options.UseFont = True
        Me.colBPORTDISS.FieldName = "BPORTDISS"
        Me.colBPORTDISS.Name = "colBPORTDISS"
        Me.colBPORTDISS.OptionsColumn.AllowEdit = False
        Me.colBPORTDISS.OptionsColumn.ReadOnly = True
        Me.colBPORTDISS.Width = 44
        '
        'colBTRANSHIP2
        '
        Me.colBTRANSHIP2.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIP2.AppearanceCell.Options.UseFont = True
        Me.colBTRANSHIP2.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIP2.AppearanceHeader.Options.UseFont = True
        Me.colBTRANSHIP2.FieldName = "BTRANSHIP2"
        Me.colBTRANSHIP2.Name = "colBTRANSHIP2"
        Me.colBTRANSHIP2.OptionsColumn.AllowEdit = False
        Me.colBTRANSHIP2.OptionsColumn.ReadOnly = True
        '
        'colBTRANSHIP2S
        '
        Me.colBTRANSHIP2S.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIP2S.AppearanceCell.Options.UseFont = True
        Me.colBTRANSHIP2S.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBTRANSHIP2S.AppearanceHeader.Options.UseFont = True
        Me.colBTRANSHIP2S.FieldName = "BTRANSHIP2S"
        Me.colBTRANSHIP2S.Name = "colBTRANSHIP2S"
        Me.colBTRANSHIP2S.OptionsColumn.AllowEdit = False
        Me.colBTRANSHIP2S.OptionsColumn.ReadOnly = True
        '
        'colBDELIVERY
        '
        Me.colBDELIVERY.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDELIVERY.AppearanceCell.Options.UseFont = True
        Me.colBDELIVERY.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDELIVERY.AppearanceHeader.Options.UseFont = True
        Me.colBDELIVERY.FieldName = "BDELIVERY"
        Me.colBDELIVERY.Name = "colBDELIVERY"
        Me.colBDELIVERY.OptionsColumn.AllowEdit = False
        Me.colBDELIVERY.OptionsColumn.ReadOnly = True
        Me.colBDELIVERY.Width = 44
        '
        'colBDELIVERYS
        '
        Me.colBDELIVERYS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDELIVERYS.AppearanceCell.Options.UseFont = True
        Me.colBDELIVERYS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDELIVERYS.AppearanceHeader.Options.UseFont = True
        Me.colBDELIVERYS.FieldName = "BDELIVERYS"
        Me.colBDELIVERYS.Name = "colBDELIVERYS"
        Me.colBDELIVERYS.OptionsColumn.AllowEdit = False
        Me.colBDELIVERYS.OptionsColumn.ReadOnly = True
        '
        'colBFINALDESTS
        '
        Me.colBFINALDESTS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFINALDESTS.AppearanceCell.Options.UseFont = True
        Me.colBFINALDESTS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFINALDESTS.AppearanceHeader.Options.UseFont = True
        Me.colBFINALDESTS.FieldName = "BFINALDESTS"
        Me.colBFINALDESTS.Name = "colBFINALDESTS"
        Me.colBFINALDESTS.OptionsColumn.AllowEdit = False
        Me.colBFINALDESTS.OptionsColumn.ReadOnly = True
        Me.colBFINALDESTS.Width = 87
        '
        'colBFINALDEST
        '
        Me.colBFINALDEST.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFINALDEST.AppearanceCell.Options.UseFont = True
        Me.colBFINALDEST.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBFINALDEST.AppearanceHeader.Options.UseFont = True
        Me.colBFINALDEST.FieldName = "BFINALDEST"
        Me.colBFINALDEST.Name = "colBFINALDEST"
        Me.colBFINALDEST.OptionsColumn.AllowEdit = False
        Me.colBFINALDEST.OptionsColumn.ReadOnly = True
        Me.colBFINALDEST.Width = 40
        '
        'colBNSHIPNAME
        '
        Me.colBNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colBNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colBNSHIPNAME.FieldName = "BNSHIPNAME"
        Me.colBNSHIPNAME.Name = "colBNSHIPNAME"
        Me.colBNSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colBNSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colBNSHIPNAME.Width = 40
        '
        'colBNFORWARDERNAME
        '
        Me.colBNFORWARDERNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNFORWARDERNAME.AppearanceCell.Options.UseFont = True
        Me.colBNFORWARDERNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNFORWARDERNAME.AppearanceHeader.Options.UseFont = True
        Me.colBNFORWARDERNAME.Caption = "Forwarder"
        Me.colBNFORWARDERNAME.FieldName = "BNFORWARDERNAME"
        Me.colBNFORWARDERNAME.Name = "colBNFORWARDERNAME"
        Me.colBNFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBNFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBNFORWARDERNAME.Visible = True
        Me.colBNFORWARDERNAME.VisibleIndex = 2
        Me.colBNFORWARDERNAME.Width = 314
        '
        'colBNICKAGENT
        '
        Me.colBNICKAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNICKAGENT.AppearanceCell.Options.UseFont = True
        Me.colBNICKAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNICKAGENT.AppearanceHeader.Options.UseFont = True
        Me.colBNICKAGENT.Caption = "Agent"
        Me.colBNICKAGENT.FieldName = "BNICKAGENT"
        Me.colBNICKAGENT.Name = "colBNICKAGENT"
        Me.colBNICKAGENT.OptionsColumn.AllowEdit = False
        Me.colBNICKAGENT.OptionsColumn.ReadOnly = True
        Me.colBNICKAGENT.Visible = True
        Me.colBNICKAGENT.VisibleIndex = 4
        Me.colBNICKAGENT.Width = 279
        '
        'colBINV
        '
        Me.colBINV.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINV.AppearanceCell.Options.UseFont = True
        Me.colBINV.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINV.AppearanceHeader.Options.UseFont = True
        Me.colBINV.FieldName = "BINV"
        Me.colBINV.Name = "colBINV"
        Me.colBINV.OptionsColumn.AllowEdit = False
        Me.colBINV.OptionsColumn.ReadOnly = True
        Me.colBINV.Width = 40
        '
        'colBDIS
        '
        Me.colBDIS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDIS.AppearanceCell.Options.UseFont = True
        Me.colBDIS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBDIS.AppearanceHeader.Options.UseFont = True
        Me.colBDIS.FieldName = "BDIS"
        Me.colBDIS.Name = "colBDIS"
        Me.colBDIS.OptionsColumn.AllowEdit = False
        Me.colBDIS.OptionsColumn.ReadOnly = True
        Me.colBDIS.Width = 40
        '
        'colBCHECKLOAD
        '
        Me.colBCHECKLOAD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCHECKLOAD.AppearanceCell.Options.UseFont = True
        Me.colBCHECKLOAD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBCHECKLOAD.AppearanceHeader.Options.UseFont = True
        Me.colBCHECKLOAD.FieldName = "BCHECKLOAD"
        Me.colBCHECKLOAD.Name = "colBCHECKLOAD"
        Me.colBCHECKLOAD.OptionsColumn.AllowEdit = False
        Me.colBCHECKLOAD.OptionsColumn.ReadOnly = True
        Me.colBCHECKLOAD.Width = 40
        '
        'colBINVOICE
        '
        Me.colBINVOICE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINVOICE.AppearanceCell.Options.UseFont = True
        Me.colBINVOICE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINVOICE.AppearanceHeader.Options.UseFont = True
        Me.colBINVOICE.FieldName = "BINVOICE"
        Me.colBINVOICE.Name = "colBINVOICE"
        Me.colBINVOICE.OptionsColumn.AllowEdit = False
        Me.colBINVOICE.OptionsColumn.ReadOnly = True
        Me.colBINVOICE.Width = 40
        '
        'colBINCOTERM
        '
        Me.colBINCOTERM.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINCOTERM.AppearanceCell.Options.UseFont = True
        Me.colBINCOTERM.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBINCOTERM.AppearanceHeader.Options.UseFont = True
        Me.colBINCOTERM.FieldName = "BINCOTERM"
        Me.colBINCOTERM.Name = "colBINCOTERM"
        Me.colBINCOTERM.OptionsColumn.AllowEdit = False
        Me.colBINCOTERM.OptionsColumn.ReadOnly = True
        Me.colBINCOTERM.Width = 356
        '
        'colBREMARK
        '
        Me.colBREMARK.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBREMARK.AppearanceCell.Options.UseFont = True
        Me.colBREMARK.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBREMARK.AppearanceHeader.Options.UseFont = True
        Me.colBREMARK.FieldName = "BREMARK"
        Me.colBREMARK.Name = "colBREMARK"
        Me.colBREMARK.OptionsColumn.AllowEdit = False
        Me.colBREMARK.OptionsColumn.ReadOnly = True
        Me.colBREMARK.Width = 356
        '
        'colBPENANG
        '
        Me.colBPENANG.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBPENANG.AppearanceCell.Options.UseFont = True
        Me.colBPENANG.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBPENANG.AppearanceHeader.Options.UseFont = True
        Me.colBPENANG.FieldName = "BPENANG"
        Me.colBPENANG.Name = "colBPENANG"
        Me.colBPENANG.OptionsColumn.AllowEdit = False
        Me.colBPENANG.OptionsColumn.ReadOnly = True
        Me.colBPENANG.Width = 359
        '
        'colBNETGROSS
        '
        Me.colBNETGROSS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNETGROSS.AppearanceCell.Options.UseFont = True
        Me.colBNETGROSS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colBNETGROSS.AppearanceHeader.Options.UseFont = True
        Me.colBNETGROSS.FieldName = "BNETGROSS"
        Me.colBNETGROSS.Name = "colBNETGROSS"
        Me.colBNETGROSS.OptionsColumn.AllowEdit = False
        Me.colBNETGROSS.OptionsColumn.ReadOnly = True
        Me.colBNETGROSS.Width = 48
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.Caption = "FCL"
        Me.GridColumn1.FieldName = "GridColumn1"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        Me.GridColumn1.UnboundExpression = "Concat([BCTNNO], ' X ', [BCTNTYPE])"
        Me.GridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 3
        Me.GridColumn1.Width = 270
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(613, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.voyageBindingSource
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(372, 40)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 4
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset3
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYAGEID.AppearanceCell.Options.UseFont = True
        Me.colVOYAGEID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYAGEID.AppearanceHeader.Options.UseFont = True
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        Me.colVOYAGEID.Width = 153
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        Me.colVOYVESIDN.Width = 164
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDS.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDS.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        Me.colVOYVESIDS.Width = 164
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        Me.colVOYDATESN.Width = 164
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESS.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESS.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        Me.colVOYDATESS.Width = 164
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEEN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        Me.colVOYDATEEN.Width = 164
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMEN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 1
        Me.colVOYVESNAMEN.Width = 523
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEES.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEES.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        Me.colVOYDATEES.Width = 164
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMES.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMES.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 2
        Me.colVOYVESNAMES.Width = 566
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYTIMEHHMMNN.AppearanceCell.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME.AppearanceCell.Options.UseFont = True
        Me.colVOYNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME.AppearanceHeader.Options.UseFont = True
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        Me.colVOYNAME.Width = 345
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.EmptySpaceItem1, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.EmptySpaceItem2, Me.LayoutControlItem21, Me.LayoutControlItem8, Me.LayoutControlItem11, Me.LayoutControlItem2, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem4, Me.LayoutControlItem1, Me.EmptySpaceItem3})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(997, 586)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.GridControl2
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(491, 218)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem6.Control = Me.DateTimePicker4
        Me.LayoutControlItem6.Location = New System.Drawing.Point(491, 68)
        Me.LayoutControlItem6.MinSize = New System.Drawing.Size(209, 24)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(272, 27)
        Me.LayoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem6.Text = "�����͡"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem7.Control = Me.DateTimePicker2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(763, 68)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(214, 27)
        Me.LayoutControlItem7.Text = "�ѹ���"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.DateTimePicker3
        Me.LayoutControlItem9.Location = New System.Drawing.Point(763, 95)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(214, 27)
        Me.LayoutControlItem9.Text = "�ѹ���"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem10.Control = Me.txt_booking_no
        Me.LayoutControlItem10.Location = New System.Drawing.Point(763, 122)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem10.Text = "Booking No."
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem12.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem12.Control = Me.txt_container
        Me.LayoutControlItem12.Location = New System.Drawing.Point(491, 148)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem12.Text = "Container"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem13.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem13.Control = Me.txt_customer
        Me.LayoutControlItem13.Location = New System.Drawing.Point(763, 148)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem13.Text = "Shipper"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(107, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(491, 330)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(486, 236)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.Control = Me.txt_sealid
        Me.LayoutControlItem14.Location = New System.Drawing.Point(491, 174)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem14.Text = "Seal ID"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.port_discharge
        Me.LayoutControlItem15.Location = New System.Drawing.Point(763, 174)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem15.Text = "������͢��"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem16.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem16.Control = Me.TextBox1
        Me.LayoutControlItem16.Location = New System.Drawing.Point(491, 226)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(215, 26)
        Me.LayoutControlItem16.Text = "Type"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem17.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem17.Control = Me.txt_insurance
        Me.LayoutControlItem17.Location = New System.Drawing.Point(763, 226)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem17.Text = "Insurance"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.TextBox4
        Me.LayoutControlItem18.Location = New System.Drawing.Point(706, 226)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(57, 26)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.RadioGroup1
        Me.LayoutControlItem19.Location = New System.Drawing.Point(491, 252)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(119, 51)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.SimpleButton1
        Me.LayoutControlItem20.Location = New System.Drawing.Point(843, 303)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(134, 27)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(491, 303)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(352, 27)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.GridControl3
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 244)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(491, 322)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem8.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem8.Control = Me.DateTimePicker1
        Me.LayoutControlItem8.Location = New System.Drawing.Point(491, 95)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(272, 27)
        Me.LayoutControlItem8.Text = "�������"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem11.Control = Me.txt_voyage
        Me.LayoutControlItem11.Location = New System.Drawing.Point(491, 122)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem11.Text = "Voyage"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.SearchControl1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 218)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(491, 26)
        Me.LayoutControlItem2.Text = "����"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem22.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem22.Control = Me.port_loading
        Me.LayoutControlItem22.Location = New System.Drawing.Point(491, 200)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem22.Text = "��º���"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem23.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem23.Control = Me.TextBox3
        Me.LayoutControlItem23.Location = New System.Drawing.Point(763, 200)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem23.Text = "Weight"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem24.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem24.Control = Me.txt_name_car
        Me.LayoutControlItem24.Location = New System.Drawing.Point(491, 44)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(272, 24)
        Me.LayoutControlItem24.Text = "����ѷö"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.txt_no_car
        Me.LayoutControlItem4.Location = New System.Drawing.Point(763, 44)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(214, 24)
        Me.LayoutControlItem4.Text = "�����Ţ����¹ö"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.slcVoyage
        Me.LayoutControlItem1.Location = New System.Drawing.Point(491, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(486, 44)
        Me.LayoutControlItem1.Text = "Voyage"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(107, 29)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(610, 252)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(367, 51)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'frmadd_gate1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(997, 586)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmadd_gate1"
        Me.Text = "Add Container"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.TextBox3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.port_loading.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_insurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.port_discharge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_sealid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_container.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_booking_no.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTimePicker3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTimePicker2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateTimePicker4 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateTimePicker2 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateTimePicker1 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateTimePicker3 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_customer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_container As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_voyage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_booking_no As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents TextBox4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_insurance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents port_discharge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_sealid As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBLANDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNWORD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNOTIFY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBSCN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCOM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBDESCRIPT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBTYPEMOVE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBSHIP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONSINAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBTUG As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBGROSS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBLOCALFOR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBVOYAGE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONFIRMDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONFIRMTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPOL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTSPORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colREMARK As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFEDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBMOTH As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBMOTHS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBTRANSHIP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBTRANSHIPS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBPORTDIS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBPORTDISS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBTRANSHIP2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBTRANSHIP2S As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBDELIVERY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBDELIVERYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFINALDESTS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBFINALDEST As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNICKAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBINV As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBDIS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCHECKLOAD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBINVOICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBINCOTERM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBREMARK As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBPENANG As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNETGROSS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colCTNMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONDI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNBOOKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNYARD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNUPGRADE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTRBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEALBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEINBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextBox3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents port_loading As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_no_car As TextBox
    Friend WithEvents txt_name_car As TextBox
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colBOOKINGID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
End Class
