﻿Imports System.Globalization

Public Class frmeditvoyage


    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset


    Dim respone As Object
    Dim idvoyn As String
    Dim idvoys As String
    Dim idvoyage As String
    Public Sub New(ByVal id As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = id
        ' Add any initialization after the InitializeComponent() call.
        Dim sql As String
        sql = "Select * from vesmain"


        connectDB.GetTable(sql, ykpset.Tables("vesmain"))

        ComboBox1.Properties.DataSource = ykpset.Tables("vesmain")
        ComboBox2.Properties.DataSource = ykpset.Tables("vesmain")


    End Sub
    Public Sub loadData()
        Dim date_as As Date
        txt_voyage.Text = idvoyage
        Dim sql As String = "Select VOYVESNAMEN,VSSNATION,VSSMASTER,VOYDATESN,VOYDATEEN,VOYVESIDN,VSSNAM,Voyname,vesmainid from voyage join vesmain on voyage.VOYVESIDN = vesmain.VESMAINID where VOYAGEID ='" & idvoyage & "'"
        Dim dt As New DataTable
        connectDB.GetTable(sql, dt)


        For i As Integer = 0 To dt.Rows.Count - 1

            txtVoyageName.Text = dt.Rows(i)("Voyname")
            idvoyn = dt.Rows(i)("VOYVESIDN")
            Try
                Dim dateValue As Date = DateTime.ParseExact(dt.Rows(i)("VOYDATESN"), "dd-MM-yyyy", CultureInfo.InvariantCulture)

                date_eta_penang.EditValue = dateValue
            Catch ex As Exception

            End Try
            Try
                Dim dateValue1 As Date = DateTime.ParseExact(dt.Rows(i)("VOYDATEEN"), "dd-MM-yyyy", CultureInfo.InvariantCulture)

                date_etd_penang.EditValue = dateValue1
            Catch ex As Exception

            End Try
            'date_as = "#" + mySqlReader("VOYDATEEN") + " " + "00:00:00#"

            ComboBox2.EditValue = dt.Rows(i)("vesmainid")


        Next




        sql = "Select * from voyage join vesmain On voyage.VOYVESIDS = vesmain.VESMAINID where VOYAGEID ='" & idvoyage & "' ;"
        dt = New DataTable
        connectDB.GetTable(sql, dt)
        For i As Integer = 0 To dt.Rows.Count - 1

            ComboBox1.EditValue = dt.Rows(i)("vesmainid")

            idvoys = dt.Rows(i)("VOYVESIDS")
            'date_as = "#" + mySqlReader("VOYDATESS") + " " + "00:00:00#"
            'date_krabi_eta.Value = date_as
            Try
                Dim dateValue2 As Date = DateTime.ParseExact(dt.Rows(i)("VOYDATESS"), "dd-MM-yyyy", CultureInfo.InvariantCulture)
                date_krabi_eta.EditValue = dateValue2

            Catch ex As Exception

            End Try

            'date_as = "#" + mySqlReader("VOYDATEES") + " " + "00:00:00#"
            'date_krabi_etd.Value = date_as
            Try
                Dim dateValue3 As Date = DateTime.ParseExact(dt.Rows(i)("VOYDATEES"), "dd-MM-yyyy", CultureInfo.InvariantCulture)

                date_krabi_etd.EditValue = dateValue3

            Catch ex As Exception

            End Try

        Next








    End Sub
    Private Sub frmedit_voyage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        loadData()



    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click

        Dim sql1 As String
        Dim sql As String
        Dim datekrabi As Date = Nothing
        sql1 += " VOYVESIDN = "
        sql1 += " '" & idvoyn & "' , "

        sql1 += " VOYVESIDS = "
        sql1 += " '" & idvoys & "' , "
        sql1 += " VOYDATESN = "
        Try

            datekrabi = date_eta_penang.EditValue
            sql1 += " '" & If(date_eta_penang.Text = "01-01-0001", "01-01-0001", If(date_eta_penang.EditValue Is Nothing, "01-01-0001", datekrabi.ToString("dd-MM-") & datekrabi.Year.ToString())) & "' , "

        Catch ex As Exception
            sql1 += "'01-01-0001' , "
        End Try

        datekrabi = Nothing
        sql1 += " VOYDATESS = "
        Try
            datekrabi = date_krabi_eta.EditValue
            sql1 += " '" & If(date_krabi_eta.Text = "01-01-0001", "01-01-0001", If(date_krabi_eta.EditValue Is Nothing, "01-01-0001", datekrabi.ToString("dd-MM-") & datekrabi.Year.ToString())) & "' , "
        Catch ex As Exception
            sql1 += "'01-01-0001' , "

        End Try

        datekrabi = Nothing

        sql1 += " VOYDATEEN = "
        Try
            datekrabi = date_etd_penang.EditValue
            sql1 += " '" & If(date_etd_penang.Text = "01-01-0001", "01-01-0001", If(date_etd_penang.EditValue Is Nothing, "01-01-0001", datekrabi.ToString("dd-MM-") & datekrabi.Year)) & "' , "
        Catch ex As Exception
            sql1 += "'01-01-0001' , "
        End Try
        datekrabi = Nothing

        sql1 += " VOYDATEES = "
        Try
            datekrabi = date_krabi_etd.EditValue
            sql1 += " '" & If(date_krabi_etd.Text = "01-01-0001", "01-01-0001", If(date_krabi_etd.EditValue Is Nothing, "01-01-0001", datekrabi.ToString("dd-MM-") & datekrabi.Year)) & "' , "
        Catch ex As Exception
            sql1 += "'01-01-0001' , "

        End Try

        sql1 += " VOYNAME = "
        sql1 += " '" & txtVoyageName.Text & "' , "
        sql1 += " VOYVESNAMEN = "
        sql1 += " '" & txt_namemaster.Text & "' , "

        sql1 += " VOYVESNAMES = "
        sql1 += " '" & txt_namemaster2.Text & "'  "


        sql = "UPDATE voyage SET " & sql1 & " WHERE VOYAGEID = '" & idvoyage & "' ;"
        connectDB.ExecuteNonQuery(sql)

        MsgBox("Save Complete")
        Dim cf As New frmsearch_voyage

        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()

    End Sub


    Private Sub ComboBox2_EditValueChanged(sender As Object, e As EventArgs) Handles ComboBox2.EditValueChanged
        Dim sql As String = "Select * from vesmain where VESMAINID = '" & ComboBox2.EditValue & "';"
        Dim dt As New DataTable
        connectDB.GetTable(sql, dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            idvoyn = dt.Rows(i)("VESMAINID")
            txt_nationality.Text = dt.Rows(i)("VSSNATION")
            txt_namemaster.Text = dt.Rows(i)("VSSMASTER")
        Next
    End Sub

    Private Sub ComboBox1_EditValueChanged(sender As Object, e As EventArgs) Handles ComboBox1.EditValueChanged
        Dim sql As String = "Select * from vesmain where VESMAINID = '" & ComboBox1.EditValue & "';"
        Dim dt As New DataTable
        connectDB.GetTable(sql, dt)
        For i As Integer = 0 To dt.Rows.Count - 1


            idvoys = dt.Rows(i)("VESMAINID")
            txt_nationality2.Text = dt.Rows(i)("VSSNATION")
            txt_namemaster2.Text = dt.Rows(i)("VSSMASTER")
        Next

    End Sub
End Class