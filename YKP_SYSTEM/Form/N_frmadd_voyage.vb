﻿Imports MySql.Data.MySqlClient
Public Class N_frmadd_voyage




    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim respone As Object
    Dim idvoys As String
    Dim idvoyn As String
    Dim dtset As New ykpdtset
    Public Sub loadVoyageNumber()
        Dim sql As String
        sql = "SELECT * FROM mascmrunning;"
        connectDB.GetTable(sql, dtset.Tables("mascmrunning"))
        slcVoyage.Properties.DataSource = dtset.Tables("mascmrunning")
        slcVoyage.EditValue = 1
    End Sub
    Private Sub frmadd_voyage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("en-US")
        loadVoyageNumber()

        Dim dt As New DataTable
        connectDB.GetTable("Select * from vesmain;", dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            ComboBox1.Items.Add(dt.Rows(i)("VSSNAM").ToString)
            ComboBox2.Items.Add(dt.Rows(i)("VSSNAM").ToString)
        Next



        'Dim dt1 As New DataTable
        'connectDB.GetTable("SELECT * FROM voyage WHERE voyname REGEXP '^[0-9]+$'  order by VOYAGEID DESC LIMIT 1", dt1)
        'For i As Integer = 0 To dt1.Rows.Count - 1
        '    txt_voyage.Text = Format(CInt(dt1.Rows(i)("VOYAGEID")) + 1, "000")
        '    txtVoyageName.Text = Format(CInt(dt1.Rows(i)("VOYAGEID")) + 1, "000")
        'Next





    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click

        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String


        sql1 = "VOYVESIDN , "
        sql2 = " '" & idvoyn & "' , "


        sql1 += " VOYVESIDS , "
        sql2 += " '" & idvoys & "' , "

        sql1 += " VOYDATESN , "
        sql2 += " '" & date_eta_penang.Value.Date.ToString("dd-MM-yyyy") & "' , "

        sql1 += " VOYDATESS , "
        sql2 += " '" & date_krabi_eta.Value.Date.ToString("dd-MM-yyyy") & "' , "




        sql1 += " VOYDATEEN , "
        sql2 += " '" & date_etd_penang.Value.Date.ToString("dd-MM-yyyy") & "' , "



        sql1 += " VOYDATEES , "
        sql2 += " '" & date_krabi_etd.Value.Date.ToString("dd-MM-yyyy") & "' , "



        sql1 += " VOYVESNAMEN , "
        sql2 += " '" & txt_namemaster.Text & "' , "


        sql1 += " VOYNAME , "
        sql2 += " '" & If(CheckBox1.Checked = True, txtVoyageName.Text, GENDOC.getIDform(slcVoyage.Text)) & "' , "


        sql1 += " VOYVESNAMES "
        sql2 += " '" & txt_namemaster2.Text & "'  "




        sql = "INSERT INTO voyage ( " & sql1 & ") VALUES ( " & sql2 & " );"
        connectDB.ExecuteNonQuery(sql)
        MsgBox("Save Complete")
        Dim cf As New frmsearch_voyage

        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

        Dim dt As New DataTable
        connectDB.GetTable("Select * from vesmain where VSSNAM = '" & ComboBox2.Text & "';", dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            idvoyn = dt.Rows(i)("VESMAINID").ToString
            txt_nationality.Text = dt.Rows(i)("VSSNATION").ToString
            txt_namemaster.Text = dt.Rows(i)("VSSMASTER").ToString

        Next

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        Dim dt As New DataTable
        connectDB.GetTable("Select * from vesmain where VSSNAM = '" & ComboBox1.Text & "'", dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            idvoys = dt.Rows(i)("VESMAINID").ToString
            txt_nationality2.Text = dt.Rows(i)("VSSNATION").ToString
            txt_namemaster2.Text = dt.Rows(i)("VSSMASTER").ToString
        Next


    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            txtVoyageName.ReadOnly = False
        Else
            txtVoyageName.ReadOnly = True
        End If
    End Sub

    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        txt_voyage.Text = GENDOC.getIDFormshow(slcVoyage.Text)
        txtVoyageName.Text = txt_voyage.Text
    End Sub
End Class