﻿
Imports DevExpress.XtraGrid.Views.Grid
Imports Microsoft.VisualBasic.CompilerServices
Imports NCalc
Imports Microsoft.Office.Interop
Imports System.Threading
Imports DevExpress.XtraEditors
Imports MySql.Data.MySqlClient
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraEditors.Repository

Public Class frmViewvoyage
    Dim idvoyage As String

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Dim inbtIndex As Integer = 0
    Dim colindex As String
    Dim idborrow As String
    Dim idcontainer As String
    Dim containername As String
    Dim before As Boolean
    Dim agentline As String
    Private stringDate As String()
    Private stringhour As String()
    Private sumtxt As String
    Private sumint As Double
    Dim idbooking As String

    Public Sub New(ByVal voyageid As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = voyageid
        ' Add any initialization after the InitializeComponent() call.
        loadDatavoyage()
        loadVoyage(idvoyage)
    End Sub
    Public Sub ClearForm()
        txtClosingDate.EditValue = Nothing
        txtClosingTime.EditValue = Nothing
        txtSailOutdate.EditValue = Nothing
        txtSailOutTime.EditValue = Nothing
    End Sub
    Private Sub frmViewvoyage_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Public Sub loadDatavoyage()
        Dim sql As String = "Select * from voyage;"

        ykpset.Tables("voyage").Clear()

        connectDB.GetTable(sql, ykpset.Tables("voyage"))


        slcVoyage.Properties.DataSource = ykpset.Tables("voyage")
    End Sub
    Public Sub loadVoyage(ByVal idvoyage As String)
        If idvoyage Is Nothing Then
            loadDatavoyage()
        End If
        slcVoyage.EditValue = idvoyage
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick

        Dim cf As New frmadd_gate1(idvoyage)
        cf.ShowDialog()

    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        Dim cf As New frmedit_gate1(idvoyage)
        cf.ShowDialog()


    End Sub

    Private Sub btnExportPdf_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExportPdf.ItemClick

        manageBooking()
    End Sub
    Public Sub manageBooking()
        Dim nextfrom As New frmAddBooking(idvoyage)
        nextfrom.ShowDialog()
        If nextfrom.DialogResult = DialogResult.OK Then
            LoadBookingname()

        End If
    End Sub

    Private Sub btnExportExcel_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnExportExcel.ItemClick

    End Sub
    Public Sub LoadBookingname()
        Dim sql As String
        sql = "Select * ,CONCAT(BCTNNO,' X ', BCTNTYPE ) AS CTNNO from booking where BVOYAGE ='" & slcVoyage.EditValue & "' ; "
        sql = "Select * ,  BNO As 'Booking No.',BSHIPNAME As 'Shipper',BFORWARDERNAME As 'Forwarder', CONCAT_WS('@',BCONFIRMDAY,BCONFIRMTIME) AS 'CloseDate', CONCAT_WS(' ',CONCAT_WS('X',BCTNNO,BCTNTYPE) ,BNICKAGENT ) AS CTNNO  from booking left join booking_bl on booking_bl.idbooking_bl =  booking.BINV where BVOYAGE ='" & slcVoyage.EditValue & "' order by  BCTNTYPE ASC , FIELD( BFORWARDERNAME ,'v.i.m','psk','queen','3gshipping' ,'baolidor','kvm','j.mac','topline') DESC;"

        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
        GridControl2.DataSource = ykpset.Tables("booking")
    End Sub
    Public Sub LoadnewVoyage()
        Dim sql As String
        If slcVoyage.EditValue Is Nothing Then
            Exit Sub
        End If
        idvoyage = slcVoyage.EditValue
        idbooking = Nothing
        sql = "Select * from (Select * FROM voyage WHERE VOYAGEID = '" & slcVoyage.EditValue & "') voyage join vesmain on voyage.VOYVESIDS = vesmain.VESMAINID where VOYAGEID ='" & slcVoyage.EditValue & "' ;"
        Dim dt As New DataTable
        connectDB.GetTable(sql, dt)
        If dt.Rows.Count > 0 Then
            txt_vessel.Text = dt.Rows(0)("VSSNAM").ToString
            txt_namemaster.Text = CStr(dt.Rows(0)("VSSMASTER")).ToString
            txt_nationality.Text = dt.Rows(0)("VSSNATION").ToString
            txt_eta.Text = CStr(dt.Rows(0)("VOYDATESS")).ToString
            txt_eta_penang.Text = dt.Rows(0)("VOYDATEES").ToString


        End If




        LoadBookingname()

        Dim dt1 As DataTable = New DataTable

        Dim countctn As String
        sql = String.Concat("Select SUM(BCTNNO) as 'SUMNO'  from booking left join booking_bl on booking_bl.idbooking_bl =  booking.BINV where BVOYAGE = '", Me.idvoyage, "'  AND BCTNTYPE  = '20\'GP' order by  BCTNTYPE ASC , FIELD( BFORWARDERNAME ,'v.i.m','psk','queen','3gshipping' ,'baolidor','kvm','j.mac','topline') DESC")
        dt1 = connectDB.GetTable(sql)
        countctn = If(dt.Rows.Count = 0, "( 0 ) ", String.Concat("( ", dt1.Rows(0)("SUMNO").ToString(), " ) "))
        sql = String.Concat("Select COUNT(CTNID) as COUNTCTN  from booking left join booking_bl on booking_bl.idbooking_bl =  booking.BINV  JOIN  borrow  ON  booking.BOOKINGID =  borrow.BOOKID  where BVOYAGE = '", Me.idvoyage, "'  AND BCTNTYPE  = '20\'GP' GROUP BY BVOYAGE")


        dt1 = connectDB.GetTable(sql)
        countctn = If(dt1.Rows.Count = 0, String.Concat(countctn, " ( 0 ) "), String.Concat(countctn, " ( ", dt1.Rows(0)("COUNTCTN").ToString(), " ) "))
        Me.TextBox2.Text = String.Concat(countctn, "  20'")



        sql = String.Concat("Select SUM(BCTNNO) as 'SUMNO'  from booking left join booking_bl on booking_bl.idbooking_bl =  booking.BINV where BVOYAGE = '", Me.idvoyage, "'  AND BCTNTYPE  = '40\'HC' order by  BCTNTYPE ASC , FIELD( BFORWARDERNAME ,'v.i.m','psk','queen','3gshipping' ,'baolidor','kvm','j.mac','topline') DESC")
        dt1 = connectDB.GetTable(sql)
        countctn = If(dt1.Rows.Count = 0, " ( 0 )", String.Concat("( ", dt1.Rows(0)("SUMNO").ToString(), " ) "))
        sql = String.Concat("Select COUNT(CTNID) as COUNTCTN  from booking left join booking_bl on booking_bl.idbooking_bl =  booking.BINV  JOIN  borrow  ON  booking.BOOKINGID =  borrow.BOOKID  where BVOYAGE = '", Me.idvoyage, "'  AND BCTNTYPE  = '40\'HC' GROUP BY BVOYAGE")

        dt1 = connectDB.GetTable(sql)
        countctn = If(dt1.Rows.Count = 0, String.Concat(countctn, " ( 0 ) "), String.Concat(countctn, " ( ", dt1.Rows(0)("COUNTCTN").ToString(), " ) "))
        Me.TextBox1.Text = String.Concat(countctn, "  40'")

        LoadSealID()

    End Sub
    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        LoadnewVoyage()
    End Sub
    Public Sub LoadSealID()
        Dim sql As String = "SELECT sealnumber FROM sealid WHERE type = 1"

        Dim dt As DataTable = connectDB.GetTable(sql)
        Me.txtSealID.Text = dt.Rows(0)("sealnumber").ToString()

        sql = "SELECT sealnumber FROM sealid WHERE type = 2"
        dt = connectDB.GetTable(sql)
        Me.txtSealID1.Text = dt.Rows(0)("sealnumber").ToString()

    End Sub
    Private Sub GridView2_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView2.RowCellClick
        LoadBooking()

    End Sub


    Public Sub LoadBooking()
        Dim remark1 As String
        Dim remark2 As String
        Dim remark3 As String
        Dim remark4 As String
        Dim remark5 As String
        Dim remark6 As String
        Dim checkupgrade As String
        Dim statctn As String
        Dim datetime As String
        Dim hhmm As String
        Dim datetime1 As String
        Dim hhmm1 As String
        Dim check1 As Boolean
        Dim check As Boolean

        idbooking = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString()
        LayoutControlGroup5.Text = "Booking No. : " & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BNO").ToString()
        LayoutControlGroup3.Text = "Booking No. : " & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BNO").ToString()
        Dim sql As String
        sql = "Select *,if(BRCHECK = '1' , True, False) AS BRCHECK_ , if(BRCHECK1 = '1' , True, False) AS BRCHECK1_   , if(CTNSTAT = '1', 'MT', if( CTNSTAT = '2' , 'MS' , if( CTNSTAT = 3 , 'FL', if(CTNSTAT = '4', 'ST', '') ) ) )  AS STAT  from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID   LEFT JOIN mascmrunning On mascmrunning.idmascmrunning = ctnmain.idmascmrunning where BOOKID ='" & idbooking & "' ;"


        Dim dt As New DataTable
        ykpset.Tables("ctnmainborow").Clear()
        connectDB.GetTable(sql, dt)

        connectDB.GetTable(sql, ykpset.Tables("ctnmainborow"))
        GridControl1.DataSource = ykpset.Tables("ctnmainborow")



        GetClosingDate(idbooking)

    End Sub
    Public Sub GetClosingDate(ByVal idbooking As String)
        txtClosingDate.EditValue = Nothing
        txtClosingTime.EditValue = Nothing
        Dim sql As String
        sql = "SELECT BCONFIRMDAY , BCONFIRMTIME FROM booking WHERE bookingid = '" & idbooking & "';"
        Dim dt As DataTable
        dt = connectDB.GetTable(sql)
        If dt.Rows.Count > 0 Then
            txtClosingDate.EditValue = dt.Rows(0)("BCONFIRMDAY").ToString
            txtClosingTime.EditValue = dt.Rows(0)("BCONFIRMTIME").ToString
        End If


    End Sub


    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        manageBooking()
    End Sub




    Private Sub ย้ายตู๊_Click(sender As Object, e As EventArgs) Handles ย้ายตู๊.Click
        Dim nextform As frmmove_ctn = New frmmove_ctn(slcVoyage.EditValue)
        nextform.ShowDialog()
    End Sub

    Private Sub BarButtonItem7_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem7.ItemClick

        Dim nextfrom As frmmovebooking_voyage = New frmmovebooking_voyage(idvoyage)
        nextfrom.ShowDialog()
        If nextfrom.DialogResult = DialogResult.OK Then
            LoadBookingname()

        End If

    End Sub

    Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click

        Dim nextfrom As frmmovebooking_voyage = New frmmovebooking_voyage(idvoyage)
        nextfrom.ShowDialog()
        If nextfrom.DialogResult = DialogResult.OK Then
            LoadBookingname()

        End If
    End Sub

    Private Sub BarButtonItem13_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem13.ItemClick
        Dim nextfrom As frmserchvoy_ctn = New frmserchvoy_ctn(slcVoyage.EditValue)
        nextfrom.ShowDialog()
    End Sub

    Private Sub BarButtonItem8_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem8.ItemClick
        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try
                Dim t = New Thread(New ThreadStart(AddressOf excelReport))
                t.Start()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BarButtonItem9_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem9.ItemClick
        Dim nextform As frmset_bl = New frmset_bl(slcVoyage.EditValue)
        nextform.Show()


    End Sub

    Private Sub excelReport()
        Dim pathExcel As String
        Dim count As Integer = 7


        pathExcel = FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add

        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)

        With excelsheets
            .PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4
            .PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape


            Try
                '.Shapes.AddPicture("C:\logo.png", False, True, 1, 1, 250, 100)
            Catch ex As Exception

            End Try
            .Range("A1").ColumnWidth = 2.75
            .Range("B1").ColumnWidth = 14.25
            .Range("C1").ColumnWidth = 12.63
            .Range("D1").ColumnWidth = 12
            .Range("E1").ColumnWidth = 14.25
            .Range("F1").ColumnWidth = 8.38
            .Range("G1").ColumnWidth = 17.63
            .Range("H1").ColumnWidth = 15.6
            .Range("I1").ColumnWidth = 9.75
            .Range("J1").ColumnWidth = 10.5
            .Range("K1").ColumnWidth = 19.38
            .Range("L1").ColumnWidth = 13.25
            .Range("M1").ColumnWidth = 16.38
            .Range("N1").ColumnWidth = 12.38

            .Range("A1:M500").Font.Name = "Tahoma"
            .Range("A1:M500").Font.Size = 10

            .Rows("1:500").rowheight = 21
            With .Range("A1:M500")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlLeft
            End With


            With .Range("E1:E1")
                .Merge()
                .Value = "VESSEL :"
                .Font.Bold = True
            End With
            With .Range("F1:H1")
                .Merge()
                .Value = txt_vessel.Text


            End With
            With .Range("E2:E2")
                .Merge()
                .Value = "VOYAGE :"
                .Font.Bold = True
            End With
            With .Range("F2:H2")
                .Merge()
                .Value = slcVoyage.Text

            End With
            With .Range("E3:E3")
                .Merge()
                .Value = "PORT OF LOADING : "

                .Font.Bold = True
            End With
            With .Range("F3:H3")
                .Merge()
                .Value = " Chokchai Kantang Port (CKP)"

            End With
            With .Range("E4:E4")
                .Merge()
                .Value = "SAIL DATE :"

                .Font.Bold = True
            End With
            With .Range("F4:H4")
                .Merge()
                .NumberFormat = "B1d-mmm-yy"
                .Value = Date.Now.Date.ToString("dd-MMM-yyyy")

            End With

            Dim sql As String
            sql = "Select * from voyage join vesmain on voyage.VOYVESIDS = vesmain.VESMAINID join booking on booking.BVOYAGE = voyage.VOYAGEID left join  booking_bl on booking_bl.booking_id = booking.BOOKINGID   where VOYAGEID ='" & slcVoyage.EditValue & "'order by  BCTNTYPE ASC , FIELD( BFORWARDERNAME ,'seatrans','pioneer','ncl','kvm','v.i.m','psk','queen','3gshipping' ,'topline','baolidor','j.mac','skr','kvm','YKP GYPSUM' ) DESC"

            Dim dt3 As New DataTable
            connectDB.GetTable(sql, dt3)
            Dim number1 As Integer = 0
            Dim num2 As Integer = dt3.Rows.Count - 1


            For i As Integer = 0 To dt3.Rows.Count - 1

                sql = "Select count(idbooking_bl_ctn) as sum1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID") & "';"
                Dim dt4 As New DataTable
                dt4 = connectDB.GetTable(sql)
                number1 = Convert.ToDecimal(dt4.Rows(0)("sum1"))

                If (number1 = 0) Then
                    With .Range("A" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = "SHIPPER : " + dt3.Rows(i)("BNSHIPNAME")
                        .Characters(1, 7).Font.Bold = True
                    End With
                    With .Range("D" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .Value = "FORWARDER : " + dt3.Rows(i)("BNFORWARDERNAME")
                        .Characters(1, 9).Font.Bold = True
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .Merge()
                        .Value = "MOTHER VESL : "
                        .Font.Bold = True

                    End With
                    count += 1
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .Merge()
                        .Value = "SCN : "
                        .Font.Bold = True
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt3.Rows(i)("BSCN")
                    End With
                    With .Range("A" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = "BOOKING NO. : " + dt3.Rows(i)("BNO")
                        .Font.Bold = True
                    End With
                    count += 1
                    If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                        With .Range("L" + count.ToString + ":N" + count.ToString)
                            .Merge()
                            .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                            .Font.Bold = True
                        End With
                    End If
                    With .Range("A" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = "VESSEL : " & txt_vessel.Text & "  VOYAGE : S" & slcVoyage.EditValue.ToString
                        .Characters(1, 6).Font.Bold = True
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .Value = "PORT OF TRANSSHIP :"
                        .Characters(1, 6).Font.Bold = True
                    End With
                    count = count + 1
                    Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                    With .Range("A" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                        .Characters(1, 30).Font.Bold = True
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .Merge()
                        .Value = "PORT OF DISCHARGE : " + dt3.Rows(i)("BPORTDIS")
                        .Font.Bold = True
                    End With
                    If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                        With .Range("L" + count.ToString + ":N" + count.ToString)
                            .Merge()
                            .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                            .Font.Bold = True
                        End With
                    End If
                    count = count + 1
                    With .Range("A" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                        .Characters(1, 11).Font.Bold = True
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .Value = "PORT OF DELIVERY : "
                        .Font.Bold = True
                    End With
                    With .Range("L" + count.ToString + ":M" + count.ToString)
                        .Merge()
                        If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                            .Value = "CLOSE DATE :"
                        ElseIf dt3.Rows(i)("BCONFIRMDAY") <> "TBA" Then
                            .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                        Else
                            .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                        End If
                        .Font.Bold = True
                    End With


                ElseIf number1 = 7 Then

                    Dim num3 As Integer = 0
                    Dim dt1 As New DataTable
                    sql = "Select name_2,name_1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID").ToString() & "'  order by idnumber ASC;"


                    dt1 = connectDB.GetTable(sql)

                    Dim num4 As Integer = dt1.Rows.Count - 1

                    For i1 As Integer = 0 To num4 Step 1
                        If (num3 = 2) Then

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Value = "SCN : "
                                .Font.Bold = True
                            End With

                            With .Range("H" + count.ToString + ":H" + count.ToString)
                                .Value = dt3.Rows(i)("BSCN").ToString()
                                .Font.Bold = True
                            End With
                            count = count + 1

                            With .Range("A" + count.ToString + ":C" + count.ToString)
                                .Merge()
                                .Value = "SHIPPER : " & dt3.Rows(i)("BNSHIPNAME").ToString()
                                .Characters(1, 7).Font.Bold = True
                            End With

                            With .Range("D" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "FORWARDER : " & dt3.Rows(i)("BNFORWARDERNAME").ToString()
                                .Characters(1, 9).Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1

                        ElseIf (num3 = 3) Then

                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "BOOKING NO. : " & dt3.Rows(i)("BNO").ToString()
                                .Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1

                        ElseIf (num3 = 4) Then

                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VESSEL : " & txt_vessel.EditValue & "  VOYAGE : S" & slcVoyage.Text
                                .Characters(1, 6).Font.Bold = True
                            End With
                            If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                                    .Font.Bold = True
                                End With
                            End If
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1

                        ElseIf (num3 = 5) Then

                            If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                                    .Font.Bold = True
                                End With
                            End If
                            Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()

                                .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                                .Characters(1, 30).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1

                        ElseIf (num3 = 6) Then

                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                                .Characters(1, 11).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            With .Range("L" + count.ToString + ":M" + count.ToString)
                                .Merge()
                                If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                                    .Value = "CLOSE DATE :"
                                ElseIf dt3.Rows(i)("BCONFIRMDAY") IsNot "TBA" Then
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                                Else
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                                End If
                                .Font.Bold = True
                            End With
                            count = count + 1

                        ElseIf (num3 = 0) Then
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()

                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1

                        ElseIf (num3 = 1) Then
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()

                                .Value = dt1.Rows(i1)("name_1") & " : " & IIf(dt1.Rows(i1)("name_2") Is DBNull.Value, "", dt1.Rows(i1)("name_2"))
                                .Characters(1, dt1.Rows(i1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1
                        End If
                        num3 = num3 + 1
                    Next
                ElseIf (number1 = 6) Then

                    Dim num5 As Integer = 0
                    Dim dt1 As New DataTable
                    sql = "Select name_2,name_1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID").ToString() & "'  order by idnumber ASC;"

                    dt1 = connectDB.GetTable(sql)



                    Dim num6 As Integer = dt1.Rows.Count - 1
                    For j1 As Integer = 0 To num6 Step 1
                        If (num5 = 1) Then
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Value = "SCN : "
                                .Font.Bold = True
                            End With
                            With .Range("H" + count.ToString + ":H" + count.ToString)
                                .Value = dt3.Rows(i)("BSCN").ToString()
                                .Font.Bold = True
                            End With
                            count = count + 1
                            With .Range("A" + count.ToString + ":C" + count.ToString)
                                .Merge()
                                .Value = "SHIPPER : " & dt3.Rows(i)("BNSHIPNAME").ToString()
                                .Characters(1, 7).Font.Bold = True
                            End With
                            With .Range("D" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "FORWARDER : " & dt3.Rows(i)("BNFORWARDERNAME").ToString()
                                .Characters(1, 9).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = dt1.Rows(j1)("name_1") & " : " & IIf(dt1.Rows(j1)("name_2") Is DBNull.Value, "", dt1.Rows(j1)("name_2"))
                                .Characters(1, dt1.Rows(j1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1
                        ElseIf (num5 = 2) Then
                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "BOOKING NO. : " + dt3.Rows(i)("BNO")
                                .Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = dt1.Rows(j1)("name_1") & " : " & IIf(dt1.Rows(j1)("name_2") Is DBNull.Value, "", dt1.Rows(j1)("name_2"))
                                .Characters(1, dt1.Rows(j1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1

                        ElseIf (num5 = 3) Then


                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "VESSEL : " & txt_vessel.Text & "  VOYAGE : S" & slcVoyage.EditValue.ToString
                                .Characters(1, 6).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = dt1.Rows(j1)("name_1") & " : " & IIf(dt1.Rows(j1)("name_2") Is DBNull.Value, "", dt1.Rows(j1)("name_2"))
                                .Characters(1, dt1.Rows(j1)("name_1").ToString().Length).Font.Bold = True
                            End With

                            If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                                    .Font.Bold = True
                                End With
                            End If
                            count = count + 1

                        ElseIf (num5 = 4) Then
                            Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                                .Characters(1, 30).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(j1)("name_1") & " : " & IIf(dt1.Rows(j1)("name_2") Is DBNull.Value, "", dt1.Rows(j1)("name_2"))
                                .Characters(1, dt1.Rows(j1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                                    .Font.Bold = True
                                End With
                            End If
                            count = count + 1

                        ElseIf (num5 = 5) Then

                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                                .Characters(1, 11).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(j1)("name_1") & " : " & IIf(dt1.Rows(j1)("name_2") Is DBNull.Value, "", dt1.Rows(j1)("name_2"))
                                .Characters(1, dt1.Rows(j1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            With .Range("L" + count.ToString + ":M" + count.ToString)
                                .Merge()
                                If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                                    .Value = "CLOSE DATE :"
                                ElseIf dt3.Rows(i)("BCONFIRMDAY") IsNot "TBA" Then
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                                Else
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                                End If
                                .Font.Bold = True
                            End With


                        ElseIf (num5 = 0) Then
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = dt1.Rows(j1)("name_1") & " : " & IIf(dt1.Rows(j1)("name_2") Is DBNull.Value, "", dt1.Rows(j1)("name_2"))
                                .Characters(1, dt1.Rows(j1)("name_1").ToString().Length).Font.Bold = True
                            End With

                            count = count + 1
                        End If
                        num5 = num5 + 1
                    Next
                ElseIf (number1 = 5) Then
                    Dim num7 As Integer = 0
                    Dim num5 As Integer = 0
                    Dim dt1 As New DataTable
                    sql = "Select name_2,name_1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID").ToString() & "'  order by idnumber ASC;"

                    dt1 = connectDB.GetTable(sql)

                    Dim num8 As Integer = dt1.Rows.Count - 1
                    For k1 As Integer = 0 To num8 Step 1
                        If (num7 = 0) Then
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = dt1.Rows(k1)("name_1") & " : " & IIf(dt1.Rows(k1)("name_2") Is DBNull.Value, "", dt1.Rows(k1)("name_2"))
                                .Characters(1, dt1.Rows(k1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1

                            With .Range("A" + count.ToString + ":C" + count.ToString)
                                .Merge()
                                .Value = "SHIPPER : " & dt3.Rows(i)("BNSHIPNAME").ToString()
                                .Characters(1, 7).Font.Bold = True
                            End With
                            With .Range("D" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "FORWARDER : " & dt3.Rows(i)("BNFORWARDERNAME").ToString()
                                .Characters(1, 9).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Value = "SCN : "
                                .Font.Bold = True
                            End With
                            With .Range("H" + count.ToString + ":H" + count.ToString)
                                .Merge()
                                .Value = dt3.Rows(i)("BSCN")
                            End With


                            count = count + 1
                        ElseIf (num7 = 1) Then
                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "BOOKING NO. : " & dt3.Rows(i)("BNO").ToString()
                                .Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(k1)("name_1") & " : " & IIf(dt1.Rows(k1)("name_2") Is DBNull.Value, "", dt1.Rows(k1)("name_2"))
                                .Characters(1, dt1.Rows(k1)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1



                        ElseIf (num7 = 2) Then

                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VESSEL : " & txt_vessel.EditValue & "  VOYAGE : S" & slcVoyage.Text
                                .Characters(1, 6).Font.Bold = True
                            End With
                            If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                                    .Font.Bold = True
                                End With
                            End If
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(k1)("name_1") & " : " & IIf(dt1.Rows(k1)("name_2") Is DBNull.Value, "", dt1.Rows(k1)("name_2"))
                                .Characters(1, dt1.Rows(k1)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1
                        ElseIf (num7 = 3) Then

                            If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                                    .Font.Bold = True
                                End With
                            End If
                            Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                                .Characters(1, 30).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(k1)("name_1") & " : " & IIf(dt1.Rows(k1)("name_2") Is DBNull.Value, "", dt1.Rows(k1)("name_2"))
                                .Characters(1, dt1.Rows(k1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1

                        ElseIf (num7 = 4) Then

                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                                .Characters(1, 11).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(k1)("name_1") & " : " & IIf(dt1.Rows(k1)("name_2") Is DBNull.Value, "", dt1.Rows(k1)("name_2"))
                                .Characters(1, dt1.Rows(k1)("name_1").ToString().Length).Font.Bold = True
                            End With
                            With .Range("L" + count.ToString + ":M" + count.ToString)
                                .Merge()
                                If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                                    .Value = "CLOSE DATE :"
                                ElseIf dt3.Rows(i)("BCONFIRMDAY") IsNot "TBA" Then
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                                Else
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                                End If
                                .Font.Bold = True
                            End With


                        End If
                        num7 = num7 + 1
                    Next
                ElseIf (number1 = 4) Then


                    Dim num9 As Integer = 0

                    Dim dt1 As New DataTable
                    sql = "Select name_2,name_1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID").ToString() & "'  order by idnumber ASC;"

                    dt1 = connectDB.GetTable(sql)

                    Dim num10 As Integer = dt1.Rows.Count - 1
                    For l As Integer = 0 To num10 Step 1
                        If (num9 = 0) Then
                            With .Range("A" + count.ToString + ":C" + count.ToString)
                                .Merge()
                                .Value = "SHIPPER : " & dt3.Rows(i)("BNSHIPNAME").ToString()
                                .Characters(1, 7).Font.Bold = True
                            End With
                            With .Range("D" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "FORWARDER : " & dt3.Rows(i)("BNFORWARDERNAME").ToString()
                                .Characters(1, 9).Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(l)("name_1") & " : " & IIf(dt1.Rows(l)("name_2") Is DBNull.Value, "", dt1.Rows(l)("name_2"))
                                .Characters(1, dt1.Rows(l)("name_1").ToString().Length).Font.Bold = True
                            End With
                            count = count + 1

                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "BOOKING NO. : " + dt3.Rows(i)("BNO")
                                .Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Value = "SCN : "
                                .Font.Bold = True
                            End With
                            With .Range("H" + count.ToString + ":H" + count.ToString)
                                .Value = dt3.Rows(i)("BSCN").ToString()
                                .Font.Bold = True
                            End With
                            count = count + 1
                        ElseIf (num9 = 1) Then
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VESSEL : " & txt_vessel.EditValue & "  VOYAGE : S" & slcVoyage.Text
                                .Characters(1, 6).Font.Bold = True
                            End With
                            If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                                    .Font.Bold = True
                                End With
                            End If
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(l)("name_1") & " : " & IIf(dt1.Rows(l)("name_2") Is DBNull.Value, "", dt1.Rows(l)("name_2"))
                                .Characters(1, dt1.Rows(l)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1

                        ElseIf (num9 = 2) Then

                            If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                                    .Font.Bold = True
                                End With
                            End If

                            Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                                .Characters(1, 30).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(l)("name_1") & " : " & IIf(dt1.Rows(l)("name_2") Is DBNull.Value, "", dt1.Rows(l)("name_2"))
                                .Characters(1, dt1.Rows(l)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1


                        ElseIf (num9 = 3) Then

                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                                .Characters(1, 11).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(l)("name_1") & " : " & IIf(dt1.Rows(l)("name_2") Is DBNull.Value, "", dt1.Rows(l)("name_2"))
                                .Characters(1, dt1.Rows(l)("name_1").ToString().Length).Font.Bold = True

                            End With
                            With .Range("L" + count.ToString + ":M" + count.ToString)
                                .Merge()
                                If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                                    .Value = "CLOSE DATE :"
                                ElseIf dt3.Rows(i)("BCONFIRMDAY") IsNot "TBA" Then
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                                Else
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                                End If
                                .Font.Bold = True
                            End With
                        End If
                        num9 = num9 + 1
                    Next

                ElseIf (number1 = 3) Then

                    Dim num11 As Integer = 0
                    Dim dt1 As New DataTable
                    sql = "Select name_2,name_1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID").ToString() & "'  order by idnumber ASC;"

                    dt1 = connectDB.GetTable(sql)

                    Dim num12 As Integer = dt1.Rows.Count - 1
                    For m As Integer = 0 To num12 Step 1
                        If (num11 = 0) Then
                            With .Range("A" + count.ToString + ":C" + count.ToString)
                                .Merge()
                                .Value = "SHIPPER : " & dt3.Rows(i)("BNSHIPNAME").ToString()
                                .Characters(1, 7).Font.Bold = True
                            End With
                            With .Range("D" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "FORWARDER : " & dt3.Rows(i)("BNFORWARDERNAME").ToString()
                                .Characters(1, 9).Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(m)("name_1") & " : " & IIf(dt1.Rows(m)("name_2") Is DBNull.Value, "", dt1.Rows(m)("name_2"))
                                .Characters(1, dt1.Rows(m)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1

                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "BOOKING NO. : " & dt3.Rows(i)("BNO").ToString()
                                .Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Value = "SCN : "
                                .Font.Bold = True
                            End With

                            With .Range("H" + count.ToString + ":H" + count.ToString)
                                .Value = dt3.Rows(i)("BSCN").ToString()
                                .Font.Bold = True
                            End With

                            count = count + 1

                        ElseIf (num11 = 1) Then
                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "VESSEL : " & txt_vessel.Text + "  VOYAGE : S" & slcVoyage.EditValue.ToString
                                .Characters(1, 6).Font.Bold = True
                            End With
                            If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                                    .Font.Bold = True
                                End With
                            End If
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(m)("name_1") & " : " & IIf(dt1.Rows(m)("name_2") Is DBNull.Value, "", dt1.Rows(m)("name_2"))
                                .Characters(1, dt1.Rows(m)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1

                        ElseIf (num11 = 2) Then
                            Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                                .Characters(1, 30).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(m)("name_1") & " : " & IIf(dt1.Rows(m)("name_2") Is DBNull.Value, "", dt1.Rows(m)("name_2"))
                                .Characters(1, dt1.Rows(m)("name_1").ToString().Length).Font.Bold = True

                            End With
                            If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                                    .Font.Bold = True
                                End With
                            End If
                            count = count + 1
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                                .Characters(1, 11).Font.Bold = True
                            End With

                            With .Range("L" + count.ToString + ":M" + count.ToString)
                                .Merge()
                                If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                                    .Value = "CLOSE DATE :"
                                ElseIf dt3.Rows(i)("BCONFIRMDAY") IsNot "TBA" Then
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                                Else
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                                End If
                                .Font.Bold = True
                            End With

                        End If
                        num11 = num11 + 1
                    Next
                ElseIf (number1 = 2) Then


                    Dim x As Integer = 0
                    Dim dt1 As New DataTable
                    sql = "Select name_2,name_1 from booking_bl_ctn where bookid ='" & dt3.Rows(i)("BOOKINGID").ToString() & "'  order by idnumber ASC;"

                    dt1 = connectDB.GetTable(sql)
                    Dim num13 As Integer = dt1.Rows.Count - 1
                    For n As Integer = 0 To num13 Step 1
                        If (x = 0) Then
                            With .Range("A" + count.ToString + ":C" + count.ToString)
                                .Merge()
                                .Value = "SHIPPER : " & dt3.Rows(i)("BNSHIPNAME").ToString()
                                .Characters(1, 7).Font.Bold = True
                            End With
                            With .Range("D" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "FORWARDER : " & dt3.Rows(i)("BNFORWARDERNAME").ToString()
                                .Characters(1, 9).Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(n)("name_1") & " : " & IIf(dt1.Rows(n)("name_2") Is DBNull.Value, "", dt1.Rows(n)("name_2"))
                                .Characters(1, dt1.Rows(n)("name_1").ToString().Length).Font.Bold = True

                            End With
                            count = count + 1
                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "BOOKING NO. : " & dt3.Rows(i)("BNO").ToString()
                                .Font.Bold = True
                            End With

                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Value = "SCN : "
                                .Font.Bold = True
                            End With

                            With .Range("H" + count.ToString + ":H" + count.ToString)
                                .Value = dt3.Rows(i)("BSCN").ToString()
                                .Font.Bold = True
                            End With

                            If dt3.Rows(i)("billbooking") IsNot DBNull.Value And dt3.Rows(i)("billbooking").ToString.Length > 3 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "INV.NO.: " & dt3.Rows(i)("billbooking")
                                    .Font.Bold = True
                                End With
                            End If

                            count = count + 1
                            If dt3.Rows(i)("BPENANG") IsNot DBNull.Value And dt3.Rows(i)("BPENANG").ToString.Length > 1 Then
                                With .Range("L" + count.ToString + ":N" + count.ToString)
                                    .Merge()
                                    .Value = "Forwarder Penang : " + dt3.Rows(i)("BPENANG")
                                    .Font.Bold = True
                                End With
                            End If

                        ElseIf (x = 1) Then


                            With .Range("A" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = "VESSEL : " & txt_vessel.Text & "  VOYAGE : S" & slcVoyage.EditValue.ToString
                                .Characters(1, 6).Font.Bold = True
                            End With
                            With .Range("G" + count.ToString + ":G" + count.ToString)
                                .Merge()
                                .Merge()
                                .Value = dt1.Rows(n)("name_1") & " : " & IIf(dt1.Rows(n)("name_2") Is DBNull.Value, "", dt1.Rows(n)("name_2"))
                                .Characters(1, dt1.Rows(n)("name_1").ToString().Length).Font.Bold = True

                            End With

                            count = count + 1
                            Dim strArrays() As String = dt3.Rows(i)("BCTNTYPE").ToString.Split("'")
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "VOLUME / CONTAINER TYPE SIZE : " & dt3.Rows(i)("BCTNNO").ToString() & " X " & strArrays(0) & "'" & dt3.Rows(i)("BNICKAGENT").ToString()
                                .Characters(1, 30).Font.Bold = True
                            End With
                            count = count + 1
                            With .Range("A" + count.ToString + ":E" + count.ToString)
                                .Merge()
                                .Value = "COMMODITY : " + dt3.Rows(i)("BCOM")
                                .Characters(1, 11).Font.Bold = True
                            End With

                            With .Range("L" + count.ToString + ":M" + count.ToString)
                                .Merge()
                                If dt3.Rows(i)("BCONFIRMDAY") Is DBNull.Value Then
                                    .Value = "CLOSE DATE :"
                                ElseIf dt3.Rows(i)("BCONFIRMDAY") IsNot "TBA" Then
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString() & "  @" & dt3.Rows(i)("BCONFIRMTIME").ToString()
                                Else
                                    .Value = "CLOSE DATE :" & dt3.Rows(i)("BCONFIRMDAY").ToString()

                                End If
                                .Font.Bold = True
                            End With


                        End If
                        x = x + 1
                    Next

                End If
                'ทำถึงตรงนี้



                count += 1
                For J = 7 To 10
                    .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    .Range("M" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin

                Next

                With .Range("A" + count.ToString + ":N" + count.ToString)
                    .Font.Bold = True
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("A" + count.ToString + ":A" + count.ToString)
                    .Value = "NO."
                End With
                With .Range("B" + count.ToString + ":B" + count.ToString)
                    .Value = "CONTAINER NO."
                End With
                With .Range("C" + count.ToString + ":C" + count.ToString)
                    .Value = "SEAL NO."
                End With
                With .Range("D" + count.ToString + ":D" + count.ToString)
                    .Value = "LINER"
                End With
                With .Range("E" + count.ToString + ":E" + count.ToString)
                    .Value = "DATE IN"
                End With
                With .Range("F" + count.ToString + ":F" + count.ToString)
                    .Value = "VOY."
                End With
                With .Range("G" + count.ToString + ":G" + count.ToString)
                    .Value = "GET OUT(EMPTY)"
                End With
                With .Range("H" + count.ToString + ":H" + count.ToString)
                    .Value = "GET IN(LADEN)"
                End With
                With .Range("I" + count.ToString + ":I" + count.ToString)
                    .Value = "DATE OUT"
                End With
                With .Range("J" + count.ToString + ":J" + count.ToString)
                    .Value = "WEIGHT"
                End With
                With .Range("K" + count.ToString + ":K" + count.ToString)
                    .Value = "SHIPPER"
                End With
                With .Range("L" + count.ToString + ":L" + count.ToString)
                    .Value = "FORWARDER"
                End With
                With .Range("M" + count.ToString + ":N" + count.ToString)
                    .Merge()
                    .Value = "TRANSPORT BY"
                End With
                count += 1

                Dim sql2 As String
                sql2 = "Select * from borrow join booking on borrow.BOOKID= booking.BOOKINGID join voyage  on booking.BVOYAGE = voyage.VOYAGEID join ctnmain on ctnmain.CTNMAINID = borrow.CTNID join vesmain on vesmain.VESMAINID = voyage.VOYVESIDN  where BVOYAGE ='" & slcVoyage.EditValue & "' and BOOKID='" & dt3.Rows(i)("BOOKINGID") & "';"

                Dim dt2 As New DataTable
                connectDB.GetTable(sql2, dt2)



                Try
                    Dim number As Integer = 1
                    For m As Integer = 0 To dt2.Rows.Count - 1
                        For j = 7 To 7
                            .Range("A" + count.ToString + ":A" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("B" + count.ToString + ":B" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("C" + count.ToString + ":C" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("D" + count.ToString + ":D" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("E" + count.ToString + ":E" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("F" + count.ToString + ":F" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("G" + count.ToString + ":G" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("H" + count.ToString + ":H" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("I" + count.ToString + ":I" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("J" + count.ToString + ":J" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("K" + count.ToString + ":K" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("L" + count.ToString + ":L" + count.ToString).Borders(j).Weight = 2 ' xlThin
                            .Range("M" + count.ToString + ":M" + count.ToString).Borders(j).Weight = 2
                            .Range("N" + count.ToString + ":N" + count.ToString).Borders(j).Weight = 2
                        Next
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders.Weight = 2

                        With .Range("A" + count.ToString + ":N" + count.ToString)
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("A" + count.ToString + ":A" + count.ToString)
                            .Value = number

                        End With
                        number += 1



                        With .Range("B" + count.ToString + ":B" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("CTNSTRING")

                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                            If (dt2.Rows(m)("CTNUPGRADE") IsNot DBNull.Value) Then
                                If (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(dt2.Rows(m)("CTNUPGRADE"), "U", False)) Then
                                    .Font.Color = Color.Red
                                End If
                            End If

                            If (dt2.Rows(m)("CTNMARKSTR") IsNot DBNull.Value) Then
                                If (Conversions.ToDouble(dt2.Rows(m)("CTNMARKSTRBOO").ToString()) = 1) Then
                                    .AddComment(dt2.Rows(m)("CTNMARKSTR").ToString())
                                    .Interior.Color = Color.Yellow
                                End If
                            End If


                        End With



                        With .Range("C" + count.ToString + ":C" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("CTNSEALID")
                            If (dt2.Rows(m)("CTNMARKSEAL") IsNot DBNull.Value) Then
                                If (Conversions.ToDouble(dt2.Rows(m)("CTNMARKSEALBOO").ToString()) = 1) Then
                                    .AddComment(dt2.Rows(m)("CTNMARKSEAL").ToString())
                                    .Interior.Color = Color.Yellow
                                End If
                            End If

                        End With
                        With .Range("D" + count.ToString + ":D" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("CTNAGENT")
                        End With

                        With .Range("E" + count.ToString + ":E" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt2.Rows(m)("VOYDATEEN").ToString

                        End With

                        With .Range("F" + count.ToString + ":F" + count.ToString)
                            .Merge()
                            .Value = My.Settings.ShipIn + dt2.Rows(m)("VOYNAME")
                        End With

                        With .Range("G" + count.ToString + ":G" + count.ToString)
                            .Merge()
                            If dt2.Rows(m)("TIMEDATE") Is DBNull.Value Or dt2.Rows(m)("TIMEHHMM") Is DBNull.Value Then
                            Else
                                .Value = dt2.Rows(m)("TIMEDATE") + "/" + dt2.Rows(m)("TIMEHHMM")
                            End If


                            If (dt2.Rows(m)("CTNMARKDATEOUT") IsNot DBNull.Value) Then
                                If (Conversions.ToDouble(dt2.Rows(m)("CTNMARKDATEOUTBOO").ToString()) = 1) Then
                                    .AddComment(dt2.Rows(m)("CTNMARKDATEOUT").ToString())
                                    .Interior.Color = Color.Yellow
                                End If
                            End If
                        End With



                        With .Range("H" + count.ToString + ":H" + count.ToString)
                            .Merge()

                            If dt2.Rows(m)("TIMEDATEIN") Is DBNull.Value Or dt2.Rows(m)("TIMEHHMMIN") Is DBNull.Value Then
                            Else
                                .Value = dt2.Rows(m)("TIMEDATEIN") + "/" + dt2.Rows(m)("TIMEHHMMIN")
                            End If
                            If (dt2.Rows(m)("CTNMARKDATEIN") IsNot DBNull.Value) Then
                                If (Conversions.ToDouble(dt2.Rows(m)("CTNMARKDATEINBOO").ToString()) = 1) Then
                                    .AddComment(dt2.Rows(m)("CTNMARKDATEIN").ToString())
                                    .Interior.Color = Color.Yellow
                                End If
                            End If

                        End With


                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .Merge() 'CTNDATEOUT
                            .NumberFormat = "@"
                            .Value = dt2.Rows(m)("CTNDATEOUT").ToString()
                        End With


                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("CTNWEIGHT").ToString()
                            If (dt2.Rows(m)("CTNMARKWEIGHT") IsNot DBNull.Value) Then
                                If (Conversions.ToDouble(dt2.Rows(m)("CTNMARKWEIGHTBOO").ToString()) = 1) Then
                                    .AddComment(dt2.Rows(m)("CTNMARKWEIGHT").ToString())
                                    .Interior.Color = Color.Yellow
                                End If
                            End If
                        End With


                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("BNSHIPNAME")
                        End With

                        With .Range("L" + count.ToString + ":L" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("BNFORWARDERNAME")
                        End With


                        With .Range("M" + count.ToString + ":M" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("COMNAME")
                            If (dt2.Rows(m)("CTNMARKTRANSPORT") IsNot DBNull.Value) Then
                                If (Conversions.ToDouble(dt2.Rows(m)("CTNMARKTRANSPORTBOO").ToString()) = 1) Then
                                    .AddComment(dt2.Rows(m)("CTNMARKTRANSPORT").ToString())
                                    .Interior.Color = Color.Yellow
                                End If
                            End If
                        End With


                        With .Range("N" + count.ToString + ":N" + count.ToString)
                            .Merge()
                            .Value = dt2.Rows(m)("CARID")
                            .Font.Size = 10

                        End With
                        count += 1
                    Next



                    .Range("A" + count.ToString + ":M" + count.ToString).Borders(8).Weight = 2 ' xlThin
                    number = 0
                    count += 1
                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try
                count += 1
            Next





        End With

        excelapp.Windows.Application.ActiveWindow.DisplayGridlines = False

        Try
            excelbooks.SaveAs(pathExcel.ToString + "\ContainerMovement S" & slcVoyage.EditValue.ToString & ".xlsx")

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try

    End Sub


    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles btnClosing.Click
        If GridView2.RowCount > 0 Then


        Else
            If txtClosingDate.EditValue Is Nothing Then
                MsgBox("กรุณาระบบ Date   ")
                Exit Sub
            End If
            If txtClosingTime.EditValue Is Nothing Then
                MsgBox("กรุณาระบบ Time   ")

                Exit Sub
            End If
            Dim sql As String

            idbooking = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString()
            'LayoutControlGroup5.Text = "Booking No. : " & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BNO").ToString()
            'LayoutControlGroup3.Text = "Booking No. : " & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BNO").ToString()

            sql = "UPDATE booking SET BCONFIRMDAY = '" & txtClosingDate.Text & "',BCONFIRMTIME ='" & txtClosingTime.Text & "'  WHERE BOOKINGID = '" & idbooking & "'; "
            connectDB.GetTable(sql)
            LoadBookingname()
        End If


    End Sub

    Private Sub BarButtonItem14_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem14.ItemClick
        If (GridView1.FocusedRowHandle > 0) Then
            Dim nextform As frmrpt_gatin = New frmrpt_gatin(GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString())
            nextform.Show()
        Else
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)

        End If
    End Sub

    Private Sub BarButtonItem15_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem15.ItemClick
        If (GridView1.FocusedRowHandle <= 0) Then
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)
        Else

            Dim rpt1 As New rptGateIn
            Dim sql As String

            sql = "Select remark,BCOM,TIMEDATE,TIMEHHMM,BNO,CTNSTRING,CTNSEALID,CTNAGENT,CTNSIZE, CTNTAREWEIGHT ,SHIPNAME,BNFORWARDERNAME,COMNAME,CARID,VOYVESNAMES,VOYAGEID,POL,TSPORT
,BFINALDEST,BCOM,voyname from borrow join booking on borrow.BOOKID = booking.BOOKINGID join voyage on voyage.VOYAGEID = booking.BVOYAGE  join shipper on shipper.SHIPPERID = booking.BSHIP join ctnmain on ctnmain.CTNMAINID = borrow.CTNID   where idborrow ='" & idborrow & "' ;"
            connectDB.GetTable(sql, ykpset.Tables("GATEINOUT"))




            rpt1 = New rptGateIn
            rpt1.DataSource = ykpset
            rpt1.CreateDocument()

            rpt1.ShowPreview()

        End If
    End Sub



    Private Sub btnSailOut_Click(sender As Object, e As EventArgs) Handles btnSailOut.Click

        If XtraMessageBox.Show("ต้องการ Confirm Voyage นี้ทั้งหมดใช่หรือไม่ กรุณาอย่าลืมระบุวันที่และเวลา", "ตกลง", MessageBoxButtons.YesNo) <> DialogResult.No Then
            Dim dt As New DataTable
            Dim ctnname As String
            Dim typectn As String
            Dim sql1 As String
            Dim sql2 As String
            Dim sql3 As String
            Dim stringDate() As String
            Dim stringhour() As String
            Dim sumtxt As String
            Dim sumint As String
            Dim sql As String = String.Concat("Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join booking on  booking.BOOKINGID = borrow.BOOKID where BVOYAGE ='", Me.idvoyage, "' ;")
            dt = connectDB.GetTable(sql)

            For i As Integer = 0 To dt.Rows.Count - 1

                Dim idcontainervoyage As String = dt.Rows(i)("CTNID").ToString
                agentline = dt.Rows(i)("CTNAGENT").ToString
                ctnname = dt.Rows(i)("CTNSTRING").ToString


                If dt.Rows(i)("CTNSIZE").ToString.Trim = "20'GP" Then
                    typectn = "22G1"
                ElseIf dt.Rows(i)("CTNSIZE").ToString.Trim = "40'HC" Then
                    typectn = "45G1"
                End If
                sql = "UPDATE ctnmain SET CTNSTAT ='4',CTNDATEOUT = '" & txtSailOutdate.Text & "' where CTNMAINID = '" & idcontainervoyage & "';"
                connectDB.ExecuteNonQuery(sql)
                sql = "UPDATE borrow SET BRCHECK='1' , BRCHECK1 = '1' where idborrow = '" & dt.Rows(i)("idborrow").ToString() & "'; "
                connectDB.ExecuteNonQuery(sql)



                sql1 += "  `rptctn`,"
                sql2 += "'" & ctnname & "',"

                sql1 += "`rptstat`,"
                sql2 += "'ST',"

                stringDate = txtSailOutdate.Text.Split("-")
                stringhour = txtSailOutTime.Text.Split(":")
                sumtxt = stringDate(2) + stringDate(1) + stringDate(0) + stringhour(0) + stringhour(1)
                sumint = Convert.ToDecimal(sumtxt) + 100

                sql1 += "     `rptctndate`,"
                sql2 += "'" & MySqlHelper.EscapeString(String.Concat(Me.sumtxt, "00")) & "',"

                sql1 += "     `rptctndatetpe`,"
                sql2 += "'" & MySqlHelper.EscapeString(String.Concat(Me.sumint.ToString(), "00")) & "',"
                sql1 += "     `rpttype`,"
                sql2 += "'" & MySqlHelper.EscapeString(typectn) & "',"
                sql1 += "     `rptidctn`,"
                sql2 += "'" & MySqlHelper.EscapeString(idcontainervoyage) & "',"
                sql1 += "     `rptagent`,"
                sql2 += "'" & MySqlHelper.EscapeString(agentline) & "'"
                sql1 += "     `rptdatetype1`,"
                sql2 += "'" & MySqlHelper.EscapeString(txtSailOutdate.Text) & "'"
                sql3 = "INSERT INTO rptwhl  ( " & sql1 & " )  VALUES ( " & sql2 & "  );"
                connectDB.ExecuteNonQuery(sql)

            Next

            MsgBox("OUT Finish ")
        End If


    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnBookingDelete.Click
        Dim dt As New DataTable
        If XtraMessageBox.Show("ยืนยันข้อมูลถูกต้อง ต้องการลบ Booking เลขที่ " & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BNO").ToString(), "ตกลง", MessageBoxButtons.YesNo) <> DialogResult.No Then
            Dim sql As String
            sql = "Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID where BOOKID ='" & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString() & "' ;"
            dt = connectDB.GetTable(sql)
            For i As Integer = 0 To dt.Rows.Count - 1
                sql = "UPDATE ctnmain SET CTNSTAT = '1',CTNSEALID = '',CTNWEIGHT ='0' where CTNMAINID = '" & dt.Rows(i)("CTNMAINID").ToString & "';"
                connectDB.ExecuteNonQuery(sql)


            Next
            sql = "DELETE FROM borrow where idborrow = '" & idborrow & "';"
            connectDB.ExecuteNonQuery(sql)
            sql = "DELETE FROM booking where BOOKINGID = '" & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString() & "';"
            connectDB.ExecuteNonQuery(sql)
            LoadBookingname()
        End If

    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        Dim nextform As frmdel_ContainerBooking = New frmdel_ContainerBooking(slcVoyage.EditValue)
        nextform.Show()



    End Sub

    Private Sub btnLoading_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnLoading.ItemClick
        Dim nextform As frmrptLoading = New frmrptLoading(slcVoyage.EditValue)
        nextform.Show()
    End Sub

    Private Sub BarButtonItem18_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem18.ItemClick
        Me.FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        Me.FolderBrowserDialog1.ShowNewFolderButton = True
        Me.FolderBrowserDialog1.SelectedPath = "C:\"
        If (Me.FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            Try
                Dim t As Thread = New Thread(New ThreadStart(AddressOf Me.excelReportETD))
                t.Start()
            Catch exception As System.Exception
                ProjectData.SetProjectError(exception)
                ProjectData.ClearProjectError()
            End Try
        End If
    End Sub
    Private Sub excelReportETD()
        Dim count As Integer = 7
        Dim dateout As String = ""
        Dim ctntype As String = ""
        Dim pathExcel As String = Me.FolderBrowserDialog1.SelectedPath
        pathExcel = FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add

        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)

        With excelsheets

            .PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4
            .PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape


            .Range("A1").ColumnWidth = 3.57
            .Range("B1").ColumnWidth = 20.29
            .Range("C1").ColumnWidth = 47.43
            .Range("D1").ColumnWidth = 64.57
            .Range("E1").ColumnWidth = 22
            .Range("F1").ColumnWidth = 80.57
            .Range("G1").ColumnWidth = 40.71
            .Range("H1").ColumnWidth = 40.29
            .Range("I1").ColumnWidth = 29.14
            .Range("J1").ColumnWidth = 27
            .Range("K1").ColumnWidth = 11.57
            .Range("L1").ColumnWidth = 23
            .Range("M1").ColumnWidth = 20.57
            .Range("N1").ColumnWidth = 30.13
            .Range("A1:M500").Font.Name = "Arial"
            .Range("A1:M500").Font.Size = 12
            .Rows("1:1").rowheight = 38
            .Rows("2:500").rowheight = 27

            With .Range("D1:F1")
                .Font.Size = 20
                .Value = "LOADING LIST V.S" & slcVoyage.Text
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True

            End With

            With .Range("B2:C2")
                .Font.Size = 14
                .Value = My.Settings.NameCompany
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True

            End With
            With .Range("B3:C3")
                .Font.Size = 14
                .Value = "87/1 M.5 T.Boonumron, Kantang, Trang, Thailand "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
                .Merge()
            End With
            With .Range("A5:A6")
                .Font.Size = 14
                .Value = "No."
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True

            End With

            With .Range("B5:B6")
                .Font.Size = 14
                .Value = "ETD from kantang "
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("C5:C6")
                .Font.Size = 14
                .Value = "Name of Cargo Owner"
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With


            With .Range("D5:D6")
                .Value = "Address"
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("E5:E6")
                .Value = "Company no."
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F5:F6")
                .Value = "Consingnee"
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G5:G6")
                .Value = "Incoterm "
                .Font.Bold = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
                .Font.Underline = True
            End With
            With .Range("H5:H6")
                .Value = "From " & Environment.NewLine & "Kantang Thailand " & Environment.NewLine & "to Penang"
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
            End With
            With .Range("I5:I6")
                .Value = " Conveyance"
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With
            With .Range("J5:J6")
                .Value = " Invoice Value"
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With
            With .Range("K5:K6")
                .Value = "Insured" & Environment.NewLine & "Value" & Environment.NewLine & "(110%)"
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
            End With
            With .Range("L5:L6")
                .Value = "Number of container"
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
            End With
            With .Range("M5:M6")
                .Value = "Booking No."
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
            End With
            With .Range("N5:N6")
                .Value = "Remark"
                .Font.Bold = True
                .WrapText = True
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With
            .Range("A5:L6").Borders.Weight = 2


            Dim dt As New DataTable
            Dim sql As String
            sql = "Select VOYDATEES,BSHIPNAME,conname,BINCOTERM,VOYVESNAMES,place,BINVOICE,BNO,BCTNNO,BCTNTYPE,BREMARK ,SHIPADD,COMPANYNO  from voyage join booking on voyage.VOYAGEID = booking.BVOYAGE left join booking_bl on booking.BOOKINGID = booking_bl.booking_id join shipper on shipper.shipperid = booking.BSHIP  where BCHECKLOAD ='True' and BVOYAGE = '" & slcVoyage.EditValue & "' order by  BSHIPNAME,conname ;"
            dt = connectDB.GetTable(sql)
            Dim number As Integer = 1
            count = 7
            Dim sumctn As Integer = 0
            For i As Integer = 0 To dt.Rows.Count - 1

                .Range("A" & count.ToString & ":N" & count.ToString).Borders.Weight = 2
                With .Range("A" & count.ToString & ":A" & count.ToString)
                    .NumberFormat = "@"
                    .Value = number
                    .Font.Bold = True
                    .WrapText = True
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                number = number + 1

                With .Range("B" & count.ToString & ":B" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("VOYDATEES").ToString

                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    dateout = dt.Rows(i)("VOYDATEES").ToString
                End With

                With .Range("C" & count.ToString & ":C" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BSHIPNAME").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("D" & count.ToString & ":D" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BSHIPNAME").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("E" & count.ToString & ":E" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("COMPANYNO").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("F" & count.ToString & ":F" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("conname").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("G" & count.ToString & ":G" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BINCOTERM").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With
                With .Range("H" & count.ToString & ":H" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("VOYVESNAMES").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With
                With .Range("I" & count.ToString & ":I" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("place").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("J" & count.ToString & ":J" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BINVOICE").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("L" & count.ToString & ":L" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BCTNNO").ToString & " X " & dt.Rows(i)("BCTNTYPE").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("M" & count.ToString & ":M" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BNO").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                sumctn = sumctn + Convert.ToDecimal(dt.Rows(i)("BCTNNO"))
                ctntype = dt.Rows(i)("BCTNTYPE").ToString()

                With .Range("N" & count.ToString & ":N" & count.ToString)
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BREMARK").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With
                count = count + 1
            Next
            count = count + 1

            With .Range("L" & count.ToString & ":L" & count.ToString)
                .NumberFormat = "@"
                .Value = sumctn.ToString & " X " & ctntype.ToString
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
            End With
        End With

        excelapp.Windows.Application.ActiveWindow.DisplayGridlines = False

        Try
            excelbooks.SaveAs(pathExcel.ToString + "\Loading_list_V.SS" + slcVoyage.EditValue + "_ETD_" + dateout + ".xlsx")

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try


    End Sub

    Private Sub BarButtonItem10_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem10.ItemClick
        Dim nextform As frmrpt_bl1 = New frmrpt_bl1
        nextform.Show()
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        Dim frmadd_invoice As New frmadd_invoyage(slcVoyage.EditValue)
        frmadd_invoice.ShowDialog()
    End Sub



    Private Sub TBABookingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TBABookingToolStripMenuItem.Click

        If (Conversions.ToDouble(idbooking) <= 0) Then
            Interaction.MsgBox("กรุณาเลือก Booking เพื่อทำการ Comfirm วัน Close date", MsgBoxStyle.OkOnly, Nothing)
            Exit Sub
        End If
        '  Me.mysql.Close()


        Dim commandText2 As String = "UPDATE booking SET BCONFIRMDAY = 'TBA',BCONFIRMTIME =' '  WHERE BOOKINGID = '" & GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString() & "'; "
        connectDB.ExecuteNonQuery(commandText2)
        Me.LoadBookingname()
    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        idborrow = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString()
        Dim dt As New DataTable
        Dim sql As String
        colindex = ""
        RadioGroup1.SelectedIndex = Nothing
        MemoEdit1.Text = Nothing
        Me.LayoutControlGroup2.Text = ""

        If e.Column.Name = "colCTNSTRING" Then
            colindex = "colCTNSTRING"
            sql = "Select CTNMARKSTR , CTNSTRING ,CTNMARKSTRBOO from ctnmain where CTNMAINID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "';"
            connectDB.GetTable(sql, dt)

            Try
                If dt.Rows.Count > 0 Then
                    Me.LayoutControlGroup2.Text = dt.Rows(0)("CTNSTRING").ToString & "[Container No.]"
                    Me.MemoEdit1.Text = dt.Rows(0)("CTNMARKSTR").ToString
                    Me.MemoEdit1.Tag = "CTNMARKSTR"
                    RadioGroup1.SelectedIndex = If(IsDBNull(dt.Rows(0)("CTNMARKSTRBOO")), Nothing, dt.Rows(0)("CTNMARKSTRBOO"))

                End If
            Catch ex As Exception

                ' MsgBox(ex.ToString)
            End Try
        End If




        If e.Column.Name = "colCTNSEALID" Then
            colindex = "colCTNSEALID"
            sql = "Select CTNMARKSEAL, CTNSTRING ,CTNMARKSEALBOO from ctnmain where CTNMAINID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "';"
            dt = New DataTable
            connectDB.GetTable(sql, dt)

            Try
                If dt.Rows.Count > 0 Then
                    Me.LayoutControlGroup2.Text = dt.Rows(0)("CTNSTRING").ToString & "[SEALID]"
                    Me.MemoEdit1.Text = dt.Rows(0)("CTNMARKSEAL").ToString
                    Me.MemoEdit1.Tag = "CTNMARKSEAL"
                    RadioGroup1.SelectedIndex = If(IsDBNull(dt.Rows(0)("CTNMARKSEALBOO")), Nothing, dt.Rows(0)("CTNMARKSEALBOO"))

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try


        End If



        If e.Column.Name = "colTIMEDATE" Then
            colindex = "colTIMEDATE"

            sql = "Select CTNMARKDATEOUT, CTNSTRING,CTNMARKDATEOUTBOO from ctnmain where CTNMAINID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "';"
            dt = New DataTable
            connectDB.GetTable(sql, dt)

            Try
                If dt.Rows.Count > 0 Then
                    Me.LayoutControlGroup2.Text = dt.Rows(0)("CTNSTRING").ToString & "[Date Out]"
                    Me.MemoEdit1.Text = dt.Rows(0)("CTNMARKDATEOUT").ToString
                    Me.MemoEdit1.Tag = "CTNMARKDATEOUT"
                    RadioGroup1.SelectedIndex = If(IsDBNull(dt.Rows(0)("CTNMARKDATEOUTBOO")), Nothing, dt.Rows(0)("CTNMARKDATEOUTBOO"))

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try


        End If


        If e.Column.Name = "colTIMEDATEIN" Then
            colindex = "colTIMEDATEIN"

            sql = "Select CTNMARKDATEIN, CTNSTRING ,CTNMARKDATEINBOO from ctnmain where CTNMAINID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "';"
            dt = New DataTable
            connectDB.GetTable(sql, dt)

            Try
                If dt.Rows.Count > 0 Then
                    Me.LayoutControlGroup2.Text = dt.Rows(0)("CTNSTRING").ToString & "[Date IN]"
                    Me.MemoEdit1.Text = dt.Rows(0)("CTNMARKDATEIN").ToString
                    Me.MemoEdit1.Tag = "CTNMARKDATEIN"
                    RadioGroup1.SelectedIndex = If(IsDBNull(dt.Rows(0)("CTNMARKDATEINBOO")), Nothing, dt.Rows(0)("CTNMARKDATEINBOO"))

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

        End If


        If e.Column.Name = "colCTNWEIGHT" Then
            colindex = "colCTNWEIGHT"

            sql = "Select CTNMARKWEIGHT, CTNSTRING , CTNMARKWEIGHTBOO from ctnmain where CTNMAINID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "';"
            dt = New DataTable
            connectDB.GetTable(sql, dt)

            Try
                If dt.Rows.Count > 0 Then
                    Me.LayoutControlGroup2.Text = dt.Rows(0)("CTNSTRING").ToString & "[Weight]"
                    Me.MemoEdit1.Text = dt.Rows(0)("CTNMARKWEIGHT").ToString
                    Me.MemoEdit1.Tag = "CTNMARKWEIGHT"
                    RadioGroup1.SelectedIndex = If(IsDBNull(dt.Rows(0)("CTNMARKWEIGHTBOO")), Nothing, dt.Rows(0)("CTNMARKWEIGHTBOO"))

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try



        End If


        If e.Column.Name = "colCOMNAME" Then
            colindex = "colCOMNAME"

            sql = "Select CTNMARKTRANSPORT, CTNSTRING ,   CTNMARKTRANSPORTBOO from ctnmain where CTNMAINID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "';"
            dt = New DataTable
            connectDB.GetTable(sql, dt)

            Try
                If dt.Rows.Count > 0 Then
                    Me.LayoutControlGroup2.Text = dt.Rows(0)("CTNSTRING").ToString & "[Transport]"
                    Me.MemoEdit1.Text = dt.Rows(0)("CTNMARKTRANSPORT").ToString
                    Me.MemoEdit1.Tag = "CTNMARKTRANSPORT"
                    RadioGroup1.SelectedIndex = If(IsDBNull(dt.Rows(0)("CTNMARKTRANSPORTBOO")), Nothing, dt.Rows(0)("CTNMARKTRANSPORTBOO"))

                End If
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

        End If

    End Sub

    Private Sub SimpleButton5_Click(sender As Object, e As EventArgs) Handles SimpleButton5.Click
        Dim stat As Integer = 0
        Dim commandText3 As String = Nothing
        'If (Me.RadioGroup1.Checked) Then
        stat = RadioGroup1.SelectedIndex
        'ElseIf (Me.RadioButton2.Checked) Then
        '    stat = 2
        'End If
        'If (Me.TextBox3.Text.Length = 0) Then
        '    stat = 0
        'End If
        'If (Me.mysql.checkstate() = ConnectionState.Closed) Then
        '    Me.mysql.Open()
        'End If

        If (Me.colindex = "colCTNSTRING") Then
            commandText3 = String.Concat(New String() {"UPDATE ctnmain SET CTNMARKSTR ='" & Me.MemoEdit1.EditValue & "', CTNMARKSTRBOO ='" & Conversions.ToString(stat) & "' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; "})
        ElseIf (Me.colindex = "colCTNSEALID") Then
            commandText3 = String.Concat(New String() {"UPDATE ctnmain SET CTNMARKSEAL ='" & Me.MemoEdit1.EditValue & "', CTNMARKSEALBOO ='" & Conversions.ToString(stat) & "' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; "})
        ElseIf (Me.colindex = "colTIMEDATE") Then
            commandText3 = String.Concat(New String() {"UPDATE ctnmain SET CTNMARKDATEOUT ='" & Me.MemoEdit1.EditValue & "', CTNMARKDATEOUTBOO ='" & Conversions.ToString(stat) & "' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; "})
        ElseIf (Me.colindex = "colTIMEDATEIN") Then
            commandText3 = String.Concat(New String() {"UPDATE ctnmain SET CTNMARKDATEIN ='" & Me.MemoEdit1.EditValue & "', CTNMARKDATEINBOO ='" & Conversions.ToString(stat) & "' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; "})
        ElseIf (Me.colindex = "colCTNWEIGHT") Then
            commandText3 = String.Concat(New String() {"UPDATE ctnmain SET CTNMARKWEIGHT ='" & Me.MemoEdit1.EditValue & "', CTNMARKWEIGHTBOO ='" & Conversions.ToString(stat) & "' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; "})
        ElseIf (Me.colindex = "colCOMNAME") Then
            commandText3 = String.Concat(New String() {"UPDATE ctnmain SET CTNMARKTRANSPORT ='" & Me.MemoEdit1.EditValue & "', CTNMARKTRANSPORTBOO ='" & Conversions.ToString(stat) & "' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; "})
        End If
        Try
            If commandText3 Is Nothing Then
                Exit Sub
            End If
            connectDB.ExecuteNonQuery(commandText3)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub SimpleButton3_Click_1(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        Dim commandText3 As String = Nothing
        Dim str As String = Nothing
        Dim typectn As String = Nothing
        Dim sql As String = ""
        Dim sql1 As String
        Dim sql2 As String
        ' GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString()


        If GridView1.RowCount > 0 Then
            For i As Integer = 0 To GridView1.RowCount - 1
                Dim check_count As Integer = 0
                sql = "UPDATE borrow SET COMNAME = '" & GridView1.GetDataRow(i)("COMNAME").ToString & "',CARID = '" & GridView1.GetDataRow(i)("CARID").ToString & "', TIMEDATE ='" & GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString() & "' ,  TIMEHHMM = '" & GridView1.GetRowCellDisplayText(i, ("TIMEHHMM")).ToString() & "' , TIMEDATEIN ='" & GridView1.GetRowCellDisplayText(i, ("TIMEDATEIN")).ToString() & "', TIMEHHMMIN ='" & GridView1.GetRowCellDisplayText(i, ("TIMEHHMMIN")).ToString() & "' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                connectDB.ExecuteNonQuery(sql)
                If GridView1.GetDataRow(i)("BRCHECK_") = False Then
                    sql = "UPDATE borrow SET BRCHECK='0' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                    connectDB.ExecuteNonQuery(sql)
                    sql = "DELETE FROM rptwhl where rptidctn = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "' and rptstat ='MS';"
                    connectDB.ExecuteNonQuery(sql)
                    sql = "UPDATE ctnmain SET CTNSTAT ='1' where CTNMAINID = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "'; "
                    connectDB.ExecuteNonQuery(sql)
                End If
                If GridView1.GetDataRow(i)("BRCHECK1_") = False Then
                    If GridView1.GetDataRow(i)("BRCHECK_") = False And GridView1.GetDataRow(i)("BRCHECK1_") = False Then
                        sql = "UPDATE borrow SET  BRCHECK1='0'  , BRCHECK ='0' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                        connectDB.ExecuteNonQuery(sql)

                    ElseIf GridView1.GetDataRow(i)("BRCHECK_") = True And GridView1.GetDataRow(i)("BRCHECK1_") = False Then
                        sql = "UPDATE borrow SET  BRCHECK1='0'  , BRCHECK ='1' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                        connectDB.ExecuteNonQuery(sql)
                    End If
                    sql = "DELETE FROM rptwhl where rptidctn = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "' and rptstat ='FL';"
                    connectDB.ExecuteNonQuery(sql)

                    If GridView1.GetDataRow(i)("BRCHECK_") = False And GridView1.GetDataRow(i)("BRCHECK1_") = False Then
                        sql = "UPDATE ctnmain SET CTNSTAT ='1' where CTNMAINID = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "'; "
                        connectDB.ExecuteNonQuery(sql)
                    ElseIf GridView1.GetDataRow(i)("BRCHECK_") = True And GridView1.GetDataRow(i)("BRCHECK1_") = False Then
                        sql = "UPDATE ctnmain SET CTNSTAT ='2' where CTNMAINID = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "'; "
                        connectDB.ExecuteNonQuery(sql)
                    End If
                End If

                Try
                    If GridView1.GetDataRow(i)("BRCHECK_") = True Then
                        Dim sql3 As String
                        Dim dt As New DataTable
                        sql = "Select COUNT(rptidctn) as ctncount from rptwhl where rptidctn ='" & GridView1.GetDataRow(i)("CTNID").ToString() & "' and rptstat ='MS' ;"
                        dt = connectDB.GetTable(sql)

                        check_count = dt.Rows(0)("ctncount")

                        If (check_count <= 0) Then
                            str = If(GridView1.GetDataRow(i)("CTNSIZE").ToString.Trim = "40'HC", "45G1", "22G1")


                            sql = "UPDATE ctnmain SET CTNSTAT ='2' where CTNMAINID = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "'; "
                            connectDB.ExecuteNonQuery(sql)


                            Me.stringDate = GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString().ToString.Split("-")
                            Me.stringhour = GridView1.GetRowCellDisplayText(i, ("TIMEHHMM")).ToString().ToString.Split(":")
                            If stringDate.ToArray.Length > 1 Then
                                Me.sumtxt = String.Concat(New String() {Me.stringDate(2), Me.stringDate(1), Me.stringDate(0), Me.stringhour(0), Me.stringhour(1)})
                                sumint = Convert.ToDecimal(sumtxt) + 100
                            Else
                                sumtxt = Nothing
                                sumint = 0
                            End If
                            ' GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString()

                            sql1 = "  `rptctn`,"
                            sql2 = "'" & GridView1.GetDataRow(i)("CTNSTRING").ToString() & "',"
                            sql1 += "`rptstat`,"
                            sql2 += "'MS',"
                            sql1 += "     `rptctndate`,"
                            sql2 += "'" & If(sumtxt Is Nothing, "", MySqlHelper.EscapeString(String.Concat(Me.sumtxt, "00"))) & "',"
                            sql1 += "     `rptctndatetpe`,"
                            sql2 += "'" & If(sumint = 0, "", MySqlHelper.EscapeString(String.Concat(Me.sumint.ToString(), "00"))) & "',"
                            sql1 += "     `rpttype`,"
                            sql2 += "'" & MySqlHelper.EscapeString(str) & "',"
                            sql1 += "     `rptidctn`,"
                            sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetDataRow(i)("CTNID").ToString()) & "',"
                            sql1 += "     `rptagent`,"
                            sql2 += "'" & MySqlHelper.EscapeString(agentline) & "'"
                            sql1 += "     `rptdatetype1`,"
                            sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString()) & "'"
                            sql3 = "INSERT INTO rptwhl  ( " & sql1 & " )  VALUES ( " & sql2 & "  );"
                            connectDB.ExecuteNonQuery(sql)
                            sql = "UPDATE borrow SET   BRCHECK ='1' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                            connectDB.ExecuteNonQuery(sql)


                        End If
                    End If

                    check_count = 0


                    If GridView1.GetDataRow(i)("BRCHECK1_") = True Then
                        Dim dt As New DataTable
                        sql = "Select COUNT(rptidctn) as ctncount from rptwhl where rptidctn ='" & GridView1.GetDataRow(i)("CTNID").ToString() & "' and rptstat ='FL' ;"
                        dt = connectDB.GetTable(sql)
                        Dim sql3 As String
                        check_count = dt.Rows(0)("ctncount")

                        If (check_count <= 0) Then
                            str = If(GridView1.GetDataRow(i)("CTNSIZE").ToString.Trim = "40'HC", "45G1", "22G1")


                            sql = "UPDATE ctnmain SET CTNSTAT ='3' where CTNMAINID = '" & GridView1.GetDataRow(i)("CTNID").ToString() & "'; "
                            connectDB.ExecuteNonQuery(sql)
                            Me.stringDate = GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString().Split("-")
                            Me.stringhour = GridView1.GetRowCellDisplayText(i, ("TIMEHHMM")).ToString().Split(":")
                            If stringDate.ToArray.Length > 1 Then
                                Me.sumtxt = String.Concat(New String() {Me.stringDate(2), Me.stringDate(1), Me.stringDate(0), Me.stringhour(0), Me.stringhour(1)})
                                sumint = Convert.ToDecimal(sumtxt) + 100
                            Else
                                sumtxt = Nothing
                                sumint = 0
                            End If


                            sql1 = "  `rptctn`,"
                            sql2 = "'" & GridView1.GetDataRow(i)("CTNSTRING").ToString() & "',"
                            sql1 += "`rptstat`,"
                            sql2 += "'FL',"
                            sql1 += "     `rptctndate`,"
                            sql2 += "'" & If(sumtxt Is Nothing, "", MySqlHelper.EscapeString(String.Concat(Me.sumtxt, "00"))) & "',"
                            sql1 += "     `rptctndatetpe`,"
                            sql2 += "'" & If(sumint = 0, "", MySqlHelper.EscapeString(String.Concat(Me.sumint.ToString(), "00"))) & "',"
                            sql1 += "     `rpttype`,"
                            sql2 += "'" & MySqlHelper.EscapeString(str) & "',"
                            sql1 += "     `rptidctn`,"
                            sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetDataRow(i)("CTNID").ToString()) & "',"
                            sql1 += "     `rptagent`,"
                            sql2 += "'" & MySqlHelper.EscapeString(agentline) & "'"
                            sql1 += "     `rptdatetype1`,"
                            sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString()) & "'"
                            sql3 = "INSERT INTO rptwhl  ( " & sql1 & " )  VALUES ( " & sql2 & "  );"
                            connectDB.ExecuteNonQuery(sql)
                            sql = "UPDATE borrow SET   BRCHECK1 ='1' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                            connectDB.ExecuteNonQuery(sql)
                        End If
                    End If
                Catch ex As System.Exception
                    MsgBox(ex.ToString)
                End Try
                'GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString()


                sql = "UPDATE borrow SET BRBILLNAME='" & GridView1.GetDataRow(i)("idborrow").ToString & "' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "
                connectDB.ExecuteNonQuery(sql)


                sql = "UPDATE ctnmain SET CTNSTRING ='" & GridView1.GetDataRow(i)("CTNSTRING").ToString & "' , CTNSEALID = '" & GridView1.GetDataRow(i)("CTNSEALID").ToString & "', CTNWEIGHT ='" & GridView1.GetRowCellDisplayText(i, ("CTNWEIGHT")).ToString() & "' , CTNTAREWEIGHT   = '" & GridView1.GetRowCellDisplayText(i, ("CTNTAREWEIGHT")).ToString() & "'   , CTNREALDATEIN   = '" & GridView1.GetRowCellDisplayText(i, ("CTNREALDATEIN")).ToString() & "'     , CTNREALDATEOUT   = '" & GridView1.GetRowCellDisplayText(i, ("CTNREALDATEOUT")).ToString() & "'       , CTNVOYS ='" & slcVoyage.Text & "' where CTNMAINID = '" & GridView1.GetDataRow(i)("CTNMAINID").ToString & "'; "
                connectDB.ExecuteNonQuery(sql)

                Dim dt4 As New DataTable

                If dt4.Rows.Count > 0 Then
                    If dt4.Rows(0)("CTNREALDATEIN").ToString.Trim.Length > 0 Then

                    Else
                        Dim dateStr As String
                        Dim timeStr As String

                        If GridView1.GetRowCellDisplayText(i, ("TIMEDATEIN")).ToString = "01-01-0001" Then

                        Else
                            sql2 = "UPDATE ctnmain SET CTNREALDATEIN = '" & GridView1.GetRowCellDisplayText(i, ("TIMEDATEIN")).ToString() & " " & GridView1.GetRowCellDisplayText(i, ("TIMEHHMMIN")).ToString() & "' WHERE CTNMAINID = '" & GridView1.GetDataRow(i)("CTNMAINID").ToString & "'; "
                            connectDB.ExecuteNonQuery(sql2)
                        End If


                    End If



                    If dt4.Rows(0)("CTNREALDATEOUT").ToString.Trim.Length > 0 Then

                    Else

                        If GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString = "01-01-0001" Then

                        Else
                            sql2 = "UPDATE ctnmain SET CTNREALDATEOUT = '" & GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString() & " " & GridView1.GetRowCellDisplayText(i, ("TIMEHHMM")).ToString() & "' WHERE CTNMAINID = '" & GridView1.GetDataRow(i)("CTNMAINID").ToString & "'; "
                            connectDB.ExecuteNonQuery(sql2)

                        End If
                        Dim dateStr As String
                        Dim timeStr As String



                    End If


                End If


            Next
        End If
        LoadBooking()
        MsgBox("บันทึกเสร็จสิ้น")


    End Sub

    Private Sub btnCTNborrow_Click(sender As Object, e As EventArgs) Handles btnCTNborrow.Click


    End Sub

    Private Sub GridView1_CustomColumnDisplayText(sender As Object, e As CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        'Dim view As ColumnView = TryCast(sender, ColumnView)
        Dim first As String = ""
        Dim sec As String = ""
        Dim third As String = ""
        Dim full As String = ""
        Dim full1 As String = ""
        Dim full2 As String = ""
        Dim full3 As String = ""
        Dim hh As String = ""
        Dim mm As String = ""

        If e.Column.FieldName = "CTNWEIGHT" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            '    Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
            Try
                Dim weight As Decimal = Convert.ToDecimal(e.Value)

                e.DisplayText = weight.ToString("#,###.00")

                ' GridView1.SetRowCellValue(e.ListSourceRowIndex, e.Column.FieldName, e.DisplayText)



            Catch ex As Exception
                ' e.DisplayText = ""

            End Try

        End If

        If e.Column.FieldName = "CTNTAREWEIGHT" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            '    Dim currencyType As Integer = CInt(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "CurrencyType"))
            Try
                Dim weight As Decimal = Convert.ToDecimal(e.Value)

                e.DisplayText = weight.ToString("#,###.00")

                ' GridView1.SetRowCellValue(e.ListSourceRowIndex, e.Column.FieldName, e.DisplayText)



            Catch ex As Exception
                ' e.DisplayText = ""

            End Try

        End If


        If e.Column.FieldName = "CTNREALDATEIN" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            If e.Value.ToString.Trim.Length = 12 Then
                full = Conversions.ToString(e.Value)
                mm = full.Substring(full.Length - 2)
                full = Microsoft.VisualBasic.Strings.Left(full, full.Length - 2)
                hh = full.Substring(full.Length - 2)
                full = Microsoft.VisualBasic.Strings.Left(full, full.Length - 2)

                first = full.Substring(full.Length - 4)
                full1 = Microsoft.VisualBasic.Strings.Left(full, full.Length - 4)
                sec = full1.Substring(full1.Length - 2)
                third = Microsoft.VisualBasic.Strings.Left(full1, full1.Length - 2)
                e.DisplayText = String.Concat(third, "-", sec, "-", first, " ", hh, ":", mm)
                'Exit Sub

            End If

        End If
        If e.Column.FieldName = "CTNREALDATEOUT" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            If e.Value.ToString.Trim.Length = 12 Then
                full = Conversions.ToString(e.Value)
                mm = full.Substring(full.Length - 2)
                full = Microsoft.VisualBasic.Strings.Left(full, full.Length - 2)
                hh = full.Substring(full.Length - 2)
                full = Microsoft.VisualBasic.Strings.Left(full, full.Length - 2)

                first = full.Substring(full.Length - 4)
                full1 = Microsoft.VisualBasic.Strings.Left(full, full.Length - 4)
                sec = full1.Substring(full1.Length - 2)
                third = Microsoft.VisualBasic.Strings.Left(full1, full1.Length - 2)
                e.DisplayText = String.Concat(third, "-", sec, "-", first, " ", hh, ":", mm)
                'Exit Sub

            End If

        End If

        If e.Column.FieldName = "TIMEDATE" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            If e.Value.ToString.Trim.Length = 8 Then
                full = Conversions.ToString(e.Value)
                first = full.Substring(full.Length - 4)
                full1 = Microsoft.VisualBasic.Strings.Left(full, full.Length - 4)
                sec = full1.Substring(full1.Length - 2)
                third = Microsoft.VisualBasic.Strings.Left(full1, full1.Length - 2)
                e.DisplayText = String.Concat(third, "-", sec, "-", first)
                'Exit Sub

            End If

        End If

        If e.Column.FieldName = "TIMEHHMM" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then

            If e.Value.ToString.Trim.Length = 4 Then
                full = Conversions.ToString(e.Value)
                first = full.Substring(full.Length - 2)
                full1 = Microsoft.VisualBasic.Strings.Left(full, full.Length - 2)
                e.DisplayText = String.Concat(full1, ":", first)
                '  GridView1.SetRowCellValue(e.ListSourceRowIndex, e.Column.FieldName, e.DisplayText)
                Exit Sub


            End If
        End If

        If e.Column.FieldName = "TIMEDATEIN" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then
            If e.Value.ToString.Trim.Length = 8 Then
                full = Conversions.ToString(e.Value)
                first = full.Substring(full.Length - 4)
                full1 = Microsoft.VisualBasic.Strings.Left(full, full.Length - 4)
                sec = full1.Substring(full1.Length - 2)
                third = Microsoft.VisualBasic.Strings.Left(full1, full1.Length - 2)
                e.DisplayText = String.Concat(third, "-", sec, "-", first)
                '  GridView1.SetRowCellValue(e.ListSourceRowIndex, e.Column.FieldName, e.DisplayText)
                Exit Sub

            End If

        End If
        If e.Column.FieldName = "TIMEHHMMIN" AndAlso e.ListSourceRowIndex <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then

            If e.Value.ToString.Trim.Length = 4 Then
                full = Conversions.ToString(e.Value)
                first = full.Substring(full.Length - 2)
                full1 = Microsoft.VisualBasic.Strings.Left(full, full.Length - 2)
                e.DisplayText = String.Concat(full1, ":", first)
                ' GridView1.SetRowCellValue(e.ListSourceRowIndex, e.Column.FieldName, e.DisplayText)
                Exit Sub
            End If
        End If


    End Sub



    Private Sub GridView1_RowClick(sender As Object, e As RowClickEventArgs) Handles GridView1.RowClick
        idborrow = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString()
    End Sub

    Private Sub GridView1_CustomDrawRowIndicator(sender As Object, e As RowIndicatorCustomDrawEventArgs) Handles GridView1.CustomDrawRowIndicator
        e.Info.DisplayText = (e.RowHandle + 1).ToString()

    End Sub

    Private Sub GridView2_CustomDrawRowIndicator(sender As Object, e As RowIndicatorCustomDrawEventArgs) Handles GridView2.CustomDrawRowIndicator
        e.Info.DisplayText = (e.RowHandle + 1).ToString()

    End Sub

    Private Sub GateInOutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GateInOutToolStripMenuItem.Click
        If (GridView1.FocusedRowHandle < 0) Then
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)
        Else

            Dim rpt1 As New rptGateIn
            Dim sql As String
            ykpset.Tables("GATEINOUT").Clear()
            sql = "Select remark,BCOM,TIMEDATE,TIMEHHMM,BNO,CTNSTRING,CTNSEALID,CTNAGENT,CTNSIZE, CTNTAREWEIGHT ,SHIPNAME,BNFORWARDERNAME,COMNAME,CARID,VOYVESNAMES,VOYAGEID,POL,TSPORT
,BFINALDEST,BCOM,voyname from borrow join booking on borrow.BOOKID = booking.BOOKINGID join voyage on voyage.VOYAGEID = booking.BVOYAGE  join shipper on shipper.SHIPPERID = booking.BSHIP join ctnmain on ctnmain.CTNMAINID = borrow.CTNID   where idborrow ='" & idborrow & "' ;"
            connectDB.GetTable(sql, ykpset.Tables("GATEINOUT"))

            rpt1 = New rptGateIn
            'rptBankSum = New rptReportBankSumary
            rpt1.DataSource = ykpset
            'rptDailyReportRev.DataSource = dtset
            '' rptDailyReportRev.Parameters("DateStart").Value = DateEdit1.EditValue 'กำหนดพารามิเตอร์ ให้ dat_print ในform report1
            'rptDailyReportRev.XrSubreport1.ReportSource = rptBankSum
            'rptDailyReportRev.Parameters("ReportType").Value = If(RadioGroup1.EditValue = 1, "นอก", "ใน")
            'rptDailyReportRev.Parameters("DateStart").Value = "ตั้งแต่วันที่ " & Convert.ToDateTime(DateEdit1.EditValue).ToString("dd-MM-yyyy") & "ถึงวันที่ " & Convert.ToDateTime(DateEdit2.EditValue).ToString("dd-MM-yyyy")
            rpt1.CreateDocument()

            rpt1.ShowPreview()

        End If
    End Sub



    Private Sub GridView1_CellValueChanging(sender As Object, e As CellValueChangedEventArgs) Handles GridView1.CellValueChanging
        idborrow = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString()
        Dim dt As New DataTable
        Dim sql As String
        colindex = ""

    End Sub

    Private Sub btnCTNborrow_ButtonClick(sender As Object, e As ButtonPressedEventArgs) Handles btnCTNborrow.ButtonClick
        Dim commandText3 As String
        Dim str As String = Nothing
        Dim typectn As String = Nothing
        Dim sql As String
        Dim check_count As Integer = 0
        Dim sql1 As String = ""
        Dim sql2 As String = ""
        Dim sql3 As String = ""

        Try

            commandText3 = "UPDATE borrow SET COMNAME = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("COMNAME").ToString() & "',CARID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CARID").ToString() & "', TIMEDATE ='" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString & "' ,  TIMEHHMM = '" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEHHMM")).ToString & "' , TIMEDATEIN ='" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATEIN")).ToString & "', TIMEHHMMIN ='" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEHHMMIN")).ToString & "' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString() & "'; "

            connectDB.ExecuteNonQuery(commandText3)

        Catch exception1 As System.Exception
            ProjectData.SetProjectError(exception1)
            ProjectData.ClearProjectError()
        End Try

        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK_") = False Then
            sql = "UPDATE borrow SET BRCHECK='0' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString & "'; "
            connectDB.ExecuteNonQuery(sql)
            sql = "DELETE FROM rptwhl where rptidctn = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "' and rptstat ='MS';"
            connectDB.ExecuteNonQuery(sql)
            sql = "UPDATE ctnmain SET CTNSTAT ='1' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "'; "
            connectDB.ExecuteNonQuery(sql)
        End If
        If GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK1_") = False Then
            If GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK_") = False And GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK1_") = False Then
                sql = "UPDATE borrow SET  BRCHECK1='0'  , BRCHECK ='0' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString & "'; "
                connectDB.ExecuteNonQuery(sql)

            ElseIf GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK_") = True And GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK1_") = False Then
                sql = "UPDATE borrow SET  BRCHECK1='0'  , BRCHECK ='1' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString & "'; "
                connectDB.ExecuteNonQuery(sql)
            End If
            sql = "DELETE FROM rptwhl where rptidctn = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "' and rptstat ='FL';"
            connectDB.ExecuteNonQuery(sql)

            If GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK_") = False And GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK1_") = False Then
                sql = "UPDATE ctnmain SET CTNSTAT ='1' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "'; "
                connectDB.ExecuteNonQuery(sql)
            ElseIf GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK_") = True And GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK1_") = False Then
                sql = "UPDATE ctnmain SET CTNSTAT ='2' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "'; "
                connectDB.ExecuteNonQuery(sql)
            End If
        End If




        Try

            If GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK_") = True Then


                Dim dt As New DataTable
                sql = "Select COUNT(rptidctn) as ctncount from rptwhl where rptidctn ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "' and rptstat ='MS' ;"
                dt = connectDB.GetTable(sql)

                check_count = dt.Rows(0)("ctncount")

                ' If (check_count <= 0) Then
                'Me.mySqlCommand.CommandText(String.Concat("Select COUNT(rptidctn) As ctncount from rptwhl where rptidctn ='", frmview_voyage.idcontainer, "' and rptstat ='MS' ;"))

                If (check_count <= 0) Then

                    str = If(GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNSIZE").ToString.Trim = "40'HC", "45G1", "22G1")
                    Me.agentline = Microsoft.VisualBasic.Strings.Trim(Conversions.ToString(GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNAGENT")))
                    'Me.mySqlCommand.CommandText(String.Concat("Select CTNSIZE,CTNAGENT from ctnmain where CTNMAINID ='", frmview_voyage.idcontainer, "';"))

                    commandText3 = "UPDATE ctnmain SET CTNSTAT ='2' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "'; "
                    connectDB.ExecuteNonQuery(commandText3)

                    Me.stringDate = GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString.Split("-")
                    Me.stringhour = GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEHHMM")).ToString.Split(":")
                    If stringDate.ToArray.Length > 1 Then
                        Me.sumtxt = String.Concat(New String() {Me.stringDate(2), Me.stringDate(1), Me.stringDate(0), Me.stringhour(0), Me.stringhour(1)})
                        sumint = Convert.ToDecimal(sumtxt) + 100
                    Else
                        sumtxt = Nothing
                        sumint = 0
                    End If
                    sql1 = "  `rptctn`,"
                    sql2 = "'" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNSTRING").ToString() & "',"
                    sql1 += "`rptstat`,"
                    sql2 += "'FL',"
                    sql1 += "     `rptctndate`,"
                    sql2 += "'" & If(sumtxt Is Nothing, "", MySqlHelper.EscapeString(String.Concat(Me.sumtxt, "00"))) & "',"
                    sql1 += "     `rptctndatetpe`,"
                    sql2 += "'" & If(sumint = 0, "", MySqlHelper.EscapeString(String.Concat(Me.sumint.ToString(), "00"))) & "',"
                    sql1 += "     `rpttype`,"
                    sql2 += "'" & MySqlHelper.EscapeString(str) & "',"
                    sql1 += "     `rptidctn`,"
                    sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString()) & "',"
                    sql1 += "     `rptagent`,"
                    sql2 += "'" & MySqlHelper.EscapeString(agentline) & "'"
                    sql1 += "     `rptdatetype1`,"
                    sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString()) & "'"
                    sql3 = "INSERT INTO rptwhl  ( " & sql1 & " )  VALUES ( " & sql2 & "  );"
                    connectDB.ExecuteNonQuery(sql)

                    sql = "UPDATE borrow SET   BRCHECK ='1' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString & "'; "
                    connectDB.ExecuteNonQuery(sql)

                    'commandText3 = String.Concat("UPDATE borrow SET BRCHECK='1' where idborrow = '", frmview_voyage.idborrow, "'; ")

                End If
            End If
            check_count = 0


            If GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRCHECK1_") = True Then


                Dim dt As New DataTable
                sql = "Select COUNT(rptidctn) as ctncount from rptwhl where rptidctn ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "' and rptstat ='FL' ;"
                dt = connectDB.GetTable(sql)

                check_count = dt.Rows(0)("ctncount")

                'Me.mySqlCommand.CommandText(String.Concat("Select COUNT(rptidctn) as ctncount from rptwhl where rptidctn ='", frmview_voyage.idcontainer, "' and rptstat ='FL' ;"))

                If (check_count <= 0) Then

                    'Me.mySqlCommand.CommandText(String.Concat("Select CTNSIZE,CTNAGENT from ctnmain where CTNMAINID ='", frmview_voyage.idcontainer, "';"))

                    str = If(GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNSIZE").ToString.Trim = "40'HC", "45G1", "22G1")
                    Me.agentline = Microsoft.VisualBasic.Strings.Trim(Conversions.ToString(GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNAGENT")))



                    sql = "UPDATE ctnmain SET CTNSTAT ='3' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString() & "'; "
                    connectDB.ExecuteNonQuery(sql)


                    Me.stringDate = GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString.Split("-")
                    Me.stringhour = GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEHHMM")).ToString().Split(":")
                    If stringDate.ToArray.Length > 1 Then
                        Me.sumtxt = String.Concat(New String() {Me.stringDate(2), Me.stringDate(1), Me.stringDate(0), Me.stringhour(0), Me.stringhour(1)})
                        sumint = Convert.ToDecimal(sumtxt) + 100
                    Else
                        sumtxt = Nothing
                        sumint = 0
                    End If


                    sql1 = "  `rptctn`,"
                    sql2 = "'" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNSTRING").ToString() & "',"
                    sql1 += "`rptstat`,"
                    sql2 += "'FL',"
                    sql1 += "     `rptctndate`,"
                    sql2 += "'" & If(sumtxt Is Nothing, "", MySqlHelper.EscapeString(String.Concat(Me.sumtxt, "00"))) & "',"
                    sql1 += "     `rptctndatetpe`,"
                    sql2 += "'" & If(sumint = 0, "", MySqlHelper.EscapeString(String.Concat(Me.sumint.ToString(), "00"))) & "',"
                    sql1 += "     `rpttype`,"
                    sql2 += "'" & MySqlHelper.EscapeString(str) & "',"
                    sql1 += "     `rptidctn`,"
                    sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString()) & "',"
                    sql1 += "     `rptagent`,"
                    sql2 += "'" & MySqlHelper.EscapeString(agentline) & "'"
                    sql1 += "     `rptdatetype1`,"
                    sql2 += "'" & MySqlHelper.EscapeString(GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString()) & "'"
                    sql3 = "INSERT INTO rptwhl  ( " & sql1 & " )  VALUES ( " & sql2 & "  );"
                    connectDB.ExecuteNonQuery(sql)


                    sql = "UPDATE borrow SET   BRCHECK1 ='1' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString & "'; "
                    connectDB.ExecuteNonQuery(sql)


                End If
            End If
        Catch exception11 As System.Exception
            ProjectData.SetProjectError(exception11)
            Interaction.MsgBox(exception11.ToString(), MsgBoxStyle.OkOnly, Nothing)
            ProjectData.ClearProjectError()
        End Try



        sql = "UPDATE borrow SET BRBILLNAME='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("BRBILLNAME").ToString & "' where idborrow = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString & "'; "
        connectDB.ExecuteNonQuery(sql)


        sql = "UPDATE ctnmain SET CTNSTRING ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNSTRING").ToString & "' , CTNSEALID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNSEALID").ToString & "', CTNTAREWEIGHT = '" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("CTNTAREWEIGHT")) & "'   , CTNWEIGHT ='" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("CTNWEIGHT")) & "' , CTNVOYS ='" & slcVoyage.Text & "'   , CTNREALDATEIN ='" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("CTNREALDATEIN")) & "'     , CTNREALDATEOUT ='" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("CTNREALDATEOUT")) & "'  where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString & "'; "
        connectDB.ExecuteNonQuery(sql)



        Dim sql23 As String
        sql23 = "SELECT CTNREALDATEIN , CTNREALDATEOUT FROM ctnmain WHERE CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString & "'; "
        Dim dt4 As New DataTable
        dt4 = connectDB.GetTable(sql23)

        If dt4.Rows.Count > 0 Then
            If dt4.Rows(0)("CTNREALDATEIN").ToString.Trim.Length > 0 Then

            Else
                Dim dateStr As String
                Dim timeStr As String
                ' sql = "UPDATE borrow SET COMNAME = '" & GridView1.GetDataRow(i)("COMNAME").ToString & "',CARID = '" & GridView1.GetDataRow(i)("CARID").ToString & "', TIMEDATE ='" & GridView1.GetRowCellDisplayText(i, ("TIMEDATE")).ToString() & "' ,  TIMEHHMM = '" & GridView1.GetRowCellDisplayText(i, ("TIMEHHMM")).ToString() & "' , TIMEDATEIN ='" & GridView1.GetRowCellDisplayText(i, ("TIMEDATEIN")).ToString() & "', TIMEHHMMIN ='" & GridView1.GetRowCellDisplayText(i, ("TIMEHHMMIN")).ToString() & "' where idborrow = '" & GridView1.GetDataRow(i)("idborrow").ToString & "'; "

                If GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATEIN")).ToString = "01-01-0001" Then

                Else
                    sql2 = "UPDATE ctnmain SET CTNREALDATEIN = '" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATEIN")).ToString() & " " & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEHHMMIN")).ToString() & "' WHERE CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString & "'; "
                    connectDB.ExecuteNonQuery(sql2)
                End If


            End If



            If dt4.Rows(0)("CTNREALDATEOUT").ToString.Trim.Length > 0 Then

            Else

                If GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString = "01-01-0001" Then

                Else
                    sql2 = "UPDATE ctnmain SET CTNREALDATEOUT = '" & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEDATE")).ToString() & " " & GridView1.GetRowCellDisplayText(GridView1.FocusedRowHandle, ("TIMEHHMM")).ToString() & "' WHERE CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString & "'; "
                    connectDB.ExecuteNonQuery(sql2)

                End If
                Dim dateStr As String
                Dim timeStr As String



            End If


        End If






        LoadBooking()
        MsgBox("บันทึกเสร็จสิ้น")

    End Sub

    Private Sub GridView1_MouseDown(sender As Object, e As MouseEventArgs) Handles GridView1.MouseDown
        Dim view As GridView = TryCast(sender, GridView)
        Dim hi As GridHitInfo = view.CalcHitInfo(e.Location)
        If hi.InRowCell AndAlso hi.HitTest <> GridHitTest.CellButton Then
            If (TryCast(hi.Column.ColumnEdit, RepositoryItemButtonEdit)) IsNot Nothing AndAlso (ModifierKeys And (Keys.Shift Or Keys.Control)) = 0 Then

                view.FocusedRowHandle = hi.RowHandle
                view.FocusedColumn = hi.Column

                view.ShowEditor()
                Dim ed As BaseEdit = view.ActiveEditor
                ed.SendMouse(ed.PointToClient(view.GridControl.PointToScreen(e.Location)), e.Button)
                DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = True
            End If
        End If



        If hi.InRowCell Then
            If TypeOf hi.Column.RealColumnEdit Is RepositoryItemCheckEdit Then
                view.FocusedColumn = hi.Column
                view.FocusedRowHandle = hi.RowHandle
                view.SetFocusedRowCellValue(view.FocusedColumn, Not CBool(view.GetFocusedRowCellValue(view.FocusedColumn)))
            End If
        End If

    End Sub

    Private Sub UpgradeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpgradeToolStripMenuItem.Click

        GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString()

        If (GridView1.FocusedRowHandle < 0) Then
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)
        Else


            Dim commandText3 As String = String.Concat("UPDATE ctnmain SET CTNUPGRADE ='U' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; ")
            connectDB.ExecuteNonQuery(commandText3)
        End If
        LoadBooking()
    End Sub

    Private Sub NormalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NormalToolStripMenuItem.Click
        GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString()

        If (GridView1.FocusedRowHandle < 0) Then
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)
        Else


            Dim commandText3 As String = String.Concat("UPDATE ctnmain SET CTNUPGRADE ='N' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; ")
            connectDB.ExecuteNonQuery(commandText3)
        End If
        LoadBooking()
    End Sub

    Private Sub MarkToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MarkContainerToolStripMenuItem.Click

        GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString()

        If (GridView1.FocusedRowHandle < 0) Then
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)
        Else


            Dim commandText3 As String = String.Concat("UPDATE ctnmain SET CTNMARKSTR ='', CTNMARKSTRBOO ='0' where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; ")
            connectDB.ExecuteNonQuery(commandText3)
        End If
        LoadBooking()


    End Sub



    Private Sub MarkSealToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MarkSealToolStripMenuItem.Click
        'CTNMARKSEAL ='',CTNMARKSEALBOO ='0' 



        GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString()

        If (GridView1.FocusedRowHandle < 0) Then
            Interaction.MsgBox("กรุณาเลือกตู้คอนเทนเนอร์", MsgBoxStyle.OkOnly, Nothing)
        Else


            Dim commandText3 As String = String.Concat("UPDATE ctnmain SET CTNMARKSEAL ='',CTNMARKSEALBOO ='0'  where CTNMAINID = '" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNMAINID").ToString() & "'; ")
            connectDB.ExecuteNonQuery(commandText3)
        End If
        LoadBooking()
    End Sub

    Private Sub MarkContainerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MarkContainerToolStripMenuItem.Click

    End Sub

    Private Sub BarButtonItem17_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem17.ItemClick
        Dim nextform As New frmSearchByBooking
        nextform.Show()
    End Sub
End Class
