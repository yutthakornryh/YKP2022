﻿Imports DevExpress.XtraEditors

Public Class frmmovebooking_voyage

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Dim ykpset As New ykpdtset
    Dim ykpsetmove As New ykpdtset

    Dim idbooking As String

    Public Sub New(ByVal voyageid As String)

        ' This call is required by the designer.
        InitializeComponent()
        loadDatavoyage()
        ' Add any initialization after the InitializeComponent() call.
        slcVoyage.EditValue = voyageid
    End Sub
    Public Sub loadDatavoyage()
        Dim sql As String = "Select * from voyage;"

        ykpset.Tables("voyage").Clear()

        connectDB.GetTable(sql, ykpset.Tables("voyage"))
        connectDB.GetTable(sql, ykpsetmove.Tables("voyage"))


        slcVoyage.Properties.DataSource = ykpset.Tables("voyage")
        slcVoyagemove.Properties.DataSource = ykpset.Tables("voyage")
    End Sub

    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        loadBooking()


    End Sub

    Private Sub slcVoyagemove_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyagemove.EditValueChanged


        LoadBookingMovie()
    End Sub

    Private Sub frmmovebooking_voyage_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnMove_Click(sender As Object, e As EventArgs) Handles btnMove.Click
        If slcVoyagemove.EditValue Is Nothing Then
            MsgBox("กรุณา เลือก Voyageที่ต้องการย้าย")
            Exit Sub
        End If
        If XtraMessageBox.Show("คุณต้องการย้าย Booking เลขที่ " & GridView1.GetDataRow(GridView1.FocusedRowHandle)("BNO").ToString & " ไปยัง Voyage เลขที่ " & slcVoyagemove.Text, "Confirmation", MessageBoxButtons.YesNo) <> DialogResult.No Then
            Dim sql As String
            sql = "UPDATE booking Set BVOYAGE ='" & slcVoyagemove.EditValue & "' WHERE BOOKINGID = " & GridView1.GetDataRow(GridView1.FocusedRowHandle)("BOOKINGID").ToString & "; "
            connectDB.ExecuteNonQuery(sql)
            LoadBookingMovie()
            loadBooking()
        End If


    End Sub
    Public Sub loadBooking()
        Dim sql As String
        sql = "Select * ,CONCAT(BCTNNO,' X ', BCTNTYPE ) AS CTNNO from booking where BVOYAGE ='" & slcVoyage.EditValue & "' ; "
        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
        GridControl1.DataSource = ykpset.Tables("booking")

    End Sub
    Public Sub LoadBookingMovie()
        Dim sql As String
        sql = "Select * ,CONCAT(BCTNNO,' X ', BCTNTYPE ) AS CTNNO from booking where BVOYAGE ='" & slcVoyagemove.EditValue & "' ; "
        ykpsetmove.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpsetmove.Tables("booking"))
        GridControl2.DataSource = ykpsetmove.Tables("booking")
    End Sub

    Private Sub frmmovebooking_voyage_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Me.DialogResult = DialogResult.OK
    End Sub
End Class