﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmaddcontainer
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.mascmrunningBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascmrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprefixchar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colshowyear = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colformatdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colseparatechar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldefault = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextBox1 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_voyagename = New DevExpress.XtraEditors.TextEdit()
        Me.txt_voyage = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.txtCount = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_voyagename.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "CTNUPLOAD"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.Location = New System.Drawing.Point(12, 64)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnDelete})
        Me.GridControl1.Size = New System.Drawing.Size(1169, 483)
        Me.GridControl1.TabIndex = 56
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNSTRING, Me.colCTNSIZE, Me.colCTNAGENT, Me.colCTNSTAT, Me.colCTNCONSI, Me.colCTNVOYN, Me.colCTNSHIPNAME, Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.CustomizationCaption = "Container No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 0
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSIZE.AppearanceCell.Options.UseFont = True
        Me.colCTNSIZE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSIZE.AppearanceHeader.Options.UseFont = True
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.Visible = True
        Me.colCTNSIZE.VisibleIndex = 1
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 2
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTAT.AppearanceCell.Options.UseFont = True
        Me.colCTNSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTAT.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        Me.colCTNSTAT.Visible = True
        Me.colCTNSTAT.VisibleIndex = 3
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONSI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONSI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        Me.colCTNCONSI.Visible = True
        Me.colCTNCONSI.VisibleIndex = 4
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYN.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYN.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        Me.colCTNVOYN.Visible = True
        Me.colCTNVOYN.VisibleIndex = 5
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colCTNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        Me.colCTNSHIPNAME.Visible = True
        Me.colCTNSHIPNAME.VisibleIndex = 6
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Delete"
        Me.GridColumn1.ColumnEdit = Me.btnDelete
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 7
        '
        'btnDelete
        '
        Me.btnDelete.AutoHeight = False
        Me.btnDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)})
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txtCount)
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.TextBox1)
        Me.LayoutControl1.Controls.Add(Me.txt_voyagename)
        Me.LayoutControl1.Controls.Add(Me.txt_voyage)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1193, 585)
        Me.LayoutControl1.TabIndex = 57
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(978, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.mascmrunningBindingSource
        Me.slcVoyage.Properties.DisplayMember = "menuname"
        Me.slcVoyage.Properties.ValueMember = "idmascmrunning"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(203, 22)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 59
        '
        'mascmrunningBindingSource
        '
        Me.mascmrunningBindingSource.DataMember = "mascmrunning"
        Me.mascmrunningBindingSource.DataSource = Me.Ykpdtset2
        Me.mascmrunningBindingSource.Sort = ""
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascmrunning, Me.colisrunning, Me.colprefixchar, Me.colshowyear, Me.colformatdate, Me.colseparatechar, Me.colmenuname, Me.coldefault})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colidmascmrunning
        '
        Me.colidmascmrunning.FieldName = "idmascmrunning"
        Me.colidmascmrunning.Name = "colidmascmrunning"
        '
        'colisrunning
        '
        Me.colisrunning.FieldName = "isrunning"
        Me.colisrunning.Name = "colisrunning"
        '
        'colprefixchar
        '
        Me.colprefixchar.FieldName = "prefixchar"
        Me.colprefixchar.Name = "colprefixchar"
        '
        'colshowyear
        '
        Me.colshowyear.FieldName = "showyear"
        Me.colshowyear.Name = "colshowyear"
        '
        'colformatdate
        '
        Me.colformatdate.FieldName = "formatdate"
        Me.colformatdate.Name = "colformatdate"
        '
        'colseparatechar
        '
        Me.colseparatechar.FieldName = "separatechar"
        Me.colseparatechar.Name = "colseparatechar"
        '
        'colmenuname
        '
        Me.colmenuname.Caption = "ท่าเรือ"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 0
        '
        'coldefault
        '
        Me.coldefault.FieldName = "default"
        Me.coldefault.Name = "coldefault"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(895, 551)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(286, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 58
        Me.SimpleButton3.Text = "บันทึกข้อมูล"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(600, 551)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "ลบข้อมูลก่อนหน้า"
        Me.CheckEdit1.Size = New System.Drawing.Size(291, 19)
        Me.CheckEdit1.StyleController = Me.LayoutControl1
        Me.CheckEdit1.TabIndex = 57
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(886, 38)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(295, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 8
        Me.SimpleButton2.Text = "Load"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(585, 38)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(297, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 7
        Me.SimpleButton1.Text = "Browse"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(106, 38)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Properties.Appearance.Options.UseFont = True
        Me.TextBox1.Size = New System.Drawing.Size(475, 22)
        Me.TextBox1.StyleController = Me.LayoutControl1
        Me.TextBox1.TabIndex = 6
        '
        'txt_voyagename
        '
        Me.txt_voyagename.Location = New System.Drawing.Point(679, 12)
        Me.txt_voyagename.Name = "txt_voyagename"
        Me.txt_voyagename.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_voyagename.Properties.Appearance.Options.UseFont = True
        Me.txt_voyagename.Size = New System.Drawing.Size(201, 22)
        Me.txt_voyagename.StyleController = Me.LayoutControl1
        Me.txt_voyagename.TabIndex = 5
        '
        'txt_voyage
        '
        Me.txt_voyage.Location = New System.Drawing.Point(106, 12)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_voyage.Properties.Appearance.Options.UseFont = True
        Me.txt_voyage.Size = New System.Drawing.Size(475, 22)
        Me.txt_voyage.StyleController = Me.LayoutControl1
        Me.txt_voyage.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.EmptySpaceItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1193, 585)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.txt_voyage
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(573, 26)
        Me.LayoutControlItem1.Text = "Voyage Number"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(91, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(294, 539)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(294, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.txt_voyagename
        Me.LayoutControlItem2.Location = New System.Drawing.Point(573, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(299, 26)
        Me.LayoutControlItem2.Text = "Voyage name"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(91, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.TextBox1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(573, 26)
        Me.LayoutControlItem3.Text = "Path File"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(91, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(573, 26)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(301, 26)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.SimpleButton2
        Me.LayoutControlItem5.Location = New System.Drawing.Point(874, 26)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(299, 26)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.GridControl1
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(1173, 487)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.CheckEdit1
        Me.LayoutControlItem7.Location = New System.Drawing.Point(588, 539)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(295, 26)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.SimpleButton3
        Me.LayoutControlItem8.Location = New System.Drawing.Point(883, 539)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(290, 26)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.slcVoyage
        Me.LayoutControlItem9.Location = New System.Drawing.Point(872, 0)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(301, 26)
        Me.LayoutControlItem9.Text = "Depot"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(91, 16)
        '
        'txtCount
        '
        Me.txtCount.Location = New System.Drawing.Point(106, 551)
        Me.txtCount.Name = "txtCount"
        Me.txtCount.Size = New System.Drawing.Size(196, 20)
        Me.txtCount.StyleController = Me.LayoutControl1
        Me.txtCount.TabIndex = 60
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.txtCount
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 539)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(294, 26)
        Me.LayoutControlItem10.Text = "จำนวนตู้"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(91, 13)
        '
        'frmaddcontainer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1193, 585)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmaddcontainer"
        Me.Text = "frmadd_container"
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_voyagename.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextBox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_voyagename As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_voyage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents mascmrunningBindingSource As BindingSource
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colidmascmrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprefixchar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colshowyear As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colformatdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colseparatechar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldefault As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents txtCount As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
End Class
