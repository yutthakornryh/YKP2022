﻿Imports MySql.Data.MySqlClient
Imports System.Threading
Imports Microsoft.Office.Interop
Imports DevExpress.XtraPrinting

Public Class frmview_ctnempty

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Dim ykpset As New ykpdtset

    Dim diff1 As TimeSpan

    Dim date1 As DateTime

    Dim date2 As DateTime

    Dim subdate1 As String()

    Dim subdate2 As String()

    Dim valuedate As String

    Dim dateend As String

    Public Sub LoadPort()
        Dim sql As String
        sql = "SELECT idmascmrunning ,menuname FROM mascmrunning"
        connectDB.GetTable(sql, ykpset.Tables("mascmrunning"))
        SearchLookUpEdit1.Properties.DataSource = ykpset.Tables("mascmrunning")
        SearchLookUpEdit1.EditValue = 1
    End Sub
    Private Sub frmview_ctnempty_Load(sender As Object, e As EventArgs) Handles Me.Load
        searchctn()
        LoadPort()
    End Sub
    Public Sub searchctn()
        Dim sql As String
        If SearchLookUpEdit1.EditValue Is Nothing Then
            sql = "Select  CTNMAINID,CTNSTRING,CTNAGENT,CTNCONSI,VOYDATEEN,CTNSIZE, Voyname , CTNSHIPNAME,CTNBOOKING,CTNYARD,CTNWEIGHT,CTNTAREWEIGHT ,idmascmrunning from (SELECT CTNMAINID,CTNSTRING,CTNAGENT,CTNCONSI,CTNSIZE,CTNVOYN,CTNSHIPNAME,CTNBOOKING,CTNYARD ,CTNSTAT , CTNWEIGHT,CTNTAREWEIGHT,idmascmrunning  FROM ctnmain WHERe CTNSTAT = 1 ) AS ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where CTNSTAT= '1' order by CTNMAINID ASC;"
        Else
            sql = "Select  CTNMAINID,CTNSTRING,CTNAGENT,CTNCONSI,VOYDATEEN,CTNSIZE, Voyname , CTNSHIPNAME,CTNBOOKING,CTNYARD,CTNWEIGHT,CTNTAREWEIGHT ,idmascmrunning from (SELECT CTNMAINID,CTNSTRING,CTNAGENT,CTNCONSI,CTNSIZE,CTNVOYN,CTNSHIPNAME,CTNBOOKING,CTNYARD ,CTNSTAT , CTNWEIGHT,CTNTAREWEIGHT,idmascmrunning  FROM ctnmain WHERe CTNSTAT = 1 ) AS ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where CTNSTAT= '1'  AND idmascmrunning = '" & SearchLookUpEdit1.EditValue & "' order by CTNMAINID ASC;"
        End If

        ykpset.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset.Tables("ctnmain"))

        For i As Integer = 0 To ykpset.Tables("ctnmain").Rows.Count - 1
            Me.subdate1 = Strings.Split(ykpset.Tables("ctnmain")(i)("VOYDATEEN").ToString, "-", -1, CompareMethod.Binary)
            Dim now As DateTime = DateTime.Now
            Me.subdate2 = Strings.Split(Date.Now.ToString("dd-MM-yyyy"), "-", -1, CompareMethod.Binary)
            Me.date1 = New DateTime(Me.subdate1(2), Me.subdate1(1), Me.subdate1(0), 0, 0, 0)
            Me.date2 = New DateTime(Me.subdate2(2), Me.subdate2(1), Me.subdate2(0), 0, 0, 0)
            Me.diff1 = Me.date2.Subtract(Me.date1)
            Me.valuedate = Me.diff1.TotalDays.ToString()

            ykpset.Tables("ctnmain")(i)("diffDay") = valuedate
        Next



        GridControl1.DataSource = ykpset.Tables("ctnmain")
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Try

            Dim sv As New SaveFileDialog
            sv.Filter = "Excel Workbook|*.xlsx"
            If sv.ShowDialog() = DialogResult.OK And sv.FileName <> Nothing Then
                If sv.FileName.EndsWith(".xlsx") Then
                    Dim path = sv.FileName.ToString()
                    Dim x As New XlsxExportOptionsEx
                    x.AllowGrouping = DevExpress.Utils.DefaultBoolean.False
                    x.AllowFixedColumnHeaderPanel = DevExpress.Utils.DefaultBoolean.False
                    GridControl1.ExportToXlsx(path, x)
                End If


                MessageBox.Show("Data Exported to :" + vbCrLf + sv.FileName, "Business Intelligence Portal", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                sv.FileName = Nothing

            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub SearchLookUpEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles SearchLookUpEdit1.EditValueChanged
        searchctn()
    End Sub
End Class