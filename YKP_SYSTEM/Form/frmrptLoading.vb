﻿Imports System.Threading
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel

Public Class frmrptLoading
    Dim voyageid As String
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Dim ykpdtset As New ykpdtset
    Dim objCLASS As New ContainerClass(ykpdtset)

    Public Sub New(ByVal idvoyage As String)

        ' This call is required by the designer.
        InitializeComponent()
        voyageid = idvoyage
        ' Add any initialization after the InitializeComponent() call.
        loadVovage()
        slcVoyage.EditValue = voyageid

    End Sub
    Public Sub loadVovage()
        objCLASS.loadVoyage()
        slcVoyage.Properties.DataSource = ykpdtset.Tables("voyage")
    End Sub
    Private Sub frmrptLoading_Load(sender As Object, e As EventArgs) Handles MyBase.Load



    End Sub
    Public Sub LoadBookingCheck()
        Dim sql As String
        sql = "Select BSHIPNAME from booking where BCHECKLOAD ='TRUE' and BVOYAGE ='" & slcVoyage.EditValue & "' order by booking.BSHIP; "
        ykpdtset.Tables("rptLoading").Clear()
        connectDB.GetTable(sql, ykpdtset.Tables("rptLoading"))
        GridControl1.DataSource = ykpdtset.Tables("rptLoading")
    End Sub

    'Private Sub frmrpt_loading_Load(ByVal sender As Object, ByVal e As EventArgs)
    '	Me.mysql.Close()
    '	If (Me.mysql.checkstate() = ConnectionState.Closed) Then
    '		Me.mysql.Open()
    '	End If
    '	Me.mySqlCommand.CommandText(String.Concat("Select * from booking where BCHECKLOAD ='TRUE' and BVOYAGE ='", Me.idvoyage, "' order by booking.BSHIP;"))
    '	Me.mySqlCommand (Me.mysql)
    '	Me.mySqlAdaptor.SelectCommand(Me.mySqlCommand)
    '	Me.sumctn40 = 0
    '	Me.sumctn20 = 0
    '	Try
    '		Me.mySqlReader = Me.mySqlCommand.ExecuteReader()
    '		Me.ListView1.Items.Clear()
    '		While Me.mySqlReader.Read()
    '			Dim $W0 As Object = NewLateBinding.LateGet(Me.ListView1.Items, Nothing, "Add", New Object() { Me.mySqlReader ("BSHIPNAME") }, Nothing, Nothing, Nothing)
    '			NewLateBinding.LateCall(NewLateBinding.LateGet($W0, Nothing, "SubItems", New Object(-1) {}, Nothing, Nothing, Nothing), Nothing, "Add", New Object() { Me.mySqlReader ("BSHIP") }, Nothing, Nothing, Nothing, True)
    '			$W0 = Nothing
    '		End While
    '	Catch exception As System.Exception
    '		ProjectData.SetProjectError(exception)
    '		Interaction.MsgBox(exception.ToString(), MsgBoxStyle.OkOnly, Nothing)
    '		ProjectData.ClearProjectError()
    '	End Try

    Public Sub LoadExcel()



        Dim count As Integer = 7
        Dim dateout As String = ""
        Dim ctntype As String = ""
        Dim pathExcel As String = Me.FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add
        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)

        excelsheets.Name = String.Concat("V.S", voyageid)


        With excelsheets
            .PageSetup.PaperSize = Excel.XlPaperSize.xlPaperA4
            .PageSetup.Orientation = Excel.XlPageOrientation.xlLandscape

            .Range("A1").ColumnWidth = 3.57
            .Range("B1").ColumnWidth = 22
            .Range("C1").ColumnWidth = 22
            .Range("D1").ColumnWidth = 46
            .Range("E1").ColumnWidth = 29
            .Range("F1").ColumnWidth = 36
            .Range("G1").ColumnWidth = 28
            .Range("H1").ColumnWidth = 26
            .Range("I1").ColumnWidth = 23
            .Range("A1:M500").Font.Name = "Arial"
            .Range("A1:M500").Font.Size = 12
            .Rows("1:1").rowheight = 38
            .Rows("2:500").rowheight = 27

            With .Range("D1:F1")
                .Merge()
                .Value = "LOADING LIST V.S"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
                .Font.Size = 20
            End With
            With .Range("B2:C2")
                .Merge()
                .Value = "YKP MARINE SERVICE CO., LTD."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
                .Font.Size = 14
            End With
            With .Range("B3:C3")
                .Merge()
                .Value = "87/1 M.5 T.Boonumron, Kantang, Trang, Thailand "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
                .Font.Size = 14
            End With
            With .Range("A5:A6")
                .Merge()
                .Value = "No."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("B5:B6")
                .Merge()
                .Value = "ETD from kantang "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("C5:C6")
                .Merge()
                .Value = "SHIPPING "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("D5:D6")
                .Merge()
                .Value = "Name of Cargo Owner "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("E5:E6")
                .Merge()
                .Value = "MOTHER VESSEL "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("F5:F6")
                .Merge()
                .WrapText = True
                .Value = "From " & Environment.NewLine & "Kantang Thailand " & Environment.NewLine & "to Penang"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With

            With .Range("G5:G6")
                .Merge()
                .WrapText = True
                .Value = "Conveyance "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("H5:H6")
                .Merge()
                .WrapText = True
                .Value = "Number of container "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With


            With .Range("I5:I6")
                .Merge()
                .WrapText = True
                .Value = "Booking No.  "
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            .Range("A5:I6").Borders.Weight = 2

            Dim sql As String

            sql = "Select VOYDATEES,BSHIPNAME,conname,BINCOTERM,VOYVESNAMES,place,BINVOICE,BNO,BCTNNO,BCTNTYPE,BREMARK,BNFORWARDERNAME,BMOTH from voyage join booking on voyage.VOYAGEID = booking.BVOYAGE left join booking_bl on booking.BOOKINGID = booking_bl.booking_id where BCHECKLOAD ='True' and BVOYAGE = '" & voyageid & "'  order by  BNFORWARDERNAME ;"

            Dim dt As New System.Data.DataTable
            dt = connectDB.GetTable(sql)
            Dim sumctn As Integer = 0
            Dim number As Integer = 1
            count = 7


            For i As Integer = 0 To dt.Rows.Count - 1
                .Range("A" & count.ToString() & ":I" & count.ToString()).Borders.Weight = 2
                With .Range("A" & count.ToString() & ":A" & count.ToString())

                    .NumberFormat = "@"
                    .Value = number
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter

                End With
                number = number + 1

                With .Range("B" & count.ToString() & ":B" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("VOYDATEES").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    dateout = dt.Rows(i)("VOYDATEES")
                End With

                With .Range("C" & count.ToString() & ":C" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BNFORWARDERNAME").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("D" & count.ToString() & ":D" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BSHIPNAME").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("E" & count.ToString() & ":E" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BMOTH").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With


                With .Range("F" & count.ToString() & ":F" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("VOYVESNAMES")
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("G" & count.ToString() & ":G" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("place")
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                With .Range("H" & count.ToString() & ":H" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BCTNNO").ToString & " X " & dt.Rows(i)("BCTNTYPE").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlLeft
                End With

                sumctn = sumctn + Convert.ToDecimal(dt.Rows(i)("BCTNNO"))
                ctntype = dt.Rows(i)("BCTNTYPE").ToString


                With .Range("I" & count.ToString() & ":I" & count.ToString())
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("BNO").ToString
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                count = count + 1
            Next
            count = count + 1

            With .Range("H" & count.ToString() & ":H" & count.ToString())
                .NumberFormat = "@"
                .Value = sumctn.ToString() & " X " & ctntype.ToString()
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Underline = XlUnderlineStyle.xlUnderlineStyleDouble
            End With


        End With

        excelapp.Windows.Application.ActiveWindow.DisplayGridlines = False

        Try
            excelbooks.SaveAs(pathExcel.ToString() & "\Loading_list_V.S" & voyageid & ".xlsx")

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception
            MsgBox(ex.ToString)
            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try


    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click


        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                Dim t = New Thread(New ThreadStart(AddressOf LoadExcel))
                t.Start()
            Catch ex As Exception

            End Try
        End If


    End Sub

    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        If slcVoyage.EditValue Is Nothing Then
            Exit Sub
        End If
        LoadBookingCheck()
    End Sub
End Class