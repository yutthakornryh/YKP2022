﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmsearch_voyage
    Inherits DevComponents.DotNetBar.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnEdit = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEdit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(5, 1)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(993, 581)
        Me.LayoutControl1.TabIndex = 172
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "voyage"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.Location = New System.Drawing.Point(12, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnEdit})
        Me.GridControl1.Size = New System.Drawing.Size(969, 531)
        Me.GridControl1.TabIndex = 172
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME, Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colVOYAGEID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYAGEID.AppearanceCell.Options.UseFont = True
        Me.colVOYAGEID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYAGEID.AppearanceHeader.Options.UseFont = True
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        Me.colVOYAGEID.OptionsColumn.AllowEdit = False
        Me.colVOYAGEID.OptionsColumn.ReadOnly = True
        Me.colVOYAGEID.Visible = True
        Me.colVOYAGEID.VisibleIndex = 0
        Me.colVOYAGEID.Width = 105
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        Me.colVOYVESIDN.OptionsColumn.AllowEdit = False
        Me.colVOYVESIDN.OptionsColumn.ReadOnly = True
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDS.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDS.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        Me.colVOYVESIDS.OptionsColumn.AllowEdit = False
        Me.colVOYVESIDS.OptionsColumn.ReadOnly = True
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESN.Caption = "ETD"
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        Me.colVOYDATESN.OptionsColumn.AllowEdit = False
        Me.colVOYDATESN.OptionsColumn.ReadOnly = True
        Me.colVOYDATESN.Visible = True
        Me.colVOYDATESN.VisibleIndex = 3
        Me.colVOYDATESN.Width = 108
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESS.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESS.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESS.Caption = "ETA1"
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        Me.colVOYDATESS.OptionsColumn.AllowEdit = False
        Me.colVOYDATESS.OptionsColumn.ReadOnly = True
        Me.colVOYDATESS.Visible = True
        Me.colVOYDATESS.VisibleIndex = 6
        Me.colVOYDATESS.Width = 145
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEEN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEEN.Caption = "ETA"
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        Me.colVOYDATEEN.OptionsColumn.AllowEdit = False
        Me.colVOYDATEEN.OptionsColumn.ReadOnly = True
        Me.colVOYDATEEN.Visible = True
        Me.colVOYDATEEN.VisibleIndex = 4
        Me.colVOYDATEEN.Width = 121
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMEN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMEN.Caption = "Name Of Vessel"
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.OptionsColumn.AllowEdit = False
        Me.colVOYVESNAMEN.OptionsColumn.ReadOnly = True
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 2
        Me.colVOYVESNAMEN.Width = 313
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEES.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEES.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEES.Caption = "ETD1"
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        Me.colVOYDATEES.OptionsColumn.AllowEdit = False
        Me.colVOYDATEES.OptionsColumn.ReadOnly = True
        Me.colVOYDATEES.Visible = True
        Me.colVOYDATEES.VisibleIndex = 7
        Me.colVOYDATEES.Width = 172
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMES.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMES.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMES.Caption = "Name Of Vessel"
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.OptionsColumn.AllowEdit = False
        Me.colVOYVESNAMES.OptionsColumn.ReadOnly = True
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 5
        Me.colVOYVESNAMES.Width = 354
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYTIMEHHMMNN.AppearanceCell.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.OptionsColumn.AllowEdit = False
        Me.colVOYTIMEHHMMNN.OptionsColumn.ReadOnly = True
        Me.colVOYTIMEHHMMNN.Width = 155
        '
        'colVOYNAME
        '
        Me.colVOYNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME.AppearanceCell.Options.UseFont = True
        Me.colVOYNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME.AppearanceHeader.Options.UseFont = True
        Me.colVOYNAME.Caption = "Voyge Name"
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.OptionsColumn.AllowEdit = False
        Me.colVOYNAME.OptionsColumn.ReadOnly = True
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 1
        Me.colVOYNAME.Width = 113
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.Caption = "Edit"
        Me.GridColumn1.ColumnEdit = Me.btnEdit
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 8
        Me.GridColumn1.Width = 100
        '
        'btnEdit
        '
        Me.btnEdit.AutoHeight = False
        Me.btnEdit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)})
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl1
        Me.SearchControl1.Location = New System.Drawing.Point(124, 12)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl1
        Me.SearchControl1.Size = New System.Drawing.Size(857, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 171
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem4})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(993, 581)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.SearchControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(973, 26)
        Me.LayoutControlItem1.Text = "Edit Voyage , ค้นหา"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(109, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.GridControl1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(973, 535)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'frmsearch_voyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1003, 584)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmsearch_voyage"
        Me.Text = "frmsearch_voyage"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEdit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnEdit As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
End Class
