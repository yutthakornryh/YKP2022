﻿Imports System.Threading
Imports Microsoft.Office.Interop
Imports MySql.Data.MySqlClient

Public Class frmReportLiner

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Dim ykpset As New ykpdtset
    Dim objCLASS As New ContainerClass(ykpset)

    Private Sub frmReportLiner_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        objCLASS.loadGroupAgentByCTN()
        slcAgent.Properties.DataSource = ykpset.Tables("Agent")
        loadmascmrunning()
    End Sub
    Public Sub loadmascmrunning()
        objCLASS.loadMascmrunning()
        slcDepot.Properties.DataSource = ykpset.Tables("mascmrunning")
        slcDepot.EditValue = 1
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try


                Dim t = New Thread(New ThreadStart(AddressOf excelDaily))
                t.Start()



            Catch ex As Exception

            End Try
        End If

    End Sub
    Private Sub excelDaily2()

        Dim pathExcel As String
        Dim count As Integer = 11

        Dim countcontainer As Integer = 1
        pathExcel = FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        Dim excelsheets2 As Excel.Worksheet
        Dim excelsheets3 As Excel.Worksheet
        Dim excelsheets4 As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add
        Dim sql As String
        Dim typen As String
        Dim types As String
        Dim dttype As New DataTable

        Sql = "SELECT typen , types FROM mascmrunning WHERE  idmascmrunning = '" & slcDepot.EditValue & "'"
        dttype = connectDB.GetTable(Sql)
        typen = dttype.Rows(0)("typen").ToString
        types = dttype.Rows(0)("types").ToString

        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)
        excelsheets2 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets3 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets4 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets3.Name = "OLD_40HC"
        excelsheets4.Name = "OLD_20GP"
        excelsheets.Name = "40HC"
        excelsheets2.Name = "20GP"
        Try
            With excelsheets4
                count = 11
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 6.71
                .Range("B1").ColumnWidth = 15
                .Range("C1").ColumnWidth = 15
                .Range("D1").ColumnWidth = 15
                .Range("E1").ColumnWidth = 15
                .Range("F1").ColumnWidth = 15
                .Range("G1").ColumnWidth = 15
                .Range("H1").ColumnWidth = 15
                .Range("I1").ColumnWidth = 15
                .Range("J1").ColumnWidth = 15
                .Range("K1").ColumnWidth = 15
                .Range("L1").ColumnWidth = 15



                For J = 7 To 10
                    .Range("A2:A2").Borders(J).Weight = 2 ' xlThin
                    .Range("B2:B2").Borders(J).Weight = 2 ' xlThin
                    .Range("C2:C2").Borders(J).Weight = 2 ' xlThin
                    .Range("D2:D2").Borders(J).Weight = 2 ' xlThin
                    .Range("E2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin

                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("H2:H2").Borders(J).Weight = 2 ' xlThin
                    .Range("I2:I2").Borders(J).Weight = 2 ' xlThin
                    .Range("J2:J2").Borders(J).Weight = 2 ' xlThin
                    .Range("K2:K2").Borders(J).Weight = 2 ' xlThin
                    .Range("L2:L2").Borders(J).Weight = 2 ' xlThin



                Next

                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 20'GPs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("A2:A2")
                    .Merge()
                    .Value = "Item"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("B2:B2")
                    .Merge()
                    .Value = "CONTAINER NO"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("C2:C2")
                    .Merge()
                    .Value = "Discharge IN:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("D2:D2")
                    .Merge()
                    .Value = "Discharge IN:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("E2:E2")
                    .Merge()
                    .Value = "Delivery OUT:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = "Delivery OUT:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "BOOKING No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("H2:H2")
                    .Merge()
                    .Value = "SEAL No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("I2:I2")
                    .Merge()
                    .Value = "SEAL No.(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("J2:J2")
                    .Merge()
                    .Value = "STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("K2:K2")
                    .Merge()
                    .Value = "REMARK"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("L2:L2")
                    .Merge()
                    .Value = "REMARK(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With

                Dim dataTable1 As DataTable
                Dim datetime As Date = DateEdit1.EditValue
                Dim str4 As String = "20'GP"
                Dim str5 As String = String.Concat(New String() {"SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO  from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str4) & "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%" & slcAgent.EditValue & "%'  " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & "  ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like '%" & slcAgent.EditValue & "%' and  rpttype ='22G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"})
                dataTable1 = connectDB.GetTable(str5)


                With .Range("A2:L" & dataTable1.Rows.Count + 6)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dataTable1.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(i + 3, j) = i + 1
                    .Cells(i + 3, j + 1) = dataTable1.Rows(i)("CTNSTRING").ToString()
                    .Cells(i + 3, j + 2) = dataTable1.Rows(i)("VOYDATEEN").ToString()
                    .Cells(i + 3, j + 3) = dataTable1.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(i + 3, j + 4) = dataTable1.Rows(i)("TIMEDATE").ToString()
                    .Cells(i + 3, j + 5) = dataTable1.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(i + 3, j + 6) = dataTable1.Rows(i)("BNO").ToString()
                    .Cells(i + 3, j + 7) = dataTable1.Rows(i)("CTNSEALID").ToString()
                Next


            End With
            With excelsheets3
                count = 11
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 6.71
                .Range("B1").ColumnWidth = 15
                .Range("C1").ColumnWidth = 15
                .Range("D1").ColumnWidth = 15
                .Range("E1").ColumnWidth = 15
                .Range("F1").ColumnWidth = 15
                .Range("G1").ColumnWidth = 15
                .Range("H1").ColumnWidth = 15
                .Range("I1").ColumnWidth = 15
                .Range("J1").ColumnWidth = 15
                .Range("K1").ColumnWidth = 15
                .Range("L1").ColumnWidth = 15



                For J = 7 To 10
                    .Range("A2:A2").Borders(J).Weight = 2 ' xlThin
                    .Range("B2:B2").Borders(J).Weight = 2 ' xlThin
                    .Range("C2:C2").Borders(J).Weight = 2 ' xlThin
                    .Range("D2:D2").Borders(J).Weight = 2 ' xlThin
                    .Range("E2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin

                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("H2:H2").Borders(J).Weight = 2 ' xlThin
                    .Range("I2:I2").Borders(J).Weight = 2 ' xlThin
                    .Range("J2:J2").Borders(J).Weight = 2 ' xlThin
                    .Range("K2:K2").Borders(J).Weight = 2 ' xlThin
                    .Range("L2:L2").Borders(J).Weight = 2 ' xlThin



                Next

                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 20'GPs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("A2:A2")
                    .Merge()
                    .Value = "Item"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("B2:B2")
                    .Merge()
                    .Value = "CONTAINER NO"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("C2:C2")
                    .Merge()
                    .Value = "Discharge IN:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("D2:D2")
                    .Merge()
                    .Value = "Discharge IN:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("E2:E2")
                    .Merge()
                    .Value = "Delivery OUT:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = "Delivery OUT:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "BOOKING No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("H2:H2")
                    .Merge()
                    .Value = "SEAL No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("I2:I2")
                    .Merge()
                    .Value = "SEAL No.(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("J2:J2")
                    .Merge()
                    .Value = "STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("K2:K2")
                    .Merge()
                    .Value = "REMARK"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("L2:L2")
                    .Merge()
                    .Value = "REMARK(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With


                Dim dataTable1 As DataTable
                Dim datetime As Date = DateEdit1.EditValue
                Dim str4 As String = "40'HC"
                Dim str5 As String = String.Concat(New String() {" SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO   from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='", MySqlHelper.EscapeString(str4), "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%" & slcAgent.EditValue & "%'  " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like  '%" & slcAgent.EditValue & "%' and  rpttype ='45G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"})
                dataTable1 = connectDB.GetTable(str5)


                With .Range("A2:L" & dataTable1.Rows.Count + 6)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dataTable1.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(i + 3, j) = i + 1
                    .Cells(i + 3, j + 1) = dataTable1.Rows(i)("CTNSTRING").ToString()
                    .Cells(i + 3, j + 2) = dataTable1.Rows(i)("VOYDATEEN").ToString()
                    .Cells(i + 3, j + 3) = dataTable1.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(i + 3, j + 4) = dataTable1.Rows(i)("TIMEDATE").ToString()
                    .Cells(i + 3, j + 5) = dataTable1.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(i + 3, j + 6) = dataTable1.Rows(i)("BNO").ToString()
                    .Cells(i + 3, j + 7) = dataTable1.Rows(i)("CTNSEALID").ToString()
                Next




            End With
            With excelsheets

                count = 3
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 6.71
                .Range("B1").ColumnWidth = 15
                .Range("C1").ColumnWidth = 15
                .Range("D1").ColumnWidth = 15
                .Range("E1").ColumnWidth = 15
                .Range("F1").ColumnWidth = 15
                .Range("G1").ColumnWidth = 15
                .Range("H1").ColumnWidth = 15
                .Range("I1").ColumnWidth = 15
                .Range("J1").ColumnWidth = 15
                .Range("K1").ColumnWidth = 15
                .Range("L1").ColumnWidth = 15



                For J = 7 To 10
                    .Range("A2:A2").Borders(J).Weight = 2 ' xlThin
                    .Range("B2:B2").Borders(J).Weight = 2 ' xlThin
                    .Range("C2:C2").Borders(J).Weight = 2 ' xlThin
                    .Range("D2:D2").Borders(J).Weight = 2 ' xlThin
                    .Range("E2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin

                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("H2:H2").Borders(J).Weight = 2 ' xlThin
                    .Range("I2:I2").Borders(J).Weight = 2 ' xlThin
                    .Range("J2:J2").Borders(J).Weight = 2 ' xlThin
                    .Range("K2:K2").Borders(J).Weight = 2 ' xlThin
                    .Range("L2:L2").Borders(J).Weight = 2 ' xlThin



                Next

                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 20'GPs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("A2:A2")
                    .Merge()
                    .Value = "Item"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("B2:B2")
                    .Merge()
                    .Value = "CONTAINER NO"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("C2:C2")
                    .Merge()
                    .Value = "Discharge IN:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("D2:D2")
                    .Merge()
                    .Value = "Discharge IN:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("E2:E2")
                    .Merge()
                    .Value = "Delivery OUT:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = "Delivery OUT:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "BOOKING No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("H2:H2")
                    .Merge()
                    .Value = "SEAL No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("I2:I2")
                    .Merge()
                    .Value = "SEAL No.(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("J2:J2")
                    .Merge()
                    .Value = "STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("K2:K2")
                    .Merge()
                    .Value = "REMARK"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("L2:L2")
                    .Merge()
                    .Value = "REMARK(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With


                Dim stringsingle As String = "40'HC"
                Dim dt As New DataTable
                Dim datetime As Date = DateEdit1.EditValue
                ' sql = "Select * from ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where CTNVOYN  >= '" & txt_namemaster.Text & "'  and CTNAGENT like '%WAN HAI%' and  CTNSTAT > 0 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' order by  CTNSTAT DESC ,VOYAGEID ASC,CTNMAINID ASC ;"
                ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql
                sql = "Select CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID   from  ( SELECT * FROM ctnmain  WHERE  CTNAGENT like '%" & slcAgent.EditValue & "%' and CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) AS ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC ;"

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next


                dt = New DataTable


                sql = String.Concat(" SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO  from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT > 1 and CTNSTAT < 4 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                'SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO  from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str4) & "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%" & slcAgent.EditValue & "%'   ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like '%" & slcAgent.EditValue & "%' and  rpttype ='22G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"})

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next





                dt = New DataTable


                sql = String.Concat("SELECT  CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO   from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 1 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain LEFT join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid LEFT join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYN ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next


                dt = New DataTable


                sql = String.Concat("SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO  from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 0 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'    " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next



            End With

            count = 3
            countcontainer = 1
            With excelsheets2

                count = 3
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 6.71
                .Range("B1").ColumnWidth = 15
                .Range("C1").ColumnWidth = 15
                .Range("D1").ColumnWidth = 15
                .Range("E1").ColumnWidth = 15
                .Range("F1").ColumnWidth = 15
                .Range("G1").ColumnWidth = 15
                .Range("H1").ColumnWidth = 15
                .Range("I1").ColumnWidth = 15
                .Range("J1").ColumnWidth = 15
                .Range("K1").ColumnWidth = 15
                .Range("L1").ColumnWidth = 15



                For J = 7 To 10
                    .Range("A2:A2").Borders(J).Weight = 2 ' xlThin
                    .Range("B2:B2").Borders(J).Weight = 2 ' xlThin
                    .Range("C2:C2").Borders(J).Weight = 2 ' xlThin
                    .Range("D2:D2").Borders(J).Weight = 2 ' xlThin
                    .Range("E2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin

                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("H2:H2").Borders(J).Weight = 2 ' xlThin
                    .Range("I2:I2").Borders(J).Weight = 2 ' xlThin
                    .Range("J2:J2").Borders(J).Weight = 2 ' xlThin
                    .Range("K2:K2").Borders(J).Weight = 2 ' xlThin
                    .Range("L2:L2").Borders(J).Weight = 2 ' xlThin



                Next

                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 20'GPs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("A2:A2")
                    .Merge()
                    .Value = "Item"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("B2:B2")
                    .Merge()
                    .Value = "CONTAINER NO"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("C2:C2")
                    .Merge()
                    .Value = "Discharge IN:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("D2:D2")
                    .Merge()
                    .Value = "Discharge IN:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("E2:E2")
                    .Merge()
                    .Value = "Delivery OUT:DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = "Delivery OUT:TIME"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "BOOKING No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("H2:H2")
                    .Merge()
                    .Value = "SEAL No."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("I2:I2")
                    .Merge()
                    .Value = "SEAL No.(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("J2:J2")
                    .Merge()
                    .Value = "STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("K2:K2")
                    .Merge()
                    .Value = "REMARK"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("L2:L2")
                    .Merge()
                    .Value = "REMARK(1)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With

                Dim stringsingle As String = "20'GP"
                Dim dt As New DataTable
                Dim datetime As Date = DateEdit1.EditValue
                sql = "Select CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID   from  ( SELECT * FROM ctnmain  WHERE  CTNAGENT like '%" & slcAgent.EditValue & "%' and CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & ") AS ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC ;"

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next

                dt = New DataTable




                sql = String.Concat(" SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO  from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT > 1 and CTNSTAT < 4 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)



                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next





                dt = New DataTable

                sql = String.Concat("SELECT  CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO   from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 1 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   ) ctnmain  LEFT join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid LEFT join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYN ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next




                dt = New DataTable

                sql = String.Concat("SELECT CTNSTRING, voyage.VOYDATEEN ,voyage.VOYTIMEHHMMNN,TIMEDATE , TIMEHHMMIN,BNO,CTNSEALID from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS,CTNSEALID ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEHHMMIN,TIMEDATEIN,  VOYDATEEN as VOYDATEENN ,BNO  from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 0 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                dt = connectDB.GetTable(sql)



                With .Range("A" & count & ":L" & dt.Rows.Count + count)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim j As Integer = 1
                    .Cells(count, j) = count
                    .Cells(count, j + 1) = dt.Rows(i)("CTNSTRING").ToString()
                    .Cells(count, j + 2) = dt.Rows(i)("VOYDATEEN").ToString()
                    .Cells(count, j + 3) = dt.Rows(i)("VOYTIMEHHMMNN").ToString()
                    .Cells(count, j + 4) = dt.Rows(i)("TIMEDATE").ToString()
                    .Cells(count, j + 5) = dt.Rows(i)("TIMEHHMMIN").ToString()
                    .Cells(count, j + 6) = dt.Rows(i)("BNO").ToString()
                    .Cells(count, j + 7) = dt.Rows(i)("CTNSEALID").ToString()
                    count = count + 1
                Next



            End With

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try



        Try

            excelbooks.SaveAs(pathExcel.ToString + "\" & slcDepot.Text & "'S DAILY STATUS REPORT_" & slcAgent.EditValue & " LINES_2 " + Date.Now.ToString("dd-MM-yyyy") + ".xlsx")
            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelsheets2 = Nothing

            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelsheets2 = Nothing

            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try

    End Sub
    Private Sub excelDaily()

        Dim pathExcel As String
        Dim count As Integer = 11

        Dim countcontainer As Integer = 1
        pathExcel = FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        Dim excelsheets2 As Excel.Worksheet
        Dim excelsheets3 As Excel.Worksheet
        Dim excelsheets4 As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add

        Dim sql As String
        Dim dttype As New DataTable
        Dim typen As String
        Dim types As String
        sql = "SELECT typen , types FROM mascmrunning WHERE  idmascmrunning = '" & slcDepot.EditValue & "'"
        dttype = connectDB.GetTable(sql)
        typen = dttype.Rows(0)("typen").ToString
        types = dttype.Rows(0)("types").ToString

        excelsheets3 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets4 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets2 = CType(excelbooks.Sheets.Add, Excel.Worksheet)

        excelsheets3.Name = "OLD_40HC"
        excelsheets4.Name = "OLD_20GP"
        excelsheets.Name = "40HC"
        excelsheets2.Name = "20GP"
        Try
            With excelsheets

                count = 11
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 2.75
                .Range("B1").ColumnWidth = 11.65
                .Range("C1").ColumnWidth = 7.38
                .Range("D1").ColumnWidth = 7.38
                .Range("E1").ColumnWidth = 9.25
                .Range("F1").ColumnWidth = 9.38
                .Range("G1").ColumnWidth = 9.75
                .Range("H1").ColumnWidth = 9.75
                .Range("I1").ColumnWidth = 9.75
                .Range("J1").ColumnWidth = 9.75
                .Range("K1").ColumnWidth = 9.75
                .Range("L1").ColumnWidth = 8.5
                .Range("M1").ColumnWidth = 8.5
                .Range("N1").ColumnWidth = 8.5
                .Range("O1").ColumnWidth = 12
                .Range("P1").ColumnWidth = 3.25
                .Range("Q1").ColumnWidth = 3.25
                .Range("R1").ColumnWidth = 3.25
                .Range("S1").ColumnWidth = 3.25
                .Range("T1").ColumnWidth = 17


                For J = 7 To 10
                    .Range("B2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin
                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("G3:G3").Borders(J).Weight = 2 ' xlThin
                    .Range("B3:E3").Borders(J).Weight = 2 ' xlThin
                    .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                    .Range("B4:E4").Borders(J).Weight = 2 ' xlThin
                    .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                    .Range("B5:E5").Borders(J).Weight = 2 ' xlThin
                    .Range("F5:F5").Borders(J).Weight = 2 ' xlThin
                    .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                    .Range("F6:F6").Borders(J).Weight = 2 ' xlThin
                    .Range("B7:E7").Borders(J).Weight = 2 ' xlThin
                    .Range("F7:F7").Borders(J).Weight = 2 ' xlThin
                    .Range("A9:A10").Borders(J).Weight = 2 ' xlThin


                    .Range("B9:B9").Borders(J).Weight = 2 ' xlThin
                    .Range("B10:B10").Borders(J).Weight = 2 ' xlThin
                    .Range("C9:E9").Borders(J).Weight = 2 ' xlThin
                    .Range("C10:C10").Borders(J).Weight = 2 ' xlThin
                    .Range("D10:D10").Borders(J).Weight = 2 ' xlThin
                    .Range("E10:E10").Borders(J).Weight = 2 ' xlThin


                    .Range("F9:G9").Borders(J).Weight = 2 ' xlThin
                    .Range("F10:F10").Borders(J).Weight = 2 ' xlThin
                    .Range("G10:G10").Borders(J).Weight = 2 ' xlThin


                    .Range("H9:L9").Borders(J).Weight = 2 ' xlThin
                    .Range("H10:H10").Borders(J).Weight = 2 ' xlThin
                    .Range("I10:I10").Borders(J).Weight = 2 ' xlThin
                    .Range("J10:J10").Borders(J).Weight = 2 ' xlThin
                    .Range("K10:K10").Borders(J).Weight = 2 ' xlThin
                    .Range("L10:L10").Borders(J).Weight = 2 ' xlThin




                    .Range("M9:O9").Borders(J).Weight = 2 ' xlThin
                    .Range("M10:M10").Borders(J).Weight = 2 ' xlThin
                    .Range("N10:N10").Borders(J).Weight = 2 ' xlThin
                    .Range("O10:O10").Borders(J).Weight = 2 ' xlThin

                    .Range("P9:T9").Borders(J).Weight = 2 ' xlThin
                    .Range("P10:P10").Borders(J).Weight = 2 ' xlThin
                    .Range("Q10:Q10").Borders(J).Weight = 2 ' xlThin
                    .Range("R10:R10").Borders(J).Weight = 2 ' xlThin
                    .Range("S10:S10").Borders(J).Weight = 2 ' xlThin
                    .Range("T10:T10").Borders(J).Weight = 2 ' xlThin
                Next




                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 40'HQs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("B2:E2")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "seals"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = Date.Now.ToString("dd/MM/yyyy")
                    .NumberFormat = "B1d-mmm-yy"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B3:E3")
                    .Merge()
                    .Value = "EMPTY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F3:F3")
                    .Merge()
                    .Value = "=SUM(P11:P5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With


                With .Range("B4:E4")
                    .Merge()
                    .Value = "RELEASE MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F4:F4")
                    .Merge()
                    .Value = "=SUM(Q11:Q5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B5:E5")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F5:F5")
                    .Merge()
                    .Value = "=SUM(R11:R5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B6:E6")
                    .Merge()
                    .Value = "SHIPPED"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F6:F6")
                    .Merge()
                    .Value = "=SUM(S11:S5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B7:E7")
                    .Merge()
                    .Value = "TOTAL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F7:F7")
                    .Merge()
                    .Value = "=SUM(F3:F6)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B8:B8")
                    .Merge()
                    .Value = "NOTICE : Due to insurance issue , cargo weight we will best on supplier packing list and invoice"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With

                With .Range("A9:A10")
                    .Merge()
                    .Value = "NO."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B9:B9")
                    .Merge()
                    .Value = "CONTAINER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B10:B10")
                    .Merge()
                    .Value = "NUMBER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C9:E9")
                    .Merge()
                    .Value = "ARRIVED  KANGTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F9:G9")
                    .Merge()
                    .Value = "LEFT KANTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("H9:L9")
                    .Merge()
                    .Value = "YARD CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("M9:O9")
                    .Merge()
                    .Value = "DAMAGE CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P9:T9")
                    .Merge()
                    .Value = "CURRENT STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C10:C10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("D10:D10")
                    .Merge()
                    .Value = "TYPE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("E10:E10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("F10:F10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter

                End With
                With .Range("G10:G10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("H10:H10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("I10:I10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("J10:J10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("K10:K10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("L10:L10")
                    .Merge()
                    .Value = "G. WEIGHT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("M10:M10")
                    .Merge()
                    .Value = "ARR"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("N10:N10")
                    .Merge()
                    .Value = "DEP"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("O10:O10")
                    .Merge()
                    .Value = "DETAILS IF ANY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P10:P10")
                    .Merge()
                    .Value = "MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("Q10:Q10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("R10:R10")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("S10:S10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("T10:T10")
                    .Merge()
                    .Value = "SHIPPER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                Dim dt3 As New DataTable
                With .Range("G3:G3")
                    .Merge()
                    dt3 = connectDB.GetTable("SELECT sealnumber FROM sealid WHERE type = 1")
                    .Value = dt3.Rows(0)("sealnumber").ToString()
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                Dim stringsingle As String = "40'HC"
                Dim dt As New DataTable
                Dim datetime As Date = DateEdit1.EditValue
                ' sql = "Select * from ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where CTNVOYN  >= '" & txt_namemaster.Text & "'  and CTNAGENT like '%WAN HAI%' and  CTNSTAT > 0 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' order by  CTNSTAT DESC ,VOYAGEID ASC,CTNMAINID ASC ;"
                ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql
                sql = "Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID ,BNSHIPNAME,CTNSTAT,TIMEDATE,TIMEDATEIN,CTNDATEOUT ,VOYNAME  from  ( SELECT * FROM ctnmain  WHERE  CTNAGENT like '%" & slcAgent.EditValue & "%' and CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & ") AS ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC ;"

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                connectDB.GetTable(sql, dt)


                'MySqlCommand.Connection = MySql
                'mySqlAdaptor.SelectCommand = MySqlCommand

                '    mySqlReader = MySqlCommand.ExecuteReader




                For i As Integer = 0 To dt.Rows.Count - 1

                    Try

                        For J = 7 To 10
                            .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        Next


                        With .Range("A" + count.ToString + ":A" + count.ToString)
                            .Merge()
                            .Value = countcontainer.ToString
                            .NumberFormat = "@"
                            .HorizontalAlignment = Excel.Constants.xlCenter
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        End With
                        countcontainer += 1

                        With .Range("B" + count.ToString + ":B" + count.ToString)
                            .Merge()
                            .Value = dt.Rows(i)("CTNSTRING")
                            .HorizontalAlignment = Excel.Constants.xlCenter
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        End With


                        With .Range("D" + count.ToString + ":D" + count.ToString)
                            .Merge()
                            .Value = dt.Rows(i)("CTNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("F" + count.ToString + ":F" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            .Value = types & dt.Rows(i)("VOYNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        sql = "Select VOYAGEID,VOYDATEEN,VOYNAME from ( SELECT * FROM ctnmain WHERE CTNMAINID ='" & dt.Rows(i)("CTNMAINID") & "'  " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) As ctnmain  join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID ;"
                        Dim dttable1 As New DataTable
                        connectDB.GetTable(sql, dttable1)

                        With .Range("H" + count.ToString + ":H" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            .Value = dttable1.Rows(0)("VOYDATEEN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("E" + count.ToString + ":E" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            .Value = dttable1.Rows(0)("VOYDATEEN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("C" + count.ToString + ":C" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            If dttable1.Rows(0)("VOYNAME") Is DBNull.Value Then
                            Else
                                .Value = typen & dttable1.Rows(0)("VOYNAME")
                            End If
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("G" + count.ToString + ":G" + count.ToString)
                            .Merge()
                            If dttable1.Rows(0) Is DBNull.Value Then
                            Else
                                .NumberFormat = "@"
                                .Value = dttable1.Rows(0)("VOYDATEEN")
                            End If
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With






                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .Value = dt.Rows(i)("CTNDATEOUT")

                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("G" + count.ToString + ":G" + count.ToString)

                            .Value = dt.Rows(i)("CTNDATEOUT").ToString
                            .NumberFormat = "@"
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)


                            .Value = dt.Rows(i)("TIMEDATE")

                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)


                            If dt.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                            Else
                                .Value = dt.Rows(i)("BNSHIPNAME")
                            End If
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With



                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try

                    count += 1
                Next
                dt = New DataTable


                sql = String.Concat("SELECT A.CTNSTRING,A.VOYAGEID,A.CTNSHIPNAME,A.CTNMAINID,A.VOYDATEEN,A.CTNSTAT,A.CTNVOYN,voyage.VOYNAME AS VOYAGEIDN ,voyage.VOYDATEEN as VOYDATEEN1,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,A.VOYNAME FROM ( Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID,VOYDATEEN,CTNSTAT,CTNVOYN,CTNVOYS,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT , VOYNAME from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT > 1 and CTNSTAT < 4 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'    " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1


                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With
                    countcontainer += 1

                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dt.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = types & dt.Rows(i)("VOYNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = typen & dt.Rows(i)("VOYAGEIDN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = If(IsDBNull(dt.Rows(i)("VOYDATEEN1")), "", If(dt.Rows(i)("VOYDATEEN1") = "DEPOT", "TZ", ""))
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    If dt.Rows(i)("CTNSTAT") = "1" Or dt.Rows(i)("CTNSTAT") = "5" Then
                        With .Range("P" + count.ToString + ":P" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "2" Then
                        With .Range("Q" + count.ToString + ":Q" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "3" Then

                        With .Range("R" + count.ToString + ":R" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                    ElseIf dt.Rows(i)("CTNSTAT") = "4" Then
                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                    End If


                    count = count + 1
                Next






                dt = New DataTable


                sql = String.Concat("SELECT A.CTNSTRING,A.VOYAGEID,A.CTNSHIPNAME,A.CTNMAINID,A.VOYDATEEN,A.CTNSTAT,A.CTNVOYN,voyage.voyname AS VOYAGEIDN ,voyage.VOYDATEEN as VOYDATEEN1,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,A.VOYNAME FROM ( Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID,VOYDATEEN,CTNSTAT,CTNVOYN,CTNVOYS,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,VOYNAME from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 1 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'    " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & "  ) ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid LEFT join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYN ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1


                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With
                    countcontainer += 1

                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dt.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = types & dt.Rows(i)("VOYNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = typen & dt.Rows(i)("VOYAGEIDN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = If(IsDBNull(dt.Rows(i)("VOYDATEEN1")), "", If(dt.Rows(i)("VOYDATEEN1") = "DEPOT", "TZ", ""))
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    If dt.Rows(i)("CTNSTAT") = "1" Or dt.Rows(i)("CTNSTAT") = "5" Then
                        With .Range("P" + count.ToString + ":P" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1

                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "2" Then
                        With .Range("Q" + count.ToString + ":Q" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "3" Then

                        With .Range("R" + count.ToString + ":R" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                    ElseIf dt.Rows(i)("CTNSTAT") = "4" Then
                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                    End If


                    count = count + 1
                Next


                dt = New DataTable


                sql = String.Concat("SELECT A.CTNSTRING,A.VOYAGEID,A.CTNSHIPNAME,A.CTNMAINID,A.VOYDATEEN,A.CTNSTAT,A.CTNVOYN,voyage.voyname AS VOYAGEIDN ,voyage.VOYDATEEN as VOYDATEEN1,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,A.VOYNAME FROM ( Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID,VOYDATEEN,CTNSTAT,CTNVOYN,CTNVOYS,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,VOYNAME from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 0 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1


                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With
                    countcontainer += 1

                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dt.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = types & dt.Rows(i)("VOYNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = typen & dt.Rows(i)("VOYAGEIDN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = If(IsDBNull(dt.Rows(i)("VOYDATEEN1")), "", If(dt.Rows(i)("VOYDATEEN1") = "DEPOT", "TZ", ""))
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    If dt.Rows(i)("CTNSTAT") = "1" Or dt.Rows(i)("CTNSTAT") = "5" Then
                        With .Range("P" + count.ToString + ":P" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "2" Then
                        With .Range("Q" + count.ToString + ":Q" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With


                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")

                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "3" Then

                        With .Range("R" + count.ToString + ":R" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")

                        End With
                    ElseIf dt.Rows(i)("CTNSTAT") = "4" Then
                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With
                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")

                        End With
                    End If


                    count = count + 1
                Next




            End With

            count = 11
            countcontainer = 1
            With excelsheets2
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 2.75
                .Range("B1").ColumnWidth = 11.65
                .Range("C1").ColumnWidth = 7.38
                .Range("D1").ColumnWidth = 7.38
                .Range("E1").ColumnWidth = 9.25
                .Range("F1").ColumnWidth = 9.38
                .Range("G1").ColumnWidth = 9.75
                .Range("H1").ColumnWidth = 9.75
                .Range("I1").ColumnWidth = 9.75
                .Range("J1").ColumnWidth = 9.75
                .Range("K1").ColumnWidth = 9.75
                .Range("L1").ColumnWidth = 8.5
                .Range("M1").ColumnWidth = 8.5
                .Range("N1").ColumnWidth = 8.5
                .Range("O1").ColumnWidth = 12
                .Range("P1").ColumnWidth = 3.25
                .Range("Q1").ColumnWidth = 3.25
                .Range("R1").ColumnWidth = 3.25
                .Range("S1").ColumnWidth = 3.25
                .Range("T1").ColumnWidth = 17


                For J = 7 To 10
                    .Range("B2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin
                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("G3:G3").Borders(J).Weight = 2 ' xlThin
                    .Range("B3:E3").Borders(J).Weight = 2 ' xlThin
                    .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                    .Range("B4:E4").Borders(J).Weight = 2 ' xlThin
                    .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                    .Range("B5:E5").Borders(J).Weight = 2 ' xlThin
                    .Range("F5:F5").Borders(J).Weight = 2 ' xlThin
                    .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                    .Range("F6:F6").Borders(J).Weight = 2 ' xlThin
                    .Range("B7:E7").Borders(J).Weight = 2 ' xlThin
                    .Range("F7:F7").Borders(J).Weight = 2 ' xlThin
                    .Range("A9:A10").Borders(J).Weight = 2 ' xlThin


                    .Range("B9:B9").Borders(J).Weight = 2 ' xlThin
                    .Range("B10:B10").Borders(J).Weight = 2 ' xlThin
                    .Range("C9:E9").Borders(J).Weight = 2 ' xlThin
                    .Range("C10:C10").Borders(J).Weight = 2 ' xlThin
                    .Range("D10:D10").Borders(J).Weight = 2 ' xlThin
                    .Range("E10:E10").Borders(J).Weight = 2 ' xlThin


                    .Range("F9:G9").Borders(J).Weight = 2 ' xlThin
                    .Range("F10:F10").Borders(J).Weight = 2 ' xlThin
                    .Range("G10:G10").Borders(J).Weight = 2 ' xlThin


                    .Range("H9:L9").Borders(J).Weight = 2 ' xlThin
                    .Range("H10:H10").Borders(J).Weight = 2 ' xlThin
                    .Range("I10:I10").Borders(J).Weight = 2 ' xlThin
                    .Range("J10:J10").Borders(J).Weight = 2 ' xlThin
                    .Range("K10:K10").Borders(J).Weight = 2 ' xlThin
                    .Range("L10:L10").Borders(J).Weight = 2 ' xlThin




                    .Range("M9:O9").Borders(J).Weight = 2 ' xlThin
                    .Range("M10:M10").Borders(J).Weight = 2 ' xlThin
                    .Range("N10:N10").Borders(J).Weight = 2 ' xlThin
                    .Range("O10:O10").Borders(J).Weight = 2 ' xlThin

                    .Range("P9:T9").Borders(J).Weight = 2 ' xlThin
                    .Range("P10:P10").Borders(J).Weight = 2 ' xlThin
                    .Range("Q10:Q10").Borders(J).Weight = 2 ' xlThin
                    .Range("R10:R10").Borders(J).Weight = 2 ' xlThin
                    .Range("S10:S10").Borders(J).Weight = 2 ' xlThin
                    .Range("T10:T10").Borders(J).Weight = 2 ' xlThin
                Next




                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 20'GPs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("B2:E2")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "seals"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = Date.Now.ToString("dd/MM/yyyy")
                    .NumberFormat = "B1d-mmm-yy"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B3:E3")
                    .Merge()
                    .Value = "EMPTY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F3:F3")
                    .Merge()
                    .Value = "=SUM(P11:P5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With


                With .Range("B4:E4")
                    .Merge()
                    .Value = "RELEASE MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F4:F4")
                    .Merge()
                    .Value = "=SUM(Q11:Q5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B5:E5")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F5:F5")
                    .Merge()
                    .Value = "=SUM(R11:R5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B6:E6")
                    .Merge()
                    .Value = "SHIPPED"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F6:F6")
                    .Merge()
                    .Value = "=SUM(S11:S5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B7:E7")
                    .Merge()
                    .Value = "TOTAL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F7:F7")
                    .Merge()
                    .Value = "=SUM(F3:F6)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B8:B8")
                    .Merge()
                    .Value = "NOTICE : Due to insurance issue , cargo weight we will best on supplier packing list and invoice"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With

                With .Range("A9:A10")
                    .Merge()
                    .Value = "NO."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B9:B9")
                    .Merge()
                    .Value = "CONTAINER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B10:B10")
                    .Merge()
                    .Value = "NUMBER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C9:E9")
                    .Merge()
                    .Value = "ARRIVED  KANGTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F9:G9")
                    .Merge()
                    .Value = "LEFT KANTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("H9:L9")
                    .Merge()
                    .Value = "YARD CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("M9:O9")
                    .Merge()
                    .Value = "DAMAGE CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P9:T9")
                    .Merge()
                    .Value = "CURRENT STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C10:C10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("D10:D10")
                    .Merge()
                    .Value = "TYPE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("E10:E10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("F10:F10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter

                End With
                With .Range("G10:G10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("H10:H10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("I10:I10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("J10:J10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("K10:K10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("L10:L10")
                    .Merge()
                    .Value = "G. WEIGHT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("M10:M10")
                    .Merge()
                    .Value = "ARR"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("N10:N10")
                    .Merge()
                    .Value = "DEP"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("O10:O10")
                    .Merge()
                    .Value = "DETAILS IF ANY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P10:P10")
                    .Merge()
                    .Value = "MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("Q10:Q10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("R10:R10")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("S10:S10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("T10:T10")
                    .Merge()
                    .Value = "SHIPPER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                Dim dt1 As New DataTable
                With .Range("G3:G3")
                    .Merge()
                    dt1 = connectDB.GetTable("SELECT sealnumber FROM sealid WHERE type = 1")
                    .Value = dt1.Rows(0)("sealnumber").ToString()
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                Dim stringsingle As String = "20'GP"
                Dim dt As New DataTable
                Dim datetime As Date = DateEdit1.EditValue
                'sql = "Select * from ctnmain join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where CTNVOYN  >= '" & txt_namemaster.Text & "'  and CTNAGENT like '%WAN HAI%' and  CTNSTAT > 0 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' order by  CTNSTAT DESC ,VOYAGEID ASC,CTNMAINID ASC ;"
                ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql
                sql = "Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID ,BNSHIPNAME,CTNSTAT,TIMEDATE,TIMEDATEIN,CTNDATEOUT ,VOYNAME  from  ( SELECT * FROM ctnmain  WHERE  CTNAGENT like '%" & slcAgent.EditValue & "%' and CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(stringsingle) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "' " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) AS ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC ;"

                With .Range("A" + count.ToString + ":T" + count.ToString)

                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                connectDB.GetTable(sql, dt)


                'MySqlCommand.Connection = MySql
                'mySqlAdaptor.SelectCommand = MySqlCommand

                '    mySqlReader = MySqlCommand.ExecuteReader




                For i As Integer = 0 To dt.Rows.Count - 1

                    Try

                        For J = 7 To 10
                            .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                            .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        Next


                        With .Range("A" + count.ToString + ":A" + count.ToString)
                            .Merge()
                            .Value = countcontainer.ToString
                            .NumberFormat = "@"
                            .HorizontalAlignment = Excel.Constants.xlCenter
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        End With
                        countcontainer += 1

                        With .Range("B" + count.ToString + ":B" + count.ToString)
                            .Merge()
                            .Value = dt.Rows(i)("CTNSTRING")
                            .HorizontalAlignment = Excel.Constants.xlCenter
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        End With

                        Try
                            With .Range("D" + count.ToString + ":D" + count.ToString)
                                .Merge()
                                .Value = dt.Rows(i)("CTNSHIPNAME")
                                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                                .HorizontalAlignment = Excel.Constants.xlCenter
                            End With

                        Catch ex As Exception

                        End Try


                        With .Range("F" + count.ToString + ":F" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            .Value = types & dt.Rows(i)("VOYNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        sql = "Select VOYAGEID,VOYDATEEN,VOYNAME from ( SELECT * FROM ctnmain WHERE CTNMAINID ='" & dt.Rows(i)("CTNMAINID") & "' " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & ") As ctnmain  join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID ;"
                        Dim dttable1 As New DataTable
                        connectDB.GetTable(sql, dttable1)

                        With .Range("H" + count.ToString + ":H" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            .Value = dttable1.Rows(0)("VOYDATEEN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("E" + count.ToString + ":E" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            .Value = dttable1.Rows(0)("VOYDATEEN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("C" + count.ToString + ":C" + count.ToString)
                            .Merge()
                            .NumberFormat = "@"
                            If dttable1.Rows(0)("VOYNAME") Is DBNull.Value Then
                            Else
                                .Value = typen & dttable1.Rows(0)("VOYNAME")
                            End If
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("G" + count.ToString + ":G" + count.ToString)
                            .Merge()
                            If dttable1.Rows(0) Is DBNull.Value Then
                            Else
                                .NumberFormat = "@"
                                .Value = dttable1.Rows(0)("VOYDATEEN")
                            End If
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With






                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .Value = dt.Rows(i)("CTNDATEOUT")

                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("G" + count.ToString + ":G" + count.ToString)

                            .Value = dt.Rows(i)("CTNDATEOUT").ToString

                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)


                            .Value = dt.Rows(i)("TIMEDATE")

                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)


                            If dt.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                            Else
                                .Value = dt.Rows(i)("BNSHIPNAME")
                            End If
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With



                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try

                    count += 1
                Next
                dt = New DataTable


                sql = String.Concat("SELECT A.CTNSTRING,A.VOYAGEID,A.CTNSHIPNAME,A.CTNMAINID,A.VOYDATEEN,A.CTNSTAT,A.CTNVOYN,voyage.voyname AS VOYAGEIDN ,voyage.VOYDATEEN as VOYDATEEN1,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,A.VOYNAME FROM ( Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID,VOYDATEEN,CTNSTAT,CTNVOYN,CTNVOYS,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT , VOYNAME from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT > 1 and CTNSTAT < 4 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1


                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With
                    countcontainer += 1

                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dt.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = types & dt.Rows(i)("VOYNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = typen & dt.Rows(i)("VOYAGEIDN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = If(IsDBNull(dt.Rows(i)("VOYDATEEN1")), "", If(dt.Rows(i)("VOYDATEEN1") = "DEPOT", "TZ", ""))
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    If dt.Rows(i)("CTNSTAT") = "1" Or dt.Rows(i)("CTNSTAT") = "5" Then
                        With .Range("P" + count.ToString + ":P" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "2" Then
                        With .Range("Q" + count.ToString + ":Q" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "3" Then

                        With .Range("R" + count.ToString + ":R" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                    ElseIf dt.Rows(i)("CTNSTAT") = "4" Then
                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                    End If


                    count = count + 1
                Next






                dt = New DataTable


                sql = String.Concat("SELECT A.CTNSTRING,A.VOYAGEID,A.CTNSHIPNAME,A.CTNMAINID,A.VOYDATEEN,A.CTNSTAT,A.CTNVOYN,voyage.voyname AS VOYAGEIDN ,voyage.VOYDATEEN as VOYDATEEN1,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,A.VOYNAME FROM ( Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID,VOYDATEEN,CTNSTAT,CTNVOYN,CTNVOYS,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,VOYNAME from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 1 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain  LEFT join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid LEFT join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYN ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1


                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With
                    countcontainer += 1

                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dt.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = types & dt.Rows(i)("VOYNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = typen & dt.Rows(i)("VOYAGEIDN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = If(IsDBNull(dt.Rows(i)("VOYDATEEN1")), "", If(dt.Rows(i)("VOYDATEEN1") = "DEPOT", "TZ", ""))
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    If dt.Rows(i)("CTNSTAT") = "1" Or dt.Rows(i)("CTNSTAT") = "5" Then
                        With .Range("P" + count.ToString + ":P" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "2" Then
                        With .Range("Q" + count.ToString + ":Q" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With


                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "3" Then

                        With .Range("R" + count.ToString + ":R" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                    ElseIf dt.Rows(i)("CTNSTAT") = "4" Then
                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")
                            .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                            .HorizontalAlignment = Excel.Constants.xlCenter

                        End With
                    End If


                    count = count + 1
                Next


                dt = New DataTable


                sql = String.Concat("SELECT A.CTNSTRING,A.VOYAGEID,A.CTNSHIPNAME,A.CTNMAINID,A.VOYDATEEN,A.CTNSTAT,A.CTNVOYN,voyage.voyname AS VOYAGEIDN ,voyage.VOYDATEEN as VOYDATEEN1,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,A.VOYNAME FROM ( Select CTNSTRING,VOYAGEID,CTNSHIPNAME,CTNMAINID,VOYDATEEN,CTNSTAT,CTNVOYN,CTNVOYS,BNSHIPNAME,TIMEDATE,TIMEDATEIN, CTNDATEOUT,VOYNAME from (SELECT * FROM ctnmain WHERE    CTNAGENT like '%" & slcAgent.EditValue & "%' and  CTNSTAT = 0 and CTNSIZE ='", MySqlHelper.EscapeString(stringsingle), "'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   LEFT join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   order by CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC ) as A  JOIN   voyage on A.CTNVOYN = voyage.VOYAGEID  order by A.CTNVOYS ASC, BNSHIPNAME ASC,CTNSTAT DESC  ;")
                dt = connectDB.GetTable(sql)

                For i As Integer = 0 To dt.Rows.Count - 1


                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("Q" + count.ToString + ":Q" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("R" + count.ToString + ":R" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("S" + count.ToString + ":S" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("T" + count.ToString + ":T" + count.ToString).Borders(J).Weight = 2 ' xlThin
                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With
                    countcontainer += 1

                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dt.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = types & dt.Rows(i)("VOYNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .Value = typen & dt.Rows(i)("VOYAGEIDN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .Merge()
                        .Value = dt.Rows(i)("VOYDATEEN1")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .Value = If(IsDBNull(dt.Rows(i)("VOYDATEEN1")), "", If(dt.Rows(i)("VOYDATEEN1") = "DEPOT", "TZ", ""))
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With


                    If dt.Rows(i)("CTNSTAT") = "1" Or dt.Rows(i)("CTNSTAT") = "5" Then
                        With .Range("P" + count.ToString + ":P" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "2" Then
                        With .Range("Q" + count.ToString + ":Q" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With


                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")

                        End With

                    ElseIf dt.Rows(i)("CTNSTAT") = "3" Then

                        With .Range("R" + count.ToString + ":R" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With
                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")

                        End With
                    ElseIf dt.Rows(i)("CTNSTAT") = "4" Then
                        With .Range("S" + count.ToString + ":S" + count.ToString)
                            .NumberFormat = "@"
                            .Value = 1
                        End With
                        With .Range("K" + count.ToString + ":K" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With

                        With .Range("I" + count.ToString + ":I" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATE")
                        End With
                        With .Range("J" + count.ToString + ":J" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("TIMEDATEIN")
                        End With
                        With .Range("T" + count.ToString + ":T" + count.ToString)
                            .NumberFormat = "@"
                            .Value = dt.Rows(i)("BNSHIPNAME")

                        End With
                    End If


                    count = count + 1
                Next


            End With
            With excelsheets4
                count = 11
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 2.75
                .Range("B1").ColumnWidth = 11.65
                .Range("C1").ColumnWidth = 7.38
                .Range("D1").ColumnWidth = 7.38
                .Range("E1").ColumnWidth = 9.25
                .Range("F1").ColumnWidth = 9.38
                .Range("G1").ColumnWidth = 9.75
                .Range("H1").ColumnWidth = 9.75
                .Range("I1").ColumnWidth = 9.75
                .Range("J1").ColumnWidth = 9.75
                .Range("K1").ColumnWidth = 9.75
                .Range("L1").ColumnWidth = 8.5
                .Range("M1").ColumnWidth = 8.5
                .Range("N1").ColumnWidth = 8.5
                .Range("O1").ColumnWidth = 12
                .Range("P1").ColumnWidth = 3.25
                .Range("Q1").ColumnWidth = 3.25
                .Range("R1").ColumnWidth = 3.25
                .Range("S1").ColumnWidth = 3.25
                .Range("T1").ColumnWidth = 17


                For J = 7 To 10
                    .Range("B2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin
                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("G3:G3").Borders(J).Weight = 2 ' xlThin
                    .Range("B3:E3").Borders(J).Weight = 2 ' xlThin
                    .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                    .Range("B4:E4").Borders(J).Weight = 2 ' xlThin
                    .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                    .Range("B5:E5").Borders(J).Weight = 2 ' xlThin
                    .Range("F5:F5").Borders(J).Weight = 2 ' xlThin
                    .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                    .Range("F6:F6").Borders(J).Weight = 2 ' xlThin
                    .Range("B7:E7").Borders(J).Weight = 2 ' xlThin
                    .Range("F7:F7").Borders(J).Weight = 2 ' xlThin
                    .Range("A9:A10").Borders(J).Weight = 2 ' xlThin


                    .Range("B9:B9").Borders(J).Weight = 2 ' xlThin
                    .Range("B10:B10").Borders(J).Weight = 2 ' xlThin
                    .Range("C9:E9").Borders(J).Weight = 2 ' xlThin
                    .Range("C10:C10").Borders(J).Weight = 2 ' xlThin
                    .Range("D10:D10").Borders(J).Weight = 2 ' xlThin
                    .Range("E10:E10").Borders(J).Weight = 2 ' xlThin


                    .Range("F9:G9").Borders(J).Weight = 2 ' xlThin
                    .Range("F10:F10").Borders(J).Weight = 2 ' xlThin
                    .Range("G10:G10").Borders(J).Weight = 2 ' xlThin


                    .Range("H9:L9").Borders(J).Weight = 2 ' xlThin
                    .Range("H10:H10").Borders(J).Weight = 2 ' xlThin
                    .Range("I10:I10").Borders(J).Weight = 2 ' xlThin
                    .Range("J10:J10").Borders(J).Weight = 2 ' xlThin
                    .Range("K10:K10").Borders(J).Weight = 2 ' xlThin
                    .Range("L10:L10").Borders(J).Weight = 2 ' xlThin




                    .Range("M9:O9").Borders(J).Weight = 2 ' xlThin
                    .Range("M10:M10").Borders(J).Weight = 2 ' xlThin
                    .Range("N10:N10").Borders(J).Weight = 2 ' xlThin
                    .Range("O10:O10").Borders(J).Weight = 2 ' xlThin

                    .Range("P9:T9").Borders(J).Weight = 2 ' xlThin
                    .Range("P10:P10").Borders(J).Weight = 2 ' xlThin
                    .Range("Q10:Q10").Borders(J).Weight = 2 ' xlThin
                    .Range("R10:R10").Borders(J).Weight = 2 ' xlThin
                    .Range("S10:S10").Borders(J).Weight = 2 ' xlThin
                    .Range("T10:T10").Borders(J).Weight = 2 ' xlThin
                Next

                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 20'GPs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("B2:E2")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "seals"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = Date.Now.ToString("dd/MM/yyyy")
                    .NumberFormat = "B1d-mmm-yy"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B3:E3")
                    .Merge()
                    .Value = "EMPTY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F3:F3")
                    .Merge()
                    .Value = "=SUM(P11:P5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With


                With .Range("B4:E4")
                    .Merge()
                    .Value = "RELEASE MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F4:F4")
                    .Merge()
                    .Value = "=SUM(Q11:Q5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B5:E5")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F5:F5")
                    .Merge()
                    .Value = "=SUM(R11:R5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B6:E6")
                    .Merge()
                    .Value = "SHIPPED"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F6:F6")
                    .Merge()
                    .Value = "=SUM(S11:S5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B7:E7")
                    .Merge()
                    .Value = "TOTAL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F7:F7")
                    .Merge()
                    .Value = "=SUM(F3:F6)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B8:B8")
                    .Merge()
                    .Value = "NOTICE : Due to insurance issue , cargo weight we will best on supplier packing list and invoice"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With

                With .Range("A9:A10")
                    .Merge()
                    .Value = "NO."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B9:B9")
                    .Merge()
                    .Value = "CONTAINER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B10:B10")
                    .Merge()
                    .Value = "NUMBER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C9:E9")
                    .Merge()
                    .Value = "ARRIVED  KANGTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F9:G9")
                    .Merge()
                    .Value = "LEFT KANTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("H9:L9")
                    .Merge()
                    .Value = "YARD CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("M9:O9")
                    .Merge()
                    .Value = "DAMAGE CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P9:T9")
                    .Merge()
                    .Value = "CURRENT STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C10:C10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("D10:D10")
                    .Merge()
                    .Value = "TYPE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("E10:E10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("F10:F10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter

                End With
                With .Range("G10:G10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("H10:H10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("I10:I10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("J10:J10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("K10:K10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("L10:L10")
                    .Merge()
                    .Value = "G. WEIGHT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("M10:M10")
                    .Merge()
                    .Value = "ARR"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("N10:N10")
                    .Merge()
                    .Value = "DEP"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("O10:O10")
                    .Merge()
                    .Value = "DETAILS IF ANY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P10:P10")
                    .Merge()
                    .Value = "MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("Q10:Q10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("R10:R10")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("S10:S10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("T10:T10")
                    .Merge()
                    .Value = "SHIPPER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                Dim dataTable1 As DataTable
                Dim datetime As Date = DateEdit1.EditValue
                Dim str4 As String = "20'GP"
                Dim str5 As String = String.Concat(New String() {"SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN ,voyage.VOYNAME  from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN   from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str4) & "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%" & slcAgent.EditValue & "%'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like '%" & slcAgent.EditValue & "%' and  rpttype ='22G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"})
                dataTable1 = connectDB.GetTable(str5)


                With .Range("A11:T" & dataTable1.Rows.Count + 6)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With


                Dim objArray(dataTable1.Rows.Count + 1 - 1, 19) As Object
                Dim empty As String = String.Empty
                Dim str6 As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                Dim length As Integer = str6.Length
                Dim num14 As Integer = 1
                Dim num15 As Integer = 0
                Do
                    Dim num16 As Integer = dataTable1.Rows.Count - 1
                    If num16 = -1 Then
                        Exit Do
                    End If
                    Dim num17 As Integer = 0
                    Do
                        If (num15 = 0) Then
                            num14 = num14 + 1
                            objArray(num17 + 1, num15) = num17 + 1
                        ElseIf (num15 = 1) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("CTNSTRING").ToString()
                        ElseIf (num15 = 2) Then
                            objArray(num17 + 1, num15) = String.Concat(My.Settings.ShipIn, dataTable1.Rows(num17)("CTNVOYN").ToString())
                        ElseIf (num15 = 3) Then
                            If (dataTable1.Rows(num17)("CTNSHIPNAMES") Is DBNull.Value) Then
                                If (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(dataTable1.Rows(num17)("CTNSHIPNAMES"), "DEPOT", False)) Then
                                    objArray(num17 + 1, num15) = "TZ"
                                End If
                            End If
                        ElseIf (num15 = 4) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("VOYDATEEN").ToString()
                        ElseIf (num15 = 5) Then
                            objArray(num17 + 1, num15) = String.Concat(types, dataTable1.Rows(num17)("VOYAGEIDS").ToString())
                        ElseIf (num15 = 6) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("CTNDATEOUT").ToString()
                        ElseIf (num15 = 7) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("VOYDATEEN").ToString()
                        ElseIf (num15 = 8) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("TIMEDATE").ToString()
                        ElseIf (num15 = 9) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("TIMEDATEIN").ToString()
                        ElseIf (num15 = 10) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("CTNDATEOUT").ToString()
                        ElseIf (num15 = 18) Then
                            objArray(num17 + 1, num15) = 1
                        ElseIf (num15 = 19) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("BNSHIPNAME").ToString()
                        End If
                        num17 = num17 + 1
                    Loop While num17 <= num16
                    num15 = num15 + 1
                Loop While num15 <= 20

                If (dataTable1.Columns.Count > length) Then
                    empty = str6.Substring(19 / length - 1, 1)
                End If
                empty = String.Concat(empty, str6.Substring(19 Mod length, 1))
                Dim str7 As String = String.Format("A11:{0}{1}", empty, dataTable1.Rows.Count + 6)
                .Range(str7).Value2 = objArray


            End With


            With excelsheets3
                count = 11
                countcontainer = 1
                .Rows("1:1").rowheight = 38.5
                .Rows("2:7").rowheight = 22.5
                .Rows("8:500").rowheight = 21
                .Range("A1:Z600").Font.Name = "Tahoma"
                .Range("A1:Z600").Font.Size = 8


                .Range("A1").ColumnWidth = 2.75
                .Range("B1").ColumnWidth = 11.65
                .Range("C1").ColumnWidth = 7.38
                .Range("D1").ColumnWidth = 7.38
                .Range("E1").ColumnWidth = 9.25
                .Range("F1").ColumnWidth = 9.38
                .Range("G1").ColumnWidth = 9.75
                .Range("H1").ColumnWidth = 9.75
                .Range("I1").ColumnWidth = 9.75
                .Range("J1").ColumnWidth = 9.75
                .Range("K1").ColumnWidth = 9.75
                .Range("L1").ColumnWidth = 8.5
                .Range("M1").ColumnWidth = 8.5
                .Range("N1").ColumnWidth = 8.5
                .Range("O1").ColumnWidth = 12
                .Range("P1").ColumnWidth = 3.25
                .Range("Q1").ColumnWidth = 3.25
                .Range("R1").ColumnWidth = 3.25
                .Range("S1").ColumnWidth = 3.25
                .Range("T1").ColumnWidth = 17


                For J = 7 To 10
                    .Range("B2:E2").Borders(J).Weight = 2 ' xlThin
                    .Range("F2:F2").Borders(J).Weight = 2 ' xlThin
                    .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                    .Range("G3:G3").Borders(J).Weight = 2 ' xlThin
                    .Range("B3:E3").Borders(J).Weight = 2 ' xlThin
                    .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                    .Range("B4:E4").Borders(J).Weight = 2 ' xlThin
                    .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                    .Range("B5:E5").Borders(J).Weight = 2 ' xlThin
                    .Range("F5:F5").Borders(J).Weight = 2 ' xlThin
                    .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                    .Range("F6:F6").Borders(J).Weight = 2 ' xlThin
                    .Range("B7:E7").Borders(J).Weight = 2 ' xlThin
                    .Range("F7:F7").Borders(J).Weight = 2 ' xlThin
                    .Range("A9:A10").Borders(J).Weight = 2 ' xlThin


                    .Range("B9:B9").Borders(J).Weight = 2 ' xlThin
                    .Range("B10:B10").Borders(J).Weight = 2 ' xlThin
                    .Range("C9:E9").Borders(J).Weight = 2 ' xlThin
                    .Range("C10:C10").Borders(J).Weight = 2 ' xlThin
                    .Range("D10:D10").Borders(J).Weight = 2 ' xlThin
                    .Range("E10:E10").Borders(J).Weight = 2 ' xlThin


                    .Range("F9:G9").Borders(J).Weight = 2 ' xlThin
                    .Range("F10:F10").Borders(J).Weight = 2 ' xlThin
                    .Range("G10:G10").Borders(J).Weight = 2 ' xlThin


                    .Range("H9:L9").Borders(J).Weight = 2 ' xlThin
                    .Range("H10:H10").Borders(J).Weight = 2 ' xlThin
                    .Range("I10:I10").Borders(J).Weight = 2 ' xlThin
                    .Range("J10:J10").Borders(J).Weight = 2 ' xlThin
                    .Range("K10:K10").Borders(J).Weight = 2 ' xlThin
                    .Range("L10:L10").Borders(J).Weight = 2 ' xlThin




                    .Range("M9:O9").Borders(J).Weight = 2 ' xlThin
                    .Range("M10:M10").Borders(J).Weight = 2 ' xlThin
                    .Range("N10:N10").Borders(J).Weight = 2 ' xlThin
                    .Range("O10:O10").Borders(J).Weight = 2 ' xlThin

                    .Range("P9:T9").Borders(J).Weight = 2 ' xlThin
                    .Range("P10:P10").Borders(J).Weight = 2 ' xlThin
                    .Range("Q10:Q10").Borders(J).Weight = 2 ' xlThin
                    .Range("R10:R10").Borders(J).Weight = 2 ' xlThin
                    .Range("S10:S10").Borders(J).Weight = 2 ' xlThin
                    .Range("T10:T10").Borders(J).Weight = 2 ' xlThin
                Next

                With .Range("B1:E1")
                    .Merge()
                    .Value = "Port Daily Reporting 40'HQs"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("B2:E2")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("G2:G2")
                    .Merge()
                    .Value = "seals"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F2:F2")
                    .Merge()
                    .Value = Date.Now.ToString("dd/MM/yyyy")
                    .NumberFormat = "B1d-mmm-yy"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B3:E3")
                    .Merge()
                    .Value = "EMPTY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F3:F3")
                    .Merge()
                    .Value = "=SUM(P11:P5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With


                With .Range("B4:E4")
                    .Merge()
                    .Value = "RELEASE MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F4:F4")
                    .Merge()
                    .Value = "=SUM(Q11:Q5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B5:E5")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F5:F5")
                    .Merge()
                    .Value = "=SUM(R11:R5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B6:E6")
                    .Merge()
                    .Value = "SHIPPED"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F6:F6")
                    .Merge()
                    .Value = "=SUM(S11:S5101)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B7:E7")
                    .Merge()
                    .Value = "TOTAL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With
                With .Range("F7:F7")
                    .Merge()
                    .Value = "=SUM(F3:F6)"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B8:B8")
                    .Merge()
                    .Value = "NOTICE : Due to insurance issue , cargo weight we will best on supplier packing list and invoice"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .Font.Size = 9
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .Font.Bold = True
                End With

                With .Range("A9:A10")
                    .Merge()
                    .Value = "NO."
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B9:B9")
                    .Merge()
                    .Value = "CONTAINER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("B10:B10")
                    .Merge()
                    .Value = "NUMBER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C9:E9")
                    .Merge()
                    .Value = "ARRIVED  KANGTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("F9:G9")
                    .Merge()
                    .Value = "LEFT KANTANG"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("H9:L9")
                    .Merge()
                    .Value = "YARD CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("M9:O9")
                    .Merge()
                    .Value = "DAMAGE CONTROL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P9:T9")
                    .Merge()
                    .Value = "CURRENT STATUS"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("C10:C10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("D10:D10")
                    .Merge()
                    .Value = "TYPE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("E10:E10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("F10:F10")
                    .Merge()
                    .Value = "VOYAGE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter

                End With
                With .Range("G10:G10")
                    .Merge()
                    .Value = "DATE"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("H10:H10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("I10:I10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("J10:J10")
                    .Merge()
                    .Value = "DATE IN"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("K10:K10")
                    .Merge()
                    .Value = "DATE OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("L10:L10")
                    .Merge()
                    .Value = "G. WEIGHT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("M10:M10")
                    .Merge()
                    .Value = "ARR"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("N10:N10")
                    .Merge()
                    .Value = "DEP"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("O10:O10")
                    .Merge()
                    .Value = "DETAILS IF ANY"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                    .Font.Bold = True
                End With
                With .Range("P10:P10")
                    .Merge()
                    .Value = "MT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With

                With .Range("Q10:Q10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("R10:R10")
                    .Merge()
                    .Value = "FCL"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("S10:S10")
                    .Merge()
                    .Value = "OUT"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With
                With .Range("T10:T10")
                    .Merge()
                    .Value = "SHIPPER"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Excel.Constants.xlCenter
                End With


                Dim dataTable1 As DataTable
                Dim datetime As Date = DateEdit1.EditValue
                Dim str4 As String = "40'HC"
                Dim str5 As String = String.Concat(New String() {"SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN ,voyage.VOYNAME  from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='", MySqlHelper.EscapeString(str4), "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%" & slcAgent.EditValue & "%'   " & If(slcDepot.EditValue Is Nothing, "", " AND idmascmrunning = '" & slcDepot.EditValue & "'") & " ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID   join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like  '%" & slcAgent.EditValue & "%' and  rpttype ='45G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"})
                dataTable1 = connectDB.GetTable(str5)


                With .Range("A11:T" & dataTable1.Rows.Count + 6)
                    .NumberFormat = "@"
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                    .Borders.Weight = 2
                    .Font.Size = 8
                    .Font.Name = "Tahoma"
                    .RowHeight = 21
                End With
                Dim objArray(dataTable1.Rows.Count + 1 - 1, 19) As Object
                Dim empty As String = String.Empty
                Dim str6 As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                Dim length As Integer = str6.Length
                Dim num14 As Integer = 1
                Dim num15 As Integer = 0
                Do
                    Dim num16 As Integer = dataTable1.Rows.Count - 1
                    If num16 = -1 Then
                        Exit Do
                    End If
                    Dim num17 As Integer = 0
                    Do
                        If (num15 = 0) Then
                            num14 = num14 + 1
                            objArray(num17 + 1, num15) = num17 + 1
                        ElseIf (num15 = 1) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("CTNSTRING").ToString()
                        ElseIf (num15 = 2) Then
                            objArray(num17 + 1, num15) = String.Concat(My.Settings.ShipIn, dataTable1.Rows(num17)("CTNVOYN").ToString())
                        ElseIf (num15 = 3) Then
                            If (dataTable1.Rows(num17)("CTNSHIPNAMES") Is DBNull.Value) Then
                                If (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(dataTable1.Rows(num17)("CTNSHIPNAMES"), "DEPOT", False)) Then
                                    objArray(num17 + 1, num15) = "TZ"
                                End If
                            End If
                        ElseIf (num15 = 4) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("VOYDATEEN").ToString()
                        ElseIf (num15 = 5) Then
                            objArray(num17 + 1, num15) = String.Concat(types, dataTable1.Rows(num17)("VOYAGEIDS").ToString())
                        ElseIf (num15 = 6) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("CTNDATEOUT").ToString()
                        ElseIf (num15 = 7) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("VOYDATEEN").ToString()
                        ElseIf (num15 = 8) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("TIMEDATE").ToString()
                        ElseIf (num15 = 9) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("TIMEDATEIN").ToString()
                        ElseIf (num15 = 10) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("CTNDATEOUT").ToString()
                        ElseIf (num15 = 18) Then
                            objArray(num17 + 1, num15) = 1
                        ElseIf (num15 = 19) Then
                            objArray(num17 + 1, num15) = dataTable1.Rows(num17)("BNSHIPNAME").ToString()
                        End If
                        num17 = num17 + 1
                    Loop While num17 <= num16
                    num15 = num15 + 1
                Loop While num15 <= 20

                If (dataTable1.Columns.Count > length) Then
                    empty = str6.Substring(19 / length - 1, 1)
                End If
                empty = String.Concat(empty, str6.Substring(19 Mod length, 1))
                Dim str7 As String = String.Format("A11:{0}{1}", empty, dataTable1.Rows.Count + 6)
                .Range(str7).Value2 = objArray




            End With



        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try



        Try
            excelbooks.SaveAs(pathExcel.ToString + "\" & slcDepot.Text & "'S DAILY STATUS REPORT_" & slcAgent.EditValue & " LINES " + Date.Now.ToString("dd-MM-yyyy") + ".xlsx")

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelsheets2 = Nothing

            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelsheets2 = Nothing

            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try

    End Sub

    Private Sub ExcelDailyYM()



        Dim pathExcel As String
        Dim count As Integer = 11

        Dim countcontainer As Integer = 1
        pathExcel = FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        Dim excelsheets2 As Excel.Worksheet
        Dim excelsheets3 As Excel.Worksheet
        Dim excelsheets4 As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add

        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)
        excelsheets2 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets3 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets4 = CType(excelbooks.Sheets.Add, Excel.Worksheet)
        excelsheets3.Name = "OLD_40HC"
        excelsheets4.Name = "OLD_20GP"
        excelsheets.Name = "40HC"
        excelsheets2.Name = "20GP"






        With excelsheets
            .Rows("1:1").rowheight = 38.5
            .Rows("2:7").rowheight = 22.5
            .Rows("8:500").rowheight = 21
            .Range("A1:Z600").Font.Name = "Tahoma"
            .Range("A1:Z600").Font.Size = 8
            With .Range("J11:M1100")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter

                .HorizontalAlignment = Excel.Constants.xlCenter
            End With

            .Range("A1").ColumnWidth = 3
            .Range("B1").ColumnWidth = 13
            .Range("C1").ColumnWidth = 8.38
            .Range("D1").ColumnWidth = 10.5
            .Range("E1").ColumnWidth = 9.75
            .Range("F1").ColumnWidth = 10
            .Range("G1").ColumnWidth = 10.75
            .Range("H1").ColumnWidth = 10.75
            .Range("I1").ColumnWidth = 12.75
            .Range("J1").ColumnWidth = 4
            .Range("K1").ColumnWidth = 4
            .Range("L1").ColumnWidth = 4
            .Range("M1").ColumnWidth = 4
            .Range("N1").ColumnWidth = 16.75
            .Range("O1").ColumnWidth = 19.63
            .Range("P1").ColumnWidth = 11.88

            For J = 7 To 10
                .Range("B2:E2").Borders(J).Weight = 2 ' xlThin
                .Range("F2:F2").Borders(J).Weight = 2 ' xlThin
                .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                .Range("G3:G3").Borders(J).Weight = 2 ' xlThin
                .Range("B3:E3").Borders(J).Weight = 2 ' xlThin
                .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                .Range("B4:E4").Borders(J).Weight = 2 ' xlThin
                .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                .Range("B5:E5").Borders(J).Weight = 2 ' xlThin
                .Range("F5:F5").Borders(J).Weight = 2 ' xlThin
                .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                .Range("F6:F6").Borders(J).Weight = 2 ' xlThin
                .Range("B7:E7").Borders(J).Weight = 2 ' xlThin
                .Range("F7:F7").Borders(J).Weight = 2 ' xlThin
                .Range("A9:A10").Borders(J).Weight = 2 ' xlThin


                .Range("B9:B9").Borders(J).Weight = 2 ' xlThin
                .Range("B10:B10").Borders(J).Weight = 2 ' xlThin
                .Range("C9:D9").Borders(J).Weight = 2 ' xlThin
                .Range("C10:C10").Borders(J).Weight = 2 ' xlThin
                .Range("D10:D10").Borders(J).Weight = 2 ' xlThin


                .Range("E9:F9").Borders(J).Weight = 2 ' xlThin
                .Range("E10:E10").Borders(J).Weight = 2 ' xlThin
                .Range("F10:F10").Borders(J).Weight = 2 ' xlThin



                .Range("G9:I9").Borders(J).Weight = 2 ' xlThin
                .Range("G10:G10").Borders(J).Weight = 2 ' xlThin
                .Range("H10:H10").Borders(J).Weight = 2 ' xlThin
                .Range("I10:I10").Borders(J).Weight = 2 ' xlThin




                .Range("J9:O9").Borders(J).Weight = 2 ' xlThin
                .Range("J10:J10").Borders(J).Weight = 2 ' xlThin
                .Range("K10:K10").Borders(J).Weight = 2 ' xlThin
                .Range("L10:L10").Borders(J).Weight = 2 ' xlThin


                .Range("M10:M10").Borders(J).Weight = 2 ' xlThin
                .Range("N10:N10").Borders(J).Weight = 2 ' xlThin
                .Range("O10:O10").Borders(J).Weight = 2 ' xlThin

                .Range("P9:P10").Borders(J).Weight = 2 ' xlThin


            Next


            With .Range("B1:E1")
                .Merge()
                .Value = "Port Daily Reporting 40'HQs"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With
            With .Range("B2:E2")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("G2:G2")
                .Merge()
                .Value = "seals"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("F2:F2")
                .Merge()
                .Value = Date.Now.ToString("dd/MM/yyyy")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B3:E3")
                .Merge()
                .Value = "EMPTY"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F3:F3")
                .Merge()
                .Value = "=SUM(J11:J5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With


            With .Range("B4:E4")
                .Merge()
                .Value = "RELEASE MT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F4:F4")
                .Merge()
                .Value = "=SUM(K11:K5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B5:E5")
                .Merge()
                .Value = "FCL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F5:F5")
                .Merge()
                .Value = "=SUM(L11:L5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B6:E6")
                .Merge()
                .Value = "SHIPPED"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F6:F6")
                .Merge()
                .Value = "=SUM(M11:M5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B7:E7")
                .Merge()
                .Value = "TOTAL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F7:F7")
                .Merge()
                .Value = "=SUM(F3:F6)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("A9:A10")
                .Merge()
                .Value = "NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B9:B9")
                .Merge()
                .Value = "CONTAINER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B10:B10")
                .Merge()
                .Value = "NUMBER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C9:D9")
                .Merge()
                .Value = "ARRIVED KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C10:C10")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("D10:D10")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E9:F9")
                .Merge()
                .Value = "LEFT KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E10:E10")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("F10:F10")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G9:I9")
                .Merge()
                .Value = "YARD CONTROL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G10:G10")
                .Merge()
                .Value = "DATE OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("H10:H10")
                .Merge()
                .Value = "DATE IN"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("I10:I10")
                .Merge()
                .Value = "DETAILS IF ANY"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("J9:O9")
                .Merge()
                .Value = "CURRENT STATUS"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("J10:J10")
                .Merge()
                .Value = "MT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("K10:K10")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("L10:L10")
                .Merge()
                .Value = "FCL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("M10:M10")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("N10:N10")
                .Merge()
                .Value = "FORWARDER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("O10:O10")
                .Merge()
                .Value = "SHIPPER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("P9:P10")
                .Merge()
                .Value = "BOOKING NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            Dim str As String = "40'HC"
            Dim sql As String
            Dim dataTable1 As New DataTable
            Dim datetime As Date = DateEdit2.EditValue
            sql = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO  from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%YANG MING%'   ) as ctnmain  left join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   left  join  borrow on ctnmain.ctnmainid = borrow.ctnid  left join booking on booking.BOOKINGID = borrow.BOOKID    ) as b  left join voyage on b.CTNVOYN =voyage.VOYAGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"


            connectDB.GetTable(sql, dataTable1)



            For i As Integer = 0 To dataTable1.Rows.Count - 1


                Try
                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin

                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With


                    countcontainer += 1
                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dataTable1.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With



                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = "N" + dataTable1.Rows(i)("VOYNAME")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("VOYDATEEN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dataTable1.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = "S" + Format(dataTable1.Rows(i)("VOYAGEID"), "000")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("CTNDATEOUT") Is DBNull.Value Then
                        Else
                            .NumberFormat = "@"
                            .Value = dataTable1.Rows(i)("CTNDATEOUT")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("N" + count.ToString + ":N" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BFORWARDERNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BFORWARDERNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("O" + count.ToString + ":O" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNSHIPNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("P" + count.ToString + ":P" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNO") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNO")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                If dataTable1.Rows(i)("CTNSTAT") = "1" Or dataTable1.Rows(i)("CTNSTAT") = "5" Then
                    With .Range("J" + count.ToString + ":J" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "2" Then
                    With .Range("K" + count.ToString + ":K" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With




                ElseIf dataTable1.Rows(i)("CTNSTAT") = "3" Then

                    With .Range("L" + count.ToString + ":L" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEIN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "4" Then
                    With .Range("M" + count.ToString + ":M" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEOUT")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                End If


                count = count + 1


            Next

            dataTable1 = New DataTable
            sql = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO  from  ( SELECT * from ctnmain WHERE  CTNSTAT > 1 and CTNSTAT < 4  and CTNSIZE ='" & MySqlHelper.EscapeString(str) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%YANG MING%'   ) as ctnmain  left join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   left  join  borrow on ctnmain.ctnmainid = borrow.ctnid  left join booking on booking.BOOKINGID = borrow.BOOKID    ) as b  left join voyage on b.CTNVOYN =voyage.VOYAGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"


            connectDB.GetTable(sql, dataTable1)

            For i As Integer = 0 To dataTable1.Rows.Count - 1


                Try
                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin

                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With


                    countcontainer += 1
                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dataTable1.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With



                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = "N" + dataTable1.Rows(i)("VOYNAME")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("VOYDATEEN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dataTable1.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = "S" + Format(dataTable1.Rows(i)("VOYAGEID"), "000")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("CTNDATEOUT") Is DBNull.Value Then
                        Else
                            .NumberFormat = "@"
                            .Value = dataTable1.Rows(i)("CTNDATEOUT")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("N" + count.ToString + ":N" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BFORWARDERNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BFORWARDERNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("O" + count.ToString + ":O" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNSHIPNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("P" + count.ToString + ":P" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNO") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNO")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                If dataTable1.Rows(i)("CTNSTAT") = "1" Or dataTable1.Rows(i)("CTNSTAT") = "5" Then
                    With .Range("J" + count.ToString + ":J" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "2" Then
                    With .Range("K" + count.ToString + ":K" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With




                ElseIf dataTable1.Rows(i)("CTNSTAT") = "3" Then

                    With .Range("L" + count.ToString + ":L" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEIN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "4" Then
                    With .Range("M" + count.ToString + ":M" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEOUT")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                End If


                count = count + 1


            Next



            dataTable1 = New DataTable
            sql = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO  from  ( SELECT * from ctnmain WHERE    CTNSTAT = 1  and CTNSIZE ='" & MySqlHelper.EscapeString(str) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%YANG MING%'   ) as ctnmain  left join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   left  join  borrow on ctnmain.ctnmainid = borrow.ctnid  left join booking on booking.BOOKINGID = borrow.BOOKID    ) as b  left join voyage on b.CTNVOYN =voyage.VOYAGEID    order by  CTNSTAT DESC ,CTNVOYN ASC,BNSHIPNAME ASC;"


            connectDB.GetTable(sql, dataTable1)

            For i As Integer = 0 To dataTable1.Rows.Count - 1


                Try
                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin

                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With


                    countcontainer += 1
                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dataTable1.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With



                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = "N" + dataTable1.Rows(i)("VOYNAME")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("VOYDATEEN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dataTable1.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = "S" + Format(dataTable1.Rows(i)("VOYAGEID"), "000")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("CTNDATEOUT") Is DBNull.Value Then
                        Else
                            .NumberFormat = "@"
                            .Value = dataTable1.Rows(i)("CTNDATEOUT")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("N" + count.ToString + ":N" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BFORWARDERNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BFORWARDERNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("O" + count.ToString + ":O" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNSHIPNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("P" + count.ToString + ":P" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNO") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNO")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                If dataTable1.Rows(i)("CTNSTAT") = "1" Or dataTable1.Rows(i)("CTNSTAT") = "5" Then
                    With .Range("J" + count.ToString + ":J" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "2" Then
                    With .Range("K" + count.ToString + ":K" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With




                ElseIf dataTable1.Rows(i)("CTNSTAT") = "3" Then

                    With .Range("L" + count.ToString + ":L" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEIN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "4" Then
                    With .Range("M" + count.ToString + ":M" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEOUT")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                End If


                count = count + 1


            Next


        End With

        With excelsheets2
            .Rows("1:1").rowheight = 38.5
            .Rows("2:7").rowheight = 22.5
            .Rows("8:500").rowheight = 21
            .Range("A1:Z600").Font.Name = "Tahoma"
            .Range("A1:Z600").Font.Size = 8
            With .Range("J11:M1100")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter

                .HorizontalAlignment = Excel.Constants.xlCenter
            End With

            .Range("A1").ColumnWidth = 3
            .Range("B1").ColumnWidth = 13
            .Range("C1").ColumnWidth = 8.38
            .Range("D1").ColumnWidth = 10.5
            .Range("E1").ColumnWidth = 9.75
            .Range("F1").ColumnWidth = 10
            .Range("G1").ColumnWidth = 10.75
            .Range("H1").ColumnWidth = 10.75
            .Range("I1").ColumnWidth = 12.75
            .Range("J1").ColumnWidth = 4
            .Range("K1").ColumnWidth = 4
            .Range("L1").ColumnWidth = 4
            .Range("M1").ColumnWidth = 4
            .Range("N1").ColumnWidth = 16.75
            .Range("O1").ColumnWidth = 19.63
            .Range("P1").ColumnWidth = 11.88

            For J = 7 To 10
                .Range("B2:E2").Borders(J).Weight = 2 ' xlThin
                .Range("F2:F2").Borders(J).Weight = 2 ' xlThin
                .Range("G2:G2").Borders(J).Weight = 2 ' xlThin
                .Range("G3:G3").Borders(J).Weight = 2 ' xlThin
                .Range("B3:E3").Borders(J).Weight = 2 ' xlThin
                .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                .Range("B4:E4").Borders(J).Weight = 2 ' xlThin
                .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                .Range("B5:E5").Borders(J).Weight = 2 ' xlThin
                .Range("F5:F5").Borders(J).Weight = 2 ' xlThin
                .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                .Range("F6:F6").Borders(J).Weight = 2 ' xlThin
                .Range("B7:E7").Borders(J).Weight = 2 ' xlThin
                .Range("F7:F7").Borders(J).Weight = 2 ' xlThin
                .Range("A9:A10").Borders(J).Weight = 2 ' xlThin


                .Range("B9:B9").Borders(J).Weight = 2 ' xlThin
                .Range("B10:B10").Borders(J).Weight = 2 ' xlThin
                .Range("C9:D9").Borders(J).Weight = 2 ' xlThin
                .Range("C10:C10").Borders(J).Weight = 2 ' xlThin
                .Range("D10:D10").Borders(J).Weight = 2 ' xlThin


                .Range("E9:F9").Borders(J).Weight = 2 ' xlThin
                .Range("E10:E10").Borders(J).Weight = 2 ' xlThin
                .Range("F10:F10").Borders(J).Weight = 2 ' xlThin



                .Range("G9:I9").Borders(J).Weight = 2 ' xlThin
                .Range("G10:G10").Borders(J).Weight = 2 ' xlThin
                .Range("H10:H10").Borders(J).Weight = 2 ' xlThin
                .Range("I10:I10").Borders(J).Weight = 2 ' xlThin




                .Range("J9:O9").Borders(J).Weight = 2 ' xlThin
                .Range("J10:J10").Borders(J).Weight = 2 ' xlThin
                .Range("K10:K10").Borders(J).Weight = 2 ' xlThin
                .Range("L10:L10").Borders(J).Weight = 2 ' xlThin


                .Range("M10:M10").Borders(J).Weight = 2 ' xlThin
                .Range("N10:N10").Borders(J).Weight = 2 ' xlThin
                .Range("O10:O10").Borders(J).Weight = 2 ' xlThin

                .Range("P9:P10").Borders(J).Weight = 2 ' xlThin


            Next


            With .Range("B1:E1")
                .Merge()
                .Value = "Port Daily Reporting 20'GPs"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With
            With .Range("B2:E2")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("G2:G2")
                .Merge()
                .Value = "seals"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("F2:F2")
                .Merge()
                .Value = Date.Now.ToString("dd/MM/yyyy")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B3:E3")
                .Merge()
                .Value = "EMPTY"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F3:F3")
                .Merge()
                .Value = "=SUM(J11:J5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With


            With .Range("B4:E4")
                .Merge()
                .Value = "RELEASE MT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F4:F4")
                .Merge()
                .Value = "=SUM(K11:K5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B5:E5")
                .Merge()
                .Value = "FCL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F5:F5")
                .Merge()
                .Value = "=SUM(L11:L5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B6:E6")
                .Merge()
                .Value = "SHIPPED"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F6:F6")
                .Merge()
                .Value = "=SUM(M11:M5101)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B7:E7")
                .Merge()
                .Value = "TOTAL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlLeft
                .Font.Bold = True
            End With
            With .Range("F7:F7")
                .Merge()
                .Value = "=SUM(F3:F6)"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("A9:A10")
                .Merge()
                .Value = "NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B9:B9")
                .Merge()
                .Value = "CONTAINER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B10:B10")
                .Merge()
                .Value = "NUMBER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C9:D9")
                .Merge()
                .Value = "ARRIVED KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C10:C10")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("D10:D10")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E9:F9")
                .Merge()
                .Value = "LEFT KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E10:E10")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("F10:F10")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G9:I9")
                .Merge()
                .Value = "YARD CONTROL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G10:G10")
                .Merge()
                .Value = "DATE OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("H10:H10")
                .Merge()
                .Value = "DATE IN"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("I10:I10")
                .Merge()
                .Value = "DETAILS IF ANY"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("J9:O9")
                .Merge()
                .Value = "CURRENT STATUS"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("J10:J10")
                .Merge()
                .Value = "MT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("K10:K10")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("L10:L10")
                .Merge()
                .Value = "FCL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("M10:M10")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("N10:N10")
                .Merge()
                .Value = "FORWARDER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("O10:O10")
                .Merge()
                .Value = "SHIPPER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("P9:P10")
                .Merge()
                .Value = "BOOKING NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            Dim str As String = "20'GP"
            Dim sql As String
            Dim dataTable1 As New DataTable
            Dim datetime As Date = DateEdit2.EditValue
            sql = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO  from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%YANG MING%'   ) as ctnmain  left join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   left  join  borrow on ctnmain.ctnmainid = borrow.ctnid  left join booking on booking.BOOKINGID = borrow.BOOKID    ) as b  left join voyage on b.CTNVOYN =voyage.VOYAGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"


            connectDB.GetTable(sql, dataTable1)



            For i As Integer = 0 To dataTable1.Rows.Count - 1


                Try
                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin

                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With


                    countcontainer += 1
                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dataTable1.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With



                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = "N" + dataTable1.Rows(i)("VOYNAME")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("VOYDATEEN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dataTable1.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = "S" + Format(dataTable1.Rows(i)("VOYAGEID"), "000")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("CTNDATEOUT") Is DBNull.Value Then
                        Else
                            .NumberFormat = "@"
                            .Value = dataTable1.Rows(i)("CTNDATEOUT")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("N" + count.ToString + ":N" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BFORWARDERNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BFORWARDERNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("O" + count.ToString + ":O" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNSHIPNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("P" + count.ToString + ":P" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNO") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNO")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                If dataTable1.Rows(i)("CTNSTAT") = "1" Or dataTable1.Rows(i)("CTNSTAT") = "5" Then
                    With .Range("J" + count.ToString + ":J" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "2" Then
                    With .Range("K" + count.ToString + ":K" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With




                ElseIf dataTable1.Rows(i)("CTNSTAT") = "3" Then

                    With .Range("L" + count.ToString + ":L" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEIN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "4" Then
                    With .Range("M" + count.ToString + ":M" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEOUT")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                End If


                count = count + 1


            Next

            dataTable1 = New DataTable
            sql = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO  from  ( SELECT * from ctnmain WHERE  CTNSTAT > 1 and CTNSTAT < 4  and CTNSIZE ='" & MySqlHelper.EscapeString(str) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%YANG MING%'   ) as ctnmain  left join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   left  join  borrow on ctnmain.ctnmainid = borrow.ctnid  left join booking on booking.BOOKINGID = borrow.BOOKID    ) as b  left join voyage on b.CTNVOYN =voyage.VOYAGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"


            connectDB.GetTable(sql, dataTable1)

            For i As Integer = 0 To dataTable1.Rows.Count - 1


                Try
                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin

                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With


                    countcontainer += 1
                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dataTable1.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With



                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = "N" + dataTable1.Rows(i)("VOYNAME")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("VOYDATEEN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dataTable1.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = "S" + Format(dataTable1.Rows(i)("VOYAGEID"), "000")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("CTNDATEOUT") Is DBNull.Value Then
                        Else
                            .NumberFormat = "@"
                            .Value = dataTable1.Rows(i)("CTNDATEOUT")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("N" + count.ToString + ":N" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BFORWARDERNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BFORWARDERNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("O" + count.ToString + ":O" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNSHIPNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("P" + count.ToString + ":P" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNO") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNO")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                If dataTable1.Rows(i)("CTNSTAT") = "1" Or dataTable1.Rows(i)("CTNSTAT") = "5" Then
                    With .Range("J" + count.ToString + ":J" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "2" Then
                    With .Range("K" + count.ToString + ":K" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With




                ElseIf dataTable1.Rows(i)("CTNSTAT") = "3" Then

                    With .Range("L" + count.ToString + ":L" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEIN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "4" Then
                    With .Range("M" + count.ToString + ":M" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEOUT")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                End If


                count = count + 1


            Next



            dataTable1 = New DataTable
            sql = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO  from  ( SELECT * from ctnmain WHERE    CTNSTAT =1  and CTNSIZE ='" & MySqlHelper.EscapeString(str) & "' and CTNDATEOUT = '" & datetime.ToString("dd-MM-") & datetime.Year & "'  and  CTNAGENT like '%YANG MING%'   ) as ctnmain  left join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID   left  join  borrow on ctnmain.ctnmainid = borrow.ctnid  left join booking on booking.BOOKINGID = borrow.BOOKID    ) as b  left join voyage on b.CTNVOYN =voyage.VOYAGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"


            connectDB.GetTable(sql, dataTable1)

            For i As Integer = 0 To dataTable1.Rows.Count - 1


                Try
                    For J = 7 To 10
                        .Range("A" + count.ToString + ":A" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("B" + count.ToString + ":B" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("C" + count.ToString + ":C" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("D" + count.ToString + ":D" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("E" + count.ToString + ":E" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("F" + count.ToString + ":F" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("G" + count.ToString + ":G" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("H" + count.ToString + ":H" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("I" + count.ToString + ":I" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("J" + count.ToString + ":J" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("K" + count.ToString + ":K" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("L" + count.ToString + ":L" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("M" + count.ToString + ":M" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("N" + count.ToString + ":N" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("O" + count.ToString + ":O" + count.ToString).Borders(J).Weight = 2 ' xlThin
                        .Range("P" + count.ToString + ":P" + count.ToString).Borders(J).Weight = 2 ' xlThin

                    Next



                    With .Range("A" + count.ToString + ":A" + count.ToString)
                        .Merge()
                        .Value = countcontainer.ToString
                        .NumberFormat = "@"
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With


                    countcontainer += 1
                    With .Range("B" + count.ToString + ":B" + count.ToString)
                        .Merge()
                        .Value = dataTable1.Rows(i)("CTNSTRING")
                        .HorizontalAlignment = Excel.Constants.xlCenter
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                    End With



                    With .Range("C" + count.ToString + ":C" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = "N" + dataTable1.Rows(i)("VOYNAME")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("D" + count.ToString + ":D" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("VOYDATEEN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("E" + count.ToString + ":E" + count.ToString)
                        .Merge()
                        .NumberFormat = "@"
                        If dataTable1.Rows(i)("VOYAGEID") Is DBNull.Value Then
                        Else
                            .Value = "S" + Format(dataTable1.Rows(i)("VOYAGEID"), "000")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("CTNDATEOUT") Is DBNull.Value Then
                        Else
                            .NumberFormat = "@"
                            .Value = dataTable1.Rows(i)("CTNDATEOUT")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("N" + count.ToString + ":N" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BFORWARDERNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BFORWARDERNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("O" + count.ToString + ":O" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNSHIPNAME") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNSHIPNAME")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("P" + count.ToString + ":P" + count.ToString)
                        .Merge()
                        If dataTable1.Rows(i)("BNO") Is DBNull.Value Then
                        Else
                            .Value = dataTable1.Rows(i)("BNO")
                        End If
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try


                If dataTable1.Rows(i)("CTNSTAT") = "1" Or dataTable1.Rows(i)("CTNSTAT") = "5" Then
                    With .Range("J" + count.ToString + ":J" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "2" Then
                    With .Range("K" + count.ToString + ":K" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With




                ElseIf dataTable1.Rows(i)("CTNSTAT") = "3" Then

                    With .Range("L" + count.ToString + ":L" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEIN")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                ElseIf dataTable1.Rows(i)("CTNSTAT") = "4" Then
                    With .Range("M" + count.ToString + ":M" + count.ToString)
                        .NumberFormat = "@"
                        .Value = 1
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("G" + count.ToString + ":G" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                    With .Range("H" + count.ToString + ":H" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATE")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With
                    With .Range("F" + count.ToString + ":F" + count.ToString)
                        .NumberFormat = "@"
                        .Value = dataTable1.Rows(i)("TIMEDATEOUT")
                        .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                        .HorizontalAlignment = Excel.Constants.xlCenter
                    End With

                End If


                count = count + 1


            Next


        End With

        With excelsheets3
            Dim datetime As Date = DateEdit2.EditValue
            Dim dt As New DataTable
            Dim str4 As String = "40'HC"
            Dim sql As String = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str4) & "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "' and  CTNAGENT like '%YANG MING%'   ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID  join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like '%YANG MING%' and  rpttype ='45G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"
            dt = connectDB.GetTable(sql)
            Dim objArray(dt.Rows.Count + 1 - 1, 19) As Object
            Dim empty As String = String.Empty
            Dim str6 As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            Dim length As Integer = str6.Length
            Dim num11 As Integer = 1



            With .Range("A3:P" & dt.Rows.Count + 4)
                .NumberFormat = "@"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                .Borders.Weight = 2
                .Font.Size = 8
                .Font.Name = "Tahoma"
                .RowHeight = 21
            End With

            Dim num12 As Integer = 0
            Do
                Dim num13 As Integer = dt.Rows.Count - 1
                Dim num14 As Integer = 0
                Do
                    If (num12 = 0) Then
                        num11 = num11 + 1
                        objArray(num14 + 1, num12) = num14 + 1
                    ElseIf (num12 = 1) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("CTNSTRING").ToString()
                    ElseIf (num12 = 2) Then
                        objArray(num14 + 1, num12) = String.Concat("N", dt.Rows(num14)("CTNVOYN").ToString())
                    ElseIf (num12 = 3) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("VOYDATEEN").ToString()
                    ElseIf (num12 = 4) Then
                        objArray(num14 + 1, num12) = String.Concat("S", dt.Rows(num14)("VOYAGEIDS").ToString())
                    ElseIf (num12 = 5) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("CTNDATEOUT").ToString()
                    ElseIf (num12 = 6) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("TIMEDATE").ToString()
                    ElseIf (num12 = 7) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("TIMEDATEIN").ToString()
                    ElseIf (num12 = 15) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("BNO").ToString()
                    ElseIf (num12 = 14) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("BNSHIPNAME").ToString()
                    ElseIf (num12 = 12) Then
                        objArray(num14 + 1, num12) = 1
                    ElseIf (num12 = 13) Then
                        objArray(num14 + 1, num12) = dt.Rows(num14)("BNFORWARDERNAME").ToString()
                    End If
                    num14 = num14 + 1
                Loop While num14 <= num13
                num12 = num12 + 1
            Loop While num12 <= 16
            If (dt.Columns.Count > length) Then
                empty = str6.Substring(19 / length - 1, 1)
            End If
            empty = String.Concat(empty, str6.Substring(19 Mod length, 1))
            Dim str7 As String = String.Format("A4:{0}{1}", empty, dt.Rows.Count + 4)



            .Rows("1:1").rowheight = 38.5
            .Rows("2:7").rowheight = 22.5
            .Rows("8:500").rowheight = 21
            .Range("A1:Z600").Font.Name = "Tahoma"
            .Range("A1:Z600").Font.Size = 8
            With .Range("J11:M1100")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter

                .HorizontalAlignment = Excel.Constants.xlCenter
            End With

            .Range("A1").ColumnWidth = 3
            .Range("B1").ColumnWidth = 13
            .Range("C1").ColumnWidth = 8.38
            .Range("D1").ColumnWidth = 10.5
            .Range("E1").ColumnWidth = 9.75
            .Range("F1").ColumnWidth = 10
            .Range("G1").ColumnWidth = 10.75
            .Range("H1").ColumnWidth = 10.75
            .Range("I1").ColumnWidth = 12.75
            .Range("J1").ColumnWidth = 4
            .Range("K1").ColumnWidth = 4
            .Range("L1").ColumnWidth = 4
            .Range("M1").ColumnWidth = 4
            .Range("N1").ColumnWidth = 16.75
            .Range("O1").ColumnWidth = 19.63
            .Range("P1").ColumnWidth = 11.88

            For J = 7 To 10
                .Range("A3:A4").Borders(J).Weight = 2 ' xlThin
                .Range("B3:B3").Borders(J).Weight = 2 ' xlThin
                .Range("B4:B4").Borders(J).Weight = 2 ' xlThin
                .Range("C3:D3").Borders(J).Weight = 2 ' xlThin
                .Range("C4:C4").Borders(J).Weight = 2 ' xlThin
                .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                .Range("D4:D4").Borders(J).Weight = 2 ' xlThin
                .Range("E3:F3").Borders(J).Weight = 2 ' xlThin
                .Range("E4:E4").Borders(J).Weight = 2 ' xlThin
                .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                .Range("G4:G4").Borders(J).Weight = 2 ' xlThin
                .Range("H4:H4").Borders(J).Weight = 2 ' xlThin
                .Range("I4:I4").Borders(J).Weight = 2 ' xlThin
                .Range("J3:O3").Borders(J).Weight = 2 ' xlThin


                .Range("J4:J4").Borders(J).Weight = 2 ' xlThin
                .Range("K4:K4").Borders(J).Weight = 2 ' xlThin
                .Range("L4:L4").Borders(J).Weight = 2 ' xlThin
                .Range("M4:M4").Borders(J).Weight = 2 ' xlThin
                .Range("N4:N4").Borders(J).Weight = 2 ' xlThin


                .Range("O4:O4").Borders(J).Weight = 2 ' xlThin
                .Range("P3:P4").Borders(J).Weight = 2 ' xlThin



            Next


            With .Range("B1:G1")
                .Merge()
                .Value = "Port Daily Reporting 20'GPs"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With

            With .Range("A3:A4")
                .Merge()
                .Value = "NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B3:B3")
                .Merge()
                .Value = "CONTAINER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B4:B4")
                .Merge()
                .Value = "NUMBER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C3:D3")
                .Merge()
                .Value = "ARRIVED KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C4:C4")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("D4:D4")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E3:F3")
                .Merge()
                .Value = "LEFT KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E4:E4")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("F4:F4")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G3:I3")
                .Merge()
                .Value = "YARD CONTROL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G4:G4")
                .Merge()
                .Value = "DATE OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("H4:H4")
                .Merge()
                .Value = "DATE IN"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("I4:I4")
                .Merge()
                .Value = "DETAILS IF ANY"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("J3:O3")
                .Merge()
                .Value = "CURRENT STATUS"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("J4:J4")
                .Merge()
                .Value = "MT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("K4:K4")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("L4:L4")
                .Merge()
                .Value = "FCL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("M4:M4")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("N4:N4")
                .Merge()
                .Value = "FORWARDER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("O4:O4")
                .Merge()
                .Value = "SHIPPER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("P3:P4")
                .Merge()
                .Value = "BOOKING NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With





        End With

        With excelsheets4
            Dim datetime As Date = DateEdit2.EditValue
            Dim dt As New DataTable
            Dim str4 As String = "20'GP"
            Dim sql As String = "SELECT CTNSTRING,  VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,   VOYDATEENN , voyage.VOYDATEEN   , BFORWARDERNAME,BNO,BNFORWARDERNAME from (   Select   CTNSTRING,VOYAGEID AS VOYAGEIDS ,CTNMAINID ,CTNVOYN , CTNVOYS , CTNSHIPNAME as CTNSHIPNAMES,CTNSTAT,CTNDATEOUT ,BNSHIPNAME,TIMEDATE,TIMEDATEIN,  VOYDATEEN as VOYDATEENN  , BFORWARDERNAME,BNFORWARDERNAME , BNO from  ( SELECT * from ctnmain WHERE CTNSTAT = 4 and CTNSIZE ='" & MySqlHelper.EscapeString(str4) & "' and CTNDATEOUT <> '" & datetime.ToString("dd-MM-") & datetime.Year & "' and  CTNAGENT like '%YANG MING%'   ) as ctnmain join voyage on ctnmain.CTNVOYS = voyage.VOYAGEID    join  borrow on ctnmain.ctnmainid = borrow.ctnid join booking on booking.BOOKINGID = borrow.BOOKID  join  (SELECT DISTINCT rptidctn ,  rptctndate from rptwhl where  rptctndate >= " & datetime.Year & "0100000000 AND rptagent like '%YANG MING%' and  rpttype ='22G1' and rptstat= 'ST' ) as rptwhl  on ctnmain.ctnmainid =  rptwhl.rptidctn  ) as b join voyage on b.CTNVOYN =voyage.VOyaGEID    order by  CTNSTAT DESC ,CTNVOYS ASC,BNSHIPNAME ASC;"
            dt = connectDB.GetTable(sql)
            Dim objArray(dt.Rows.Count + 1 - 1, 19) As Object
            Dim empty As String = String.Empty
            Dim str6 As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            Dim length As Integer = str6.Length
            Dim num11 As Integer = 1



            With .Range("A3:P" & dt.Rows.Count + 4)
                .NumberFormat = "@"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
                .Borders.Weight = 2
                .Font.Size = 8
                .Font.Name = "Tahoma"
                .RowHeight = 21
            End With

            If dt.Rows.Count > 0 Then

                Dim num12 As Integer = 0
                Do
                    Dim num13 As Integer = dt.Rows.Count - 1
                    Dim num14 As Integer = 0
                    Do
                        If (num12 = 0) Then
                            num11 = num11 + 1
                            objArray(num14 + 1, num12) = num14 + 1
                        ElseIf (num12 = 1) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("CTNSTRING").ToString()
                        ElseIf (num12 = 2) Then
                            objArray(num14 + 1, num12) = String.Concat("N", dt.Rows(num14)("CTNVOYN").ToString())
                        ElseIf (num12 = 3) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("VOYDATEEN").ToString()
                        ElseIf (num12 = 4) Then
                            objArray(num14 + 1, num12) = String.Concat("S", dt.Rows(num14)("VOYAGEIDS").ToString())
                        ElseIf (num12 = 5) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("CTNDATEOUT").ToString()
                        ElseIf (num12 = 6) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("TIMEDATE").ToString()
                        ElseIf (num12 = 7) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("TIMEDATEIN").ToString()
                        ElseIf (num12 = 15) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("BNO").ToString()
                        ElseIf (num12 = 14) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("BNSHIPNAME").ToString()
                        ElseIf (num12 = 12) Then
                            objArray(num14 + 1, num12) = 1
                        ElseIf (num12 = 13) Then
                            objArray(num14 + 1, num12) = dt.Rows(num14)("BNFORWARDERNAME").ToString()
                        End If
                        num14 = num14 + 1
                    Loop While num14 <= num13
                    num12 = num12 + 1
                Loop While num12 <= 16
                If (dt.Columns.Count > length) Then
                    empty = str6.Substring(19 / length - 1, 1)
                End If
                empty = String.Concat(empty, str6.Substring(19 Mod length, 1))
                Dim str7 As String = String.Format("A4:{0}{1}", empty, dt.Rows.Count + 4)
            End If



            .Rows("1:1").rowheight = 38.5
            .Rows("2:7").rowheight = 22.5
            .Rows("8:500").rowheight = 21
            .Range("A1:Z600").Font.Name = "Tahoma"
            .Range("A1:Z600").Font.Size = 8
            With .Range("J11:M1100")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter

                .HorizontalAlignment = Excel.Constants.xlCenter
            End With

            .Range("A1").ColumnWidth = 3
            .Range("B1").ColumnWidth = 13
            .Range("C1").ColumnWidth = 8.38
            .Range("D1").ColumnWidth = 10.5
            .Range("E1").ColumnWidth = 9.75
            .Range("F1").ColumnWidth = 10
            .Range("G1").ColumnWidth = 10.75
            .Range("H1").ColumnWidth = 10.75
            .Range("I1").ColumnWidth = 12.75
            .Range("J1").ColumnWidth = 4
            .Range("K1").ColumnWidth = 4
            .Range("L1").ColumnWidth = 4
            .Range("M1").ColumnWidth = 4
            .Range("N1").ColumnWidth = 16.75
            .Range("O1").ColumnWidth = 19.63
            .Range("P1").ColumnWidth = 11.88

            For J = 7 To 10
                .Range("A3:A4").Borders(J).Weight = 2 ' xlThin
                .Range("B3:B3").Borders(J).Weight = 2 ' xlThin
                .Range("B4:B4").Borders(J).Weight = 2 ' xlThin
                .Range("C3:D3").Borders(J).Weight = 2 ' xlThin
                .Range("C4:C4").Borders(J).Weight = 2 ' xlThin
                .Range("F3:F3").Borders(J).Weight = 2 ' xlThin

                .Range("D4:D4").Borders(J).Weight = 2 ' xlThin
                .Range("E3:F3").Borders(J).Weight = 2 ' xlThin
                .Range("E4:E4").Borders(J).Weight = 2 ' xlThin
                .Range("F4:F4").Borders(J).Weight = 2 ' xlThin
                .Range("B6:E6").Borders(J).Weight = 2 ' xlThin
                .Range("G4:G4").Borders(J).Weight = 2 ' xlThin
                .Range("H4:H4").Borders(J).Weight = 2 ' xlThin
                .Range("I4:I4").Borders(J).Weight = 2 ' xlThin
                .Range("J3:O3").Borders(J).Weight = 2 ' xlThin


                .Range("J4:J4").Borders(J).Weight = 2 ' xlThin
                .Range("K4:K4").Borders(J).Weight = 2 ' xlThin
                .Range("L4:L4").Borders(J).Weight = 2 ' xlThin
                .Range("M4:M4").Borders(J).Weight = 2 ' xlThin
                .Range("N4:N4").Borders(J).Weight = 2 ' xlThin


                .Range("O4:O4").Borders(J).Weight = 2 ' xlThin
                .Range("P3:P4").Borders(J).Weight = 2 ' xlThin



            Next


            With .Range("B1:G1")
                .Merge()
                .Value = "Port Daily Reporting 20'GPs"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Size = 9
                .HorizontalAlignment = Excel.Constants.xlCenter
            End With

            With .Range("A3:A4")
                .Merge()
                .Value = "NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B3:B3")
                .Merge()
                .Value = "CONTAINER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("B4:B4")
                .Merge()
                .Value = "NUMBER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C3:D3")
                .Merge()
                .Value = "ARRIVED KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("C4:C4")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("D4:D4")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E3:F3")
                .Merge()
                .Value = "LEFT KANTANG"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("E4:E4")
                .Merge()
                .Value = "VOYAGE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("F4:F4")
                .Merge()
                .Value = "DATE"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G3:I3")
                .Merge()
                .Value = "YARD CONTROL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("G4:G4")
                .Merge()
                .Value = "DATE OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("H4:H4")
                .Merge()
                .Value = "DATE IN"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("I4:I4")
                .Merge()
                .Value = "DETAILS IF ANY"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("J3:O3")
                .Merge()
                .Value = "CURRENT STATUS"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("J4:J4")
                .Merge()
                .Value = "MT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("K4:K4")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("L4:L4")
                .Merge()
                .Value = "FCL"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("M4:M4")
                .Merge()
                .Value = "OUT"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("N4:N4")
                .Merge()
                .Value = "FORWARDER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With
            With .Range("O4:O4")
                .Merge()
                .Value = "SHIPPER"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With

            With .Range("P3:P4")
                .Merge()
                .Value = "BOOKING NO."
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlCenter
                .Font.Bold = True
            End With




        End With

        Try
            excelbooks.SaveAs(pathExcel.ToString + "\" & My.Settings.Nickname & "'S DAILY STATUS REPORT_YM LINES " + Date.Now.ToString("dd-MM-yyyy") + ".xlsx")

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelsheets2 = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelsheets2 = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try


    End Sub

    Private Sub ExcelDailyWHL()
        Dim pathExcel As String
        Dim count As Integer = 1


        pathExcel = FolderBrowserDialog1.SelectedPath
        Dim excelapp As New Excel.Application
        Dim excelbooks As Excel.Workbook
        Dim excelsheets As Excel.Worksheet
        excelbooks = excelapp.Workbooks.Add

        excelsheets = CType(excelbooks.Worksheets(1), Excel.Worksheet)

        With excelsheets

            .Range("A1:S500").Font.Name = "Tahoma"
            .Range("A1:S500").Font.Size = 11
            .Range("A1").ColumnWidth = 12.13
            .Range("B1").ColumnWidth = 2.13
            .Range("C1").ColumnWidth = 14.13
            .Range("D1").ColumnWidth = 7.13
            .Range("E1").ColumnWidth = 4.13
            .Range("F1").ColumnWidth = 14.13
            .Range("G1").ColumnWidth = 5.13
            .Range("H1").ColumnWidth = 5.13
            .Range("I1").ColumnWidth = 10.13
            .Range("J1").ColumnWidth = 10.13
            .Range("K1").ColumnWidth = 12.13
            .Range("L1").ColumnWidth = 7.13
            .Range("M1").ColumnWidth = 14.13
            .Range("N1").ColumnWidth = 7.13
            .Range("O1").ColumnWidth = 18.13
            .Range("P1").ColumnWidth = 5.13
            .Range("Q1").ColumnWidth = 5.13
            .Range("R1").ColumnWidth = 1.13
            .Range("S1").ColumnWidth = 50.13

            With .Range("A1:A1")
                .Merge()
                .Value = "Container No"
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .HorizontalAlignment = Excel.Constants.xlLeft
            End With
            With .Range("A1:S500")
                .VerticalAlignment = Excel.XlVAlign.xlVAlignBottom
            End With
            With .Range("B1:B1")
                .Merge()
                .Value = "Status"
            End With

            With .Range("C1:C1")
                .Merge()
                .Value = "Ctnr Date"

            End With
            With .Range("D1:D1")
                .Merge()
                .Value = "Lessee"
            End With
            With .Range("E1:E1")
                .Merge()
                .Value = "Type"
            End With
            With .Range("F1:F1")
                .Merge()
                .Value = "Ctnr Date Tpe"


            End With
            With .Range("G1:G1")
                .Merge()
                .Value = "Ctnr Place"


            End With

            With .Range("H1:H1")
                .Merge()
                .Value = "Ctnr Depot"


            End With
            With .Range("I1:I1")
                .Merge()
                .Value = "TFC Exp"


            End With
            With .Range("J1:J1")
                .Merge()
                .Value = "TFC Imp"


            End With
            With .Range("K1:K1")
                .Merge()
                .Value = "Chassis"

            End With
            With .Range("L1:L1")
                .Merge()
                .Value = "Chassis"

            End With
            With .Range("M1:M1")
                .Merge()
                .Value = "Trans date"

            End With
            With .Range("N1:N1")
                .Merge()
                .Value = "Entry office"
            End With
            With .Range("O1:O1")
                .Merge()
                .Value = "Book No"
            End With
            With .Range("P1:P1")
                .Merge()
                .Value = "From Place"
            End With
            With .Range("Q1:Q1")
                .Merge()
                .Value = "To place"
            End With
            With .Range("R1:R1")
                .Merge()
                .Value = "Flag"
            End With
            With .Range("S1:S1")
                .Merge()
                .Value = "Remark"
            End With
            Dim date1 As Date = DateEdit3.EditValue
            Dim sql As String
            Dim dt As New DataTable

            sql = "Select * from ( Select * from rptwhl where rptctndate >= '" & date1.Year & date1.ToString("MM") & date1.ToString("dd") & "000000" & "' and  rptctndate <= '" & date1.Year & date1.ToString("MM") & date1.ToString("dd") & "999999'  and rptagent like '%" & slcAgent.EditValue & "%' ) AS rptwhl  LEFT JOIN borrow ON borrow.ctnid  = rptwhl.rptidctn JOIN 
(SELECT CTNMAINID FROM ctnmain WHERE idmascmrunning  = '" & slcDepot.EditValue & "' AND CTNSTRING IN ( Select rptctn from rptwhl where rptctndate >= '" & date1.Year & date1.ToString("MM") & date1.ToString("dd") & "000000" & "' and  rptctndate <= '" & date1.Year & date1.ToString("MM") & date1.ToString("dd") & "999999'  and rptagent like '%" & slcAgent.EditValue & "%'  ) ) AS ctnmain ON ctnmain.CTNMAINID = borrow.CTNID  LEFT JOIN booking ON booking.bookingid = borrow.bookid  order by FIELD( rptstat,'MA','MS','FL','ST') DESC,FIELD(rpttype,'45G1','22G1') DESC ;"

            connectDB.GetTable(sql, dt)

            For i As Integer = 0 To dt.Rows.Count - 1
                count += 1
                With .Range("A" + count.ToString + ":A" + count.ToString)
                    .Merge()
                    .Value = dt.Rows(i)("rptctn")
                    .HorizontalAlignment = Excel.Constants.xlLeft
                    .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                End With
                With .Range("B" + count.ToString + ":B" + count.ToString)
                    .Merge()
                    .Value = dt.Rows(i)("rptstat")

                End With
                With .Range("C" + count.ToString + ":C" + count.ToString)
                    .Merge()
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("rptctndate")
                End With
                With .Range("D" + count.ToString + ":D" + count.ToString)
                    .Merge()
                    .Value = "WHL"
                End With
                With .Range("E" + count.ToString + ":E" + count.ToString)
                    .Merge()
                    .Value = dt.Rows(i)("rpttype")
                End With
                With .Range("F" + count.ToString + ":F" + count.ToString)
                    .Merge()
                    .NumberFormat = "@"
                    .Value = dt.Rows(i)("rptctndate")
                End With
                With .Range("G" + count.ToString + ":G" + count.ToString)
                    .Merge()
                    .Value = "THKAN"
                End With
                With .Range("H" + count.ToString + ":H" + count.ToString)
                    .Merge()
                    .Value = "KAN03"
                End With
                With .Range("N" + count.ToString + ":N" + count.ToString)
                    .Merge()
                    .Value = "THKAN01"
                End With
                With .Range("O" + count.ToString + ":O" + count.ToString)
                    .Merge()
                    .Value = If(dt.Rows(i)("BNO") Is DBNull.Value, "", dt.Rows(i)("BNO"))
                End With
            Next





        End With
        Try
            Dim date1 As Date = DateEdit3.EditValue
            excelbooks.SaveAs(pathExcel.ToString + "\KAN03~03 flat file format_" & slcDepot.EditValue & "_" & date1.Now.ToString("yyyyMMdd") & ".xlsx")

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next
            MsgBox("Report Complete")


        Catch ex As Exception

            excelbooks.Close()
            excelapp.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelapp)
            excelbooks = Nothing
            excelsheets = Nothing
            excelapp = Nothing
            Dim proc As System.Diagnostics.Process

            For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
                proc.Kill()
            Next


        End Try

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try


                Dim t = New Thread(New ThreadStart(AddressOf ExcelDailyYM))
                t.Start()




            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try


                Dim t = New Thread(New ThreadStart(AddressOf ExcelDailyWHL))
                t.Start()




            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
        FolderBrowserDialog1.ShowNewFolderButton = True
        FolderBrowserDialog1.SelectedPath = "C:\"
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try


                Dim t = New Thread(New ThreadStart(AddressOf excelDaily2))
                t.Start()



            Catch ex As Exception

            End Try
        End If

    End Sub
End Class
