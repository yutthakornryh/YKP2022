Imports DevExpress.XtraGrid.Views.Grid
Imports MySql.Data.MySqlClient

Public Class frmadd_gate1
    Dim idvoyage As String
    Dim idbooking As String
    Dim idcontainer As String
    Dim agentline As String
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Dim objCLASS As New ContainerClass(ykpset)
    Public Sub New(ByVal voyageid As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = voyageid
        ' Add any initialization after the InitializeComponent() call.
        port_discharge.Text = My.Settings.Nickname
    End Sub
    Public Sub LoadVoyage()
        objCLASS.loadVoyage()
        slcVoyage.Properties.DataSource = ykpset.Tables("voyage")

    End Sub

    Public Sub clearcode()
        Me.idcontainer = ""
        Me.agentline = ""
        Me.idbooking = ""
        Me.DateTimePicker4.ResetText()
        Me.DateTimePicker1.ResetText()
        Me.txt_booking_no.Text = ""
        txt_container.Text = ""
        Me.txt_customer.Text = ""
        Me.txt_sealid.Text = ""
        Me.TextBox3.Text = ""
        Me.txt_insurance.Text = ""
    End Sub

    Private Sub frmadd_gate1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadCar()
        LoadVoyage()
        slcVoyage.EditValue = idvoyage
    End Sub
    Private s1 As FILTERCLASS
    Public Sub loadCar()
        Dim sql2 As String
        sql2 = "Select idtransport,transportname FROM mastransport"
        s1 = New FILTERCLASS(txt_name_car, sql2, "ID,Transport Name", "25,120", "1,1", "1,1")

        sql2 = "Select idmascarid,caridname FROM mascarid;"
        Me.s1 = New FILTERCLASS(txt_no_car, sql2, "ID,Transport ID", "25,120", "1,1", "1,1")
    End Sub

    Public Sub loadBooking()
        objCLASS.loadBookingAll(idvoyage)
        GridControl2.DataSource = ykpset.Tables("booking")
    End Sub
    Public Sub loadContainer()
        objCLASS.loadContainerByStatus(1)
        GridControl3.DataSource = ykpset.Tables("ctnmain")
    End Sub

    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        If slcVoyage.EditValue Is Nothing Then

            Exit Sub
        Else
            idvoyage = slcVoyage.EditValue
            loadBooking()
            loadContainer()
        End If
    End Sub

    Private Sub GridView2_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView2.RowCellClick

        Me.txt_booking_no.Text = ""
        Me.txt_customer.Text = ""
        Me.txt_booking_no.Text = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BNO").ToString
        Me.txt_customer.Text = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BSHIPNAME").ToString
        Me.idbooking = GridView2.GetDataRow(GridView2.FocusedRowHandle)("BOOKINGID").ToString


    End Sub

    Private Sub GridView3_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView3.RowCellClick
        idcontainer = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNMAINID").ToString
        txt_container.Text = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSTRING").ToString
        TextBox1.Text = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSIZE").ToString
        agentline = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNAGENT").ToString

        If Trim(GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSIZE").ToString) = "40'HC" Then
            TextBox4.Text = "45G1"
        ElseIf Trim(GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSIZE").ToString) = "20'GP" Then
            TextBox4.Text = "22G1"
        End If





    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        'If MySql.State = ConnectionState.Closed Then
        '    MySql.Open()
        'End If
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String
        sql = "Select count(idborrow) as borrow from borrow where bookid = '" & Me.idbooking & "' and ctnid ='" & Me.idcontainer & "' ;"
        Dim dt As New DataTable
        connectDB.GetTable(sql, dt)
        If dt.Rows(0)("borrow") > 0 Then
            MsgBox("�����١��ҹ����", MsgBoxStyle.ApplicationModal, Nothing)
            Exit Sub
        ElseIf idbooking = "" And idcontainer = "" Then
            MsgBox("��سҵ�Ǩ�ͺ Booking ���� Container", MsgBoxStyle.ApplicationModal, Nothing)
            Exit Sub
        End If

        If IsNumeric(idbooking) = False Then
            MsgBox("��سҵ�Ǩ�ͺ Booking ���� Container", MsgBoxStyle.ApplicationModal, Nothing)
            Exit Sub
        End If

        Dim commandText3 As String
        commandText3 = "UPDATE ctnmain Set CTNSTRING  = '" & txt_container.Text & "',CTNSTAT = '1',CTNSEALID = '" & txt_sealid.Text & "',CTNWEIGHT ='" & TextBox3.Text & "',CTNVOYS = '" & idvoyage & "',CTNINS ='" & txt_insurance.Text & "' where CTNMAINID = '" & idcontainer & "'; "

        connectDB.ExecuteNonQuery(commandText3)
        'Try
        '    mySqlCommand.CommandText = commandText3
        '    mySqlCommand.CommandType = CommandType.Text
        '    mySqlCommand.Connection = MySql

        '    mySqlCommand.ExecuteNonQuery()

        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try
        'MySql.Close()





        sql1 = "CTNID , "
        sql2 = " '" & idcontainer & "' , "

        sql1 += " BOOKID , "
        sql2 += " '" & idbooking & "' , "


        sql1 += " COMNAME , "
        sql2 += " '" & txt_name_car.Text & "' , "

        sql1 += " CARID , "
        sql2 += " '" & txt_no_car.Text & "' , "

        sql1 += " TIMEHHMM , "
        sql2 += " '" & DateTimePicker4.Text & "' , "

        sql1 += " TIMEDATE , "
        sql2 += " '" & DateTimePicker2.Value.Date.ToString("dd-MM-yyyy") & "' , "

        sql1 += " INSURANCE , "
        sql2 += " '" & MySqlHelper.EscapeString(txt_insurance.Text) & "' , "


        sql1 += " TIMEDATEIN , "
        sql2 += " '" & DateTimePicker3.Value.Date.ToString("dd-MM-yyyy") & "' , "


        sql1 += " TIMEHHMMIN , "
        sql2 += " '" & DateTimePicker1.Text & "' , "

        sql1 += " BCTNTYPETHAI , "
        sql2 += " '" & MySqlHelper.EscapeString(TextBox1.Text) & "' , "

        sql1 += " BCTNTYPEPAE , "
        sql2 += " '" & MySqlHelper.EscapeString(TextBox4.Text) & "' , "

        sql1 += " BRCHECK , "
        sql2 += " '0' , "

        sql1 += " BRCHECK1 "
        sql2 += " '0'  "

        sql = "INSERT INTO borrow ( " & sql1 & ") VALUES ( " & sql2 & " );"
        connectDB.ExecuteNonQuery(sql)



        'If MySql.State = ConnectionState.Closed Then
        '    MySql.Open()
        'End If
        'Try
        '    mySqlCommand.Parameters.Clear()
        '    mySqlCommand.CommandText = "insert into borrow ( CTNID, BOOKID, COMNAME, CARID,TIMEHHMM,TIMEDATE,INSURANCE,TIMEDATEIN,TIMEHHMMIN,BCTNTYPETHAI,BCTNTYPEPAE,BRCHECK,BRCHECK1) values (@CTNID,@BOOKID,@COMNAME,@CARID,@TIMEHHMM,@TIMEDATE,@INSURANCE,@TIMEDATEIN,@TIMEHHMMIN,@BCTNTYPETHAI,@BCTNTYPEPAE,@CHECK,@CHECK1)"
        '    mySqlCommand.Connection = MySql
        '    mySqlCommand.Parameters.AddWithValue("@CTNID", idcontainer)
        '    mySqlCommand.Parameters.AddWithValue("@BOOKID", idbooking)
        '    mySqlCommand.Parameters.AddWithValue("@COMNAME", txt_name_car.Text)
        '    mySqlCommand.Parameters.AddWithValue("@CARID", txt_no_car.Text)

        '    mySqlCommand.Parameters.AddWithValue("@TIMEHHMM", DateTimePicker4.Text)
        '    mySqlCommand.Parameters.AddWithValue("@TIMEDATE", DateTimePicker2.Value.Date.ToString("dd-MM-yyyy"))

        '    mySqlCommand.Parameters.AddWithValue("@INSURANCE", txt_insurance.Text)
        '    mySqlCommand.Parameters.AddWithValue("@TIMEHHMMIN", DateTimePicker1.Text)
        '    mySqlCommand.Parameters.AddWithValue("@TIMEDATEIN", DateTimePicker3.Value.Date.ToString("dd-MM-yyyy"))

        '    mySqlCommand.Parameters.AddWithValue("@BCTNTYPETHAI", TextBox1.Text)
        '    mySqlCommand.Parameters.AddWithValue("@BCTNTYPEPAE", TextBox4.Text)
        '    mySqlCommand.Parameters.AddWithValue("@CHECK", "0")
        '    mySqlCommand.Parameters.AddWithValue("@CHECK1", "0")


        '    mySqlCommand.ExecuteNonQuery()
        '    MySql.Close()

        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try




        clearcode()

        loadContainer()

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs)
        Dim cf As New frmViewvoyage(slcVoyage.EditValue)

        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub
End Class