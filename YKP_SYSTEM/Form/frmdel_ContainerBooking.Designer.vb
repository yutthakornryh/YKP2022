﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmdel_ContainerBooking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONDI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNBOOKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNYARD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNUPGRADE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTRBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEALBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEINBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.GridControl5)
        Me.LayoutControl1.Controls.Add(Me.GridControl4)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(864, 286, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1056, 581)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'GridControl5
        '
        Me.GridControl5.DataMember = "booking"
        Me.GridControl5.DataSource = Me.Ykpdtset3
        Me.GridControl5.Location = New System.Drawing.Point(24, 42)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(479, 489)
        Me.GridControl5.TabIndex = 209
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.FieldName = "BOOKINGID"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn2.AppearanceHeader.Options.UseFont = True
        Me.GridColumn2.Caption = "Booking No"
        Me.GridColumn2.FieldName = "BNO"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.ReadOnly = True
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 223
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn3.AppearanceHeader.Options.UseFont = True
        Me.GridColumn3.FieldName = "BLANDNO"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.ReadOnly = True
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn4.AppearanceHeader.Options.UseFont = True
        Me.GridColumn4.FieldName = "BCTNTYPE"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.ReadOnly = True
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn5.AppearanceHeader.Options.UseFont = True
        Me.GridColumn5.FieldName = "BCOM"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.OptionsColumn.ReadOnly = True
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 313
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn6.AppearanceHeader.Options.UseFont = True
        Me.GridColumn6.FieldName = "BSHIPNAME"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.ReadOnly = True
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 341
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn7.AppearanceHeader.Options.UseFont = True
        Me.GridColumn7.Caption = "FCL"
        Me.GridColumn7.FieldName = "CTNNO"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.OptionsColumn.ReadOnly = True
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 4
        Me.GridColumn7.Width = 382
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn8.AppearanceHeader.Options.UseFont = True
        Me.GridColumn8.FieldName = "BCONFIRMTIME"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.OptionsColumn.AllowEdit = False
        Me.GridColumn8.OptionsColumn.ReadOnly = True
        Me.GridColumn8.Width = 153
        '
        'GridColumn9
        '
        Me.GridColumn9.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn9.AppearanceCell.Options.UseFont = True
        Me.GridColumn9.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn9.AppearanceHeader.Options.UseFont = True
        Me.GridColumn9.FieldName = "BCONFIRMDAY"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.OptionsColumn.AllowEdit = False
        Me.GridColumn9.OptionsColumn.ReadOnly = True
        Me.GridColumn9.Width = 164
        '
        'GridColumn10
        '
        Me.GridColumn10.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn10.AppearanceCell.Options.UseFont = True
        Me.GridColumn10.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn10.AppearanceHeader.Options.UseFont = True
        Me.GridColumn10.Caption = "Close Date"
        Me.GridColumn10.FieldName = "GridColumn8"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.OptionsColumn.AllowEdit = False
        Me.GridColumn10.OptionsColumn.ReadOnly = True
        Me.GridColumn10.UnboundExpression = "Concat([BCONFIRMDAY], ' ', [BCONFIRMTIME])"
        Me.GridColumn10.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 3
        Me.GridColumn10.Width = 328
        '
        'GridControl4
        '
        Me.GridControl4.DataMember = "ctnmain"
        Me.GridControl4.DataSource = Me.Ykpdtset2
        Me.GridControl4.Location = New System.Drawing.Point(507, 42)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(525, 489)
        Me.GridControl4.TabIndex = 8
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNMAINID, Me.colCTNSTRING, Me.colCTNSEALID, Me.colCTNAGENT, Me.colCTNSIZE, Me.colCTNCONDI, Me.colCTNSTAT, Me.colCTNCONSI, Me.colCTNVOYN, Me.colCTNVOYS, Me.colCTNWEIGHT, Me.colCTNINS, Me.colCTNSHIPNAME, Me.colCTNDATEOUT, Me.colCTNBOOKING, Me.colCTNFORWARDER, Me.colCTNYARD, Me.colCTNUPGRADE, Me.colCTNMARKSTR, Me.colCTNMARKSEAL, Me.colCTNMARKSTRBOO, Me.colCTNMARKSEALBOO, Me.colCTNMARKDATEIN, Me.colCTNMARKDATEINBOO, Me.colCTNMARKDATEOUT, Me.colCTNMARKDATEOUTBOO, Me.colCTNMARKWEIGHT, Me.colCTNMARKWEIGHTBOO, Me.colCTNMARKTRANSPORT, Me.colCTNMARKTRANSPORTBOO})
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.MultiSelect = True
        Me.GridView4.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView4.OptionsView.ShowGroupPanel = False
        Me.GridView4.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCTNSHIPNAME, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCTNMAINID
        '
        Me.colCTNMAINID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMAINID.AppearanceCell.Options.UseFont = True
        Me.colCTNMAINID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMAINID.AppearanceHeader.Options.UseFont = True
        Me.colCTNMAINID.FieldName = "CTNMAINID"
        Me.colCTNMAINID.Name = "colCTNMAINID"
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.Caption = "Container No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 1
        Me.colCTNSTRING.Width = 88
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSEALID.AppearanceCell.Options.UseFont = True
        Me.colCTNSEALID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSEALID.AppearanceHeader.Options.UseFont = True
        Me.colCTNSEALID.Caption = "Seal"
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.Visible = True
        Me.colCTNSEALID.VisibleIndex = 2
        Me.colCTNSEALID.Width = 115
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.Caption = "Agent"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 3
        Me.colCTNAGENT.Width = 61
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSIZE.AppearanceCell.Options.UseFont = True
        Me.colCTNSIZE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSIZE.AppearanceHeader.Options.UseFont = True
        Me.colCTNSIZE.Caption = "Size"
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.Visible = True
        Me.colCTNSIZE.VisibleIndex = 4
        Me.colCTNSIZE.Width = 102
        '
        'colCTNCONDI
        '
        Me.colCTNCONDI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONDI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONDI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONDI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONDI.FieldName = "CTNCONDI"
        Me.colCTNCONDI.Name = "colCTNCONDI"
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTAT.AppearanceCell.Options.UseFont = True
        Me.colCTNSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTAT.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONSI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONSI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYN.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYN.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        '
        'colCTNVOYS
        '
        Me.colCTNVOYS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYS.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYS.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYS.FieldName = "CTNVOYS"
        Me.colCTNVOYS.Name = "colCTNVOYS"
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.Visible = True
        Me.colCTNWEIGHT.VisibleIndex = 5
        Me.colCTNWEIGHT.Width = 77
        '
        'colCTNINS
        '
        Me.colCTNINS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNINS.AppearanceCell.Options.UseFont = True
        Me.colCTNINS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNINS.AppearanceHeader.Options.UseFont = True
        Me.colCTNINS.FieldName = "CTNINS"
        Me.colCTNINS.Name = "colCTNINS"
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colCTNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        '
        'colCTNDATEOUT
        '
        Me.colCTNDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNDATEOUT.FieldName = "CTNDATEOUT"
        Me.colCTNDATEOUT.Name = "colCTNDATEOUT"
        '
        'colCTNBOOKING
        '
        Me.colCTNBOOKING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNBOOKING.AppearanceCell.Options.UseFont = True
        Me.colCTNBOOKING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNBOOKING.AppearanceHeader.Options.UseFont = True
        Me.colCTNBOOKING.FieldName = "CTNBOOKING"
        Me.colCTNBOOKING.Name = "colCTNBOOKING"
        '
        'colCTNFORWARDER
        '
        Me.colCTNFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colCTNFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colCTNFORWARDER.FieldName = "CTNFORWARDER"
        Me.colCTNFORWARDER.Name = "colCTNFORWARDER"
        '
        'colCTNYARD
        '
        Me.colCTNYARD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNYARD.AppearanceCell.Options.UseFont = True
        Me.colCTNYARD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNYARD.AppearanceHeader.Options.UseFont = True
        Me.colCTNYARD.FieldName = "CTNYARD"
        Me.colCTNYARD.Name = "colCTNYARD"
        '
        'colCTNUPGRADE
        '
        Me.colCTNUPGRADE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNUPGRADE.AppearanceCell.Options.UseFont = True
        Me.colCTNUPGRADE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNUPGRADE.AppearanceHeader.Options.UseFont = True
        Me.colCTNUPGRADE.FieldName = "CTNUPGRADE"
        Me.colCTNUPGRADE.Name = "colCTNUPGRADE"
        '
        'colCTNMARKSTR
        '
        Me.colCTNMARKSTR.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTR.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTR.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTR.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTR.FieldName = "CTNMARKSTR"
        Me.colCTNMARKSTR.Name = "colCTNMARKSTR"
        '
        'colCTNMARKSEAL
        '
        Me.colCTNMARKSEAL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEAL.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEAL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEAL.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEAL.FieldName = "CTNMARKSEAL"
        Me.colCTNMARKSEAL.Name = "colCTNMARKSEAL"
        '
        'colCTNMARKSTRBOO
        '
        Me.colCTNMARKSTRBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTRBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTRBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTRBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTRBOO.FieldName = "CTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.Name = "colCTNMARKSTRBOO"
        '
        'colCTNMARKSEALBOO
        '
        Me.colCTNMARKSEALBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEALBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEALBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEALBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEALBOO.FieldName = "CTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.Name = "colCTNMARKSEALBOO"
        '
        'colCTNMARKDATEIN
        '
        Me.colCTNMARKDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEIN.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEIN.FieldName = "CTNMARKDATEIN"
        Me.colCTNMARKDATEIN.Name = "colCTNMARKDATEIN"
        '
        'colCTNMARKDATEINBOO
        '
        Me.colCTNMARKDATEINBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEINBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.FieldName = "CTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.Name = "colCTNMARKDATEINBOO"
        '
        'colCTNMARKDATEOUT
        '
        Me.colCTNMARKDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUT.FieldName = "CTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.Name = "colCTNMARKDATEOUT"
        '
        'colCTNMARKDATEOUTBOO
        '
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.FieldName = "CTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.Name = "colCTNMARKDATEOUTBOO"
        '
        'colCTNMARKWEIGHT
        '
        Me.colCTNMARKWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHT.FieldName = "CTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.Name = "colCTNMARKWEIGHT"
        '
        'colCTNMARKWEIGHTBOO
        '
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.FieldName = "CTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.Name = "colCTNMARKWEIGHTBOO"
        '
        'colCTNMARKTRANSPORT
        '
        Me.colCTNMARKTRANSPORT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.FieldName = "CTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.Name = "colCTNMARKTRANSPORT"
        '
        'colCTNMARKTRANSPORTBOO
        '
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.FieldName = "CTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.Name = "colCTNMARKTRANSPORTBOO"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(575, 547)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(469, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "ลบตู้ "
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlItem3, Me.EmptySpaceItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1056, 581)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem7})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1036, 535)
        Me.LayoutControlGroup2.Text = "ลบตู้ ออกจาก Booking"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.GridControl4
        Me.LayoutControlItem5.Location = New System.Drawing.Point(483, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(529, 493)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.GridControl5
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(483, 493)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.SimpleButton1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(563, 535)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(473, 26)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 535)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(563, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmdel_ContainerBooking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1056, 581)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmdel_ContainerBooking"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ลบตู้"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colCTNMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONDI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNBOOKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNYARD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNUPGRADE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTRBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEALBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEINBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem

End Class
