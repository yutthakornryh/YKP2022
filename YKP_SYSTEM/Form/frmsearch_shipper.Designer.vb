﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmsearch_shipper
    Inherits DevComponents.DotNetBar.RibbonForm
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIPPERID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPNICKNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPADD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPTELL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPFAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnEdit = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEdit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(5, 1)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(975, 444)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl1
        Me.SearchControl1.Location = New System.Drawing.Point(45, 12)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl1
        Me.SearchControl1.Size = New System.Drawing.Size(918, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 5
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "shipper"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.Location = New System.Drawing.Point(12, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnEdit})
        Me.GridControl1.Size = New System.Drawing.Size(951, 394)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIPPERID, Me.colSHIPNAME, Me.colSHIPNICKNAME, Me.colSHIPADD, Me.colSHIPTELL, Me.colSHIPFAX, Me.colCOMPANYNO, Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSHIPPERID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colSHIPPERID
        '
        Me.colSHIPPERID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPPERID.AppearanceCell.Options.UseFont = True
        Me.colSHIPPERID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPPERID.AppearanceHeader.Options.UseFont = True
        Me.colSHIPPERID.FieldName = "SHIPPERID"
        Me.colSHIPPERID.Name = "colSHIPPERID"
        Me.colSHIPPERID.OptionsColumn.AllowEdit = False
        Me.colSHIPPERID.OptionsColumn.ReadOnly = True
        Me.colSHIPPERID.Visible = True
        Me.colSHIPPERID.VisibleIndex = 0
        Me.colSHIPPERID.Width = 68
        '
        'colSHIPNAME
        '
        Me.colSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colSHIPNAME.FieldName = "SHIPNAME"
        Me.colSHIPNAME.Name = "colSHIPNAME"
        Me.colSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colSHIPNAME.Visible = True
        Me.colSHIPNAME.VisibleIndex = 1
        Me.colSHIPNAME.Width = 335
        '
        'colSHIPNICKNAME
        '
        Me.colSHIPNICKNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPNICKNAME.AppearanceCell.Options.UseFont = True
        Me.colSHIPNICKNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPNICKNAME.AppearanceHeader.Options.UseFont = True
        Me.colSHIPNICKNAME.FieldName = "SHIPNICKNAME"
        Me.colSHIPNICKNAME.Name = "colSHIPNICKNAME"
        Me.colSHIPNICKNAME.OptionsColumn.AllowEdit = False
        Me.colSHIPNICKNAME.OptionsColumn.ReadOnly = True
        Me.colSHIPNICKNAME.Visible = True
        Me.colSHIPNICKNAME.VisibleIndex = 2
        Me.colSHIPNICKNAME.Width = 217
        '
        'colSHIPADD
        '
        Me.colSHIPADD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPADD.AppearanceCell.Options.UseFont = True
        Me.colSHIPADD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPADD.AppearanceHeader.Options.UseFont = True
        Me.colSHIPADD.FieldName = "SHIPADD"
        Me.colSHIPADD.Name = "colSHIPADD"
        Me.colSHIPADD.OptionsColumn.AllowEdit = False
        Me.colSHIPADD.OptionsColumn.ReadOnly = True
        Me.colSHIPADD.Visible = True
        Me.colSHIPADD.VisibleIndex = 3
        Me.colSHIPADD.Width = 217
        '
        'colSHIPTELL
        '
        Me.colSHIPTELL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPTELL.AppearanceCell.Options.UseFont = True
        Me.colSHIPTELL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPTELL.AppearanceHeader.Options.UseFont = True
        Me.colSHIPTELL.FieldName = "SHIPTELL"
        Me.colSHIPTELL.Name = "colSHIPTELL"
        Me.colSHIPTELL.OptionsColumn.AllowEdit = False
        Me.colSHIPTELL.OptionsColumn.ReadOnly = True
        Me.colSHIPTELL.Visible = True
        Me.colSHIPTELL.VisibleIndex = 4
        Me.colSHIPTELL.Width = 217
        '
        'colSHIPFAX
        '
        Me.colSHIPFAX.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPFAX.AppearanceCell.Options.UseFont = True
        Me.colSHIPFAX.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colSHIPFAX.AppearanceHeader.Options.UseFont = True
        Me.colSHIPFAX.FieldName = "SHIPFAX"
        Me.colSHIPFAX.Name = "colSHIPFAX"
        Me.colSHIPFAX.OptionsColumn.AllowEdit = False
        Me.colSHIPFAX.OptionsColumn.ReadOnly = True
        Me.colSHIPFAX.Visible = True
        Me.colSHIPFAX.VisibleIndex = 5
        Me.colSHIPFAX.Width = 217
        '
        'colCOMPANYNO
        '
        Me.colCOMPANYNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCOMPANYNO.AppearanceCell.Options.UseFont = True
        Me.colCOMPANYNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCOMPANYNO.AppearanceHeader.Options.UseFont = True
        Me.colCOMPANYNO.FieldName = "COMPANYNO"
        Me.colCOMPANYNO.Name = "colCOMPANYNO"
        Me.colCOMPANYNO.OptionsColumn.AllowEdit = False
        Me.colCOMPANYNO.OptionsColumn.ReadOnly = True
        Me.colCOMPANYNO.Visible = True
        Me.colCOMPANYNO.VisibleIndex = 6
        Me.colCOMPANYNO.Width = 217
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.Caption = "Edit"
        Me.GridColumn1.ColumnEdit = Me.btnEdit
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 7
        Me.GridColumn1.Width = 99
        '
        'btnEdit
        '
        Me.btnEdit.AutoHeight = False
        Me.btnEdit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up)})
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(975, 444)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(955, 398)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.SearchControl1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(955, 26)
        Me.LayoutControlItem2.Text = "ค้นหา"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(30, 16)
        '
        'frmsearch_shipper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(985, 447)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmsearch_shipper"
        Me.Text = "frmsearch_shipper"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEdit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colSHIPPERID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPNICKNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPADD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPTELL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPFAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnEdit As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
End Class
