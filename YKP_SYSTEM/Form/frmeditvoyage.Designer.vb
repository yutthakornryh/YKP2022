﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmeditvoyage
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm
    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.ButtonX1 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtVoyageName = New DevExpress.XtraEditors.TextEdit()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.mascmrunningBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascmrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprefixchar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colshowyear = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colformatdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colseparatechar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldefault = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_voyage = New DevExpress.XtraEditors.TextEdit()
        Me.date_krabi_etd = New DevExpress.XtraEditors.DateEdit()
        Me.date_krabi_eta = New DevExpress.XtraEditors.DateEdit()
        Me.txt_namemaster2 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_nationality2 = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBox1 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.vesmainBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DateTimePicker4 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.date_eta_penang = New DevExpress.XtraEditors.DateEdit()
        Me.date_etd_penang = New DevExpress.XtraEditors.DateEdit()
        Me.txt_namemaster = New DevExpress.XtraEditors.TextEdit()
        Me.txt_nationality = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBox2 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.vesmainBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVESMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSNAM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSMASTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSNATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSDATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.txtVoyageName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_krabi_etd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_krabi_etd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_krabi_eta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_krabi_eta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_namemaster2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nationality2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.vesmainBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_eta_penang.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_eta_penang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_etd_penang.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_etd_penang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_namemaster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nationality.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.vesmainBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.ButtonX1)
        Me.LayoutControl1.Controls.Add(Me.txtVoyageName)
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.txt_voyage)
        Me.LayoutControl1.Controls.Add(Me.date_krabi_etd)
        Me.LayoutControl1.Controls.Add(Me.date_krabi_eta)
        Me.LayoutControl1.Controls.Add(Me.txt_namemaster2)
        Me.LayoutControl1.Controls.Add(Me.txt_nationality2)
        Me.LayoutControl1.Controls.Add(Me.ComboBox1)
        Me.LayoutControl1.Controls.Add(Me.DateTimePicker4)
        Me.LayoutControl1.Controls.Add(Me.date_eta_penang)
        Me.LayoutControl1.Controls.Add(Me.date_etd_penang)
        Me.LayoutControl1.Controls.Add(Me.txt_namemaster)
        Me.LayoutControl1.Controls.Add(Me.txt_nationality)
        Me.LayoutControl1.Controls.Add(Me.ComboBox2)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(831, 267, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1073, 505)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'ButtonX1
        '
        Me.ButtonX1.Location = New System.Drawing.Point(914, 312)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(147, 22)
        Me.ButtonX1.StyleController = Me.LayoutControl1
        Me.ButtonX1.TabIndex = 212
        Me.ButtonX1.Text = "Update"
        '
        'txtVoyageName
        '
        Me.txtVoyageName.Location = New System.Drawing.Point(367, 12)
        Me.txtVoyageName.Name = "txtVoyageName"
        Me.txtVoyageName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoyageName.Properties.Appearance.Options.UseFont = True
        Me.txtVoyageName.Size = New System.Drawing.Size(167, 26)
        Me.txtVoyageName.StyleController = Me.LayoutControl1
        Me.txtVoyageName.TabIndex = 211
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(857, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.mascmrunningBindingSource
        Me.slcVoyage.Properties.DisplayMember = "menuname"
        Me.slcVoyage.Properties.ReadOnly = True
        Me.slcVoyage.Properties.ValueMember = "idmascmrunning"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(204, 26)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 210
        '
        'mascmrunningBindingSource
        '
        Me.mascmrunningBindingSource.DataMember = "mascmrunning"
        Me.mascmrunningBindingSource.DataSource = Me.Ykpdtset4
        Me.mascmrunningBindingSource.Sort = ""
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascmrunning, Me.colisrunning, Me.colprefixchar, Me.colshowyear, Me.colformatdate, Me.colseparatechar, Me.colmenuname, Me.coldefault})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colidmascmrunning
        '
        Me.colidmascmrunning.FieldName = "idmascmrunning"
        Me.colidmascmrunning.Name = "colidmascmrunning"
        '
        'colisrunning
        '
        Me.colisrunning.FieldName = "isrunning"
        Me.colisrunning.Name = "colisrunning"
        '
        'colprefixchar
        '
        Me.colprefixchar.FieldName = "prefixchar"
        Me.colprefixchar.Name = "colprefixchar"
        '
        'colshowyear
        '
        Me.colshowyear.FieldName = "showyear"
        Me.colshowyear.Name = "colshowyear"
        '
        'colformatdate
        '
        Me.colformatdate.FieldName = "formatdate"
        Me.colformatdate.Name = "colformatdate"
        '
        'colseparatechar
        '
        Me.colseparatechar.FieldName = "separatechar"
        Me.colseparatechar.Name = "colseparatechar"
        '
        'colmenuname
        '
        Me.colmenuname.Caption = "Format"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 0
        '
        'coldefault
        '
        Me.coldefault.FieldName = "default"
        Me.coldefault.Name = "coldefault"
        '
        'txt_voyage
        '
        Me.txt_voyage.Enabled = False
        Me.txt_voyage.Location = New System.Drawing.Point(622, 12)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt_voyage.Properties.Appearance.Options.UseFont = True
        Me.txt_voyage.Properties.ReadOnly = True
        Me.txt_voyage.Size = New System.Drawing.Size(231, 26)
        Me.txt_voyage.StyleController = Me.LayoutControl1
        Me.txt_voyage.TabIndex = 209
        '
        'date_krabi_etd
        '
        Me.date_krabi_etd.EditValue = Nothing
        Me.date_krabi_etd.Location = New System.Drawing.Point(736, 270)
        Me.date_krabi_etd.Name = "date_krabi_etd"
        Me.date_krabi_etd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.date_krabi_etd.Properties.Appearance.Options.UseFont = True
        Me.date_krabi_etd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_krabi_etd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_krabi_etd.Size = New System.Drawing.Size(313, 26)
        Me.date_krabi_etd.StyleController = Me.LayoutControl1
        Me.date_krabi_etd.TabIndex = 208
        '
        'date_krabi_eta
        '
        Me.date_krabi_eta.EditValue = Nothing
        Me.date_krabi_eta.Location = New System.Drawing.Point(222, 270)
        Me.date_krabi_eta.Name = "date_krabi_eta"
        Me.date_krabi_eta.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.date_krabi_eta.Properties.Appearance.Options.UseFont = True
        Me.date_krabi_eta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_krabi_eta.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_krabi_eta.Size = New System.Drawing.Size(312, 26)
        Me.date_krabi_eta.StyleController = Me.LayoutControl1
        Me.date_krabi_eta.TabIndex = 207
        '
        'txt_namemaster2
        '
        Me.txt_namemaster2.Location = New System.Drawing.Point(222, 240)
        Me.txt_namemaster2.Name = "txt_namemaster2"
        Me.txt_namemaster2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt_namemaster2.Properties.Appearance.Options.UseFont = True
        Me.txt_namemaster2.Size = New System.Drawing.Size(312, 26)
        Me.txt_namemaster2.StyleController = Me.LayoutControl1
        Me.txt_namemaster2.TabIndex = 206
        '
        'txt_nationality2
        '
        Me.txt_nationality2.Location = New System.Drawing.Point(736, 210)
        Me.txt_nationality2.Name = "txt_nationality2"
        Me.txt_nationality2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt_nationality2.Properties.Appearance.Options.UseFont = True
        Me.txt_nationality2.Size = New System.Drawing.Size(313, 26)
        Me.txt_nationality2.StyleController = Me.LayoutControl1
        Me.txt_nationality2.TabIndex = 205
        '
        'ComboBox1
        '
        Me.ComboBox1.Location = New System.Drawing.Point(222, 210)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.ComboBox1.Properties.Appearance.Options.UseFont = True
        Me.ComboBox1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox1.Properties.DataSource = Me.vesmainBindingSource1
        Me.ComboBox1.Properties.DisplayMember = "VSSNAM"
        Me.ComboBox1.Properties.ValueMember = "VESMAINID"
        Me.ComboBox1.Properties.View = Me.SearchLookUpEdit3View
        Me.ComboBox1.Size = New System.Drawing.Size(312, 26)
        Me.ComboBox1.StyleController = Me.LayoutControl1
        Me.ComboBox1.TabIndex = 204
        '
        'vesmainBindingSource1
        '
        Me.vesmainBindingSource1.DataMember = "vesmain"
        Me.vesmainBindingSource1.DataSource = Me.Ykpdtset2
        Me.vesmainBindingSource1.Sort = ""
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6})
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "VESMAINID"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "VSSNAM"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "VSSMASTER"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        '
        'GridColumn4
        '
        Me.GridColumn4.FieldName = "VSSNATION"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 2
        '
        'GridColumn5
        '
        Me.GridColumn5.FieldName = "VSSINS"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'GridColumn6
        '
        Me.GridColumn6.FieldName = "VSSDATE"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        '
        'DateTimePicker4
        '
        '
        '
        '
        Me.DateTimePicker4.BackgroundStyle.Class = "TextBoxBorder"
        Me.DateTimePicker4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker4.ButtonClear.Visible = True
        Me.DateTimePicker4.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.DateTimePicker4.Location = New System.Drawing.Point(948, 135)
        Me.DateTimePicker4.Mask = "90:00"
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(101, 26)
        Me.DateTimePicker4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.DateTimePicker4.TabIndex = 203
        Me.DateTimePicker4.Text = "0000"
        Me.DateTimePicker4.ValidatingType = GetType(Date)
        '
        'date_eta_penang
        '
        Me.date_eta_penang.EditValue = Nothing
        Me.date_eta_penang.Location = New System.Drawing.Point(222, 135)
        Me.date_eta_penang.Name = "date_eta_penang"
        Me.date_eta_penang.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.date_eta_penang.Properties.Appearance.Options.UseFont = True
        Me.date_eta_penang.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_eta_penang.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_eta_penang.Size = New System.Drawing.Size(312, 26)
        Me.date_eta_penang.StyleController = Me.LayoutControl1
        Me.date_eta_penang.TabIndex = 12
        '
        'date_etd_penang
        '
        Me.date_etd_penang.EditValue = Nothing
        Me.date_etd_penang.Location = New System.Drawing.Point(736, 135)
        Me.date_etd_penang.Name = "date_etd_penang"
        Me.date_etd_penang.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.date_etd_penang.Properties.Appearance.Options.UseFont = True
        Me.date_etd_penang.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_etd_penang.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.date_etd_penang.Size = New System.Drawing.Size(208, 26)
        Me.date_etd_penang.StyleController = Me.LayoutControl1
        Me.date_etd_penang.TabIndex = 11
        '
        'txt_namemaster
        '
        Me.txt_namemaster.Location = New System.Drawing.Point(222, 105)
        Me.txt_namemaster.Name = "txt_namemaster"
        Me.txt_namemaster.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt_namemaster.Properties.Appearance.Options.UseFont = True
        Me.txt_namemaster.Size = New System.Drawing.Size(312, 26)
        Me.txt_namemaster.StyleController = Me.LayoutControl1
        Me.txt_namemaster.TabIndex = 10
        '
        'txt_nationality
        '
        Me.txt_nationality.Location = New System.Drawing.Point(736, 75)
        Me.txt_nationality.Name = "txt_nationality"
        Me.txt_nationality.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.txt_nationality.Properties.Appearance.Options.UseFont = True
        Me.txt_nationality.Size = New System.Drawing.Size(313, 26)
        Me.txt_nationality.StyleController = Me.LayoutControl1
        Me.txt_nationality.TabIndex = 9
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(222, 75)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.ComboBox2.Properties.Appearance.Options.UseFont = True
        Me.ComboBox2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox2.Properties.DataSource = Me.vesmainBindingSource
        Me.ComboBox2.Properties.DisplayMember = "VSSNAM"
        Me.ComboBox2.Properties.ValueMember = "VESMAINID"
        Me.ComboBox2.Properties.View = Me.SearchLookUpEdit2View
        Me.ComboBox2.Size = New System.Drawing.Size(312, 26)
        Me.ComboBox2.StyleController = Me.LayoutControl1
        Me.ComboBox2.TabIndex = 8
        '
        'vesmainBindingSource
        '
        Me.vesmainBindingSource.DataMember = "vesmain"
        Me.vesmainBindingSource.DataSource = Me.Ykpdtset1
        Me.vesmainBindingSource.Sort = ""
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVESMAINID, Me.colVSSNAM, Me.colVSSMASTER, Me.colVSSNATION, Me.colVSSINS, Me.colVSSDATE})
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'colVESMAINID
        '
        Me.colVESMAINID.FieldName = "VESMAINID"
        Me.colVESMAINID.Name = "colVESMAINID"
        '
        'colVSSNAM
        '
        Me.colVSSNAM.FieldName = "VSSNAM"
        Me.colVSSNAM.Name = "colVSSNAM"
        Me.colVSSNAM.Visible = True
        Me.colVSSNAM.VisibleIndex = 0
        '
        'colVSSMASTER
        '
        Me.colVSSMASTER.FieldName = "VSSMASTER"
        Me.colVSSMASTER.Name = "colVSSMASTER"
        Me.colVSSMASTER.Visible = True
        Me.colVSSMASTER.VisibleIndex = 1
        '
        'colVSSNATION
        '
        Me.colVSSNATION.FieldName = "VSSNATION"
        Me.colVSSNATION.Name = "colVSSNATION"
        Me.colVSSNATION.Visible = True
        Me.colVSSNATION.VisibleIndex = 2
        '
        'colVSSINS
        '
        Me.colVSSINS.FieldName = "VSSINS"
        Me.colVSSINS.Name = "colVSSINS"
        '
        'colVSSDATE
        '
        Me.colVSSDATE.FieldName = "VSSDATE"
        Me.colVSSDATE.Name = "colVSSDATE"
        Me.colVSSDATE.Visible = True
        Me.colVSSDATE.VisibleIndex = 3
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceGroup.Options.UseFont = True
        Me.LayoutControlGroup1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup1.AppearanceTabPage.PageClient.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceTabPage.PageClient.Options.UseFont = True
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.LayoutControlGroup2, Me.LayoutControlGroup3, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem16, Me.EmptySpaceItem2, Me.EmptySpaceItem5})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1073, 505)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 326)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(1053, 159)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem7, Me.LayoutControlItem9, Me.LayoutControlItem6, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.EmptySpaceItem3})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1053, 135)
        Me.LayoutControlGroup2.Text = "Port Type N"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.ComboBox2
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(514, 30)
        Me.LayoutControlItem5.Text = "Name"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txt_namemaster
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(514, 30)
        Me.LayoutControlItem7.Text = "Name Master"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.date_eta_penang
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(514, 30)
        Me.LayoutControlItem9.Text = "Estimate Time of Departure (ETD)"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txt_nationality
        Me.LayoutControlItem6.Location = New System.Drawing.Point(514, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(515, 30)
        Me.LayoutControlItem6.Text = "Nationality"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.date_etd_penang
        Me.LayoutControlItem8.Location = New System.Drawing.Point(514, 60)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(410, 30)
        Me.LayoutControlItem8.Text = " Estimated time of arrival (ETA)"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.DateTimePicker4
        Me.LayoutControlItem10.Location = New System.Drawing.Point(924, 60)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(105, 30)
        Me.LayoutControlItem10.Text = "HH:mm"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(514, 30)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(515, 30)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem11, Me.LayoutControlItem15, Me.LayoutControlItem12, Me.EmptySpaceItem4})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 165)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1053, 135)
        Me.LayoutControlGroup3.Text = "Port Type S"
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.txt_namemaster2
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(514, 30)
        Me.LayoutControlItem13.Text = "Name Master"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.date_krabi_eta
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 60)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(514, 30)
        Me.LayoutControlItem14.Text = "Estimate Time of Departure (ETD)"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.ComboBox1
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(514, 30)
        Me.LayoutControlItem11.Text = "Name"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.date_krabi_etd
        Me.LayoutControlItem15.Location = New System.Drawing.Point(514, 60)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(515, 30)
        Me.LayoutControlItem15.Text = " Estimated time of arrival (ETA)"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(195, 16)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.txt_nationality2
        Me.LayoutControlItem12.Location = New System.Drawing.Point(514, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(515, 30)
        Me.LayoutControlItem12.Text = "Nationality"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(195, 16)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(514, 30)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(515, 30)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.txt_voyage
        Me.LayoutControlItem1.Location = New System.Drawing.Point(526, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(319, 30)
        Me.LayoutControlItem1.Text = "Voyage Name"
        Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(79, 16)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.slcVoyage
        Me.LayoutControlItem2.Location = New System.Drawing.Point(845, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(208, 30)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtVoyageName
        Me.LayoutControlItem4.Location = New System.Drawing.Point(263, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(263, 30)
        Me.LayoutControlItem4.Text = "Voyage Manual"
        Me.LayoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(87, 16)
        Me.LayoutControlItem4.TextToControlDistance = 5
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.ButtonX1
        Me.LayoutControlItem16.Location = New System.Drawing.Point(902, 300)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(151, 26)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 300)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(902, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(263, 30)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset3
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmeditvoyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1073, 505)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmeditvoyage"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.txtVoyageName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_krabi_etd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_krabi_etd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_krabi_eta.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_krabi_eta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_namemaster2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nationality2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.vesmainBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_eta_penang.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_eta_penang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_etd_penang.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_etd_penang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_namemaster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nationality.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.vesmainBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents txt_namemaster As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_nationality As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBox2 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents date_eta_penang As DevExpress.XtraEditors.DateEdit
    Friend WithEvents date_etd_penang As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents DateTimePicker4 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_namemaster2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_nationality2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBox1 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents date_krabi_etd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents date_krabi_eta As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txt_voyage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtVoyageName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ButtonX1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents vesmainBindingSource As BindingSource
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colVESMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSNAM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSMASTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSNATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSDATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents vesmainBindingSource1 As BindingSource
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents mascmrunningBindingSource As BindingSource
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents colidmascmrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprefixchar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colshowyear As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colformatdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colseparatechar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldefault As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
End Class
