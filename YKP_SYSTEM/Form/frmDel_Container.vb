﻿Imports DevExpress.XtraEditors

Public Class frmDel_Container
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection

    Dim ykpset As New ykpdtset
    Dim ykpsetmove As New ykpdtset

    Dim objCLASS As New ContainerClass(ykpset)

    Dim idbooking As String
    Public Sub loadDatavoyage()
        objCLASS.loadVoyage()

        slcVoyage.Properties.DataSource = ykpset.Tables("voyage")
    End Sub

    Private Sub frmDel_Container_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadDatavoyage()
    End Sub

    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        loadCTN(slcVoyage.EditValue)
    End Sub
    Public Sub loadCTN(ByVal slcvoyage As String)
        objCLASS.LoadCTNByVoyn(slcvoyage)
        GridControl4.DataSource = ykpset.Tables("ctnmain")
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        If XtraMessageBox.Show("ต้องการลบ ตู้ Containerใช่หรือไม่ ", "ตกลง", MessageBoxButtons.YesNo) <> DialogResult.No Then

            Dim sql As String
            For i As Integer = 0 To GridView4.SelectedRowsCount - 1


                sql = "DELETE FROM ctnmain where CTNMAINID = '" & GridView4.GetDataRow(GridView4.GetSelectedRows()(i))("CTNMAINID").ToString & "';"
                connectDB.ExecuteNonQuery(sql)


            Next
        Else
            Interaction.MsgBox("ยังไม่ได้เลือก Voyage ที่ต้องการย้าย", MsgBoxStyle.OkOnly, Nothing)
            Exit Sub
        End If

        loadCTN(slcVoyage.EditValue)

    End Sub
End Class
