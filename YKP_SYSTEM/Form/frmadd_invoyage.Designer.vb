﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmadd_invoyage
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ButtonX4 = New DevComponents.DotNetBar.ButtonX()
        Me.txtPlaceDeliver = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInvoice = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.slcConsignee = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.consigneeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCONSIGNEEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONADD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONTELL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONFAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidbooking_bl = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbillbooking1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colconname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colnumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colplace = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCloseDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFCL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbillbooking = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBOOKINGID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcConsignee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.consigneeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtNumber
        '
        Me.txtNumber.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtNumber.Border.Class = "TextBoxBorder"
        Me.txtNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNumber.DisabledBackColor = System.Drawing.Color.White
        Me.txtNumber.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.ForeColor = System.Drawing.Color.Black
        Me.txtNumber.Location = New System.Drawing.Point(834, 36)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.PreventEnterBeep = True
        Me.txtNumber.Size = New System.Drawing.Size(149, 28)
        Me.txtNumber.TabIndex = 227
        '
        'ButtonX4
        '
        Me.ButtonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX4.Location = New System.Drawing.Point(1060, 549)
        Me.ButtonX4.Name = "ButtonX4"
        Me.ButtonX4.Size = New System.Drawing.Size(156, 31)
        Me.ButtonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.ButtonX4.Symbol = ""
        Me.ButtonX4.TabIndex = 226
        Me.ButtonX4.Text = "Save"
        '
        'txtPlaceDeliver
        '
        Me.txtPlaceDeliver.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPlaceDeliver.Border.Class = "TextBoxBorder"
        Me.txtPlaceDeliver.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPlaceDeliver.DisabledBackColor = System.Drawing.Color.White
        Me.txtPlaceDeliver.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlaceDeliver.ForeColor = System.Drawing.Color.Black
        Me.txtPlaceDeliver.Location = New System.Drawing.Point(834, 60)
        Me.txtPlaceDeliver.Name = "txtPlaceDeliver"
        Me.txtPlaceDeliver.PreventEnterBeep = True
        Me.txtPlaceDeliver.Size = New System.Drawing.Size(382, 28)
        Me.txtPlaceDeliver.TabIndex = 220
        '
        'txtInvoice
        '
        Me.txtInvoice.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtInvoice.Border.Class = "TextBoxBorder"
        Me.txtInvoice.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInvoice.DisabledBackColor = System.Drawing.Color.White
        Me.txtInvoice.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoice.ForeColor = System.Drawing.Color.Black
        Me.txtInvoice.Location = New System.Drawing.Point(834, 12)
        Me.txtInvoice.Name = "txtInvoice"
        Me.txtInvoice.PreventEnterBeep = True
        Me.txtInvoice.Size = New System.Drawing.Size(382, 28)
        Me.txtInvoice.TabIndex = 221
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.Color.White
        Me.txtAddress.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAddress.ForeColor = System.Drawing.Color.Black
        Me.txtAddress.Location = New System.Drawing.Point(763, 154)
        Me.txtAddress.Multiline = True
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ReadOnly = True
        Me.txtAddress.Size = New System.Drawing.Size(441, 379)
        Me.txtAddress.TabIndex = 178
        '
        'txtWord
        '
        Me.txtWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtWord.Border.Class = "TextBoxBorder"
        Me.txtWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWord.DisabledBackColor = System.Drawing.Color.White
        Me.txtWord.Font = New System.Drawing.Font("Cordia New", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWord.ForeColor = System.Drawing.Color.Black
        Me.txtWord.Location = New System.Drawing.Point(1070, 36)
        Me.txtWord.Name = "txtWord"
        Me.txtWord.PreventEnterBeep = True
        Me.txtWord.Size = New System.Drawing.Size(146, 28)
        Me.txtWord.TabIndex = 231
        '
        'LayoutControl1
        '
        Me.LayoutControl1.BackColor = System.Drawing.Color.White
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.slcConsignee)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.txtAddress)
        Me.LayoutControl1.Controls.Add(Me.ButtonX4)
        Me.LayoutControl1.Controls.Add(Me.txtWord)
        Me.LayoutControl1.Controls.Add(Me.txtPlaceDeliver)
        Me.LayoutControl1.Controls.Add(Me.txtNumber)
        Me.LayoutControl1.Controls.Add(Me.txtInvoice)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.ForeColor = System.Drawing.Color.Black
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1228, 592)
        Me.LayoutControl1.TabIndex = 233
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(95, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.slcVoyage.Properties.Appearance.Options.UseBackColor = True
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Appearance.Options.UseForeColor = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.voyageBindingSource
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.GridView3
        Me.slcVoyage.Size = New System.Drawing.Size(250, 26)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 235
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset4
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYAGEID.AppearanceCell.Options.UseFont = True
        Me.colVOYAGEID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYAGEID.AppearanceHeader.Options.UseFont = True
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESIDN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESIDN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESIDS.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESIDS.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATESN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATESN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATESS.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATESS.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATEEN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATEEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESNAMEN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESNAMEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 1
        Me.colVOYVESNAMEN.Width = 624
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATEES.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYDATEES.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESNAMES.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYVESNAMES.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 2
        Me.colVOYVESNAMES.Width = 628
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYTIMEHHMMNN.AppearanceCell.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYNAME.AppearanceCell.Options.UseFont = True
        Me.colVOYNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colVOYNAME.AppearanceHeader.Options.UseFont = True
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        Me.colVOYNAME.Width = 370
        '
        'slcConsignee
        '
        Me.slcConsignee.Location = New System.Drawing.Point(846, 114)
        Me.slcConsignee.Name = "slcConsignee"
        Me.slcConsignee.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.slcConsignee.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.slcConsignee.Properties.Appearance.Options.UseBackColor = True
        Me.slcConsignee.Properties.Appearance.Options.UseForeColor = True
        Me.slcConsignee.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcConsignee.Properties.DataSource = Me.consigneeBindingSource
        Me.slcConsignee.Properties.DisplayMember = "CONNAME"
        Me.slcConsignee.Properties.ValueMember = "CONSIGNEEID"
        Me.slcConsignee.Properties.View = Me.SearchLookUpEdit1View
        Me.slcConsignee.Size = New System.Drawing.Size(358, 20)
        Me.slcConsignee.StyleController = Me.LayoutControl1
        Me.slcConsignee.TabIndex = 234
        '
        'consigneeBindingSource
        '
        Me.consigneeBindingSource.DataMember = "consignee"
        Me.consigneeBindingSource.DataSource = Me.Ykpdtset3
        Me.consigneeBindingSource.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCONSIGNEEID, Me.GridColumn1, Me.colCONADD, Me.colCONTELL, Me.colCONFAX})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colCONSIGNEEID
        '
        Me.colCONSIGNEEID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONSIGNEEID.AppearanceCell.Options.UseFont = True
        Me.colCONSIGNEEID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONSIGNEEID.AppearanceHeader.Options.UseFont = True
        Me.colCONSIGNEEID.FieldName = "CONSIGNEEID"
        Me.colCONSIGNEEID.Name = "colCONSIGNEEID"
        Me.colCONSIGNEEID.OptionsColumn.AllowEdit = False
        Me.colCONSIGNEEID.OptionsColumn.ReadOnly = True
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.FieldName = "CONNAME"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        Me.GridColumn1.Width = 432
        '
        'colCONADD
        '
        Me.colCONADD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONADD.AppearanceCell.Options.UseFont = True
        Me.colCONADD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONADD.AppearanceHeader.Options.UseFont = True
        Me.colCONADD.FieldName = "CONADD"
        Me.colCONADD.Name = "colCONADD"
        Me.colCONADD.OptionsColumn.AllowEdit = False
        Me.colCONADD.OptionsColumn.ReadOnly = True
        Me.colCONADD.Visible = True
        Me.colCONADD.VisibleIndex = 1
        Me.colCONADD.Width = 394
        '
        'colCONTELL
        '
        Me.colCONTELL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONTELL.AppearanceCell.Options.UseFont = True
        Me.colCONTELL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONTELL.AppearanceHeader.Options.UseFont = True
        Me.colCONTELL.FieldName = "CONTELL"
        Me.colCONTELL.Name = "colCONTELL"
        Me.colCONTELL.OptionsColumn.AllowEdit = False
        Me.colCONTELL.OptionsColumn.ReadOnly = True
        Me.colCONTELL.Visible = True
        Me.colCONTELL.VisibleIndex = 2
        Me.colCONTELL.Width = 207
        '
        'colCONFAX
        '
        Me.colCONFAX.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONFAX.AppearanceCell.Options.UseFont = True
        Me.colCONFAX.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCONFAX.AppearanceHeader.Options.UseFont = True
        Me.colCONFAX.FieldName = "CONFAX"
        Me.colCONFAX.Name = "colCONFAX"
        Me.colCONFAX.OptionsColumn.AllowEdit = False
        Me.colCONFAX.OptionsColumn.ReadOnly = True
        Me.colCONFAX.Width = 145
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "booking_bl"
        Me.GridControl2.DataSource = Me.Ykpdtset2
        Me.GridControl2.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White
        Me.GridControl2.EmbeddedNavigator.Appearance.ForeColor = System.Drawing.Color.Black
        Me.GridControl2.EmbeddedNavigator.Appearance.Options.UseBackColor = True
        Me.GridControl2.EmbeddedNavigator.Appearance.Options.UseForeColor = True
        Me.GridControl2.Location = New System.Drawing.Point(12, 387)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(735, 158)
        Me.GridControl2.TabIndex = 233
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidbooking_bl, Me.colbillbooking1, Me.colconname, Me.colnumber, Me.colplace})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colidbooking_bl
        '
        Me.colidbooking_bl.FieldName = "idbooking_bl"
        Me.colidbooking_bl.Name = "colidbooking_bl"
        Me.colidbooking_bl.OptionsColumn.AllowEdit = False
        Me.colidbooking_bl.OptionsColumn.ReadOnly = True
        '
        'colbillbooking1
        '
        Me.colbillbooking1.Caption = "Bill Of Landing No."
        Me.colbillbooking1.FieldName = "billbooking"
        Me.colbillbooking1.Name = "colbillbooking1"
        Me.colbillbooking1.OptionsColumn.AllowEdit = False
        Me.colbillbooking1.OptionsColumn.ReadOnly = True
        Me.colbillbooking1.Visible = True
        Me.colbillbooking1.VisibleIndex = 0
        '
        'colconname
        '
        Me.colconname.Caption = "Consignee"
        Me.colconname.FieldName = "conname"
        Me.colconname.Name = "colconname"
        Me.colconname.OptionsColumn.AllowEdit = False
        Me.colconname.OptionsColumn.ReadOnly = True
        Me.colconname.Visible = True
        Me.colconname.VisibleIndex = 1
        '
        'colnumber
        '
        Me.colnumber.Caption = "No"
        Me.colnumber.FieldName = "number"
        Me.colnumber.Name = "colnumber"
        Me.colnumber.OptionsColumn.AllowEdit = False
        Me.colnumber.OptionsColumn.ReadOnly = True
        Me.colnumber.Visible = True
        Me.colnumber.VisibleIndex = 2
        '
        'colplace
        '
        Me.colplace.Caption = "Place Of Delivery"
        Me.colplace.FieldName = "place"
        Me.colplace.Name = "colplace"
        Me.colplace.OptionsColumn.AllowEdit = False
        Me.colplace.OptionsColumn.ReadOnly = True
        Me.colplace.Visible = True
        Me.colplace.VisibleIndex = 3
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "booking"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.EmbeddedNavigator.Appearance.BackColor = System.Drawing.Color.White
        Me.GridControl1.EmbeddedNavigator.Appearance.ForeColor = System.Drawing.Color.Black
        Me.GridControl1.EmbeddedNavigator.Appearance.Options.UseBackColor = True
        Me.GridControl1.EmbeddedNavigator.Appearance.Options.UseForeColor = True
        Me.GridControl1.Location = New System.Drawing.Point(12, 42)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(735, 341)
        Me.GridControl1.TabIndex = 232
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBNO, Me.colCloseDate, Me.colBSHIPNAME, Me.colBNFORWARDERNAME, Me.colFCL, Me.colBCTNTYPE, Me.colBCTNNO, Me.colbillbooking, Me.colBOOKINGID})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colBNO
        '
        Me.colBNO.Caption = "Booking No."
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Visible = True
        Me.colBNO.VisibleIndex = 0
        Me.colBNO.Width = 378
        '
        'colCloseDate
        '
        Me.colCloseDate.FieldName = "CloseDate"
        Me.colCloseDate.Name = "colCloseDate"
        Me.colCloseDate.OptionsColumn.AllowEdit = False
        Me.colCloseDate.OptionsColumn.ReadOnly = True
        Me.colCloseDate.Visible = True
        Me.colCloseDate.VisibleIndex = 3
        Me.colCloseDate.Width = 279
        '
        'colBSHIPNAME
        '
        Me.colBSHIPNAME.Caption = "Shipper"
        Me.colBSHIPNAME.FieldName = "BSHIPNAME"
        Me.colBSHIPNAME.Name = "colBSHIPNAME"
        Me.colBSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colBSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colBSHIPNAME.Visible = True
        Me.colBSHIPNAME.VisibleIndex = 1
        Me.colBSHIPNAME.Width = 310
        '
        'colBNFORWARDERNAME
        '
        Me.colBNFORWARDERNAME.Caption = "Consignee"
        Me.colBNFORWARDERNAME.FieldName = "BNFORWARDERNAME"
        Me.colBNFORWARDERNAME.Name = "colBNFORWARDERNAME"
        Me.colBNFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBNFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBNFORWARDERNAME.Visible = True
        Me.colBNFORWARDERNAME.VisibleIndex = 2
        Me.colBNFORWARDERNAME.Width = 370
        '
        'colFCL
        '
        Me.colFCL.FieldName = "FCL"
        Me.colFCL.Name = "colFCL"
        Me.colFCL.OptionsColumn.AllowEdit = False
        Me.colFCL.OptionsColumn.ReadOnly = True
        Me.colFCL.Visible = True
        Me.colFCL.VisibleIndex = 4
        Me.colFCL.Width = 285
        '
        'colBCTNTYPE
        '
        Me.colBCTNTYPE.FieldName = "BCTNTYPE"
        Me.colBCTNTYPE.Name = "colBCTNTYPE"
        Me.colBCTNTYPE.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPE.OptionsColumn.ReadOnly = True
        '
        'colBCTNNO
        '
        Me.colBCTNNO.FieldName = "BCTNNO"
        Me.colBCTNNO.Name = "colBCTNNO"
        Me.colBCTNNO.OptionsColumn.AllowEdit = False
        Me.colBCTNNO.OptionsColumn.ReadOnly = True
        '
        'colbillbooking
        '
        Me.colbillbooking.FieldName = "billbooking"
        Me.colbillbooking.Name = "colbillbooking"
        Me.colbillbooking.OptionsColumn.AllowEdit = False
        Me.colbillbooking.OptionsColumn.ReadOnly = True
        '
        'colBOOKINGID
        '
        Me.colBOOKINGID.FieldName = "BOOKINGID"
        Me.colBOOKINGID.Name = "colBOOKINGID"
        Me.colBOOKINGID.OptionsColumn.AllowEdit = False
        Me.colBOOKINGID.OptionsColumn.ReadOnly = True
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem9, Me.EmptySpaceItem1, Me.LayoutControlGroup2, Me.LayoutControlItem7, Me.LayoutControlItem13, Me.LayoutControlItem1, Me.EmptySpaceItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1228, 592)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.txtInvoice
        Me.LayoutControlItem3.Location = New System.Drawing.Point(739, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(469, 24)
        Me.LayoutControlItem3.Text = "Invoice No."
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(80, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.txtNumber
        Me.LayoutControlItem4.Location = New System.Drawing.Point(739, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(236, 24)
        Me.LayoutControlItem4.Text = "Number"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(80, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txtWord
        Me.LayoutControlItem5.Location = New System.Drawing.Point(975, 24)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(233, 24)
        Me.LayoutControlItem5.Text = "Word"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(80, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txtPlaceDeliver
        Me.LayoutControlItem6.Location = New System.Drawing.Point(739, 48)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(469, 24)
        Me.LayoutControlItem6.Text = "Place of Delivery"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(80, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.ButtonX4
        Me.LayoutControlItem9.Location = New System.Drawing.Point(1048, 537)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(160, 35)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 537)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(1048, 35)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12, Me.LayoutControlItem14})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(739, 72)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(469, 465)
        Me.LayoutControlGroup2.Text = "Consignee"
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.txtAddress
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(445, 399)
        Me.LayoutControlItem12.Text = "Address"
        Me.LayoutControlItem12.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(80, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.slcConsignee
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(445, 24)
        Me.LayoutControlItem14.Text = "Company Name"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(80, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.GridControl1
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 30)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(739, 345)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.GridControl2
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 375)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(739, 162)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.slcVoyage
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(337, 30)
        Me.LayoutControlItem1.Text = "Voyage"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(80, 18)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(337, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(402, 30)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'frmadd_invoyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1228, 592)
        Me.Controls.Add(Me.LayoutControl1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmadd_invoyage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Invoice"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcConsignee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.consigneeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ButtonX4 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtPlaceDeliver As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInvoice As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCloseDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFCL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBOOKINGID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbillbooking As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colidbooking_bl As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbillbooking1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colconname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colnumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colplace As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents slcConsignee As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents consigneeBindingSource As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents colCONSIGNEEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONADD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONTELL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONFAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
End Class
