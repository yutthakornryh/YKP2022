﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class frmmove_ctn
    Dim idbooking As String

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Dim ykpset_move As New ykpdtset
    Dim idvoyage As String

    Dim idbookingmove As String
    Public Sub New(ByVal voyage As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = voyage
        ' Add any initialization after the InitializeComponent() call.
        loadBooking(voyage)
        loadDAta()
    End Sub
    Public Sub loadBooking(ByVal voyage As String)
        Dim sql As String
        sql = "Select * ,CONCAT(BCTNNO,' X ', BCTNTYPE ) AS CTNNO from booking where BVOYAGE ='" & voyage & "' ; "
        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
        GridControl5.DataSource = ykpset.Tables("booking")

    End Sub
    Public Sub loadBooking_move(ByVal voyage As String)
        Dim sql As String
        sql = "Select * ,CONCAT(BCTNNO,' X ', BCTNTYPE ) AS CTNNO from booking where BVOYAGE ='" & voyage & "' ; "
        ykpset_move.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset_move.Tables("booking"))
        GridControl1.DataSource = ykpset_move.Tables("booking")

        ykpset_move.Tables("ctnmain").Clear()
    End Sub
    Private Sub frmmove_ctn_Load(sender As Object, e As EventArgs) Handles MyBase.Load


    End Sub

    Public Sub loadDAta()
        Dim sql As String = "Select * from voyage;"
        ykpset_move.Tables("voyage").Clear()
        connectDB.GetTable(sql, ykpset_move.Tables("voyage"))
        GridControl3.DataSource = ykpset_move.Tables("voyage")
    End Sub

    Private Sub GridView5_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView5.RowCellClick

        Dim idbooking As String
        idbooking = GridView5.GetDataRow(GridView5.FocusedRowHandle)("BOOKINGID").ToString()
        Dim sql As String
        sql = "Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where BOOKID ='" & idbooking & "' ;"
        ykpset.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset.Tables("ctnmain"))
        GridControl4.DataSource = ykpset.Tables("ctnmain")

    End Sub

    Private Sub GridView3_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView3.RowCellClick


        loadBooking_move(GridView3.GetDataRow(GridView3.FocusedRowHandle)("VOYAGEID").ToString())
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If (GridView1.FocusedRowHandle < 0) Then
            Interaction.MsgBox("กรุณาเลือก Booking ที่ต้องการย้าย", MsgBoxStyle.OkOnly, Nothing)

            Exit Sub



        Else
            If (GridView3.FocusedRowHandle > 0) Then

                Dim sql As String
                For i As Integer = 0 To GridView4.SelectedRowsCount - 1

                    sql = "UPDATE borrow left join ctnmain On borrow.CTNID = ctnmain.CTNMAINID  Set borrow.BOOKID ='" & GridView1.GetDataRow(GridView1.FocusedRowHandle)("BOOKINGID").ToString() & "' , ctnmain.CTNVOYS ='" & GridView3.GetDataRow(GridView3.FocusedRowHandle)("VOYAGEID").ToString() & "' WHERE idborrow = " & GridView4.GetDataRow(GridView4.GetSelectedRows()(i))("idborrow").ToString & "; "
                    connectDB.ExecuteNonQuery(sql)

                Next
                LoadContainerMove()
            Else
                Interaction.MsgBox("ยังไม่ได้เลือก Voyage ที่ต้องการย้าย", MsgBoxStyle.OkOnly, Nothing)
                Exit Sub
            End If

        End If



        '  Dim commandText2 As String = String.Concat(New String() {})
    End Sub
    Public Sub LoadContainerMove()
        idbookingmove = GridView1.GetDataRow(GridView1.FocusedRowHandle)("BOOKINGID").ToString()

        Dim sql As String
        sql = "Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where BOOKID ='" & idbookingmove & "' ;"
        ykpset_move.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset_move.Tables("ctnmain"))
        GridControl2.DataSource = ykpset_move.Tables("ctnmain")

        Dim idbooking As String
        idbooking = GridView5.GetDataRow(GridView5.FocusedRowHandle)("BOOKINGID").ToString()
        sql = "Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where BOOKID ='" & idbooking & "' ;"
        ykpset.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset.Tables("ctnmain"))
        GridControl4.DataSource = ykpset.Tables("ctnmain")

    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        idbookingmove = GridView1.GetDataRow(GridView1.FocusedRowHandle)("BOOKINGID").ToString()

        Dim sql As String
        sql = "Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where BOOKID ='" & idbookingmove & "' ;"
        ykpset_move.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset_move.Tables("ctnmain"))
        GridControl2.DataSource = ykpset_move.Tables("ctnmain")

    End Sub
End Class