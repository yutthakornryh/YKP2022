﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class N_frmadd_voyage
    Inherits DevComponents.DotNetBar.RibbonForm


    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.date_krabi_etd = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.date_krabi_eta = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DateTimePicker4 = New DevComponents.DotNetBar.Controls.MaskedTextBoxAdv()
        Me.date_eta_penang = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.date_etd_penang = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.mascmrunningBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascmrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprefixchar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colshowyear = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colformatdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colseparatechar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldefault = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.txtVoyageName = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_voyage = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_nationality = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_namemaster = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.txt_nationality2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_namemaster2 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.GroupBox2.SuspendLayout()
        CType(Me.date_krabi_etd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_krabi_eta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.date_eta_penang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.date_etd_penang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.date_krabi_etd)
        Me.GroupBox2.Controls.Add(Me.date_krabi_eta)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Location = New System.Drawing.Point(31, 356)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1002, 78)
        Me.GroupBox2.TabIndex = 168
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "KANTANG PORT YKP TYPE S"
        '
        'date_krabi_etd
        '
        '
        '
        '
        Me.date_krabi_etd.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_krabi_etd.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_krabi_etd.ButtonDropDown.Visible = True
        Me.date_krabi_etd.CustomFormat = "dd-MM-yyyy"
        Me.date_krabi_etd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_krabi_etd.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_krabi_etd.IsPopupCalendarOpen = False
        Me.date_krabi_etd.Location = New System.Drawing.Point(451, 35)
        Me.date_krabi_etd.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_krabi_etd.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_krabi_etd.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_krabi_etd.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_krabi_etd.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_krabi_etd.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_krabi_etd.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_krabi_etd.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_etd.MonthCalendar.TodayButtonVisible = True
        Me.date_krabi_etd.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_krabi_etd.Name = "date_krabi_etd"
        Me.date_krabi_etd.Size = New System.Drawing.Size(200, 22)
        Me.date_krabi_etd.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.date_krabi_etd.TabIndex = 181
        '
        'date_krabi_eta
        '
        '
        '
        '
        Me.date_krabi_eta.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_krabi_eta.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_krabi_eta.ButtonDropDown.Visible = True
        Me.date_krabi_eta.CustomFormat = "dd-MM-yyyy"
        Me.date_krabi_eta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_krabi_eta.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_krabi_eta.IsPopupCalendarOpen = False
        Me.date_krabi_eta.Location = New System.Drawing.Point(145, 35)
        Me.date_krabi_eta.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_krabi_eta.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_krabi_eta.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_krabi_eta.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_krabi_eta.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_krabi_eta.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_krabi_eta.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_krabi_eta.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_krabi_eta.MonthCalendar.TodayButtonVisible = True
        Me.date_krabi_eta.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_krabi_eta.Name = "date_krabi_eta"
        Me.date_krabi_eta.Size = New System.Drawing.Size(200, 22)
        Me.date_krabi_eta.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.date_krabi_eta.TabIndex = 180
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(92, 35)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(42, 26)
        Me.Label12.TabIndex = 144
        Me.Label12.Text = "ETD :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(405, 35)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(40, 26)
        Me.Label20.TabIndex = 143
        Me.Label20.Text = "ETA :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DateTimePicker4)
        Me.GroupBox1.Controls.Add(Me.date_eta_penang)
        Me.GroupBox1.Controls.Add(Me.date_etd_penang)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Location = New System.Drawing.Point(31, 172)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1002, 72)
        Me.GroupBox1.TabIndex = 167
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "PENANG PORT TYPE N"
        '
        'DateTimePicker4
        '
        '
        '
        '
        Me.DateTimePicker4.BackgroundStyle.Class = "TextBoxBorder"
        Me.DateTimePicker4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimePicker4.ButtonClear.Visible = True
        Me.DateTimePicker4.Location = New System.Drawing.Point(675, 29)
        Me.DateTimePicker4.Mask = "90:00"
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(90, 19)
        Me.DateTimePicker4.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.DateTimePicker4.TabIndex = 202
        Me.DateTimePicker4.Text = "0000"
        Me.DateTimePicker4.ValidatingType = GetType(Date)
        '
        'date_eta_penang
        '
        '
        '
        '
        Me.date_eta_penang.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_eta_penang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_eta_penang.ButtonDropDown.Visible = True
        Me.date_eta_penang.CustomFormat = "dd-MM-yyyy"
        Me.date_eta_penang.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_eta_penang.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_eta_penang.IsPopupCalendarOpen = False
        Me.date_eta_penang.Location = New System.Drawing.Point(140, 32)
        Me.date_eta_penang.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_eta_penang.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_eta_penang.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_eta_penang.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_eta_penang.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_eta_penang.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_eta_penang.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_eta_penang.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_eta_penang.MonthCalendar.TodayButtonVisible = True
        Me.date_eta_penang.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_eta_penang.Name = "date_eta_penang"
        Me.date_eta_penang.Size = New System.Drawing.Size(200, 22)
        Me.date_eta_penang.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.date_eta_penang.TabIndex = 180
        '
        'date_etd_penang
        '
        '
        '
        '
        Me.date_etd_penang.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.date_etd_penang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.date_etd_penang.ButtonDropDown.Visible = True
        Me.date_etd_penang.CustomFormat = "dd-MM-yyyy"
        Me.date_etd_penang.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.date_etd_penang.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.date_etd_penang.IsPopupCalendarOpen = False
        Me.date_etd_penang.Location = New System.Drawing.Point(451, 29)
        Me.date_etd_penang.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.date_etd_penang.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.date_etd_penang.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.MonthCalendar.DisplayMonth = New Date(2013, 11, 1, 0, 0, 0, 0)
        Me.date_etd_penang.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.date_etd_penang.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.date_etd_penang.MonthCalendar.MinDate = New Date(1457, 1, 1, 0, 0, 0, 0)
        Me.date_etd_penang.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.date_etd_penang.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.date_etd_penang.MonthCalendar.TodayButtonVisible = True
        Me.date_etd_penang.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.date_etd_penang.Name = "date_etd_penang"
        Me.date_etd_penang.Size = New System.Drawing.Size(209, 22)
        Me.date_etd_penang.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.date_etd_penang.TabIndex = 179
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(92, 29)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 26)
        Me.Label19.TabIndex = 144
        Me.Label19.Text = "ETD :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(405, 29)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(40, 26)
        Me.Label18.TabIndex = 143
        Me.Label18.Text = "ETA :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.Panel1.Controls.Add(Me.slcVoyage)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.txtVoyageName)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txt_voyage)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Location = New System.Drawing.Point(20, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1013, 49)
        Me.Panel1.TabIndex = 166
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(843, 6)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Cordia New", 15.75!)
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.mascmrunningBindingSource
        Me.slcVoyage.Properties.DisplayMember = "menuname"
        Me.slcVoyage.Properties.ValueMember = "idmascmrunning"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(141, 36)
        Me.slcVoyage.TabIndex = 178
        '
        'mascmrunningBindingSource
        '
        Me.mascmrunningBindingSource.DataMember = "mascmrunning"
        Me.mascmrunningBindingSource.DataSource = Me.Ykpdtset1
        Me.mascmrunningBindingSource.Sort = ""
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascmrunning, Me.colisrunning, Me.colprefixchar, Me.colshowyear, Me.colformatdate, Me.colseparatechar, Me.colmenuname, Me.coldefault})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colidmascmrunning
        '
        Me.colidmascmrunning.FieldName = "idmascmrunning"
        Me.colidmascmrunning.Name = "colidmascmrunning"
        '
        'colisrunning
        '
        Me.colisrunning.FieldName = "isrunning"
        Me.colisrunning.Name = "colisrunning"
        '
        'colprefixchar
        '
        Me.colprefixchar.FieldName = "prefixchar"
        Me.colprefixchar.Name = "colprefixchar"
        '
        'colshowyear
        '
        Me.colshowyear.FieldName = "showyear"
        Me.colshowyear.Name = "colshowyear"
        '
        'colformatdate
        '
        Me.colformatdate.FieldName = "formatdate"
        Me.colformatdate.Name = "colformatdate"
        '
        'colseparatechar
        '
        Me.colseparatechar.FieldName = "separatechar"
        Me.colseparatechar.Name = "colseparatechar"
        '
        'colmenuname
        '
        Me.colmenuname.Caption = "ท่าเรือ"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 0
        '
        'coldefault
        '
        Me.coldefault.FieldName = "default"
        Me.coldefault.Name = "coldefault"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(153, 16)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(108, 17)
        Me.CheckBox1.TabIndex = 52
        Me.CheckBox1.Text = "Generate Manual"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'txtVoyageName
        '
        Me.txtVoyageName.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoyageName.Location = New System.Drawing.Point(355, 6)
        Me.txtVoyageName.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txtVoyageName.Name = "txtVoyageName"
        Me.txtVoyageName.ReadOnly = True
        Me.txtVoyageName.Size = New System.Drawing.Size(214, 37)
        Me.txtVoyageName.TabIndex = 51
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(277, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 26)
        Me.Label8.TabIndex = 50
        Me.Label8.Text = "Voyage No"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Cordia New", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(3, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(142, 45)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Add Voyage "
        '
        'txt_voyage
        '
        Me.txt_voyage.Font = New System.Drawing.Font("Cordia New", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_voyage.Location = New System.Drawing.Point(669, 6)
        Me.txt_voyage.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.ReadOnly = True
        Me.txt_voyage.Size = New System.Drawing.Size(153, 37)
        Me.txt_voyage.TabIndex = 49
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(576, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(87, 26)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "VOYAGE  NO"
        '
        'txt_nationality
        '
        Me.txt_nationality.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nationality.Location = New System.Drawing.Point(578, 87)
        Me.txt_nationality.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_nationality.Name = "txt_nationality"
        Me.txt_nationality.Size = New System.Drawing.Size(218, 30)
        Me.txt_nationality.TabIndex = 163
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(477, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 26)
        Me.Label6.TabIndex = 162
        Me.Label6.Text = "NATIONALITY :"
        '
        'txt_namemaster
        '
        Me.txt_namemaster.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_namemaster.Location = New System.Drawing.Point(171, 133)
        Me.txt_namemaster.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_namemaster.Name = "txt_namemaster"
        Me.txt_namemaster.Size = New System.Drawing.Size(281, 30)
        Me.txt_namemaster.TabIndex = 161
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(64, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 26)
        Me.Label2.TabIndex = 159
        Me.Label2.Text = "NAME MASTER :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(42, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 26)
        Me.Label1.TabIndex = 158
        Me.Label1.Text = "NAME OF VESSEL. :"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(922, 440)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(12)
        Me.ButtonX1.Size = New System.Drawing.Size(111, 43)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 25.0!
        Me.ButtonX1.TabIndex = 169
        Me.ButtonX1.Text = "Save"
        '
        'txt_nationality2
        '
        Me.txt_nationality2.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nationality2.Location = New System.Drawing.Point(569, 265)
        Me.txt_nationality2.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_nationality2.Name = "txt_nationality2"
        Me.txt_nationality2.Size = New System.Drawing.Size(218, 30)
        Me.txt_nationality2.TabIndex = 175
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(468, 273)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 26)
        Me.Label3.TabIndex = 174
        Me.Label3.Text = "NATIONALITY :"
        '
        'txt_namemaster2
        '
        Me.txt_namemaster2.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_namemaster2.Location = New System.Drawing.Point(171, 311)
        Me.txt_namemaster2.Margin = New System.Windows.Forms.Padding(3, 6, 3, 6)
        Me.txt_namemaster2.Name = "txt_namemaster2"
        Me.txt_namemaster2.Size = New System.Drawing.Size(281, 30)
        Me.txt_namemaster2.TabIndex = 173
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(64, 311)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(101, 26)
        Me.Label4.TabIndex = 171
        Me.Label4.Text = "NAME MASTER :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(42, 271)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 26)
        Me.Label7.TabIndex = 170
        Me.Label7.Text = "NAME OF VESSEL. :"
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(171, 273)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(281, 30)
        Me.ComboBox1.TabIndex = 176
        '
        'ComboBox2
        '
        Me.ComboBox2.Font = New System.Drawing.Font("Cordia New", 12.0!)
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(171, 87)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(281, 30)
        Me.ComboBox2.TabIndex = 177
        '
        'frmadd_voyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1162, 510)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.txt_nationality2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.txt_namemaster2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txt_nationality)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_namemaster)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmadd_voyage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "frmadd_voyage"
        Me.Text = "frmadd_voyage"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.date_krabi_etd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_krabi_eta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.date_eta_penang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.date_etd_penang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_voyage As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_nationality As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_namemaster As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txt_nationality2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_namemaster2 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents date_krabi_etd As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents date_krabi_eta As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents date_etd_penang As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents date_eta_penang As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents DateTimePicker4 As DevComponents.DotNetBar.Controls.MaskedTextBoxAdv
    Friend WithEvents txtVoyageName As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents mascmrunningBindingSource As BindingSource
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colidmascmrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprefixchar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colshowyear As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colformatdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colseparatechar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldefault As DevExpress.XtraGrid.Columns.GridColumn
End Class
