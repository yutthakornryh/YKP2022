﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmedit_gate1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SearchControl2 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidborrow = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBOOKID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCARID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEHHMM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEDATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMMUDITY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMARKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINSURANCE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEHHMMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPETHAI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPEPAE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRCHECK1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRCHECK = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRBILLNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_no_car = New System.Windows.Forms.TextBox()
        Me.txt_name_car = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New DevExpress.XtraEditors.TextEdit()
        Me.port_loading = New DevExpress.XtraEditors.TextEdit()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONDI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNBOOKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNYARD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNUPGRADE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTRBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEALBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEINBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.TextBox4 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_insurance = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox1 = New DevExpress.XtraEditors.TextEdit()
        Me.port_discharge = New DevExpress.XtraEditors.TextEdit()
        Me.txt_sealid = New DevExpress.XtraEditors.TextEdit()
        Me.txt_customer = New DevExpress.XtraEditors.TextEdit()
        Me.txt_container = New DevExpress.XtraEditors.TextEdit()
        Me.txt_voyage = New DevExpress.XtraEditors.TextEdit()
        Me.txt_booking_no = New DevExpress.XtraEditors.TextEdit()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.SearchControl2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.port_loading.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_insurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.port_discharge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_sealid.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_customer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_container.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_booking_no.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SearchControl2)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.txt_no_car)
        Me.LayoutControl1.Controls.Add(Me.txt_name_car)
        Me.LayoutControl1.Controls.Add(Me.TextBox3)
        Me.LayoutControl1.Controls.Add(Me.port_loading)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup1)
        Me.LayoutControl1.Controls.Add(Me.TextBox4)
        Me.LayoutControl1.Controls.Add(Me.txt_insurance)
        Me.LayoutControl1.Controls.Add(Me.TextBox1)
        Me.LayoutControl1.Controls.Add(Me.port_discharge)
        Me.LayoutControl1.Controls.Add(Me.txt_sealid)
        Me.LayoutControl1.Controls.Add(Me.txt_customer)
        Me.LayoutControl1.Controls.Add(Me.txt_container)
        Me.LayoutControl1.Controls.Add(Me.txt_voyage)
        Me.LayoutControl1.Controls.Add(Me.txt_booking_no)
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(997, 586)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SearchControl2
        '
        Me.SearchControl2.Location = New System.Drawing.Point(122, 12)
        Me.SearchControl2.Name = "SearchControl2"
        Me.SearchControl2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl2.Properties.Appearance.Options.UseFont = True
        Me.SearchControl2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl2.Size = New System.Drawing.Size(377, 22)
        Me.SearchControl2.StyleController = Me.LayoutControl1
        Me.SearchControl2.TabIndex = 208
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "borrow"
        Me.GridControl1.DataSource = Me.Ykpdtset4
        Me.GridControl1.Location = New System.Drawing.Point(12, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(487, 181)
        Me.GridControl1.TabIndex = 207
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidborrow, Me.colCTNID, Me.colCTNSTRING1, Me.colCTNSIZE1, Me.colCTNSEALID1, Me.colCTNAGENT1, Me.colBOOKID, Me.colCOMNAME, Me.colCARID, Me.colTIMEHHMM, Me.colTIMEDATE, Me.colCOMMUDITY, Me.colMARKING, Me.colINSURANCE, Me.colTIMEDATEIN, Me.colTIMEHHMMIN, Me.colBCTNTYPETHAI, Me.colBCTNTYPEPAE, Me.colBRCHECK1, Me.colBRCHECK, Me.colBRBILLNAME, Me.colBNO})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colidborrow
        '
        Me.colidborrow.FieldName = "idborrow"
        Me.colidborrow.Name = "colidborrow"
        Me.colidborrow.OptionsColumn.AllowEdit = False
        Me.colidborrow.OptionsColumn.ReadOnly = True
        '
        'colCTNID
        '
        Me.colCTNID.FieldName = "CTNID"
        Me.colCTNID.Name = "colCTNID"
        Me.colCTNID.OptionsColumn.AllowEdit = False
        Me.colCTNID.OptionsColumn.ReadOnly = True
        '
        'colCTNSTRING1
        '
        Me.colCTNSTRING1.Caption = "Container No."
        Me.colCTNSTRING1.FieldName = "CTNSTRING"
        Me.colCTNSTRING1.Name = "colCTNSTRING1"
        Me.colCTNSTRING1.OptionsColumn.AllowEdit = False
        Me.colCTNSTRING1.OptionsColumn.ReadOnly = True
        Me.colCTNSTRING1.Visible = True
        Me.colCTNSTRING1.VisibleIndex = 1
        Me.colCTNSTRING1.Width = 268
        '
        'colCTNSIZE1
        '
        Me.colCTNSIZE1.Caption = "Size"
        Me.colCTNSIZE1.FieldName = "CTNSIZE"
        Me.colCTNSIZE1.Name = "colCTNSIZE1"
        Me.colCTNSIZE1.OptionsColumn.AllowEdit = False
        Me.colCTNSIZE1.OptionsColumn.ReadOnly = True
        Me.colCTNSIZE1.Visible = True
        Me.colCTNSIZE1.VisibleIndex = 3
        Me.colCTNSIZE1.Width = 89
        '
        'colCTNSEALID1
        '
        Me.colCTNSEALID1.FieldName = "CTNSEALID"
        Me.colCTNSEALID1.Name = "colCTNSEALID1"
        Me.colCTNSEALID1.OptionsColumn.AllowEdit = False
        Me.colCTNSEALID1.OptionsColumn.ReadOnly = True
        Me.colCTNSEALID1.Visible = True
        Me.colCTNSEALID1.VisibleIndex = 2
        Me.colCTNSEALID1.Width = 199
        '
        'colCTNAGENT1
        '
        Me.colCTNAGENT1.Caption = "Agent"
        Me.colCTNAGENT1.FieldName = "CTNAGENT"
        Me.colCTNAGENT1.Name = "colCTNAGENT1"
        Me.colCTNAGENT1.OptionsColumn.AllowEdit = False
        Me.colCTNAGENT1.OptionsColumn.ReadOnly = True
        Me.colCTNAGENT1.Visible = True
        Me.colCTNAGENT1.VisibleIndex = 4
        Me.colCTNAGENT1.Width = 206
        '
        'colBOOKID
        '
        Me.colBOOKID.FieldName = "BOOKID"
        Me.colBOOKID.Name = "colBOOKID"
        Me.colBOOKID.OptionsColumn.AllowEdit = False
        Me.colBOOKID.OptionsColumn.ReadOnly = True
        '
        'colCOMNAME
        '
        Me.colCOMNAME.FieldName = "COMNAME"
        Me.colCOMNAME.Name = "colCOMNAME"
        Me.colCOMNAME.OptionsColumn.AllowEdit = False
        Me.colCOMNAME.OptionsColumn.ReadOnly = True
        Me.colCOMNAME.Width = 106
        '
        'colCARID
        '
        Me.colCARID.Caption = "Car ID"
        Me.colCARID.FieldName = "CARID"
        Me.colCARID.Name = "colCARID"
        Me.colCARID.OptionsColumn.AllowEdit = False
        Me.colCARID.OptionsColumn.ReadOnly = True
        Me.colCARID.Visible = True
        Me.colCARID.VisibleIndex = 5
        Me.colCARID.Width = 222
        '
        'colTIMEHHMM
        '
        Me.colTIMEHHMM.FieldName = "TIMEHHMM"
        Me.colTIMEHHMM.Name = "colTIMEHHMM"
        Me.colTIMEHHMM.OptionsColumn.AllowEdit = False
        Me.colTIMEHHMM.OptionsColumn.ReadOnly = True
        Me.colTIMEHHMM.Width = 162
        '
        'colTIMEDATE
        '
        Me.colTIMEDATE.FieldName = "TIMEDATE"
        Me.colTIMEDATE.Name = "colTIMEDATE"
        Me.colTIMEDATE.OptionsColumn.AllowEdit = False
        Me.colTIMEDATE.OptionsColumn.ReadOnly = True
        Me.colTIMEDATE.Width = 162
        '
        'colCOMMUDITY
        '
        Me.colCOMMUDITY.FieldName = "COMMUDITY"
        Me.colCOMMUDITY.Name = "colCOMMUDITY"
        Me.colCOMMUDITY.OptionsColumn.AllowEdit = False
        Me.colCOMMUDITY.OptionsColumn.ReadOnly = True
        '
        'colMARKING
        '
        Me.colMARKING.FieldName = "MARKING"
        Me.colMARKING.Name = "colMARKING"
        Me.colMARKING.OptionsColumn.AllowEdit = False
        Me.colMARKING.OptionsColumn.ReadOnly = True
        '
        'colINSURANCE
        '
        Me.colINSURANCE.FieldName = "INSURANCE"
        Me.colINSURANCE.Name = "colINSURANCE"
        Me.colINSURANCE.OptionsColumn.AllowEdit = False
        Me.colINSURANCE.OptionsColumn.ReadOnly = True
        Me.colINSURANCE.Visible = True
        Me.colINSURANCE.VisibleIndex = 6
        Me.colINSURANCE.Width = 296
        '
        'colTIMEDATEIN
        '
        Me.colTIMEDATEIN.FieldName = "TIMEDATEIN"
        Me.colTIMEDATEIN.Name = "colTIMEDATEIN"
        Me.colTIMEDATEIN.OptionsColumn.AllowEdit = False
        Me.colTIMEDATEIN.OptionsColumn.ReadOnly = True
        Me.colTIMEDATEIN.Width = 162
        '
        'colTIMEHHMMIN
        '
        Me.colTIMEHHMMIN.FieldName = "TIMEHHMMIN"
        Me.colTIMEHHMMIN.Name = "colTIMEHHMMIN"
        Me.colTIMEHHMMIN.OptionsColumn.AllowEdit = False
        Me.colTIMEHHMMIN.OptionsColumn.ReadOnly = True
        Me.colTIMEHHMMIN.Width = 162
        '
        'colBCTNTYPETHAI
        '
        Me.colBCTNTYPETHAI.FieldName = "BCTNTYPETHAI"
        Me.colBCTNTYPETHAI.Name = "colBCTNTYPETHAI"
        Me.colBCTNTYPETHAI.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPETHAI.OptionsColumn.ReadOnly = True
        Me.colBCTNTYPETHAI.Width = 162
        '
        'colBCTNTYPEPAE
        '
        Me.colBCTNTYPEPAE.FieldName = "BCTNTYPEPAE"
        Me.colBCTNTYPEPAE.Name = "colBCTNTYPEPAE"
        Me.colBCTNTYPEPAE.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPEPAE.OptionsColumn.ReadOnly = True
        Me.colBCTNTYPEPAE.Width = 172
        '
        'colBRCHECK1
        '
        Me.colBRCHECK1.FieldName = "BRCHECK1"
        Me.colBRCHECK1.Name = "colBRCHECK1"
        Me.colBRCHECK1.OptionsColumn.AllowEdit = False
        Me.colBRCHECK1.OptionsColumn.ReadOnly = True
        '
        'colBRCHECK
        '
        Me.colBRCHECK.FieldName = "BRCHECK"
        Me.colBRCHECK.Name = "colBRCHECK"
        Me.colBRCHECK.OptionsColumn.AllowEdit = False
        Me.colBRCHECK.OptionsColumn.ReadOnly = True
        '
        'colBRBILLNAME
        '
        Me.colBRBILLNAME.FieldName = "BRBILLNAME"
        Me.colBRBILLNAME.Name = "colBRBILLNAME"
        Me.colBRBILLNAME.OptionsColumn.AllowEdit = False
        Me.colBRBILLNAME.OptionsColumn.ReadOnly = True
        '
        'colBNO
        '
        Me.colBNO.Caption = "Booking No."
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Visible = True
        Me.colBNO.VisibleIndex = 0
        Me.colBNO.Width = 201
        '
        'txt_no_car
        '
        Me.txt_no_car.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_no_car.Location = New System.Drawing.Point(885, 56)
        Me.txt_no_car.Name = "txt_no_car"
        Me.txt_no_car.Size = New System.Drawing.Size(100, 20)
        Me.txt_no_car.TabIndex = 205
        '
        'txt_name_car
        '
        Me.txt_name_car.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_name_car.Location = New System.Drawing.Point(613, 56)
        Me.txt_name_car.Name = "txt_name_car"
        Me.txt_name_car.Size = New System.Drawing.Size(158, 20)
        Me.txt_name_car.TabIndex = 204
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(885, 158)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox3.Properties.Appearance.Options.UseFont = True
        Me.TextBox3.Size = New System.Drawing.Size(100, 22)
        Me.TextBox3.StyleController = Me.LayoutControl1
        Me.TextBox3.TabIndex = 203
        '
        'port_loading
        '
        Me.port_loading.EditValue = "PENANG"
        Me.port_loading.Location = New System.Drawing.Point(613, 158)
        Me.port_loading.Name = "port_loading"
        Me.port_loading.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.port_loading.Properties.Appearance.Options.UseFont = True
        Me.port_loading.Size = New System.Drawing.Size(158, 22)
        Me.port_loading.StyleController = Me.LayoutControl1
        Me.port_loading.TabIndex = 202
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl3
        Me.SearchControl1.Location = New System.Drawing.Point(122, 223)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl3
        Me.SearchControl1.Size = New System.Drawing.Size(377, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 201
        '
        'GridControl3
        '
        Me.GridControl3.DataMember = "ctnmain"
        Me.GridControl3.DataSource = Me.Ykpdtset2
        Me.GridControl3.Location = New System.Drawing.Point(12, 249)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(487, 325)
        Me.GridControl3.TabIndex = 200
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNMAINID, Me.colCTNSTRING, Me.colCTNSEALID, Me.colCTNAGENT, Me.colCTNSIZE, Me.colCTNCONDI, Me.colCTNSTAT, Me.colCTNCONSI, Me.colCTNVOYN, Me.colCTNVOYS, Me.colCTNWEIGHT, Me.colCTNINS, Me.colCTNSHIPNAME, Me.colCTNDATEOUT, Me.colCTNBOOKING, Me.colCTNFORWARDER, Me.colCTNYARD, Me.colCTNUPGRADE, Me.colCTNMARKSTR, Me.colCTNMARKSEAL, Me.colCTNMARKSTRBOO, Me.colCTNMARKSEALBOO, Me.colCTNMARKDATEIN, Me.colCTNMARKDATEINBOO, Me.colCTNMARKDATEOUT, Me.colCTNMARKDATEOUTBOO, Me.colCTNMARKWEIGHT, Me.colCTNMARKWEIGHTBOO, Me.colCTNMARKTRANSPORT, Me.colCTNMARKTRANSPORTBOO, Me.colmenuname})
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsView.ShowGroupPanel = False
        Me.GridView3.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCTNAGENT, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colCTNMAINID
        '
        Me.colCTNMAINID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMAINID.AppearanceCell.Options.UseFont = True
        Me.colCTNMAINID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMAINID.AppearanceHeader.Options.UseFont = True
        Me.colCTNMAINID.FieldName = "CTNMAINID"
        Me.colCTNMAINID.Name = "colCTNMAINID"
        Me.colCTNMAINID.OptionsColumn.AllowEdit = False
        Me.colCTNMAINID.OptionsColumn.ReadOnly = True
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.Caption = "Container No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.OptionsColumn.AllowEdit = False
        Me.colCTNSTRING.OptionsColumn.ReadOnly = True
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 0
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSEALID.AppearanceCell.Options.UseFont = True
        Me.colCTNSEALID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSEALID.AppearanceHeader.Options.UseFont = True
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.OptionsColumn.AllowEdit = False
        Me.colCTNSEALID.OptionsColumn.ReadOnly = True
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.Caption = "Agent Line"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.OptionsColumn.AllowEdit = False
        Me.colCTNAGENT.OptionsColumn.ReadOnly = True
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 1
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSIZE.AppearanceCell.Options.UseFont = True
        Me.colCTNSIZE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSIZE.AppearanceHeader.Options.UseFont = True
        Me.colCTNSIZE.Caption = "Size"
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.OptionsColumn.AllowEdit = False
        Me.colCTNSIZE.OptionsColumn.ReadOnly = True
        Me.colCTNSIZE.Visible = True
        Me.colCTNSIZE.VisibleIndex = 2
        '
        'colCTNCONDI
        '
        Me.colCTNCONDI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONDI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONDI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONDI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONDI.FieldName = "CTNCONDI"
        Me.colCTNCONDI.Name = "colCTNCONDI"
        Me.colCTNCONDI.OptionsColumn.AllowEdit = False
        Me.colCTNCONDI.OptionsColumn.ReadOnly = True
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTAT.AppearanceCell.Options.UseFont = True
        Me.colCTNSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSTAT.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        Me.colCTNSTAT.OptionsColumn.AllowEdit = False
        Me.colCTNSTAT.OptionsColumn.ReadOnly = True
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONSI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNCONSI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        Me.colCTNCONSI.OptionsColumn.AllowEdit = False
        Me.colCTNCONSI.OptionsColumn.ReadOnly = True
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYN.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYN.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYN.Caption = "Voy N"
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        Me.colCTNVOYN.OptionsColumn.AllowEdit = False
        Me.colCTNVOYN.OptionsColumn.ReadOnly = True
        Me.colCTNVOYN.Visible = True
        Me.colCTNVOYN.VisibleIndex = 3
        '
        'colCTNVOYS
        '
        Me.colCTNVOYS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYS.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNVOYS.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYS.FieldName = "CTNVOYS"
        Me.colCTNVOYS.Name = "colCTNVOYS"
        Me.colCTNVOYS.OptionsColumn.AllowEdit = False
        Me.colCTNVOYS.OptionsColumn.ReadOnly = True
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNWEIGHT.OptionsColumn.ReadOnly = True
        '
        'colCTNINS
        '
        Me.colCTNINS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNINS.AppearanceCell.Options.UseFont = True
        Me.colCTNINS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNINS.AppearanceHeader.Options.UseFont = True
        Me.colCTNINS.FieldName = "CTNINS"
        Me.colCTNINS.Name = "colCTNINS"
        Me.colCTNINS.OptionsColumn.AllowEdit = False
        Me.colCTNINS.OptionsColumn.ReadOnly = True
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colCTNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colCTNSHIPNAME.Caption = "Shipper"
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        Me.colCTNSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colCTNSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colCTNSHIPNAME.Visible = True
        Me.colCTNSHIPNAME.VisibleIndex = 5
        '
        'colCTNDATEOUT
        '
        Me.colCTNDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNDATEOUT.FieldName = "CTNDATEOUT"
        Me.colCTNDATEOUT.Name = "colCTNDATEOUT"
        Me.colCTNDATEOUT.OptionsColumn.AllowEdit = False
        Me.colCTNDATEOUT.OptionsColumn.ReadOnly = True
        '
        'colCTNBOOKING
        '
        Me.colCTNBOOKING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNBOOKING.AppearanceCell.Options.UseFont = True
        Me.colCTNBOOKING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNBOOKING.AppearanceHeader.Options.UseFont = True
        Me.colCTNBOOKING.Caption = "Booking"
        Me.colCTNBOOKING.FieldName = "CTNBOOKING"
        Me.colCTNBOOKING.Name = "colCTNBOOKING"
        Me.colCTNBOOKING.OptionsColumn.AllowEdit = False
        Me.colCTNBOOKING.OptionsColumn.ReadOnly = True
        Me.colCTNBOOKING.Visible = True
        Me.colCTNBOOKING.VisibleIndex = 4
        '
        'colCTNFORWARDER
        '
        Me.colCTNFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colCTNFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colCTNFORWARDER.FieldName = "CTNFORWARDER"
        Me.colCTNFORWARDER.Name = "colCTNFORWARDER"
        Me.colCTNFORWARDER.OptionsColumn.AllowEdit = False
        Me.colCTNFORWARDER.OptionsColumn.ReadOnly = True
        '
        'colCTNYARD
        '
        Me.colCTNYARD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNYARD.AppearanceCell.Options.UseFont = True
        Me.colCTNYARD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNYARD.AppearanceHeader.Options.UseFont = True
        Me.colCTNYARD.Caption = "CTNYARD"
        Me.colCTNYARD.FieldName = "CTNYARD"
        Me.colCTNYARD.Name = "colCTNYARD"
        Me.colCTNYARD.OptionsColumn.AllowEdit = False
        Me.colCTNYARD.OptionsColumn.ReadOnly = True
        '
        'colCTNUPGRADE
        '
        Me.colCTNUPGRADE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNUPGRADE.AppearanceCell.Options.UseFont = True
        Me.colCTNUPGRADE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNUPGRADE.AppearanceHeader.Options.UseFont = True
        Me.colCTNUPGRADE.FieldName = "CTNUPGRADE"
        Me.colCTNUPGRADE.Name = "colCTNUPGRADE"
        Me.colCTNUPGRADE.OptionsColumn.AllowEdit = False
        Me.colCTNUPGRADE.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSTR
        '
        Me.colCTNMARKSTR.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTR.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTR.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTR.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTR.FieldName = "CTNMARKSTR"
        Me.colCTNMARKSTR.Name = "colCTNMARKSTR"
        Me.colCTNMARKSTR.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSTR.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSEAL
        '
        Me.colCTNMARKSEAL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEAL.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEAL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEAL.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEAL.FieldName = "CTNMARKSEAL"
        Me.colCTNMARKSEAL.Name = "colCTNMARKSEAL"
        Me.colCTNMARKSEAL.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSEAL.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSTRBOO
        '
        Me.colCTNMARKSTRBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTRBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTRBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSTRBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTRBOO.FieldName = "CTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.Name = "colCTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSTRBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKSEALBOO
        '
        Me.colCTNMARKSEALBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEALBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEALBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKSEALBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEALBOO.FieldName = "CTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.Name = "colCTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSEALBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEIN
        '
        Me.colCTNMARKDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEIN.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEIN.FieldName = "CTNMARKDATEIN"
        Me.colCTNMARKDATEIN.Name = "colCTNMARKDATEIN"
        Me.colCTNMARKDATEIN.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEIN.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEINBOO
        '
        Me.colCTNMARKDATEINBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEINBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.FieldName = "CTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.Name = "colCTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEINBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEOUT
        '
        Me.colCTNMARKDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUT.FieldName = "CTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.Name = "colCTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEOUT.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKDATEOUTBOO
        '
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.FieldName = "CTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.Name = "colCTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEOUTBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKWEIGHT
        '
        Me.colCTNMARKWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHT.FieldName = "CTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.Name = "colCTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKWEIGHT.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKWEIGHTBOO
        '
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.FieldName = "CTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.Name = "colCTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKWEIGHTBOO.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKTRANSPORT
        '
        Me.colCTNMARKTRANSPORT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.FieldName = "CTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.Name = "colCTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKTRANSPORT.OptionsColumn.ReadOnly = True
        '
        'colCTNMARKTRANSPORTBOO
        '
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.FieldName = "CTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.Name = "colCTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKTRANSPORTBOO.OptionsColumn.ReadOnly = True
        '
        'colmenuname
        '
        Me.colmenuname.Caption = "Depot"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 6
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(855, 315)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(130, 23)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 199
        Me.SimpleButton1.Text = "บันทึก"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(503, 210)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Normal"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Upgrade")})
        Me.RadioGroup1.Size = New System.Drawing.Size(482, 101)
        Me.RadioGroup1.StyleController = Me.LayoutControl1
        Me.RadioGroup1.TabIndex = 198
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(718, 184)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox4.Properties.Appearance.Options.UseFont = True
        Me.TextBox4.Size = New System.Drawing.Size(50, 22)
        Me.TextBox4.StyleController = Me.LayoutControl1
        Me.TextBox4.TabIndex = 197
        '
        'txt_insurance
        '
        Me.txt_insurance.Location = New System.Drawing.Point(882, 184)
        Me.txt_insurance.Name = "txt_insurance"
        Me.txt_insurance.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_insurance.Properties.Appearance.Options.UseFont = True
        Me.txt_insurance.Size = New System.Drawing.Size(103, 22)
        Me.txt_insurance.StyleController = Me.LayoutControl1
        Me.txt_insurance.TabIndex = 196
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(613, 184)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TextBox1.Properties.Appearance.Options.UseFont = True
        Me.TextBox1.Size = New System.Drawing.Size(101, 22)
        Me.TextBox1.StyleController = Me.LayoutControl1
        Me.TextBox1.TabIndex = 195
        '
        'port_discharge
        '
        Me.port_discharge.EditValue = "SPH"
        Me.port_discharge.Location = New System.Drawing.Point(885, 132)
        Me.port_discharge.Name = "port_discharge"
        Me.port_discharge.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.port_discharge.Properties.Appearance.Options.UseFont = True
        Me.port_discharge.Size = New System.Drawing.Size(100, 22)
        Me.port_discharge.StyleController = Me.LayoutControl1
        Me.port_discharge.TabIndex = 194
        '
        'txt_sealid
        '
        Me.txt_sealid.Location = New System.Drawing.Point(613, 132)
        Me.txt_sealid.Name = "txt_sealid"
        Me.txt_sealid.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_sealid.Properties.Appearance.Options.UseFont = True
        Me.txt_sealid.Size = New System.Drawing.Size(158, 22)
        Me.txt_sealid.StyleController = Me.LayoutControl1
        Me.txt_sealid.TabIndex = 193
        '
        'txt_customer
        '
        Me.txt_customer.Location = New System.Drawing.Point(885, 106)
        Me.txt_customer.Name = "txt_customer"
        Me.txt_customer.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_customer.Properties.Appearance.Options.UseFont = True
        Me.txt_customer.Size = New System.Drawing.Size(100, 22)
        Me.txt_customer.StyleController = Me.LayoutControl1
        Me.txt_customer.TabIndex = 192
        '
        'txt_container
        '
        Me.txt_container.Location = New System.Drawing.Point(613, 106)
        Me.txt_container.Name = "txt_container"
        Me.txt_container.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_container.Properties.Appearance.Options.UseFont = True
        Me.txt_container.Size = New System.Drawing.Size(158, 22)
        Me.txt_container.StyleController = Me.LayoutControl1
        Me.txt_container.TabIndex = 191
        '
        'txt_voyage
        '
        Me.txt_voyage.Location = New System.Drawing.Point(613, 80)
        Me.txt_voyage.Name = "txt_voyage"
        Me.txt_voyage.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_voyage.Properties.Appearance.Options.UseFont = True
        Me.txt_voyage.Size = New System.Drawing.Size(158, 22)
        Me.txt_voyage.StyleController = Me.LayoutControl1
        Me.txt_voyage.TabIndex = 190
        '
        'txt_booking_no
        '
        Me.txt_booking_no.Location = New System.Drawing.Point(885, 80)
        Me.txt_booking_no.Name = "txt_booking_no"
        Me.txt_booking_no.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt_booking_no.Properties.Appearance.Options.UseFont = True
        Me.txt_booking_no.Size = New System.Drawing.Size(100, 22)
        Me.txt_booking_no.StyleController = Me.LayoutControl1
        Me.txt_booking_no.TabIndex = 189
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(613, 12)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.voyageBindingSource
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(372, 40)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 4
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset3
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYAGEID.AppearanceCell.Options.UseFont = True
        Me.colVOYAGEID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYAGEID.AppearanceHeader.Options.UseFont = True
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        Me.colVOYAGEID.Visible = True
        Me.colVOYAGEID.VisibleIndex = 0
        Me.colVOYAGEID.Width = 153
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        Me.colVOYVESIDN.Width = 164
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDS.AppearanceCell.Options.UseFont = True
        Me.colVOYVESIDS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESIDS.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        Me.colVOYVESIDS.Width = 164
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        Me.colVOYDATESN.Width = 164
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESS.AppearanceCell.Options.UseFont = True
        Me.colVOYDATESS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATESS.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        Me.colVOYDATESS.Width = 164
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEEN.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        Me.colVOYDATEEN.Width = 164
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMEN.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMEN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMEN.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 2
        Me.colVOYVESNAMEN.Width = 523
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEES.AppearanceCell.Options.UseFont = True
        Me.colVOYDATEES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYDATEES.AppearanceHeader.Options.UseFont = True
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        Me.colVOYDATEES.Width = 164
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMES.AppearanceCell.Options.UseFont = True
        Me.colVOYVESNAMES.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYVESNAMES.AppearanceHeader.Options.UseFont = True
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 3
        Me.colVOYVESNAMES.Width = 566
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYTIMEHHMMNN.AppearanceCell.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYTIMEHHMMNN.AppearanceHeader.Options.UseFont = True
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME.AppearanceCell.Options.UseFont = True
        Me.colVOYNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVOYNAME.AppearanceHeader.Options.UseFont = True
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 1
        Me.colVOYNAME.Width = 345
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.EmptySpaceItem1, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.EmptySpaceItem2, Me.LayoutControlItem21, Me.LayoutControlItem11, Me.LayoutControlItem2, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem4, Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem25})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(997, 586)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem10.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem10.Control = Me.txt_booking_no
        Me.LayoutControlItem10.Location = New System.Drawing.Point(763, 68)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem10.Text = "Booking No."
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem12.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem12.Control = Me.txt_container
        Me.LayoutControlItem12.Location = New System.Drawing.Point(491, 94)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem12.Text = "Container"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem13.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem13.Control = Me.txt_customer
        Me.LayoutControlItem13.Location = New System.Drawing.Point(763, 94)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem13.Text = "Shipper"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(107, 16)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(491, 330)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(486, 236)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.Control = Me.txt_sealid
        Me.LayoutControlItem14.Location = New System.Drawing.Point(491, 120)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem14.Text = "Seal ID"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.port_discharge
        Me.LayoutControlItem15.Location = New System.Drawing.Point(763, 120)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem15.Text = "ท่าเรือขึ้น"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem16.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem16.Control = Me.TextBox1
        Me.LayoutControlItem16.Location = New System.Drawing.Point(491, 172)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(215, 26)
        Me.LayoutControlItem16.Text = "Type"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem17.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem17.Control = Me.txt_insurance
        Me.LayoutControlItem17.Location = New System.Drawing.Point(760, 172)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(217, 26)
        Me.LayoutControlItem17.Text = "Insurance"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.TextBox4
        Me.LayoutControlItem18.Location = New System.Drawing.Point(706, 172)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(54, 26)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.RadioGroup1
        Me.LayoutControlItem19.Location = New System.Drawing.Point(491, 198)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(486, 105)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.SimpleButton1
        Me.LayoutControlItem20.Location = New System.Drawing.Point(843, 303)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(134, 27)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(491, 303)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(352, 27)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.GridControl3
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 237)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(491, 329)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem11.Control = Me.txt_voyage
        Me.LayoutControlItem11.Location = New System.Drawing.Point(491, 68)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem11.Text = "Voyage"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.SearchControl1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 211)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(491, 26)
        Me.LayoutControlItem2.Text = "ค้นหา"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem22.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem22.Control = Me.port_loading
        Me.LayoutControlItem22.Location = New System.Drawing.Point(491, 146)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem22.Text = "เทียบท่า"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem23.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem23.Control = Me.TextBox3
        Me.LayoutControlItem23.Location = New System.Drawing.Point(763, 146)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(214, 26)
        Me.LayoutControlItem23.Text = "Weight"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem24.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem24.Control = Me.txt_name_car
        Me.LayoutControlItem24.Location = New System.Drawing.Point(491, 44)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(272, 24)
        Me.LayoutControlItem24.Text = "บริษัทรถ"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.txt_no_car
        Me.LayoutControlItem4.Location = New System.Drawing.Point(763, 44)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(214, 24)
        Me.LayoutControlItem4.Text = "หมายเลขทะเบียนรถ"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(107, 16)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.slcVoyage
        Me.LayoutControlItem1.Location = New System.Drawing.Point(491, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(486, 44)
        Me.LayoutControlItem1.Text = "Voyage"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(107, 29)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.GridControl1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(491, 185)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem25.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem25.Control = Me.SearchControl2
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(491, 26)
        Me.LayoutControlItem25.Text = "ค้นหา"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(107, 16)
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmedit_gate1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(997, 586)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmedit_gate1"
        Me.Text = "Edit Container"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.SearchControl2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.port_loading.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_insurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.port_discharge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_sealid.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_customer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_container.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_voyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_booking_no.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_customer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_container As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_voyage As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_booking_no As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents TextBox4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_insurance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents port_discharge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_sealid As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colCTNMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONDI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNBOOKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNYARD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNUPGRADE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTRBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEALBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEINBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextBox3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents port_loading As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_no_car As TextBox
    Friend WithEvents txt_name_car As TextBox
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SearchControl2 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents colidborrow As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBOOKID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCARID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEHHMM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEDATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMMUDITY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMARKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINSURANCE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEHHMMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPETHAI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPEPAE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRCHECK1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRCHECK As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRBILLNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
End Class
