﻿Imports DevExpress.XtraPrinting

Public Class frmSearchByBooking
    Dim ykpset As New ykpdtset
    Dim objCLASS As New ContainerClass(ykpset)
    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If slcVoyageStart.EditValue Is Nothing Then
            MsgBox("กรุณาเลือก Voyage")
            Exit Sub
        End If
        If slcVoyageEnd.EditValue Is Nothing Then
            MsgBox("กรุณาเลือก Voyage")
            Exit Sub
        End If
        objCLASS.LoadBookingByvoyage(slcVoyageStart.EditValue, slcVoyageEnd.EditValue)
        GridControl2.DataSource = ykpset.Tables("booking")
    End Sub

    Private Sub frmSearchByBooking_Load(sender As Object, e As EventArgs) Handles Me.Load
        objCLASS.loadVoyage()
        slcVoyageStart.Properties.DataSource = ykpset.Tables("voyage")
        slcVoyageEnd.Properties.DataSource = ykpset.Tables("voyage")

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        Try

            Dim sv As New SaveFileDialog
            sv.Filter = "Excel Workbook|*.xlsx"
            If sv.ShowDialog() = DialogResult.OK And sv.FileName <> Nothing Then
                If sv.FileName.EndsWith(".xlsx") Then
                    Dim path = sv.FileName.ToString()
                    Dim x As New XlsxExportOptionsEx
                    x.AllowGrouping = DevExpress.Utils.DefaultBoolean.False
                    x.AllowFixedColumnHeaderPanel = DevExpress.Utils.DefaultBoolean.False
                    GridControl2.ExportToXlsx(path, x)
                End If


                MessageBox.Show("Data Exported to :" + vbCrLf + sv.FileName, "Business Intelligence Portal", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                sv.FileName = Nothing

            End If
        Catch ex As Exception
        End Try
    End Sub
End Class