﻿Imports DevExpress.XtraGrid.Views.Grid

Public Class frmedit_gate1
    Dim idvoyage As String
    Dim idbooking As String
    Dim idcontainer As String
    Dim idcontainerberfor As String
    Dim agentline As String
    Dim idborrow As String
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset

    Dim objCLASS As New ContainerClass(ykpset)
    Private s1 As FILTERCLASS
    Public Sub LoadVoyage()
        objCLASS.loadVoyage()
        slcVoyage.Properties.DataSource = ykpset.Tables("voyage")

    End Sub
    Public Sub New(ByVal voyageid As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = voyageid
        ' Add any initialization after the InitializeComponent() call.
        loadContainer2()

    End Sub
    Public Sub clearcode()
        Me.idcontainer = ""
        Me.agentline = ""
        Me.idbooking = ""

        Me.txt_booking_no.Text = ""

        Me.txt_customer.Text = ""
        Me.txt_sealid.Text = ""
        Me.TextBox3.Text = ""
        Me.txt_insurance.Text = ""
    End Sub
    Private Sub frmedit_gate1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadCar()
        LoadVoyage()
        slcVoyage.EditValue = idvoyage
    End Sub
    Public Sub loadContainer()
        Dim sql As String
        sql = "Select * , (SELECT bNO FROM booking WHERE BOOKINGID = borrow.BOOKID ) AS BNO from (SELECT * FROM borrow WHERe bookid IN (SELECT bookingid FROM booking WHERE BVOYAGE = '" & idvoyage & "')) borrow   join ctnmain on borrow.CTNID = ctnmain.CTNMAINID "
        ykpset.Tables("borrow").Clear()
        connectDB.GetTable(sql, ykpset.Tables("borrow"))
        GridControl1.DataSource = ykpset.Tables("borrow")
    End Sub
    Public Sub loadCar()
        Dim sql2 As String
        sql2 = "Select idtransport,transportname FROM.mastransport"
        s1 = New FILTERCLASS(txt_name_car, sql2, "ID,Transport Name", "25,120", "1,1", "1,1")

        sql2 = "Select idmascarid,caridname FROM mascarid;"
        Me.s1 = New FILTERCLASS(txt_no_car, sql2, "ID,Transport ID", "25,120", "1,1", "1,1")
    End Sub

    Public Sub loadContainer2()
        objCLASS.loadContainerByStatus(1)
        GridControl3.DataSource = ykpset.Tables("ctnmain")
    End Sub
    Private Sub slcVoyage_EditValueChanged(sender As Object, e As EventArgs) Handles slcVoyage.EditValueChanged
        If slcVoyage.EditValue Is Nothing Then

            Exit Sub
        Else
            idvoyage = slcVoyage.EditValue
            loadContainer()
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim sql As String
        Try
            sql = "UPDATE ctnmain Set CTNSTAT = '1',CTNSEALID = '',CTNWEIGHT ='0' where CTNMAINID = '" & Me.idcontainer & "'; "
            connectDB.ExecuteNonQuery(sql)

            sql = "UPDATE ctnmain SET CTNSTRING  = '" & Me.txt_container.Text & "',CTNWEIGHT ='" & Me.TextBox3.Text & "',CTNSEALID = '" & Me.txt_sealid.Text & "',CTNVOYS = '" & Me.idvoyage & "' ,CTNINS ='" & Me.txt_insurance.Text & "'where CTNMAINID = '" & Me.idcontainerberfor & "'; "
            connectDB.ExecuteNonQuery(sql)


            sql = "UPDATE borrow Set CTNID  = '" & Me.idcontainer & "',COMNAME = '" & Me.txt_name_car.Text & "',CARID = '" & Me.txt_no_car.Text & "',INSURANCE ='" & Me.txt_insurance.Text & "' where idborrow = '" & Me.idborrow & "'; "

            connectDB.ExecuteNonQuery(sql)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        loadContainer2()


    End Sub

    Public Sub LoaadContainter()
        Dim sql As String
        Dim dt As New DataTable
        sql = "Select * from borrow join booking  on  borrow.BOOKID = booking.BOOKINGID join ctnmain on borrow.CTNID = ctnmain.CTNMAINID where CTNID ='" & Me.idborrow & "' ;"
        dt = connectDB.GetTable(sql)

        If dt.Rows.Count > 0 Then
            Me.idcontainerberfor = dt.Rows(0)("CTNID").ToString
            Me.idcontainer = dt.Rows(0)("CTNID")
            Me.txt_name_car.Text = dt.Rows(0)("COMNAME").ToString
            Me.txt_no_car.Text = dt.Rows(0)("CARID").ToString


            Me.txt_insurance.Text = dt.Rows(0)("INSURANCE").ToString
            Me.txt_customer.Text = dt.Rows(0)("BCONSINAME").ToString


            Me.txt_voyage.Text = dt.Rows(0)("BVOYAGE").ToString

            Me.txt_booking_no.Text = dt.Rows(0)("BNO").ToString
            Me.txt_container.Text = dt.Rows(0)("CTNSTRING").ToString
            If (dt.Rows(0)("CTNSEALID") IsNot DBNull.Value) Then
                Me.txt_sealid.Text = dt.Rows(0)("CTNSEALID").ToString
            End If
            If (dt.Rows(0)("CTNSEALID") IsNot DBNull.Value) Then
                Me.TextBox3.Text = dt.Rows(0)("CTNWEIGHT").ToString
            End If
            If (dt.Rows(0)("CTNINS") IsNot DBNull.Value) Then
                Me.txt_insurance.Text = dt.Rows(0)("CTNINS").ToString
            End If
            Me.TextBox1.Text = dt.Rows(0)("BCTNTYPETHAI").ToString
            Me.TextBox4.Text = dt.Rows(0)("BCTNTYPEPAE").ToString

        Else

        End If


    End Sub

    Private Sub GridView1_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView1.RowCellClick
        Me.idcontainerberfor = GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString
        Me.idcontainer = GridView1.GetDataRow(GridView1.FocusedRowHandle)("CTNID").ToString
        Me.idborrow = GridView1.GetDataRow(GridView1.FocusedRowHandle)("idborrow").ToString
        LoaadContainter()
    End Sub

    Private Sub GridView3_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView3.RowCellClick
        Me.idcontainer = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNMAINID").ToString
        Me.txt_container.Text = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSTRING").ToString

        idcontainer = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNMAINID").ToString
        txt_container.Text = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSTRING").ToString
        TextBox1.Text = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSIZE").ToString
        agentline = GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNAGENT").ToString

        If Trim(GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSIZE").ToString) = "40'HC" Then
            TextBox4.Text = "45G1"
        ElseIf Trim(GridView3.GetDataRow(GridView3.FocusedRowHandle)("CTNSIZE").ToString) = "20'GP" Then
            TextBox4.Text = "22G1"
        End If

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs)

    End Sub
End Class