﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmViewvoyage
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm
    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewvoyage))
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridFormatRule15 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression15 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule16 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression16 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule17 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression17 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule18 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression18 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule19 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression19 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule20 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression20 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule21 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression21 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule22 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression22 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule23 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression23 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule24 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression24 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule25 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression25 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule26 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression26 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule27 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression27 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Dim GridFormatRule28 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleExpression28 As DevExpress.XtraEditors.FormatConditionRuleExpression = New DevExpress.XtraEditors.FormatConditionRuleExpression()
        Me.colCTNMARKSTRBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEALBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEDATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEINBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnExportPdf = New DevExpress.XtraBars.BarButtonItem()
        Me.btnExportExcel = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem9 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem10 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem11 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem12 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem13 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarHeaderItem1 = New DevExpress.XtraBars.BarHeaderItem()
        Me.BarEditItem1 = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.BarEditItem2 = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.BarButtonItem14 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem15 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem16 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnLoading = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem18 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem17 = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ย้ายตู๊ = New System.Windows.Forms.ToolStripMenuItem()
        Me.GateInOutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UpgradeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NormalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarkContainerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarkSealToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONDI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNBOOKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNYARD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNUPGRADE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNTAREWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCARID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEHHMM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMARKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colINSURANCE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidborrow = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTIMEHHMMIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPETHAI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPEPAE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRCHECK1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRCHECK = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRBILLNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNREALDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNREALDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnCTNborrow = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.txtWeight = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.RepositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnSailOut = New DevExpress.XtraEditors.SimpleButton()
        Me.txtSailOutTime = New DevExpress.XtraEditors.TextEdit()
        Me.txtSailOutdate = New DevExpress.XtraEditors.TextEdit()
        Me.btnClosing = New DevExpress.XtraEditors.SimpleButton()
        Me.txtClosingTime = New DevExpress.XtraEditors.TextEdit()
        Me.txtClosingDate = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtUp = New DevExpress.XtraEditors.TextEdit()
        Me.txtSealID = New DevExpress.XtraEditors.TextEdit()
        Me.txtDown = New DevExpress.XtraEditors.TextEdit()
        Me.txtSealID1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtUp1 = New DevExpress.XtraEditors.TextEdit()
        Me.txtDown1 = New DevExpress.XtraEditors.TextEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TBABookingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnBookingDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNFORWARDERNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONFIRMTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCONFIRMDAY = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBINVOICE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_eta_penang = New DevExpress.XtraEditors.TextEdit()
        Me.txt_eta = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox1 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_gross = New DevExpress.XtraEditors.TextEdit()
        Me.txt_from = New DevExpress.XtraEditors.TextEdit()
        Me.txt_namemaster = New DevExpress.XtraEditors.TextEdit()
        Me.txt_nett = New DevExpress.XtraEditors.TextEdit()
        Me.txt_nationality = New DevExpress.XtraEditors.TextEdit()
        Me.txt_vessel = New DevExpress.XtraEditors.TextEdit()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.VoyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip2.SuspendLayout()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCTNborrow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtWeight, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSailOutTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSailOutdate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClosingTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClosingDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSealID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDown.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSealID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUp1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDown1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnBookingDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_eta_penang.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_eta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_gross.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_from.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_namemaster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nett.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nationality.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_vessel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VoyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colCTNMARKSTRBOO
        '
        Me.colCTNMARKSTRBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSTRBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTRBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSTRBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTRBOO.FieldName = "CTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.Name = "colCTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSTRBOO.OptionsColumn.ReadOnly = True
        Me.colCTNMARKSTRBOO.Width = 54
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.Caption = "Container No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.OptionsColumn.AllowEdit = False
        Me.colCTNSTRING.OptionsColumn.AllowFocus = False
        Me.colCTNSTRING.OptionsColumn.FixedWidth = True
        Me.colCTNSTRING.OptionsColumn.ReadOnly = True
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 0
        Me.colCTNSTRING.Width = 90
        '
        'colCTNMARKSEALBOO
        '
        Me.colCTNMARKSEALBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSEALBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEALBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSEALBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEALBOO.FieldName = "CTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.Name = "colCTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSEALBOO.OptionsColumn.ReadOnly = True
        Me.colCTNMARKSEALBOO.Width = 31
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSEALID.AppearanceCell.Options.UseFont = True
        Me.colCTNSEALID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSEALID.AppearanceHeader.Options.UseFont = True
        Me.colCTNSEALID.Caption = "Seal ID"
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.OptionsColumn.FixedWidth = True
        Me.colCTNSEALID.Visible = True
        Me.colCTNSEALID.VisibleIndex = 1
        Me.colCTNSEALID.Width = 90
        '
        'colCTNMARKDATEIN
        '
        Me.colCTNMARKDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEIN.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEIN.FieldName = "CTNMARKDATEIN"
        Me.colCTNMARKDATEIN.Name = "colCTNMARKDATEIN"
        Me.colCTNMARKDATEIN.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEIN.OptionsColumn.ReadOnly = True
        Me.colCTNMARKDATEIN.Width = 31
        '
        'colTIMEDATEIN
        '
        Me.colTIMEDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEDATEIN.AppearanceCell.Options.UseFont = True
        Me.colTIMEDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colTIMEDATEIN.Caption = "Date In"
        Me.colTIMEDATEIN.FieldName = "TIMEDATEIN"
        Me.colTIMEDATEIN.Name = "colTIMEDATEIN"
        Me.colTIMEDATEIN.OptionsColumn.FixedWidth = True
        Me.colTIMEDATEIN.Visible = True
        Me.colTIMEDATEIN.VisibleIndex = 7
        Me.colTIMEDATEIN.Width = 80
        '
        'colCTNMARKDATEOUT
        '
        Me.colCTNMARKDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUT.FieldName = "CTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.Name = "colCTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEOUT.OptionsColumn.ReadOnly = True
        Me.colCTNMARKDATEOUT.Width = 31
        '
        'colTIMEDATE
        '
        Me.colTIMEDATE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEDATE.AppearanceCell.Options.UseFont = True
        Me.colTIMEDATE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEDATE.AppearanceHeader.Options.UseFont = True
        Me.colTIMEDATE.Caption = "Date Out"
        Me.colTIMEDATE.FieldName = "TIMEDATE"
        Me.colTIMEDATE.Name = "colTIMEDATE"
        Me.colTIMEDATE.OptionsColumn.FixedWidth = True
        Me.colTIMEDATE.Visible = True
        Me.colTIMEDATE.VisibleIndex = 4
        Me.colTIMEDATE.Width = 80
        '
        'colCTNMARKWEIGHT
        '
        Me.colCTNMARKWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHT.FieldName = "CTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.Name = "colCTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKWEIGHT.OptionsColumn.ReadOnly = True
        Me.colCTNMARKWEIGHT.Width = 31
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNWEIGHT.Caption = "Weight"
        Me.colCTNWEIGHT.DisplayFormat.FormatString = "##,###.00"
        Me.colCTNWEIGHT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.Visible = True
        Me.colCTNWEIGHT.VisibleIndex = 11
        '
        'colSTAT
        '
        Me.colSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colSTAT.AppearanceCell.Options.UseFont = True
        Me.colSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colSTAT.AppearanceHeader.Options.UseFont = True
        Me.colSTAT.FieldName = "STAT"
        Me.colSTAT.Name = "colSTAT"
        Me.colSTAT.OptionsColumn.AllowEdit = False
        Me.colSTAT.OptionsColumn.ReadOnly = True
        Me.colSTAT.Visible = True
        Me.colSTAT.VisibleIndex = 10
        Me.colSTAT.Width = 36
        '
        'colCTNMARKTRANSPORT
        '
        Me.colCTNMARKTRANSPORT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKTRANSPORT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.FieldName = "CTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.Name = "colCTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.OptionsColumn.AllowEdit = False
        Me.colCTNMARKTRANSPORT.OptionsColumn.ReadOnly = True
        Me.colCTNMARKTRANSPORT.Width = 31
        '
        'colCOMNAME
        '
        Me.colCOMNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCOMNAME.AppearanceCell.Options.UseFont = True
        Me.colCOMNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCOMNAME.AppearanceHeader.Options.UseFont = True
        Me.colCOMNAME.Caption = "Transport"
        Me.colCOMNAME.FieldName = "COMNAME"
        Me.colCOMNAME.Name = "colCOMNAME"
        Me.colCOMNAME.OptionsColumn.FixedWidth = True
        Me.colCOMNAME.Visible = True
        Me.colCOMNAME.VisibleIndex = 13
        Me.colCOMNAME.Width = 80
        '
        'colCTNMARKDATEINBOO
        '
        Me.colCTNMARKDATEINBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEINBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.FieldName = "CTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.Name = "colCTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEINBOO.OptionsColumn.ReadOnly = True
        Me.colCTNMARKDATEINBOO.Width = 31
        '
        'colCTNMARKDATEOUTBOO
        '
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.FieldName = "CTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.Name = "colCTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKDATEOUTBOO.OptionsColumn.ReadOnly = True
        Me.colCTNMARKDATEOUTBOO.Width = 31
        '
        'colCTNMARKWEIGHTBOO
        '
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.FieldName = "CTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.Name = "colCTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKWEIGHTBOO.OptionsColumn.ReadOnly = True
        Me.colCTNMARKWEIGHTBOO.Width = 31
        '
        'RibbonControl1
        '
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.BarButtonItem1, Me.BarButtonItem4, Me.BarButtonItem5, Me.btnExportPdf, Me.btnExportExcel, Me.BarButtonItem2, Me.BarButtonItem3, Me.BarButtonItem6, Me.BarButtonItem7, Me.BarButtonItem8, Me.BarButtonItem9, Me.BarButtonItem10, Me.BarButtonItem11, Me.BarButtonItem12, Me.BarButtonItem13, Me.BarHeaderItem1, Me.BarEditItem1, Me.BarEditItem2, Me.BarButtonItem14, Me.BarButtonItem15, Me.BarButtonItem16, Me.btnLoading, Me.BarButtonItem18, Me.BarButtonItem17})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 27
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage1})
        Me.RibbonControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2})
        Me.RibbonControl1.Size = New System.Drawing.Size(1501, 143)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Export to Excel"
        Me.BarButtonItem1.Id = 1
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Add Container"
        Me.BarButtonItem4.Id = 4
        Me.BarButtonItem4.ImageOptions.Image = CType(resources.GetObject("BarButtonItem4.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Edit Container"
        Me.BarButtonItem5.Id = 5
        Me.BarButtonItem5.ImageOptions.Image = CType(resources.GetObject("BarButtonItem5.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'btnExportPdf
        '
        Me.btnExportPdf.Caption = "Mange Booking"
        Me.btnExportPdf.Id = 6
        Me.btnExportPdf.ImageOptions.Image = CType(resources.GetObject("btnExportPdf.ImageOptions.Image"), System.Drawing.Image)
        Me.btnExportPdf.Name = "btnExportPdf"
        '
        'btnExportExcel
        '
        Me.btnExportExcel.Caption = "Edit booking"
        Me.btnExportExcel.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.btnExportExcel.Id = 7
        Me.btnExportExcel.Name = "btnExportExcel"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Move Booking"
        Me.BarButtonItem2.Id = 8
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Delete Container"
        Me.BarButtonItem3.Id = 9
        Me.BarButtonItem3.ImageOptions.Image = CType(resources.GetObject("BarButtonItem3.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Create Invoice"
        Me.BarButtonItem6.Id = 10
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "ย้าย Booking "
        Me.BarButtonItem7.Id = 11
        Me.BarButtonItem7.ImageOptions.Image = CType(resources.GetObject("BarButtonItem7.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "Container Movement"
        Me.BarButtonItem8.Id = 12
        Me.BarButtonItem8.ImageOptions.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'BarButtonItem9
        '
        Me.BarButtonItem9.Caption = "BL Report"
        Me.BarButtonItem9.Id = 13
        Me.BarButtonItem9.ImageOptions.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.BarButtonItem9.Name = "BarButtonItem9"
        '
        'BarButtonItem10
        '
        Me.BarButtonItem10.Caption = "YKP Ocean"
        Me.BarButtonItem10.Id = 14
        Me.BarButtonItem10.ImageOptions.Image = Global.YKP_SYSTEM.My.Resources.Resources.blogs
        Me.BarButtonItem10.Name = "BarButtonItem10"
        '
        'BarButtonItem11
        '
        Me.BarButtonItem11.Caption = "Manage Booking"
        Me.BarButtonItem11.Id = 15
        Me.BarButtonItem11.Name = "BarButtonItem11"
        '
        'BarButtonItem12
        '
        Me.BarButtonItem12.Caption = "Move Booking"
        Me.BarButtonItem12.Id = 16
        Me.BarButtonItem12.Name = "BarButtonItem12"
        '
        'BarButtonItem13
        '
        Me.BarButtonItem13.Caption = "Search Container"
        Me.BarButtonItem13.Id = 17
        Me.BarButtonItem13.ImageOptions.Image = CType(resources.GetObject("BarButtonItem13.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem13.Name = "BarButtonItem13"
        '
        'BarHeaderItem1
        '
        Me.BarHeaderItem1.Caption = "Closing Date"
        Me.BarHeaderItem1.Id = 18
        Me.BarHeaderItem1.Name = "BarHeaderItem1"
        '
        'BarEditItem1
        '
        Me.BarEditItem1.Edit = Me.RepositoryItemTextEdit1
        Me.BarEditItem1.Id = 19
        Me.BarEditItem1.Name = "BarEditItem1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'BarEditItem2
        '
        Me.BarEditItem2.Caption = "BarEditItem2"
        Me.BarEditItem2.Edit = Me.RepositoryItemTextEdit2
        Me.BarEditItem2.Id = 20
        Me.BarEditItem2.Name = "BarEditItem2"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'BarButtonItem14
        '
        Me.BarButtonItem14.Caption = "Gate In"
        Me.BarButtonItem14.Id = 21
        Me.BarButtonItem14.ImageOptions.Image = CType(resources.GetObject("BarButtonItem14.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem14.Name = "BarButtonItem14"
        '
        'BarButtonItem15
        '
        Me.BarButtonItem15.Caption = "Gate Out"
        Me.BarButtonItem15.Id = 22
        Me.BarButtonItem15.ImageOptions.Image = CType(resources.GetObject("BarButtonItem15.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem15.Name = "BarButtonItem15"
        '
        'BarButtonItem16
        '
        Me.BarButtonItem16.Caption = "BarButtonItem16"
        Me.BarButtonItem16.Id = 23
        Me.BarButtonItem16.Name = "BarButtonItem16"
        '
        'btnLoading
        '
        Me.btnLoading.Caption = "Loading"
        Me.btnLoading.Id = 24
        Me.btnLoading.ImageOptions.Image = CType(resources.GetObject("btnLoading.ImageOptions.Image"), System.Drawing.Image)
        Me.btnLoading.Name = "btnLoading"
        '
        'BarButtonItem18
        '
        Me.BarButtonItem18.Caption = "LoadingETD"
        Me.BarButtonItem18.Id = 25
        Me.BarButtonItem18.ImageOptions.Image = CType(resources.GetObject("BarButtonItem18.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem18.Name = "BarButtonItem18"
        '
        'BarButtonItem17
        '
        Me.BarButtonItem17.Caption = "Search Booking"
        Me.BarButtonItem17.Id = 26
        Me.BarButtonItem17.Name = "BarButtonItem17"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.RibbonPage1.Appearance.Options.UseBackColor = True
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup2, Me.RibbonPageGroup3, Me.RibbonPageGroup5})
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Export"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnExportPdf)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem7)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Management Booking"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem4)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem5)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem3)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Container"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BarButtonItem6)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BarButtonItem13)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BarButtonItem17)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Invoice"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem8)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem9)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.btnLoading)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem18)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "Report"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton5)
        Me.LayoutControl1.Controls.Add(Me.btnSailOut)
        Me.LayoutControl1.Controls.Add(Me.txtSailOutTime)
        Me.LayoutControl1.Controls.Add(Me.txtSailOutdate)
        Me.LayoutControl1.Controls.Add(Me.btnClosing)
        Me.LayoutControl1.Controls.Add(Me.txtClosingTime)
        Me.LayoutControl1.Controls.Add(Me.txtClosingDate)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.txtUp)
        Me.LayoutControl1.Controls.Add(Me.txtSealID)
        Me.LayoutControl1.Controls.Add(Me.txtDown)
        Me.LayoutControl1.Controls.Add(Me.txtSealID1)
        Me.LayoutControl1.Controls.Add(Me.txtUp1)
        Me.LayoutControl1.Controls.Add(Me.txtDown1)
        Me.LayoutControl1.Controls.Add(Me.MemoEdit1)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.txt_eta_penang)
        Me.LayoutControl1.Controls.Add(Me.txt_eta)
        Me.LayoutControl1.Controls.Add(Me.TextBox2)
        Me.LayoutControl1.Controls.Add(Me.TextBox1)
        Me.LayoutControl1.Controls.Add(Me.txt_gross)
        Me.LayoutControl1.Controls.Add(Me.txt_from)
        Me.LayoutControl1.Controls.Add(Me.txt_namemaster)
        Me.LayoutControl1.Controls.Add(Me.txt_nett)
        Me.LayoutControl1.Controls.Add(Me.txt_nationality)
        Me.LayoutControl1.Controls.Add(Me.txt_vessel)
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 143)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(419, 317, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1501, 652)
        Me.LayoutControl1.TabIndex = 1
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(1329, 592)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(160, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 227
        Me.SimpleButton3.Text = "Save All"
        '
        'GridControl1
        '
        Me.GridControl1.ContextMenuStrip = Me.ContextMenuStrip2
        Me.GridControl1.DataMember = "ctnmainborow"
        Me.GridControl1.DataSource = Me.Ykpdtset3
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl1.Location = New System.Drawing.Point(584, 208)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.MenuManager = Me.RibbonControl1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnCTNborrow, Me.txtWeight, Me.RepositoryItemTextEdit3, Me.RepositoryItemCheckEdit1, Me.RepositoryItemCheckEdit2})
        Me.GridControl1.Size = New System.Drawing.Size(893, 368)
        Me.GridControl1.TabIndex = 226
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ย้ายตู๊, Me.GateInOutToolStripMenuItem, Me.UpgradeToolStripMenuItem, Me.NormalToolStripMenuItem, Me.MarkContainerToolStripMenuItem, Me.MarkSealToolStripMenuItem})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(157, 136)
        '
        'ย้ายตู๊
        '
        Me.ย้ายตู๊.Name = "ย้ายตู๊"
        Me.ย้ายตู๊.Size = New System.Drawing.Size(156, 22)
        Me.ย้ายตู๊.Text = "ย้ายตู๊"
        '
        'GateInOutToolStripMenuItem
        '
        Me.GateInOutToolStripMenuItem.Name = "GateInOutToolStripMenuItem"
        Me.GateInOutToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.GateInOutToolStripMenuItem.Text = "Gate In / Out"
        '
        'UpgradeToolStripMenuItem
        '
        Me.UpgradeToolStripMenuItem.Name = "UpgradeToolStripMenuItem"
        Me.UpgradeToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.UpgradeToolStripMenuItem.Text = "Upgrade"
        '
        'NormalToolStripMenuItem
        '
        Me.NormalToolStripMenuItem.Name = "NormalToolStripMenuItem"
        Me.NormalToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.NormalToolStripMenuItem.Text = "Normal"
        '
        'MarkContainerToolStripMenuItem
        '
        Me.MarkContainerToolStripMenuItem.Name = "MarkContainerToolStripMenuItem"
        Me.MarkContainerToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.MarkContainerToolStripMenuItem.Text = "Mark Container"
        '
        'MarkSealToolStripMenuItem
        '
        Me.MarkSealToolStripMenuItem.Name = "MarkSealToolStripMenuItem"
        Me.MarkSealToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.MarkSealToolStripMenuItem.Text = "Mark Seal"
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNMAINID, Me.colCTNSTRING, Me.colCTNSEALID, Me.colCTNAGENT, Me.colCTNSIZE, Me.colCTNCONDI, Me.colCTNSTAT, Me.colCTNCONSI, Me.colCTNVOYN, Me.colCTNVOYS, Me.colCTNWEIGHT, Me.colCTNINS, Me.colCTNSHIPNAME, Me.colCTNDATEOUT, Me.colCTNBOOKING, Me.colCTNFORWARDER, Me.colCTNYARD, Me.colCTNUPGRADE, Me.colCTNMARKSTR, Me.colCTNMARKSEAL, Me.colCTNMARKSTRBOO, Me.colCTNMARKSEALBOO, Me.colCTNMARKDATEIN, Me.colCTNMARKDATEINBOO, Me.colCOMNAME, Me.colCTNMARKDATEOUT, Me.colCTNMARKDATEOUTBOO, Me.colCTNTAREWEIGHT, Me.colCTNMARKWEIGHT, Me.colSTAT, Me.colCTNMARKWEIGHTBOO, Me.colCTNMARKTRANSPORT, Me.colCTNMARKTRANSPORTBOO, Me.colCARID, Me.colTIMEHHMM, Me.colTIMEDATE, Me.colMARKING, Me.colINSURANCE, Me.colTIMEDATEIN, Me.colidborrow, Me.colTIMEHHMMIN, Me.colBCTNTYPETHAI, Me.colBCTNTYPEPAE, Me.colBRCHECK1, Me.colBRCHECK, Me.colBRBILLNAME, Me.colBNO, Me.colCTNREALDATEIN, Me.colCTNREALDATEOUT, Me.GridColumn9})
        GridFormatRule15.Column = Me.colCTNMARKSTRBOO
        GridFormatRule15.ColumnApplyTo = Me.colCTNSTRING
        GridFormatRule15.Name = "CTNSTRING"
        FormatConditionRuleExpression15.Appearance.BackColor = System.Drawing.Color.DarkOrange
        FormatConditionRuleExpression15.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression15.Expression = "[CTNMARKSTRBOO] = '1'"
        GridFormatRule15.Rule = FormatConditionRuleExpression15
        GridFormatRule16.Column = Me.colCTNMARKSEALBOO
        GridFormatRule16.ColumnApplyTo = Me.colCTNSEALID
        GridFormatRule16.Name = "CTNSEAL"
        FormatConditionRuleExpression16.Appearance.BackColor = System.Drawing.Color.DarkOrange
        FormatConditionRuleExpression16.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression16.Expression = "[CTNMARKSEALBOO] = '1'"
        FormatConditionRuleExpression16.PredefinedName = "Red Fill"
        GridFormatRule16.Rule = FormatConditionRuleExpression16
        GridFormatRule17.Column = Me.colCTNMARKDATEIN
        GridFormatRule17.ColumnApplyTo = Me.colTIMEDATEIN
        GridFormatRule17.Name = "CTNDATEIN"
        FormatConditionRuleExpression17.Appearance.BackColor = System.Drawing.Color.DarkOrange
        FormatConditionRuleExpression17.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression17.Expression = "[CTNMARKDATEINBOO] = '1'"
        GridFormatRule17.Rule = FormatConditionRuleExpression17
        GridFormatRule18.Column = Me.colCTNMARKDATEOUT
        GridFormatRule18.ColumnApplyTo = Me.colTIMEDATE
        GridFormatRule18.Name = "CTNDATEOUT"
        FormatConditionRuleExpression18.Appearance.BackColor = System.Drawing.Color.DarkOrange
        FormatConditionRuleExpression18.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression18.Expression = "[CTNMARKDATEOUTBOO] = '1'"
        GridFormatRule18.Rule = FormatConditionRuleExpression18
        GridFormatRule19.Column = Me.colCTNMARKWEIGHT
        GridFormatRule19.ColumnApplyTo = Me.colCTNWEIGHT
        GridFormatRule19.Name = "CTNWEIGHT"
        FormatConditionRuleExpression19.Appearance.BackColor = System.Drawing.Color.DarkOrange
        FormatConditionRuleExpression19.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression19.Expression = "[CTNMARKWEIGHTBOO] = '1'"
        GridFormatRule19.Rule = FormatConditionRuleExpression19
        GridFormatRule20.Column = Me.colSTAT
        GridFormatRule20.ColumnApplyTo = Me.colSTAT
        GridFormatRule20.Name = "Format0"
        FormatConditionRuleExpression20.Appearance.BackColor = System.Drawing.Color.DeepSkyBlue
        FormatConditionRuleExpression20.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression20.Expression = "[BRCHECK_] = True And [BRCHECK1_] = True"
        GridFormatRule20.Rule = FormatConditionRuleExpression20
        GridFormatRule21.Column = Me.colSTAT
        GridFormatRule21.ColumnApplyTo = Me.colSTAT
        GridFormatRule21.Name = "Format1"
        FormatConditionRuleExpression21.Appearance.BackColor = System.Drawing.Color.Orange
        FormatConditionRuleExpression21.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression21.Expression = "[BRCHECK_] = True And [BRCHECK1_] = False"
        GridFormatRule21.Rule = FormatConditionRuleExpression21
        GridFormatRule22.Column = Me.colCTNMARKSTRBOO
        GridFormatRule22.ColumnApplyTo = Me.colCTNSTRING
        GridFormatRule22.Name = "CTNSTRING2"
        FormatConditionRuleExpression22.Appearance.BackColor = System.Drawing.Color.LawnGreen
        FormatConditionRuleExpression22.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression22.Expression = "[CTNMARKSTRBOO] = '0'"
        FormatConditionRuleExpression22.PredefinedName = "Green Fill"
        GridFormatRule22.Rule = FormatConditionRuleExpression22
        GridFormatRule23.Column = Me.colCTNMARKSEALBOO
        GridFormatRule23.ColumnApplyTo = Me.colCTNSEALID
        GridFormatRule23.Name = "CTNSEAL2"
        FormatConditionRuleExpression23.Appearance.BackColor = System.Drawing.Color.LawnGreen
        FormatConditionRuleExpression23.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression23.Expression = "[CTNMARKSEALBOO] = '0'"
        FormatConditionRuleExpression23.PredefinedName = "Green Fill"
        GridFormatRule23.Rule = FormatConditionRuleExpression23
        GridFormatRule24.Column = Me.colCTNMARKDATEIN
        GridFormatRule24.ColumnApplyTo = Me.colTIMEDATEIN
        GridFormatRule24.Name = "CTNDATEIN2"
        FormatConditionRuleExpression24.Appearance.BackColor = System.Drawing.Color.LawnGreen
        FormatConditionRuleExpression24.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression24.Expression = "[CTNMARKDATEINBOO] = '0'"
        FormatConditionRuleExpression24.PredefinedName = "Green Fill"
        GridFormatRule24.Rule = FormatConditionRuleExpression24
        GridFormatRule25.Column = Me.colCTNMARKDATEOUT
        GridFormatRule25.ColumnApplyTo = Me.colTIMEDATE
        GridFormatRule25.Name = "CTNDATEOUT2"
        FormatConditionRuleExpression25.Appearance.BackColor = System.Drawing.Color.LawnGreen
        FormatConditionRuleExpression25.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression25.Expression = "[CTNMARKDATEOUTBOO] = '0'"
        FormatConditionRuleExpression25.PredefinedName = "Green Fill"
        GridFormatRule25.Rule = FormatConditionRuleExpression25
        GridFormatRule26.Column = Me.colCTNMARKWEIGHT
        GridFormatRule26.ColumnApplyTo = Me.colCTNWEIGHT
        GridFormatRule26.Name = "CTNWEIGHT2"
        FormatConditionRuleExpression26.Appearance.BackColor = System.Drawing.Color.LawnGreen
        FormatConditionRuleExpression26.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression26.Expression = "[CTNMARKWEIGHTBOO] = '0'"
        FormatConditionRuleExpression26.PredefinedName = "Green Fill"
        GridFormatRule26.Rule = FormatConditionRuleExpression26
        GridFormatRule27.Column = Me.colCTNMARKTRANSPORT
        GridFormatRule27.ColumnApplyTo = Me.colCOMNAME
        GridFormatRule27.Name = "CTNTRAN"
        FormatConditionRuleExpression27.Appearance.BackColor = System.Drawing.Color.DarkOrange
        FormatConditionRuleExpression27.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression27.Expression = "[CTNMARKTRANSPORTBOO] = '1'"
        GridFormatRule27.Rule = FormatConditionRuleExpression27
        GridFormatRule28.Column = Me.colCTNMARKTRANSPORT
        GridFormatRule28.ColumnApplyTo = Me.colCOMNAME
        GridFormatRule28.Name = "CTNTRAN2"
        FormatConditionRuleExpression28.Appearance.BackColor = System.Drawing.Color.LawnGreen
        FormatConditionRuleExpression28.Appearance.Options.UseBackColor = True
        FormatConditionRuleExpression28.Expression = "[CTNMARKTRANSPORTBOO] = '0'"
        GridFormatRule28.Rule = FormatConditionRuleExpression28
        Me.GridView1.FormatRules.Add(GridFormatRule15)
        Me.GridView1.FormatRules.Add(GridFormatRule16)
        Me.GridView1.FormatRules.Add(GridFormatRule17)
        Me.GridView1.FormatRules.Add(GridFormatRule18)
        Me.GridView1.FormatRules.Add(GridFormatRule19)
        Me.GridView1.FormatRules.Add(GridFormatRule20)
        Me.GridView1.FormatRules.Add(GridFormatRule21)
        Me.GridView1.FormatRules.Add(GridFormatRule22)
        Me.GridView1.FormatRules.Add(GridFormatRule23)
        Me.GridView1.FormatRules.Add(GridFormatRule24)
        Me.GridView1.FormatRules.Add(GridFormatRule25)
        Me.GridView1.FormatRules.Add(GridFormatRule26)
        Me.GridView1.FormatRules.Add(GridFormatRule27)
        Me.GridView1.FormatRules.Add(GridFormatRule28)
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.IndicatorWidth = 25
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCOMNAME, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCTNMAINID
        '
        Me.colCTNMAINID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMAINID.AppearanceCell.Options.UseFont = True
        Me.colCTNMAINID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMAINID.AppearanceHeader.Options.UseFont = True
        Me.colCTNMAINID.FieldName = "CTNMAINID"
        Me.colCTNMAINID.Name = "colCTNMAINID"
        Me.colCTNMAINID.OptionsColumn.AllowEdit = False
        Me.colCTNMAINID.OptionsColumn.ReadOnly = True
        Me.colCTNMAINID.Width = 36
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.Caption = "Agent"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.OptionsColumn.AllowEdit = False
        Me.colCTNAGENT.OptionsColumn.AllowFocus = False
        Me.colCTNAGENT.OptionsColumn.FixedWidth = True
        Me.colCTNAGENT.OptionsColumn.ReadOnly = True
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 2
        Me.colCTNAGENT.Width = 45
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSIZE.AppearanceCell.Options.UseFont = True
        Me.colCTNSIZE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSIZE.AppearanceHeader.Options.UseFont = True
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.OptionsColumn.AllowEdit = False
        Me.colCTNSIZE.OptionsColumn.ReadOnly = True
        Me.colCTNSIZE.Width = 51
        '
        'colCTNCONDI
        '
        Me.colCTNCONDI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNCONDI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONDI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNCONDI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONDI.FieldName = "CTNCONDI"
        Me.colCTNCONDI.Name = "colCTNCONDI"
        Me.colCTNCONDI.OptionsColumn.AllowEdit = False
        Me.colCTNCONDI.OptionsColumn.ReadOnly = True
        Me.colCTNCONDI.Width = 65
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSTAT.AppearanceCell.Options.UseFont = True
        Me.colCTNSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSTAT.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        Me.colCTNSTAT.OptionsColumn.AllowEdit = False
        Me.colCTNSTAT.OptionsColumn.ReadOnly = True
        Me.colCTNSTAT.Width = 72
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNCONSI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNCONSI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        Me.colCTNCONSI.OptionsColumn.AllowEdit = False
        Me.colCTNCONSI.OptionsColumn.ReadOnly = True
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNVOYN.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNVOYN.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYN.Caption = "Voy N"
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        Me.colCTNVOYN.OptionsColumn.AllowEdit = False
        Me.colCTNVOYN.OptionsColumn.ReadOnly = True
        Me.colCTNVOYN.Visible = True
        Me.colCTNVOYN.VisibleIndex = 3
        Me.colCTNVOYN.Width = 56
        '
        'colCTNVOYS
        '
        Me.colCTNVOYS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNVOYS.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNVOYS.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYS.FieldName = "CTNVOYS"
        Me.colCTNVOYS.Name = "colCTNVOYS"
        Me.colCTNVOYS.OptionsColumn.AllowEdit = False
        Me.colCTNVOYS.OptionsColumn.ReadOnly = True
        Me.colCTNVOYS.Width = 92
        '
        'colCTNINS
        '
        Me.colCTNINS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNINS.AppearanceCell.Options.UseFont = True
        Me.colCTNINS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNINS.AppearanceHeader.Options.UseFont = True
        Me.colCTNINS.FieldName = "CTNINS"
        Me.colCTNINS.Name = "colCTNINS"
        Me.colCTNINS.OptionsColumn.AllowEdit = False
        Me.colCTNINS.OptionsColumn.ReadOnly = True
        Me.colCTNINS.Width = 31
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colCTNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        Me.colCTNSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colCTNSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colCTNSHIPNAME.Width = 31
        '
        'colCTNDATEOUT
        '
        Me.colCTNDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNDATEOUT.FieldName = "CTNDATEOUT"
        Me.colCTNDATEOUT.Name = "colCTNDATEOUT"
        Me.colCTNDATEOUT.OptionsColumn.AllowEdit = False
        Me.colCTNDATEOUT.OptionsColumn.ReadOnly = True
        Me.colCTNDATEOUT.Width = 96
        '
        'colCTNBOOKING
        '
        Me.colCTNBOOKING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNBOOKING.AppearanceCell.Options.UseFont = True
        Me.colCTNBOOKING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNBOOKING.AppearanceHeader.Options.UseFont = True
        Me.colCTNBOOKING.FieldName = "CTNBOOKING"
        Me.colCTNBOOKING.Name = "colCTNBOOKING"
        Me.colCTNBOOKING.OptionsColumn.AllowEdit = False
        Me.colCTNBOOKING.OptionsColumn.ReadOnly = True
        Me.colCTNBOOKING.Width = 31
        '
        'colCTNFORWARDER
        '
        Me.colCTNFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colCTNFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colCTNFORWARDER.FieldName = "CTNFORWARDER"
        Me.colCTNFORWARDER.Name = "colCTNFORWARDER"
        Me.colCTNFORWARDER.OptionsColumn.AllowEdit = False
        Me.colCTNFORWARDER.OptionsColumn.ReadOnly = True
        Me.colCTNFORWARDER.Width = 31
        '
        'colCTNYARD
        '
        Me.colCTNYARD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNYARD.AppearanceCell.Options.UseFont = True
        Me.colCTNYARD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNYARD.AppearanceHeader.Options.UseFont = True
        Me.colCTNYARD.FieldName = "CTNYARD"
        Me.colCTNYARD.Name = "colCTNYARD"
        Me.colCTNYARD.OptionsColumn.AllowEdit = False
        Me.colCTNYARD.OptionsColumn.ReadOnly = True
        Me.colCTNYARD.Width = 31
        '
        'colCTNUPGRADE
        '
        Me.colCTNUPGRADE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNUPGRADE.AppearanceCell.Options.UseFont = True
        Me.colCTNUPGRADE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNUPGRADE.AppearanceHeader.Options.UseFont = True
        Me.colCTNUPGRADE.FieldName = "CTNUPGRADE"
        Me.colCTNUPGRADE.Name = "colCTNUPGRADE"
        Me.colCTNUPGRADE.OptionsColumn.AllowEdit = False
        Me.colCTNUPGRADE.OptionsColumn.ReadOnly = True
        Me.colCTNUPGRADE.Width = 31
        '
        'colCTNMARKSTR
        '
        Me.colCTNMARKSTR.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSTR.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTR.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSTR.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTR.FieldName = "CTNMARKSTR"
        Me.colCTNMARKSTR.Name = "colCTNMARKSTR"
        Me.colCTNMARKSTR.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSTR.OptionsColumn.ReadOnly = True
        Me.colCTNMARKSTR.Width = 78
        '
        'colCTNMARKSEAL
        '
        Me.colCTNMARKSEAL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSEAL.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEAL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKSEAL.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEAL.FieldName = "CTNMARKSEAL"
        Me.colCTNMARKSEAL.Name = "colCTNMARKSEAL"
        Me.colCTNMARKSEAL.OptionsColumn.AllowEdit = False
        Me.colCTNMARKSEAL.OptionsColumn.ReadOnly = True
        Me.colCTNMARKSEAL.Width = 31
        '
        'colCTNTAREWEIGHT
        '
        Me.colCTNTAREWEIGHT.Caption = "TAREWEIGHT"
        Me.colCTNTAREWEIGHT.FieldName = "CTNTAREWEIGHT"
        Me.colCTNTAREWEIGHT.Name = "colCTNTAREWEIGHT"
        Me.colCTNTAREWEIGHT.Visible = True
        Me.colCTNTAREWEIGHT.VisibleIndex = 12
        Me.colCTNTAREWEIGHT.Width = 72
        '
        'colCTNMARKTRANSPORTBOO
        '
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.FieldName = "CTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.Name = "colCTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.OptionsColumn.AllowEdit = False
        Me.colCTNMARKTRANSPORTBOO.OptionsColumn.ReadOnly = True
        Me.colCTNMARKTRANSPORTBOO.Width = 31
        '
        'colCARID
        '
        Me.colCARID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCARID.AppearanceCell.Options.UseFont = True
        Me.colCARID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colCARID.AppearanceHeader.Options.UseFont = True
        Me.colCARID.Caption = "No-Car"
        Me.colCARID.FieldName = "CARID"
        Me.colCARID.Name = "colCARID"
        Me.colCARID.OptionsColumn.FixedWidth = True
        Me.colCARID.OptionsColumn.Printable = DevExpress.Utils.DefaultBoolean.[False]
        Me.colCARID.Visible = True
        Me.colCARID.VisibleIndex = 14
        Me.colCARID.Width = 64
        '
        'colTIMEHHMM
        '
        Me.colTIMEHHMM.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEHHMM.AppearanceCell.Options.UseFont = True
        Me.colTIMEHHMM.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEHHMM.AppearanceHeader.Options.UseFont = True
        Me.colTIMEHHMM.Caption = "Time Out"
        Me.colTIMEHHMM.FieldName = "TIMEHHMM"
        Me.colTIMEHHMM.Name = "colTIMEHHMM"
        Me.colTIMEHHMM.OptionsColumn.FixedWidth = True
        Me.colTIMEHHMM.Visible = True
        Me.colTIMEHHMM.VisibleIndex = 5
        Me.colTIMEHHMM.Width = 49
        '
        'colMARKING
        '
        Me.colMARKING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colMARKING.AppearanceCell.Options.UseFont = True
        Me.colMARKING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colMARKING.AppearanceHeader.Options.UseFont = True
        Me.colMARKING.FieldName = "MARKING"
        Me.colMARKING.Name = "colMARKING"
        Me.colMARKING.OptionsColumn.AllowEdit = False
        Me.colMARKING.OptionsColumn.ReadOnly = True
        Me.colMARKING.Width = 107
        '
        'colINSURANCE
        '
        Me.colINSURANCE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colINSURANCE.AppearanceCell.Options.UseFont = True
        Me.colINSURANCE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colINSURANCE.AppearanceHeader.Options.UseFont = True
        Me.colINSURANCE.FieldName = "INSURANCE"
        Me.colINSURANCE.Name = "colINSURANCE"
        Me.colINSURANCE.OptionsColumn.AllowEdit = False
        Me.colINSURANCE.OptionsColumn.ReadOnly = True
        Me.colINSURANCE.Width = 96
        '
        'colidborrow
        '
        Me.colidborrow.FieldName = "idborrow"
        Me.colidborrow.Name = "colidborrow"
        '
        'colTIMEHHMMIN
        '
        Me.colTIMEHHMMIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEHHMMIN.AppearanceCell.Options.UseFont = True
        Me.colTIMEHHMMIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colTIMEHHMMIN.AppearanceHeader.Options.UseFont = True
        Me.colTIMEHHMMIN.Caption = "Time In"
        Me.colTIMEHHMMIN.FieldName = "TIMEHHMMIN"
        Me.colTIMEHHMMIN.Name = "colTIMEHHMMIN"
        Me.colTIMEHHMMIN.OptionsColumn.FixedWidth = True
        Me.colTIMEHHMMIN.Visible = True
        Me.colTIMEHHMMIN.VisibleIndex = 8
        Me.colTIMEHHMMIN.Width = 45
        '
        'colBCTNTYPETHAI
        '
        Me.colBCTNTYPETHAI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBCTNTYPETHAI.AppearanceCell.Options.UseFont = True
        Me.colBCTNTYPETHAI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBCTNTYPETHAI.AppearanceHeader.Options.UseFont = True
        Me.colBCTNTYPETHAI.FieldName = "BCTNTYPETHAI"
        Me.colBCTNTYPETHAI.Name = "colBCTNTYPETHAI"
        Me.colBCTNTYPETHAI.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPETHAI.OptionsColumn.ReadOnly = True
        Me.colBCTNTYPETHAI.Width = 133
        '
        'colBCTNTYPEPAE
        '
        Me.colBCTNTYPEPAE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBCTNTYPEPAE.AppearanceCell.Options.UseFont = True
        Me.colBCTNTYPEPAE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBCTNTYPEPAE.AppearanceHeader.Options.UseFont = True
        Me.colBCTNTYPEPAE.FieldName = "BCTNTYPEPAE"
        Me.colBCTNTYPEPAE.Name = "colBCTNTYPEPAE"
        Me.colBCTNTYPEPAE.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPEPAE.OptionsColumn.ReadOnly = True
        Me.colBCTNTYPEPAE.Width = 120
        '
        'colBRCHECK1
        '
        Me.colBRCHECK1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBRCHECK1.AppearanceCell.Options.UseFont = True
        Me.colBRCHECK1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBRCHECK1.AppearanceHeader.Options.UseFont = True
        Me.colBRCHECK1.Caption = "C1"
        Me.colBRCHECK1.FieldName = "BRCHECK1_"
        Me.colBRCHECK1.Name = "colBRCHECK1"
        Me.colBRCHECK1.Visible = True
        Me.colBRCHECK1.VisibleIndex = 9
        Me.colBRCHECK1.Width = 30
        '
        'colBRCHECK
        '
        Me.colBRCHECK.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBRCHECK.AppearanceCell.Options.UseFont = True
        Me.colBRCHECK.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBRCHECK.AppearanceHeader.Options.UseFont = True
        Me.colBRCHECK.Caption = "C"
        Me.colBRCHECK.FieldName = "BRCHECK_"
        Me.colBRCHECK.Name = "colBRCHECK"
        Me.colBRCHECK.Visible = True
        Me.colBRCHECK.VisibleIndex = 6
        Me.colBRCHECK.Width = 20
        '
        'colBRBILLNAME
        '
        Me.colBRBILLNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBRBILLNAME.AppearanceCell.Options.UseFont = True
        Me.colBRBILLNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBRBILLNAME.AppearanceHeader.Options.UseFont = True
        Me.colBRBILLNAME.FieldName = "BRBILLNAME"
        Me.colBRBILLNAME.Name = "colBRBILLNAME"
        Me.colBRBILLNAME.OptionsColumn.AllowEdit = False
        Me.colBRBILLNAME.OptionsColumn.ReadOnly = True
        Me.colBRBILLNAME.Width = 105
        '
        'colBNO
        '
        Me.colBNO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBNO.AppearanceCell.Options.UseFont = True
        Me.colBNO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.colBNO.AppearanceHeader.Options.UseFont = True
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Width = 77
        '
        'colCTNREALDATEIN
        '
        Me.colCTNREALDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNREALDATEIN.AppearanceCell.Options.UseFont = True
        Me.colCTNREALDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNREALDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colCTNREALDATEIN.Caption = "Realdate IN"
        Me.colCTNREALDATEIN.FieldName = "CTNREALDATEIN"
        Me.colCTNREALDATEIN.Name = "colCTNREALDATEIN"
        Me.colCTNREALDATEIN.OptionsColumn.FixedWidth = True
        Me.colCTNREALDATEIN.Visible = True
        Me.colCTNREALDATEIN.VisibleIndex = 16
        Me.colCTNREALDATEIN.Width = 91
        '
        'colCTNREALDATEOUT
        '
        Me.colCTNREALDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNREALDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNREALDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCTNREALDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNREALDATEOUT.Caption = "Realdate Out"
        Me.colCTNREALDATEOUT.FieldName = "CTNREALDATEOUT"
        Me.colCTNREALDATEOUT.Name = "colCTNREALDATEOUT"
        Me.colCTNREALDATEOUT.OptionsColumn.FixedWidth = True
        Me.colCTNREALDATEOUT.Visible = True
        Me.colCTNREALDATEOUT.VisibleIndex = 15
        Me.colCTNREALDATEOUT.Width = 80
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Update"
        Me.GridColumn9.ColumnEdit = Me.btnCTNborrow
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.OptionsColumn.FixedWidth = True
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 17
        Me.GridColumn9.Width = 40
        '
        'btnCTNborrow
        '
        Me.btnCTNborrow.AutoHeight = False
        Me.btnCTNborrow.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinUp)})
        Me.btnCTNborrow.Name = "btnCTNborrow"
        Me.btnCTNborrow.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'txtWeight
        '
        Me.txtWeight.AutoHeight = False
        Me.txtWeight.Mask.EditMask = "n2"
        Me.txtWeight.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txtWeight.Name = "txtWeight"
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'RepositoryItemCheckEdit2
        '
        Me.RepositoryItemCheckEdit2.AutoHeight = False
        Me.RepositoryItemCheckEdit2.Name = "RepositoryItemCheckEdit2"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Location = New System.Drawing.Point(1192, 145)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(294, 22)
        Me.SimpleButton5.StyleController = Me.LayoutControl1
        Me.SimpleButton5.TabIndex = 225
        Me.SimpleButton5.Text = "Update"
        '
        'btnSailOut
        '
        Me.btnSailOut.Location = New System.Drawing.Point(1127, 42)
        Me.btnSailOut.Name = "btnSailOut"
        Me.btnSailOut.Size = New System.Drawing.Size(46, 22)
        Me.btnSailOut.StyleController = Me.LayoutControl1
        Me.btnSailOut.TabIndex = 224
        Me.btnSailOut.Text = "Sail Out"
        '
        'txtSailOutTime
        '
        Me.txtSailOutTime.Location = New System.Drawing.Point(1073, 42)
        Me.txtSailOutTime.MenuManager = Me.RibbonControl1
        Me.txtSailOutTime.Name = "txtSailOutTime"
        Me.txtSailOutTime.Properties.Mask.EditMask = "90:00"
        Me.txtSailOutTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtSailOutTime.Size = New System.Drawing.Size(50, 20)
        Me.txtSailOutTime.StyleController = Me.LayoutControl1
        Me.txtSailOutTime.TabIndex = 223
        '
        'txtSailOutdate
        '
        Me.txtSailOutdate.Location = New System.Drawing.Point(703, 42)
        Me.txtSailOutdate.MenuManager = Me.RibbonControl1
        Me.txtSailOutdate.Name = "txtSailOutdate"
        Me.txtSailOutdate.Properties.Mask.EditMask = "00-00-0000"
        Me.txtSailOutdate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtSailOutdate.Size = New System.Drawing.Size(366, 20)
        Me.txtSailOutdate.StyleController = Me.LayoutControl1
        Me.txtSailOutdate.TabIndex = 222
        '
        'btnClosing
        '
        Me.btnClosing.Location = New System.Drawing.Point(497, 204)
        Me.btnClosing.Name = "btnClosing"
        Me.btnClosing.Size = New System.Drawing.Size(59, 22)
        Me.btnClosing.StyleController = Me.LayoutControl1
        Me.btnClosing.TabIndex = 221
        Me.btnClosing.Text = "Closing"
        '
        'txtClosingTime
        '
        Me.txtClosingTime.Location = New System.Drawing.Point(376, 204)
        Me.txtClosingTime.MenuManager = Me.RibbonControl1
        Me.txtClosingTime.Name = "txtClosingTime"
        Me.txtClosingTime.Properties.DisplayFormat.FormatString = "HH:mm"
        Me.txtClosingTime.Properties.Mask.EditMask = "90:00"
        Me.txtClosingTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtClosingTime.Size = New System.Drawing.Size(117, 20)
        Me.txtClosingTime.StyleController = Me.LayoutControl1
        Me.txtClosingTime.TabIndex = 220
        '
        'txtClosingDate
        '
        Me.txtClosingDate.Location = New System.Drawing.Point(24, 204)
        Me.txtClosingDate.MenuManager = Me.RibbonControl1
        Me.txtClosingDate.Name = "txtClosingDate"
        Me.txtClosingDate.Properties.DisplayFormat.FormatString = "00-00-0000"
        Me.txtClosingDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.txtClosingDate.Properties.Mask.EditMask = "00-00-0000"
        Me.txtClosingDate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.txtClosingDate.Size = New System.Drawing.Size(348, 20)
        Me.txtClosingDate.StyleController = Me.LayoutControl1
        Me.txtClosingDate.TabIndex = 219
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(1129, 618)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(102, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 218
        Me.SimpleButton2.Text = "Update"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(1129, 592)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(102, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 217
        Me.SimpleButton1.Text = "Update"
        '
        'txtUp
        '
        Me.txtUp.Location = New System.Drawing.Point(781, 592)
        Me.txtUp.MenuManager = Me.RibbonControl1
        Me.txtUp.Name = "txtUp"
        Me.txtUp.Size = New System.Drawing.Size(159, 20)
        Me.txtUp.StyleController = Me.LayoutControl1
        Me.txtUp.TabIndex = 216
        '
        'txtSealID
        '
        Me.txtSealID.Location = New System.Drawing.Point(688, 592)
        Me.txtSealID.MenuManager = Me.RibbonControl1
        Me.txtSealID.Name = "txtSealID"
        Me.txtSealID.Size = New System.Drawing.Size(68, 20)
        Me.txtSealID.StyleController = Me.LayoutControl1
        Me.txtSealID.TabIndex = 215
        '
        'txtDown
        '
        Me.txtDown.Location = New System.Drawing.Point(970, 592)
        Me.txtDown.MenuManager = Me.RibbonControl1
        Me.txtDown.Name = "txtDown"
        Me.txtDown.Size = New System.Drawing.Size(155, 20)
        Me.txtDown.StyleController = Me.LayoutControl1
        Me.txtDown.TabIndex = 214
        '
        'txtSealID1
        '
        Me.txtSealID1.Location = New System.Drawing.Point(688, 616)
        Me.txtSealID1.MenuManager = Me.RibbonControl1
        Me.txtSealID1.Name = "txtSealID1"
        Me.txtSealID1.Size = New System.Drawing.Size(68, 20)
        Me.txtSealID1.StyleController = Me.LayoutControl1
        Me.txtSealID1.TabIndex = 213
        '
        'txtUp1
        '
        Me.txtUp1.Location = New System.Drawing.Point(781, 616)
        Me.txtUp1.MenuManager = Me.RibbonControl1
        Me.txtUp1.Name = "txtUp1"
        Me.txtUp1.Size = New System.Drawing.Size(159, 20)
        Me.txtUp1.StyleController = Me.LayoutControl1
        Me.txtUp1.TabIndex = 212
        '
        'txtDown1
        '
        Me.txtDown1.Location = New System.Drawing.Point(970, 616)
        Me.txtDown1.MenuManager = Me.RibbonControl1
        Me.txtDown1.Name = "txtDown1"
        Me.txtDown1.Size = New System.Drawing.Size(155, 20)
        Me.txtDown1.StyleController = Me.LayoutControl1
        Me.txtDown1.TabIndex = 211
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(1248, 101)
        Me.MemoEdit1.MenuManager = Me.RibbonControl1
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(238, 40)
        Me.MemoEdit1.StyleController = Me.LayoutControl1
        Me.MemoEdit1.TabIndex = 209
        '
        'GridControl2
        '
        Me.GridControl2.ContextMenuStrip = Me.ContextMenuStrip1
        Me.GridControl2.DataMember = "booking"
        Me.GridControl2.DataSource = Me.Ykpdtset1
        Me.GridControl2.Location = New System.Drawing.Point(12, 242)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnBookingDelete})
        Me.GridControl2.Size = New System.Drawing.Size(556, 398)
        Me.GridControl2.TabIndex = 208
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.TBABookingToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(165, 70)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(164, 22)
        Me.ToolStripMenuItem1.Text = "Manage Booking"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(164, 22)
        Me.ToolStripMenuItem2.Text = "Move Booking"
        '
        'TBABookingToolStripMenuItem
        '
        Me.TBABookingToolStripMenuItem.Name = "TBABookingToolStripMenuItem"
        Me.TBABookingToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.TBABookingToolStripMenuItem.Text = "TBA Booking"
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.colBNFORWARDERNAME, Me.colBFORWARDER, Me.colCTNNO, Me.colBCONFIRMTIME, Me.colBCONFIRMDAY, Me.GridColumn8, Me.colBINVOICE})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.IndicatorWidth = 25
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsImageLoad.DesiredThumbnailSize = New System.Drawing.Size(10, 0)
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn7.AppearanceCell.Options.UseFont = True
        Me.GridColumn7.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn7.AppearanceHeader.Options.UseFont = True
        Me.GridColumn7.Caption = "Delete"
        Me.GridColumn7.ColumnEdit = Me.btnBookingDelete
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 7
        Me.GridColumn7.Width = 54
        '
        'btnBookingDelete
        '
        Me.btnBookingDelete.AutoHeight = False
        Me.btnBookingDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)})
        Me.btnBookingDelete.Name = "btnBookingDelete"
        Me.btnBookingDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.FieldName = "BOOKINGID"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.ReadOnly = True
        '
        'GridColumn2
        '
        Me.GridColumn2.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn2.AppearanceCell.Options.UseFont = True
        Me.GridColumn2.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn2.AppearanceHeader.Options.UseFont = True
        Me.GridColumn2.Caption = "Booking No."
        Me.GridColumn2.FieldName = "BNO"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.ReadOnly = True
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 135
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn3.AppearanceCell.Options.UseFont = True
        Me.GridColumn3.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn3.AppearanceHeader.Options.UseFont = True
        Me.GridColumn3.FieldName = "BLANDNO"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.ReadOnly = True
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn4.AppearanceCell.Options.UseFont = True
        Me.GridColumn4.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn4.AppearanceHeader.Options.UseFont = True
        Me.GridColumn4.FieldName = "BCTNTYPE"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.ReadOnly = True
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn5.AppearanceCell.Options.UseFont = True
        Me.GridColumn5.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn5.AppearanceHeader.Options.UseFont = True
        Me.GridColumn5.FieldName = "BCOM"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.OptionsColumn.ReadOnly = True
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        Me.GridColumn5.Width = 150
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn6.AppearanceCell.Options.UseFont = True
        Me.GridColumn6.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn6.AppearanceHeader.Options.UseFont = True
        Me.GridColumn6.Caption = "Shipper"
        Me.GridColumn6.FieldName = "BSHIPNAME"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.ReadOnly = True
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        Me.GridColumn6.Width = 167
        '
        'colBNFORWARDERNAME
        '
        Me.colBNFORWARDERNAME.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBNFORWARDERNAME.AppearanceCell.Options.UseFont = True
        Me.colBNFORWARDERNAME.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBNFORWARDERNAME.AppearanceHeader.Options.UseFont = True
        Me.colBNFORWARDERNAME.FieldName = "BNFORWARDERNAME"
        Me.colBNFORWARDERNAME.Name = "colBNFORWARDERNAME"
        Me.colBNFORWARDERNAME.OptionsColumn.AllowEdit = False
        Me.colBNFORWARDERNAME.OptionsColumn.ReadOnly = True
        Me.colBNFORWARDERNAME.Visible = True
        Me.colBNFORWARDERNAME.VisibleIndex = 3
        Me.colBNFORWARDERNAME.Width = 179
        '
        'colBFORWARDER
        '
        Me.colBFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colBFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colBFORWARDER.Caption = "Forwarder"
        Me.colBFORWARDER.FieldName = "BFORWARDER"
        Me.colBFORWARDER.Name = "colBFORWARDER"
        Me.colBFORWARDER.OptionsColumn.AllowEdit = False
        Me.colBFORWARDER.OptionsColumn.ReadOnly = True
        Me.colBFORWARDER.Width = 183
        '
        'colCTNNO
        '
        Me.colCTNNO.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colCTNNO.AppearanceCell.Options.UseFont = True
        Me.colCTNNO.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colCTNNO.AppearanceHeader.Options.UseFont = True
        Me.colCTNNO.Caption = "FCL"
        Me.colCTNNO.FieldName = "CTNNO"
        Me.colCTNNO.Name = "colCTNNO"
        Me.colCTNNO.OptionsColumn.AllowEdit = False
        Me.colCTNNO.OptionsColumn.ReadOnly = True
        Me.colCTNNO.Visible = True
        Me.colCTNNO.VisibleIndex = 5
        Me.colCTNNO.Width = 142
        '
        'colBCONFIRMTIME
        '
        Me.colBCONFIRMTIME.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBCONFIRMTIME.AppearanceCell.Options.UseFont = True
        Me.colBCONFIRMTIME.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBCONFIRMTIME.AppearanceHeader.Options.UseFont = True
        Me.colBCONFIRMTIME.FieldName = "BCONFIRMTIME"
        Me.colBCONFIRMTIME.Name = "colBCONFIRMTIME"
        Me.colBCONFIRMTIME.OptionsColumn.AllowEdit = False
        Me.colBCONFIRMTIME.OptionsColumn.ReadOnly = True
        Me.colBCONFIRMTIME.Width = 153
        '
        'colBCONFIRMDAY
        '
        Me.colBCONFIRMDAY.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBCONFIRMDAY.AppearanceCell.Options.UseFont = True
        Me.colBCONFIRMDAY.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBCONFIRMDAY.AppearanceHeader.Options.UseFont = True
        Me.colBCONFIRMDAY.FieldName = "BCONFIRMDAY"
        Me.colBCONFIRMDAY.Name = "colBCONFIRMDAY"
        Me.colBCONFIRMDAY.OptionsColumn.AllowEdit = False
        Me.colBCONFIRMDAY.OptionsColumn.ReadOnly = True
        Me.colBCONFIRMDAY.Width = 164
        '
        'GridColumn8
        '
        Me.GridColumn8.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn8.AppearanceCell.Options.UseFont = True
        Me.GridColumn8.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GridColumn8.AppearanceHeader.Options.UseFont = True
        Me.GridColumn8.Caption = "Close Date"
        Me.GridColumn8.FieldName = "GridColumn8"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.OptionsColumn.AllowEdit = False
        Me.GridColumn8.OptionsColumn.ReadOnly = True
        Me.GridColumn8.UnboundExpression = "Concat([BCONFIRMDAY], ' ', [BCONFIRMTIME])"
        Me.GridColumn8.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 4
        Me.GridColumn8.Width = 118
        '
        'colBINVOICE
        '
        Me.colBINVOICE.AppearanceCell.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBINVOICE.AppearanceCell.Options.UseFont = True
        Me.colBINVOICE.AppearanceHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.colBINVOICE.AppearanceHeader.Options.UseFont = True
        Me.colBINVOICE.Caption = "Invoice"
        Me.colBINVOICE.FieldName = "BINVOICE"
        Me.colBINVOICE.Name = "colBINVOICE"
        Me.colBINVOICE.OptionsColumn.AllowEdit = False
        Me.colBINVOICE.OptionsColumn.ReadOnly = True
        Me.colBINVOICE.Visible = True
        Me.colBINVOICE.VisibleIndex = 6
        Me.colBINVOICE.Width = 88
        '
        'txt_eta_penang
        '
        Me.txt_eta_penang.Location = New System.Drawing.Point(996, 140)
        Me.txt_eta_penang.MenuManager = Me.RibbonControl1
        Me.txt_eta_penang.Name = "txt_eta_penang"
        Me.txt_eta_penang.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_eta_penang.Properties.Appearance.Options.UseFont = True
        Me.txt_eta_penang.Properties.ReadOnly = True
        Me.txt_eta_penang.Size = New System.Drawing.Size(189, 26)
        Me.txt_eta_penang.StyleController = Me.LayoutControl1
        Me.txt_eta_penang.TabIndex = 15
        '
        'txt_eta
        '
        Me.txt_eta.Location = New System.Drawing.Point(688, 140)
        Me.txt_eta.MenuManager = Me.RibbonControl1
        Me.txt_eta.Name = "txt_eta"
        Me.txt_eta.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_eta.Properties.Appearance.Options.UseFont = True
        Me.txt_eta.Properties.ReadOnly = True
        Me.txt_eta.Size = New System.Drawing.Size(188, 26)
        Me.txt_eta.StyleController = Me.LayoutControl1
        Me.txt_eta.TabIndex = 14
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(382, 140)
        Me.TextBox2.MenuManager = Me.RibbonControl1
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Properties.Appearance.Options.UseFont = True
        Me.TextBox2.Properties.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(186, 26)
        Me.TextBox2.StyleController = Me.LayoutControl1
        Me.TextBox2.TabIndex = 13
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(128, 140)
        Me.TextBox1.MenuManager = Me.RibbonControl1
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Properties.Appearance.Options.UseFont = True
        Me.TextBox1.Properties.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(250, 26)
        Me.TextBox1.StyleController = Me.LayoutControl1
        Me.TextBox1.TabIndex = 12
        '
        'txt_gross
        '
        Me.txt_gross.Location = New System.Drawing.Point(996, 110)
        Me.txt_gross.MenuManager = Me.RibbonControl1
        Me.txt_gross.Name = "txt_gross"
        Me.txt_gross.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_gross.Properties.Appearance.Options.UseFont = True
        Me.txt_gross.Properties.ReadOnly = True
        Me.txt_gross.Size = New System.Drawing.Size(189, 26)
        Me.txt_gross.StyleController = Me.LayoutControl1
        Me.txt_gross.TabIndex = 11
        '
        'txt_from
        '
        Me.txt_from.EditValue = "Thailand"
        Me.txt_from.Location = New System.Drawing.Point(688, 110)
        Me.txt_from.MenuManager = Me.RibbonControl1
        Me.txt_from.Name = "txt_from"
        Me.txt_from.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_from.Properties.Appearance.Options.UseFont = True
        Me.txt_from.Properties.ReadOnly = True
        Me.txt_from.Size = New System.Drawing.Size(188, 26)
        Me.txt_from.StyleController = Me.LayoutControl1
        Me.txt_from.TabIndex = 10
        '
        'txt_namemaster
        '
        Me.txt_namemaster.Location = New System.Drawing.Point(128, 110)
        Me.txt_namemaster.MenuManager = Me.RibbonControl1
        Me.txt_namemaster.Name = "txt_namemaster"
        Me.txt_namemaster.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_namemaster.Properties.Appearance.Options.UseFont = True
        Me.txt_namemaster.Properties.ReadOnly = True
        Me.txt_namemaster.Size = New System.Drawing.Size(440, 26)
        Me.txt_namemaster.StyleController = Me.LayoutControl1
        Me.txt_namemaster.TabIndex = 9
        '
        'txt_nett
        '
        Me.txt_nett.Location = New System.Drawing.Point(996, 80)
        Me.txt_nett.MenuManager = Me.RibbonControl1
        Me.txt_nett.Name = "txt_nett"
        Me.txt_nett.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nett.Properties.Appearance.Options.UseFont = True
        Me.txt_nett.Properties.ReadOnly = True
        Me.txt_nett.Size = New System.Drawing.Size(189, 26)
        Me.txt_nett.StyleController = Me.LayoutControl1
        Me.txt_nett.TabIndex = 8
        '
        'txt_nationality
        '
        Me.txt_nationality.Location = New System.Drawing.Point(688, 80)
        Me.txt_nationality.MenuManager = Me.RibbonControl1
        Me.txt_nationality.Name = "txt_nationality"
        Me.txt_nationality.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nationality.Properties.Appearance.Options.UseFont = True
        Me.txt_nationality.Properties.ReadOnly = True
        Me.txt_nationality.Size = New System.Drawing.Size(188, 26)
        Me.txt_nationality.StyleController = Me.LayoutControl1
        Me.txt_nationality.TabIndex = 7
        '
        'txt_vessel
        '
        Me.txt_vessel.Location = New System.Drawing.Point(128, 80)
        Me.txt_vessel.MenuManager = Me.RibbonControl1
        Me.txt_vessel.Name = "txt_vessel"
        Me.txt_vessel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_vessel.Properties.Appearance.Options.UseFont = True
        Me.txt_vessel.Properties.ReadOnly = True
        Me.txt_vessel.Size = New System.Drawing.Size(440, 26)
        Me.txt_vessel.StyleController = Me.LayoutControl1
        Me.txt_vessel.TabIndex = 6
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(1305, 12)
        Me.slcVoyage.MenuManager = Me.RibbonControl1
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.VoyageBindingSource
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(184, 30)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 4
        '
        'VoyageBindingSource
        '
        Me.VoyageBindingSource.DataMember = "voyage"
        Me.VoyageBindingSource.DataSource = Me.Ykpdtset1
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 1
        Me.colVOYVESNAMEN.Width = 253
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 2
        Me.colVOYVESNAMES.Width = 257
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        Me.colVOYNAME.Width = 182
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(1192, 101)
        Me.RadioGroup1.MenuManager = Me.RibbonControl1
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Columns = 1
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "1"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "2")})
        Me.RadioGroup1.Size = New System.Drawing.Size(52, 40)
        Me.RadioGroup1.StyleController = Me.LayoutControl1
        Me.RadioGroup1.TabIndex = 210
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem12, Me.LayoutControlItem14, Me.LayoutControlItem1, Me.LayoutControlGroup2, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutControlItem11, Me.LayoutControlGroup4, Me.EmptySpaceItem1, Me.LayoutControlItem32, Me.TabbedControlGroup1, Me.LayoutControlGroup3})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1501, 652)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.txt_vessel
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 68)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(560, 30)
        Me.LayoutControlItem3.Text = "Name Of Vessel"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem4.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem4.Control = Me.txt_nationality
        Me.LayoutControlItem4.Location = New System.Drawing.Point(560, 68)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(308, 30)
        Me.LayoutControlItem4.Text = "Nationality"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem5.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem5.Control = Me.txt_nett
        Me.LayoutControlItem5.Location = New System.Drawing.Point(868, 68)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(309, 30)
        Me.LayoutControlItem5.Text = "NETT REQ. :"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem6.Control = Me.txt_namemaster
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(560, 30)
        Me.LayoutControlItem6.Text = "Name Master"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem7.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem7.Control = Me.txt_from
        Me.LayoutControlItem7.Location = New System.Drawing.Point(560, 98)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(308, 30)
        Me.LayoutControlItem7.Text = "From"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem8.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem8.Control = Me.txt_gross
        Me.LayoutControlItem8.Location = New System.Drawing.Point(868, 98)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(309, 30)
        Me.LayoutControlItem8.Text = "GROSS :"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem9.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem9.Control = Me.TextBox1
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 128)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(370, 34)
        Me.LayoutControlItem9.Text = "จำนวนตู้ทั้งหมด"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.TextBox2
        Me.LayoutControlItem10.Location = New System.Drawing.Point(370, 128)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(190, 34)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem12.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem12.Control = Me.txt_eta_penang
        Me.LayoutControlItem12.Location = New System.Drawing.Point(868, 128)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(309, 34)
        Me.LayoutControlItem12.Text = "ETA. PENANG :"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.GridControl2
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 230)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(560, 402)
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem14.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.slcVoyage
        Me.LayoutControlItem1.Location = New System.Drawing.Point(1177, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(304, 68)
        Me.LayoutControlItem1.Text = "Voayge"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(113, 23)
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem15, Me.LayoutControlItem30, Me.LayoutControlItem2})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(1177, 68)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(304, 94)
        Me.LayoutControlGroup2.Text = "N"
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.RadioGroup1
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(56, 44)
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem15.TextVisible = False
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.SimpleButton5
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 44)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(298, 26)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.MemoEdit1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(56, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(242, 44)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem16.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem16.Control = Me.txtDown1
        Me.LayoutControlItem16.Location = New System.Drawing.Point(932, 604)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(185, 28)
        Me.LayoutControlItem16.Text = "เพิ่ม"
        Me.LayoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(21, 16)
        Me.LayoutControlItem16.TextToControlDistance = 5
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem17.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem17.Control = Me.txtUp1
        Me.LayoutControlItem17.Location = New System.Drawing.Point(748, 604)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(184, 28)
        Me.LayoutControlItem17.Text = "ลด"
        Me.LayoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(16, 16)
        Me.LayoutControlItem17.TextToControlDistance = 5
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem18.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem18.Control = Me.txtSealID1
        Me.LayoutControlItem18.Location = New System.Drawing.Point(560, 604)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(188, 28)
        Me.LayoutControlItem18.Text = "Seal YANG MING"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(113, 16)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem19.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem19.Control = Me.txtDown
        Me.LayoutControlItem19.Location = New System.Drawing.Point(932, 580)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(185, 24)
        Me.LayoutControlItem19.Text = "เพิ่ม"
        Me.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(21, 16)
        Me.LayoutControlItem19.TextToControlDistance = 5
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem20.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem20.Control = Me.txtSealID
        Me.LayoutControlItem20.Location = New System.Drawing.Point(560, 580)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(188, 24)
        Me.LayoutControlItem20.Text = "Seal WAN HAI"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(113, 16)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem21.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem21.Control = Me.txtUp
        Me.LayoutControlItem21.Location = New System.Drawing.Point(748, 580)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(184, 24)
        Me.LayoutControlItem21.Text = "ลด"
        Me.LayoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(16, 16)
        Me.LayoutControlItem21.TextToControlDistance = 5
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.SimpleButton1
        Me.LayoutControlItem22.Location = New System.Drawing.Point(1117, 580)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(106, 26)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.SimpleButton2
        Me.LayoutControlItem23.Location = New System.Drawing.Point(1117, 606)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(106, 26)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(1223, 580)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(94, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(1223, 606)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(258, 26)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem11.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem11.Control = Me.txt_eta
        Me.LayoutControlItem11.Location = New System.Drawing.Point(560, 128)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(308, 34)
        Me.LayoutControlItem11.Text = "ETA:"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(113, 19)
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem27, Me.LayoutControlItem28, Me.LayoutControlItem29})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(679, 0)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(498, 68)
        Me.LayoutControlGroup4.Text = "Sail Out"
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.txtSailOutdate
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(370, 26)
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextVisible = False
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.txtSailOutTime
        Me.LayoutControlItem28.Location = New System.Drawing.Point(370, 0)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(54, 26)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.btnSailOut
        Me.LayoutControlItem29.Location = New System.Drawing.Point(424, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(50, 26)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(679, 68)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.SimpleButton3
        Me.LayoutControlItem32.Location = New System.Drawing.Point(1317, 580)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(164, 26)
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'TabbedControlGroup1
        '
        Me.TabbedControlGroup1.Location = New System.Drawing.Point(560, 162)
        Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
        Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup5
        Me.TabbedControlGroup1.SelectedTabPageIndex = 0
        Me.TabbedControlGroup1.Size = New System.Drawing.Size(921, 418)
        Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup5})
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem31})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(897, 372)
        Me.LayoutControlGroup5.Text = "Booking No. :"
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.GridControl1
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(897, 372)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = False
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 162)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(560, 68)
        Me.LayoutControlGroup3.Text = "Closing Date"
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.txtClosingDate
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(352, 26)
        Me.LayoutControlItem24.Text = "Closing Date"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.txtClosingTime
        Me.LayoutControlItem25.Location = New System.Drawing.Point(352, 0)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(121, 26)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.btnClosing
        Me.LayoutControlItem26.Location = New System.Drawing.Point(473, 0)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(63, 26)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmViewvoyage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1501, 795)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Name = "frmViewvoyage"
        Me.Ribbon = Me.RibbonControl1
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip2.ResumeLayout(False)
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCTNborrow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtWeight, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSailOutTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSailOutdate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClosingTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClosingDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSealID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDown.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSealID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUp1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDown1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnBookingDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_eta_penang.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_eta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_gross.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_from.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_namemaster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nett.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nationality.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_vessel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VoyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnExportPdf As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnExportExcel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem9 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem10 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txt_nationality As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_vessel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_nett As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_from As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_namemaster As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_gross As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents TextBox2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_eta_penang As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_eta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colCTNNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONFIRMTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCONFIRMDAY As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents VoyageBindingSource As BindingSource
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarButtonItem11 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem12 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtUp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSealID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDown As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSealID1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtUp1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtDown1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents ย้ายตู๊ As ToolStripMenuItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BarButtonItem13 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents BarHeaderItem1 As DevExpress.XtraBars.BarHeaderItem
    Friend WithEvents BarEditItem1 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents BarEditItem2 As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents btnClosing As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtClosingTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtClosingDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents btnSailOut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtSailOutTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSailOutdate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BarButtonItem14 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem15 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem16 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnBookingDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents btnLoading As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem18 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TBABookingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents colBFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBINVOICE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNFORWARDERNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colCTNMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONDI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNBOOKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNYARD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNUPGRADE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTRBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEALBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEINBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCARID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEHHMM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEDATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMARKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colINSURANCE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTIMEHHMMIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPETHAI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPEPAE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRCHECK1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRCHECK As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRBILLNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents colCOMNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnCTNborrow As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents colidborrow As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtWeight As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents RepositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colCTNTAREWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GateInOutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents colCTNREALDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNREALDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents UpgradeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NormalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MarkContainerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MarkSealToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BarButtonItem17 As DevExpress.XtraBars.BarButtonItem
End Class
