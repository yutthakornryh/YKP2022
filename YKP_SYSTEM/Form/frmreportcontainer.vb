﻿Imports System.IO
Imports System.Text
Imports DevExpress.XtraPrinting

Public Class frmreportcontainer

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset

    Dim idtemplateKPI As String
    Dim layoutClass As layoutClass
    Dim layoutSet As New layoutSet
    Dim nextLayout As frmMainLayout
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        loadDAta()

        layoutClass = New layoutClass(layoutSet, Me.Tag)
        setDesign()
    End Sub
    Public Sub setDesign()
        slcmasprgtemplate.Properties.DataSource = layoutSet.Tables("masprgtemplate")

    End Sub
    Public Sub loadDAta()
        Dim sql As String = "Select * from voyage;"

        ykpset.Tables("voyage").Clear()

        connectDB.GetTable(sql, ykpset.Tables("voyage"))


        slcStart.Properties.DataSource = ykpset.Tables("voyage")

        slcEnd.Properties.DataSource = ykpset.Tables("voyage")
    End Sub
    Public Sub loadReport()
        Dim sql As String
        ykpset.Tables("CTNREPORT").Clear()
        sql = " 

SELECT  1 AS CountIN ,0 AS CountOut, CTNSTRING, CTNAGENT, CTNSIZE , voyvesnamen , voydateen , str_to_date(voydateen, '%d-%m-%y') AS daten, 
voynamen , 
voyname , voydatees ,  BNO , BSHIPNAME ,
 BFORWARDERNAME , BNSHIPNAME FROM (SELECT ctnstring , CTNMAINID ,ctnagent,ctnsize , ctnvoyn , ctnvoys From ctnmain ) 
 AS ctnmain 
 JOIN ( SELECT CTNID,BOOKID FROM borrow WHERE BOOKID IN ( SELECT BOOKINGID FROM booking WHERE  BVOYAGE > " & slcStart.EditValue & " and BVOYAGE < " & slcEnd.EditValue & " )) AS voyage ON ctnmain.CTNMAINID = voyage.CTNID JOIN ( SELECT voyageid , VOYVESNAMEN  ,VOYDATEEN ,VOYNAME AS VOYNAMEN FROM  voyage  WHERE VOYAGEID >  " & slcStart.EditValue & " and VOYAGEID <  " & slcEnd.EditValue & "  ) AS VOYN  ON ctnmain.ctnvoyn = VOYN.voyageid LEFT JOIN 
( SELECT voyageid , VOYVESNAMES  VOYNAME ,VOYDATEES ,VOYNAME AS VOYNAMES ,  BVOYAGE, BNO , BSHIPNAME , BFORWARDERNAME , BNSHIPNAME  ,
BOOKINGID    FROM  voyage JOIN   booking ON voyage.voyageid =  booking.BVOYAGE  WHERE  BVOYAGE > " & slcStart.EditValue & " and BVOYAGE <  " & slcEnd.EditValue & "    and VOYAGEID > " & slcStart.EditValue & " 
and VOYAGEID < " & slcEnd.EditValue & "  )  AS  VOYS ON voyage.BOOKID = VOYS.BOOKINGID 
  "

        connectDB.GetTable(sql, ykpset.Tables("CTNREPORT"))

        sql = " 

SELECT  0 AS CountIN ,1 AS CountOut, CTNSTRING, CTNAGENT, CTNSIZE , voyvesnamen , voydateen , str_to_date(voydatees, '%d-%m-%y') AS daten, 
voynamen , 
voyname , voydatees ,  BNO , BSHIPNAME ,
 BFORWARDERNAME , BNSHIPNAME FROM (SELECT ctnstring , CTNMAINID ,ctnagent,ctnsize , ctnvoyn , ctnvoys From ctnmain ) 
 AS ctnmain 
 JOIN ( SELECT CTNID,BOOKID FROM borrow WHERE BOOKID IN ( SELECT BOOKINGID FROM booking WHERE  BVOYAGE > " & slcStart.EditValue & " and BVOYAGE < " & slcEnd.EditValue & "  )) AS voyage ON ctnmain.CTNMAINID = voyage.CTNID JOIN ( SELECT voyageid , VOYVESNAMEN  ,VOYDATEEN ,VOYNAME AS VOYNAMEN FROM  voyage  WHERE VOYAGEID > " & slcStart.EditValue & " and VOYAGEID < " & slcEnd.EditValue & " ) AS VOYN  ON ctnmain.ctnvoyn = VOYN.voyageid LEFT JOIN 
( SELECT voyageid , VOYVESNAMES  VOYNAME ,VOYDATEES ,VOYNAME AS VOYNAMES ,  BVOYAGE, BNO , BSHIPNAME , BFORWARDERNAME , BNSHIPNAME  ,
BOOKINGID    FROM  voyage JOIN   booking ON voyage.voyageid =  booking.BVOYAGE  WHERE  BVOYAGE > " & slcStart.EditValue & " and BVOYAGE < " & slcEnd.EditValue & "   and VOYAGEID > " & slcStart.EditValue & " 
and VOYAGEID < " & slcEnd.EditValue & " )  AS  VOYS ON voyage.BOOKID = VOYS.BOOKINGID 
  "

        connectDB.GetTable(sql, ykpset.Tables("CTNREPORT"))


    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        If slcStart.EditValue Is Nothing Then
            MsgBox("กรุณาเลือก Voyage เริ่มต้น")
            Exit Sub
        End If
        If slcEnd.EditValue Is Nothing Then
            MsgBox("กรุณาเลือก Voyage เริ่มต้น")
            Exit Sub
        End If
        loadReport()
        PivotGridControl1.DataSource = ykpset.Tables("CTNREPORT")
    End Sub

    Private Sub slcmasprgtemplate_EditValueChanged(sender As Object, e As EventArgs) Handles slcmasprgtemplate.EditValueChanged
        Try
            Dim editor As DevExpress.XtraEditors.SearchLookUpEdit = CType(sender, DevExpress.XtraEditors.SearchLookUpEdit)
            Dim index As Integer = editor.Properties.GetIndexByKeyValue(editor.EditValue)
            Dim row As DataRow = TryCast(editor.Properties.DataSource, DataTable).Rows(index)
            Dim text As String = row("Layout").ToString()


            Dim byteArray As Byte() = Encoding.ASCII.GetBytes(text)
            Dim stream As New MemoryStream(byteArray)

            PivotGridControl1.BeginUpdate()
            Try
                PivotGridControl1.OptionsLayout.StoreAllOptions = True
                PivotGridControl1.RestoreLayoutFromStream(stream)
            Finally
                PivotGridControl1.RefreshData()
                PivotGridControl1.EndUpdate()
            End Try

            stream.Seek(0, System.IO.SeekOrigin.Begin)
        Catch ex As Exception

        End Try


    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        If slcmasprgtemplate.EditValue Is Nothing Then
            MsgBox("กรุณาเลือก Template ที่ต้องการแก้ไข")
            Exit Sub
        Else

            Dim stream As System.IO.Stream
            stream = New System.IO.MemoryStream()
            PivotGridControl1.SaveLayoutToStream(stream)
            stream.Seek(0, System.IO.SeekOrigin.Begin)
            Dim nextLayout As frmMainLayout
            nextLayout = New frmMainLayout(Me.Tag, stream, slcmasprgtemplate.EditValue, slcmasprgtemplate.Text)
            nextLayout.ShowDialog()
        End If
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        Dim stream As System.IO.Stream
        stream = New System.IO.MemoryStream()
        PivotGridControl1.SaveLayoutToStream(stream)
        stream.Seek(0, System.IO.SeekOrigin.Begin)
        Dim nextLayout As frmMainLayout
        nextLayout = New frmMainLayout(Me.Tag, stream)
        nextLayout.ShowDialog()
        layoutClass.getTemplate(Me.Tag)
    End Sub

    Private Sub SimpleButton4_Click(sender As Object, e As EventArgs) Handles SimpleButton4.Click
        Try

            Dim sv As New SaveFileDialog
            sv.Filter = "Excel Workbook|*.xlsx"
            If sv.ShowDialog() = DialogResult.OK And sv.FileName <> Nothing Then
                If sv.FileName.EndsWith(".xlsx") Then
                    Dim path = sv.FileName.ToString()
                    'PivotGridControl1.Groups
                    Dim x As New XlsxExportOptionsEx
                    x.AllowGrouping = DevExpress.Utils.DefaultBoolean.False
                    x.AllowFixedColumnHeaderPanel = DevExpress.Utils.DefaultBoolean.False


                    PivotGridControl1.ExportToXlsx(path, x)


                End If


                MessageBox.Show("Data Exported to :" + vbCrLf + sv.FileName, "Business Intelligence Portal", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                sv.FileName = Nothing

            End If
        Catch ex As Exception
        End Try
    End Sub
End Class
