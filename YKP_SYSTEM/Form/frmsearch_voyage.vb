﻿Imports MySql.Data.MySqlClient
Public Class frmsearch_voyage


    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset


    Private Sub frmsearch_voyage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        loadDAta()


    End Sub
    Public Sub loadDAta()
        Dim sql As String = "Select * from voyage;"

        ykpset.Tables("voyage").Clear()

        connectDB.GetTable(sql, ykpset.Tables("voyage"))


        GridControl1.DataSource = ykpset.Tables("voyage")
    End Sub

    Public Sub searchData()


    End Sub
    Private Sub TextBoxItem2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            searchData()

        End If
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        nextpage()
    End Sub

    Public Sub nextpage()
        Dim idvoyage As String
        idvoyage = GridView1.GetDataRow(GridView1.FocusedRowHandle)("VOYAGEID").ToString
        Dim cf As New frmeditvoyage(idvoyage)

        cf.MdiParent = Me.MdiParent
        Me.Close()
        cf.Dock = DockStyle.Fill
        cf.Show()
    End Sub

    Private Sub GridView1_DoubleClick(sender As Object, e As EventArgs) Handles GridView1.DoubleClick
        nextpage()
    End Sub
End Class