﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmview_ctnempty
    Inherits DevComponents.DotNetBar.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SearchLookUpEdit1 = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.mascmrunningBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmascmrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colisrunning = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colprefixchar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colshowyear = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colformatdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colseparatechar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldefault = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNTAREWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNBOOKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVoyname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldiffDay = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnEdit = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.SearchLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnEdit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SearchLookUpEdit1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(5, 1)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(993, 569)
        Me.LayoutControl1.TabIndex = 172
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SearchLookUpEdit1
        '
        Me.SearchLookUpEdit1.Location = New System.Drawing.Point(567, 12)
        Me.SearchLookUpEdit1.Name = "SearchLookUpEdit1"
        Me.SearchLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.SearchLookUpEdit1.Properties.DataSource = Me.mascmrunningBindingSource
        Me.SearchLookUpEdit1.Properties.DisplayMember = "menuname"
        Me.SearchLookUpEdit1.Properties.ValueMember = "idmascmrunning"
        Me.SearchLookUpEdit1.Properties.View = Me.SearchLookUpEdit1View
        Me.SearchLookUpEdit1.Size = New System.Drawing.Size(195, 20)
        Me.SearchLookUpEdit1.StyleController = Me.LayoutControl1
        Me.SearchLookUpEdit1.TabIndex = 174
        '
        'mascmrunningBindingSource
        '
        Me.mascmrunningBindingSource.DataMember = "mascmrunning"
        Me.mascmrunningBindingSource.DataSource = Me.Ykpdtset3
        Me.mascmrunningBindingSource.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmascmrunning, Me.colisrunning, Me.colprefixchar, Me.colshowyear, Me.colformatdate, Me.colseparatechar, Me.colmenuname, Me.coldefault})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colidmascmrunning
        '
        Me.colidmascmrunning.FieldName = "idmascmrunning"
        Me.colidmascmrunning.Name = "colidmascmrunning"
        '
        'colisrunning
        '
        Me.colisrunning.FieldName = "isrunning"
        Me.colisrunning.Name = "colisrunning"
        '
        'colprefixchar
        '
        Me.colprefixchar.FieldName = "prefixchar"
        Me.colprefixchar.Name = "colprefixchar"
        '
        'colshowyear
        '
        Me.colshowyear.FieldName = "showyear"
        Me.colshowyear.Name = "colshowyear"
        '
        'colformatdate
        '
        Me.colformatdate.FieldName = "formatdate"
        Me.colformatdate.Name = "colformatdate"
        '
        'colseparatechar
        '
        Me.colseparatechar.FieldName = "separatechar"
        Me.colseparatechar.Name = "colseparatechar"
        '
        'colmenuname
        '
        Me.colmenuname.Caption = "Depot"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 0
        '
        'coldefault
        '
        Me.coldefault.FieldName = "default"
        Me.coldefault.Name = "coldefault"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(766, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(215, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 173
        Me.SimpleButton1.Text = "Export Excel"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "ctnmain"
        Me.GridControl1.DataSource = Me.Ykpdtset2
        Me.GridControl1.Location = New System.Drawing.Point(12, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnEdit})
        Me.GridControl1.Size = New System.Drawing.Size(969, 519)
        Me.GridControl1.TabIndex = 172
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNMAINID, Me.colCTNAGENT, Me.colCTNSTRING, Me.colCTNSEALID, Me.colCTNSHIPNAME, Me.colCTNSIZE, Me.colCTNSTAT, Me.colCTNVOYN, Me.colCTNWEIGHT, Me.colCTNTAREWEIGHT, Me.colCTNBOOKING, Me.colCTNCONSI, Me.colVoyname, Me.coldiffDay})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCTNMAINID
        '
        Me.colCTNMAINID.FieldName = "CTNMAINID"
        Me.colCTNMAINID.Name = "colCTNMAINID"
        Me.colCTNMAINID.OptionsColumn.AllowEdit = False
        Me.colCTNMAINID.OptionsColumn.ReadOnly = True
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.Caption = "Agent"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 2
        Me.colCTNAGENT.Width = 119
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.Caption = "Containner No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.OptionsColumn.AllowEdit = False
        Me.colCTNSTRING.OptionsColumn.ReadOnly = True
        Me.colCTNSTRING.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CTNSTRING", "จำนวนทั้งหมด {0}")})
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 1
        Me.colCTNSTRING.Width = 142
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.Caption = "Seal"
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.OptionsColumn.AllowEdit = False
        Me.colCTNSEALID.OptionsColumn.ReadOnly = True
        Me.colCTNSEALID.Visible = True
        Me.colCTNSEALID.VisibleIndex = 3
        Me.colCTNSEALID.Width = 129
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.Caption = "Shipper"
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        Me.colCTNSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colCTNSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colCTNSHIPNAME.Visible = True
        Me.colCTNSHIPNAME.VisibleIndex = 6
        Me.colCTNSHIPNAME.Width = 119
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.Caption = "Size"
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.OptionsColumn.AllowEdit = False
        Me.colCTNSIZE.OptionsColumn.ReadOnly = True
        Me.colCTNSIZE.Visible = True
        Me.colCTNSIZE.VisibleIndex = 7
        Me.colCTNSIZE.Width = 119
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.Caption = "Stat"
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        Me.colCTNSTAT.OptionsColumn.AllowEdit = False
        Me.colCTNSTAT.OptionsColumn.ReadOnly = True
        Me.colCTNSTAT.Visible = True
        Me.colCTNSTAT.VisibleIndex = 8
        Me.colCTNSTAT.Width = 119
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.Caption = "Voy N"
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        Me.colCTNVOYN.OptionsColumn.AllowEdit = False
        Me.colCTNVOYN.OptionsColumn.ReadOnly = True
        Me.colCTNVOYN.Width = 119
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.Caption = "Wight"
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNWEIGHT.OptionsColumn.ReadOnly = True
        Me.colCTNWEIGHT.Visible = True
        Me.colCTNWEIGHT.VisibleIndex = 9
        Me.colCTNWEIGHT.Width = 119
        '
        'colCTNTAREWEIGHT
        '
        Me.colCTNTAREWEIGHT.Caption = "TareWeight"
        Me.colCTNTAREWEIGHT.FieldName = "CTNTAREWEIGHT"
        Me.colCTNTAREWEIGHT.Name = "colCTNTAREWEIGHT"
        Me.colCTNTAREWEIGHT.OptionsColumn.AllowEdit = False
        Me.colCTNTAREWEIGHT.OptionsColumn.ReadOnly = True
        Me.colCTNTAREWEIGHT.Visible = True
        Me.colCTNTAREWEIGHT.VisibleIndex = 10
        Me.colCTNTAREWEIGHT.Width = 141
        '
        'colCTNBOOKING
        '
        Me.colCTNBOOKING.Caption = "Booking No."
        Me.colCTNBOOKING.FieldName = "CTNBOOKING"
        Me.colCTNBOOKING.Name = "colCTNBOOKING"
        Me.colCTNBOOKING.OptionsColumn.AllowEdit = False
        Me.colCTNBOOKING.OptionsColumn.ReadOnly = True
        Me.colCTNBOOKING.Visible = True
        Me.colCTNBOOKING.VisibleIndex = 4
        Me.colCTNBOOKING.Width = 148
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        Me.colCTNCONSI.OptionsColumn.AllowEdit = False
        Me.colCTNCONSI.OptionsColumn.ReadOnly = True
        Me.colCTNCONSI.Visible = True
        Me.colCTNCONSI.VisibleIndex = 5
        Me.colCTNCONSI.Width = 108
        '
        'colVoyname
        '
        Me.colVoyname.Caption = "Voyage Name"
        Me.colVoyname.FieldName = "Voyname"
        Me.colVoyname.Name = "colVoyname"
        Me.colVoyname.OptionsColumn.AllowEdit = False
        Me.colVoyname.OptionsColumn.ReadOnly = True
        Me.colVoyname.Visible = True
        Me.colVoyname.VisibleIndex = 0
        Me.colVoyname.Width = 162
        '
        'coldiffDay
        '
        Me.coldiffDay.Caption = "DayInterval"
        Me.coldiffDay.FieldName = "diffDay"
        Me.coldiffDay.Name = "coldiffDay"
        Me.coldiffDay.OptionsColumn.AllowEdit = False
        Me.coldiffDay.OptionsColumn.ReadOnly = True
        Me.coldiffDay.Visible = True
        Me.coldiffDay.VisibleIndex = 11
        Me.coldiffDay.Width = 78
        '
        'btnEdit
        '
        Me.btnEdit.AutoHeight = False
        Me.btnEdit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)})
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl1
        Me.SearchControl1.Location = New System.Drawing.Point(48, 12)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl1
        Me.SearchControl1.Size = New System.Drawing.Size(479, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 171
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem4, Me.LayoutControlItem2, Me.LayoutControlItem3})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(993, 569)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.SearchControl1
        Me.LayoutControlItem1.CustomizationFormText = "ค้นหา"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(519, 26)
        Me.LayoutControlItem1.Text = "ค้นหา"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(33, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.GridControl1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(973, 523)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.SimpleButton1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(754, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(219, 26)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.SearchLookUpEdit1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(519, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(235, 26)
        Me.LayoutControlItem3.Text = "Depot"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(33, 16)
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmview_ctnempty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1003, 572)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmview_ctnempty"
        Me.Text = "frmsearch_voyage"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.SearchLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mascmrunningBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnEdit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents btnEdit As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colCTNMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNTAREWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNBOOKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVoyname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents coldiffDay As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SearchLookUpEdit1 As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents mascmrunningBindingSource As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents colidmascmrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colisrunning As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colprefixchar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colshowyear As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colformatdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colseparatechar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldefault As DevExpress.XtraGrid.Columns.GridColumn
End Class
