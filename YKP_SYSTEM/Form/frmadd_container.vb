﻿Imports Microsoft.Office.Interop
Imports MySql.Data.MySqlClient

Public Class frmadd_container
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim idvoyage As String
    Public Sub New(ByVal Voyageid As String, ByVal voyagename As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = Voyageid
        txt_voyage.Text = Voyageid
        txt_voyagename.Text = voyagename
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Structure ExcelRows
        Dim C1 As String
        Dim C2 As String
        Dim C3 As String
        Dim C4 As String
        Dim C5 As String
    End Structure
    Structure shipper
        Dim id As String
        Dim name As String
    End Structure
    Dim insertshipper() As shipper
    Dim count As Integer = 0
    Dim max As Integer
    Private ExcelRowlist As List(Of ExcelRows) = New List(Of ExcelRows)
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        With OpenFileDialog1
            .Filter = "Excel File  (*.xlsx) | *.xls"
            If (.ShowDialog() = DialogResult.OK) Then
                TextBox1.Text = .FileName
            End If
        End With
    End Sub
    Public Sub add_voyage()

        Dim dt As New DataTable
        connectDB.GetTable("Select COUNT(SHIPPERID) from shipper;", dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            ReDim insertshipper(dt.Rows(i)("COUNT(SHIPPERID)"))
            max = dt.Rows(i)("COUNT(SHIPPERID)")

        Next

        dt = New DataTable
        connectDB.GetTable("Select * from shipper;", dt)
        For i As Integer = 0 To dt.Rows.Count - 1

            insertshipper(i).id = dt.Rows(i)("SHIPPERID")
            If dt.Rows(i)("SHIPNICKNAME") IsNot DBNull.Value Then

                insertshipper(i).name = dt.Rows(i)("SHIPNICKNAME")
            Else
                insertshipper(i).name = ""
            End If



        Next

    End Sub
    Private Function getInfo() As Boolean
        ExcelRowlist.Clear()
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        Dim Excel As New Excel.Application
        If TextBox1.Text <> Nothing Then

            Excel.Workbooks.Open(TextBox1.Text)


            Excel.Sheets("container").Activate()
            Excel.Range("A4").Activate()

            Dim row As New ExcelRows

            Do
                If Excel.ActiveCell.Value > Nothing Or Excel.ActiveCell.Text > Nothing Then


                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C1 = Trim(Excel.ActiveCell.Value)

                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C2 = Trim(Excel.ActiveCell.Value)

                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C3 = Trim(Excel.ActiveCell.Value)

                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C4 = Trim(Excel.ActiveCell.Value)
                    Excel.ActiveCell.Offset(0, 2).Activate()
                    row.C5 = Trim(Excel.ActiveCell.Value)
                    ExcelRowlist.Add(row)
                    Excel.ActiveCell.Offset(1, -6).Activate()


                Else

                    Exit Do

                End If

            Loop

            Return True
        Else


        End If
        Excel.Quit()
        Return True
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click


        ListView1.Items.Clear()


        If getInfo() Then
            For Each Xitem In ExcelRowlist
                Dim ivite As ListViewItem
                ivite = Me.ListView1.Items.Add(Xitem.C1)
                ivite.SubItems.AddRange(New String() {Xitem.C2, Xitem.C3, Xitem.C4, Xitem.C5})
            Next
        End If
        txt_count.Text = ListView1.Items.Count.ToString

    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim sql As String

        If CheckBox1.Checked = True Then
            Sql = "DELETE FROM ctnmain WHERE CTNVOYN = '" & idvoyage & "'"
            connectDB.ExecuteNonQuery(Sql)
        End If

        Dim sql1 As String
        Dim sql2 As String

        Dim idshipper As String
        Dim checkshipper As Integer = 0
        For counter1 = 0 To ListView1.Items.Count - 1
            Try
                For count2 = 0 To max - 1
                    If Trim(ListView1.Items(counter1).SubItems(4).Text) <> "" Then
                        If Trim(insertshipper(count2).name) = Trim(ListView1.Items(counter1).SubItems(4).Text) Then
                            idshipper = insertshipper(count2).id
                            checkshipper = 1
                            Exit For
                        End If

                    End If

                Next
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            If checkshipper = 1 Then
                sql1 = "CTNSTRING , "
                sql2 = " '" & ListView1.Items(counter1).SubItems(0).Text & "' , "
                sql1 += " CTNSIZE , "
                sql2 += " '" & ListView1.Items(counter1).SubItems(1).Text & "' , "
                sql1 += " CTNAGENT , "
                sql2 += " '" & ListView1.Items(counter1).SubItems(2).Text & "' , "
                sql1 += " CTNSTAT , "
                sql2 += " '1' , "
                sql1 += " CTNCONSI , "
                sql2 += " '" & idshipper & "' , "
                sql1 += " CTNVOYN , "
                sql2 += " '" & idvoyage & "' , "
                sql1 += " CTNSHIPNAME  "
                sql2 += " '" & ListView1.Items(counter1).SubItems(4).Text & "'  "
                Sql = "INSERT INTO ctnmain ( " & sql1 & ") VALUES ( " & sql2 & " );"
                connectDB.ExecuteNonQuery(Sql)
                checkshipper = 0
            ElseIf ListView1.Items(counter1).SubItems(4).Text = "DEPOT" Then
                Dim idlast As String
                Try
                    sql1 = "CTNSTRING , "
                    sql2 = " '" & ListView1.Items(counter1).SubItems(0).Text & "' , "
                    sql1 += " CTNSIZE , "
                    sql2 += " '" & ListView1.Items(counter1).SubItems(1).Text & "' , "
                    sql1 += " CTNAGENT , "
                    sql2 += " '" & ListView1.Items(counter1).SubItems(2).Text & "' , "
                    sql1 += " CTNSTAT , "
                    sql2 += " '0' , "
                    sql1 += " CTNCONSI , "
                    sql2 += " 'DEPOT' , "
                    sql1 += " CTNVOYN , "
                    sql2 += " '" & idvoyage & "' , "
                    sql1 += " CTNSHIPNAME  "
                    sql2 += " '" & ListView1.Items(counter1).SubItems(4).Text & "'  "
                    Sql = "INSERT INTO ctnmain ( " & sql1 & ") VALUES ( " & sql2 & " ); SELECT LAST_INSERT_ID() "
                    idlast = connectDB.ExecuteScalar(Sql)

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try

                sql1 = "CTNID , "
                sql2 = " '" & idlast & "' , "
                sql1 += " CTNDATEIN , "
                sql2 += " '0' , "
                sql1 += " CTNSTAT , "
                sql2 += " '1' , "
                sql1 += " CTNDAYRE "
                sql2 += " '0' "

                Sql = "INSERT INTO ctndepot ( " & sql1 & ") VALUES ( " & sql2 & " ); "
                connectDB.ExecuteNonQuery(Sql)
            Else
                sql1 = "CTNSTRING , "
                sql2 = " '" & ListView1.Items(counter1).SubItems(0).Text & "' , "
                sql1 += " CTNSIZE , "
                sql2 += " '" & ListView1.Items(counter1).SubItems(1).Text & "' , "
                sql1 += " CTNAGENT , "
                sql2 += " '" & ListView1.Items(counter1).SubItems(2).Text & "' , "
                sql1 += " CTNSTAT , "
                sql2 += " '1' , "
                sql1 += " CTNCONSI , "
                sql2 += " '' , "
                sql1 += " CTNVOYN , "
                sql2 += " '" & idvoyage & "' , "
                sql = "INSERT INTO ctnmain ( " & sql1 & ") VALUES ( " & sql2 & " );"
                connectDB.ExecuteNonQuery(Sql)

            End If




            checkshipper = 0
        Next


        MsgBox("บันทึกข้อมูลเรียบร้อย")
        ListView1.Items.Clear()

    End Sub


    Private Sub frmadd_container_Load_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        add_voyage()

    End Sub
End Class