﻿Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Grid

Public Class frmdel_ContainerBooking
    Dim idbooking As String

    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Dim ykpset_move As New ykpdtset
    Dim idvoyage As String

    Dim idbookingmove As String
    Public Sub New(ByVal voyage As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = voyage
        ' Add any initialization after the InitializeComponent() call.
        loadBooking(voyage)

    End Sub


    Public Sub loadBooking(ByVal voyage As String)
        Dim sql As String
        sql = "Select * ,CONCAT(BCTNNO,' X ', BCTNTYPE ) AS CTNNO from booking where BVOYAGE ='" & voyage & "' ; "
        ykpset.Tables("booking").Clear()
        connectDB.GetTable(sql, ykpset.Tables("booking"))
        GridControl5.DataSource = ykpset.Tables("booking")

    End Sub

    Private Sub frmmove_ctn_Load(sender As Object, e As EventArgs) Handles MyBase.Load


    End Sub
    Private Sub GridView5_RowCellClick(sender As Object, e As RowCellClickEventArgs) Handles GridView5.RowCellClick

        LoadContainer(GridView5.GetDataRow(GridView5.FocusedRowHandle)("BOOKINGID").ToString())

    End Sub
    Public Sub LoadContainer(ByVal bookingid As String)
        Dim sql As String
        sql = "Select * from borrow join ctnmain on borrow.CTNID = ctnmain.CTNMAINID join voyage on ctnmain.CTNVOYN = voyage.VOYAGEID  where BOOKID ='" & bookingid & "' ;"
        ykpset.Tables("ctnmain").Clear()
        connectDB.GetTable(sql, ykpset.Tables("ctnmain"))
        GridControl4.DataSource = ykpset.Tables("ctnmain")
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        If XtraMessageBox.Show("ต้องการลบ ตู้ Containerใช่หรือไม่ ", "ตกลง", MessageBoxButtons.YesNo) <> DialogResult.No Then

            Dim sql As String
            For i As Integer = 0 To GridView4.SelectedRowsCount - 1


                sql = "UPDATE ctnmain SET CTNSTAT = '1',CTNSEALID = '',CTNWEIGHT ='0' where CTNMAINID = '" & GridView4.GetDataRow(GridView4.GetSelectedRows()(i))("CTNMAINID").ToString() & "'; "
                connectDB.ExecuteNonQuery(sql)
                sql = "DELETE FROM borrow where idborrow = '" & GridView4.GetDataRow(GridView4.GetSelectedRows()(i))("idborrow").ToString & "';"
                connectDB.ExecuteNonQuery(sql)


            Next
        Else
            Interaction.MsgBox("ยังไม่ได้เลือก Voyage ที่ต้องการย้าย", MsgBoxStyle.OkOnly, Nothing)
            Exit Sub
        End If
        LoadContainer(GridView5.GetDataRow(GridView5.FocusedRowHandle)("BOOKINGID").ToString())


    End Sub
End Class
