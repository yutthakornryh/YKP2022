﻿Imports DevExpress.XtraPrinting
Imports MySql.Data.MySqlClient
Public Class frmdel_shipper

    Dim respone As Object
    Dim idvoys As String
    Dim idvoyn As String
    Dim id_primary As String
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Private Sub frmdel_shipper_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        searchData()
    End Sub

    Public Sub deleteData()
        Dim sql As String
        Dim respone As Object
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            Try
                Dim SHIPPERID As String
                SHIPPERID = GridView1.GetDataRow(GridView1.FocusedRowHandle)("SHIPPERID").ToString
                sql = "DELETE FROM shipper where SHIPPERID = '" & SHIPPERID & "';"
                connectDB.ExecuteNonQuery(sql)
                searchData()
            Catch ex As Exception
                MsgBox(ex.ToString)
                Exit Sub
            End Try
        End If
    End Sub
    Public Sub searchData()
        Dim sql As String
        Dim dt As New DataTable
        sql = "Select * from shipper ;"
        ykpset.Tables("shipper").Clear()
        connectDB.GetTable(sql, ykpset.Tables("shipper"))
        GridControl1.DataSource = ykpset.Tables("shipper")
    End Sub
    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        searchData()
    End Sub

    Private Sub TextBoxItem2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = Keys.Enter Then
            searchData()
        End If
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        deleteData()
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Try

            Dim sv As New SaveFileDialog
            sv.Filter = "Excel Workbook|*.xlsx"
            If sv.ShowDialog() = DialogResult.OK And sv.FileName <> Nothing Then
                If sv.FileName.EndsWith(".xlsx") Then
                    Dim path = sv.FileName.ToString()
                    Dim x As New XlsxExportOptionsEx
                    x.AllowGrouping = DevExpress.Utils.DefaultBoolean.False
                    x.AllowFixedColumnHeaderPanel = DevExpress.Utils.DefaultBoolean.False
                    GridControl1.ExportToXlsx(path, x)
                End If


                MessageBox.Show("Data Exported to :" + vbCrLf + sv.FileName, "Business Intelligence Portal", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                sv.FileName = Nothing

            End If
        Catch ex As Exception

        End Try
    End Sub
End Class