﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreportcontainer
    Inherits System.Windows.Forms.Form

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.slcmasprgtemplate = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.masprgtemplateBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LayoutSet1 = New YKP_SYSTEM.layoutSet()
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colprgtemp_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidmasprgtemplate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.collayout = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colf_default = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.slcEnd = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.slcStart = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.fieldBFORWARDERNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldBNO1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldBNSHIPNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldCountIN1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldCountOut1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldCTNAGENT1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldCTNSIZE1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldCTNSTRING1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldvoyname1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldvoynamen1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldvoyvesnamen1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fielddatenMonth = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.slcmasprgtemplate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.masprgtemplateBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SimpleButton4)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton3)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.slcmasprgtemplate)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.slcEnd)
        Me.LayoutControl1.Controls.Add(Me.slcStart)
        Me.LayoutControl1.Controls.Add(Me.PivotGridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(959, 623)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Location = New System.Drawing.Point(501, 12)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(78, 22)
        Me.SimpleButton4.StyleController = Me.LayoutControl1
        Me.SimpleButton4.TabIndex = 13
        Me.SimpleButton4.Text = "Excel"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(872, 12)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(75, 22)
        Me.SimpleButton3.StyleController = Me.LayoutControl1
        Me.SimpleButton3.TabIndex = 12
        Me.SimpleButton3.Text = "เพิ่มใหม่"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(792, 12)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(76, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 11
        Me.SimpleButton2.Text = "แก้ไข"
        '
        'slcmasprgtemplate
        '
        Me.slcmasprgtemplate.Location = New System.Drawing.Point(649, 12)
        Me.slcmasprgtemplate.Name = "slcmasprgtemplate"
        Me.slcmasprgtemplate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcmasprgtemplate.Properties.DataSource = Me.masprgtemplateBindingSource
        Me.slcmasprgtemplate.Properties.DisplayMember = "prgtemp_name"
        Me.slcmasprgtemplate.Properties.View = Me.SearchLookUpEdit3View
        Me.slcmasprgtemplate.Size = New System.Drawing.Size(139, 20)
        Me.slcmasprgtemplate.StyleController = Me.LayoutControl1
        Me.slcmasprgtemplate.TabIndex = 9
        '
        'masprgtemplateBindingSource
        '
        Me.masprgtemplateBindingSource.DataMember = "masprgtemplate"
        Me.masprgtemplateBindingSource.DataSource = Me.LayoutSet1
        Me.masprgtemplateBindingSource.Sort = ""
        '
        'LayoutSet1
        '
        Me.LayoutSet1.DataSetName = "layoutSet"
        Me.LayoutSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colprgtemp_name, Me.colidmasprgtemplate, Me.collayout, Me.colstatus, Me.colf_default})
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'colprgtemp_name
        '
        Me.colprgtemp_name.Caption = "Report Name"
        Me.colprgtemp_name.FieldName = "prgtemp_name"
        Me.colprgtemp_name.Name = "colprgtemp_name"
        Me.colprgtemp_name.Visible = True
        Me.colprgtemp_name.VisibleIndex = 0
        '
        'colidmasprgtemplate
        '
        Me.colidmasprgtemplate.FieldName = "idmasprgtemplate"
        Me.colidmasprgtemplate.Name = "colidmasprgtemplate"
        '
        'collayout
        '
        Me.collayout.FieldName = "layout"
        Me.collayout.Name = "collayout"
        '
        'colstatus
        '
        Me.colstatus.FieldName = "status"
        Me.colstatus.Name = "colstatus"
        '
        'colf_default
        '
        Me.colf_default.FieldName = "f_default"
        Me.colf_default.Name = "colf_default"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(420, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(77, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 8
        Me.SimpleButton1.Text = "Load"
        '
        'slcEnd
        '
        Me.slcEnd.Location = New System.Drawing.Point(276, 12)
        Me.slcEnd.Name = "slcEnd"
        Me.slcEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcEnd.Properties.DataSource = Me.voyageBindingSource1
        Me.slcEnd.Properties.DisplayMember = "VOYNAME"
        Me.slcEnd.Properties.ValueMember = "VOYAGEID"
        Me.slcEnd.Properties.View = Me.SearchLookUpEdit2View
        Me.slcEnd.Size = New System.Drawing.Size(140, 20)
        Me.slcEnd.StyleController = Me.LayoutControl1
        Me.slcEnd.TabIndex = 7
        '
        'voyageBindingSource1
        '
        Me.voyageBindingSource1.DataMember = "voyage"
        Me.voyageBindingSource1.DataSource = Me.Ykpdtset2
        Me.voyageBindingSource1.Sort = ""
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11})
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "VOYAGEID"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "VOYVESIDN"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "VOYVESIDS"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'GridColumn4
        '
        Me.GridColumn4.FieldName = "VOYDATESN"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn5
        '
        Me.GridColumn5.FieldName = "VOYDATESS"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 3
        Me.GridColumn5.Width = 336
        '
        'GridColumn6
        '
        Me.GridColumn6.FieldName = "VOYDATEEN"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GridColumn7
        '
        Me.GridColumn7.FieldName = "VOYVESNAMEN"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 1
        Me.GridColumn7.Width = 336
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "VOYDATEES"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 4
        Me.GridColumn8.Width = 340
        '
        'GridColumn9
        '
        Me.GridColumn9.FieldName = "VOYVESNAMES"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 2
        Me.GridColumn9.Width = 336
        '
        'GridColumn10
        '
        Me.GridColumn10.FieldName = "VOYTIMEHHMMNN"
        Me.GridColumn10.Name = "GridColumn10"
        '
        'GridColumn11
        '
        Me.GridColumn11.FieldName = "VOYNAME"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        Me.GridColumn11.Width = 258
        '
        'slcStart
        '
        Me.slcStart.Location = New System.Drawing.Point(78, 12)
        Me.slcStart.Name = "slcStart"
        Me.slcStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcStart.Properties.DataSource = Me.voyageBindingSource
        Me.slcStart.Properties.DisplayMember = "VOYNAME"
        Me.slcStart.Properties.ValueMember = "VOYAGEID"
        Me.slcStart.Properties.View = Me.SearchLookUpEdit1View
        Me.slcStart.Size = New System.Drawing.Size(128, 20)
        Me.slcStart.StyleController = Me.LayoutControl1
        Me.slcStart.TabIndex = 6
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset1
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        Me.colVOYDATESS.Visible = True
        Me.colVOYDATESS.VisibleIndex = 3
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 1
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        Me.colVOYDATEES.Visible = True
        Me.colVOYDATEES.VisibleIndex = 4
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 2
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.DataMember = "CTNREPORT"
        Me.PivotGridControl1.DataSource = Me.Ykpdtset4
        Me.PivotGridControl1.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.fieldBFORWARDERNAME1, Me.fieldBNO1, Me.fieldBNSHIPNAME1, Me.fieldCountIN1, Me.fieldCountOut1, Me.fieldCTNAGENT1, Me.fieldCTNSIZE1, Me.fieldCTNSTRING1, Me.fieldvoyname1, Me.fieldvoynamen1, Me.fieldvoyvesnamen1, Me.fielddatenMonth})
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 38)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.OptionsData.DataProcessingEngine = DevExpress.XtraPivotGrid.PivotDataProcessingEngine.LegacyOptimized
        Me.PivotGridControl1.Size = New System.Drawing.Size(935, 573)
        Me.PivotGridControl1.TabIndex = 5
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldBFORWARDERNAME1
        '
        Me.fieldBFORWARDERNAME1.AreaIndex = 0
        Me.fieldBFORWARDERNAME1.Caption = "Forwardername"
        Me.fieldBFORWARDERNAME1.FieldName = "BFORWARDERNAME"
        Me.fieldBFORWARDERNAME1.Name = "fieldBFORWARDERNAME1"
        '
        'fieldBNO1
        '
        Me.fieldBNO1.AreaIndex = 1
        Me.fieldBNO1.FieldName = "BNO"
        Me.fieldBNO1.Name = "fieldBNO1"
        '
        'fieldBNSHIPNAME1
        '
        Me.fieldBNSHIPNAME1.AreaIndex = 2
        Me.fieldBNSHIPNAME1.Caption = "Shipname"
        Me.fieldBNSHIPNAME1.FieldName = "BNSHIPNAME"
        Me.fieldBNSHIPNAME1.Name = "fieldBNSHIPNAME1"
        '
        'fieldCountIN1
        '
        Me.fieldCountIN1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldCountIN1.AreaIndex = 0
        Me.fieldCountIN1.Caption = "IN"
        Me.fieldCountIN1.CellFormat.FormatString = "n"
        Me.fieldCountIN1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldCountIN1.FieldName = "CountIN"
        Me.fieldCountIN1.Name = "fieldCountIN1"
        '
        'fieldCountOut1
        '
        Me.fieldCountOut1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldCountOut1.AreaIndex = 1
        Me.fieldCountOut1.Caption = "Out"
        Me.fieldCountOut1.CellFormat.FormatString = "n"
        Me.fieldCountOut1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldCountOut1.FieldName = "CountOut"
        Me.fieldCountOut1.Name = "fieldCountOut1"
        '
        'fieldCTNAGENT1
        '
        Me.fieldCTNAGENT1.AreaIndex = 9
        Me.fieldCTNAGENT1.Caption = "Agent"
        Me.fieldCTNAGENT1.FieldName = "CTNAGENT"
        Me.fieldCTNAGENT1.Name = "fieldCTNAGENT1"
        '
        'fieldCTNSIZE1
        '
        Me.fieldCTNSIZE1.AreaIndex = 8
        Me.fieldCTNSIZE1.Caption = "Size"
        Me.fieldCTNSIZE1.FieldName = "CTNSIZE"
        Me.fieldCTNSIZE1.Name = "fieldCTNSIZE1"
        '
        'fieldCTNSTRING1
        '
        Me.fieldCTNSTRING1.AreaIndex = 3
        Me.fieldCTNSTRING1.FieldName = "CTNSTRING"
        Me.fieldCTNSTRING1.Name = "fieldCTNSTRING1"
        '
        'fieldvoyname1
        '
        Me.fieldvoyname1.AreaIndex = 4
        Me.fieldvoyname1.FieldName = "voyname"
        Me.fieldvoyname1.Name = "fieldvoyname1"
        '
        'fieldvoynamen1
        '
        Me.fieldvoynamen1.AreaIndex = 5
        Me.fieldvoynamen1.FieldName = "voynamen"
        Me.fieldvoynamen1.Name = "fieldvoynamen1"
        '
        'fieldvoyvesnamen1
        '
        Me.fieldvoyvesnamen1.AreaIndex = 6
        Me.fieldvoyvesnamen1.FieldName = "voyvesnamen"
        Me.fieldvoyvesnamen1.Name = "fieldvoyvesnamen1"
        '
        'fielddatenMonth
        '
        Me.fielddatenMonth.AreaIndex = 7
        Me.fielddatenMonth.FieldName = "daten.Month"
        Me.fielddatenMonth.Name = "fielddatenMonth"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem7, Me.LayoutControlItem6, Me.LayoutControlItem8})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(959, 623)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.PivotGridControl1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(939, 577)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.slcStart
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(198, 26)
        Me.LayoutControlItem1.Text = "Voyage Start"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(63, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.slcEnd
        Me.LayoutControlItem3.Location = New System.Drawing.Point(198, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(210, 26)
        Me.LayoutControlItem3.Text = "Voyage End"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(63, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(408, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(81, 26)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.slcmasprgtemplate
        Me.LayoutControlItem5.Location = New System.Drawing.Point(571, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(209, 26)
        Me.LayoutControlItem5.Text = "Report"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(63, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.SimpleButton2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(780, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(80, 26)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.SimpleButton3
        Me.LayoutControlItem6.Location = New System.Drawing.Point(860, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(79, 26)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.SimpleButton4
        Me.LayoutControlItem8.Location = New System.Drawing.Point(489, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(82, 26)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmreportcontainer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(959, 623)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmreportcontainer"
        Me.Tag = "frmreportcontainer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.slcmasprgtemplate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.masprgtemplateBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents slcEnd As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents slcStart As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents voyageBindingSource1 As BindingSource
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents slcmasprgtemplate As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents masprgtemplateBindingSource As BindingSource
    Friend WithEvents LayoutSet1 As layoutSet
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents fieldBFORWARDERNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldBNO1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldBNSHIPNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldCountIN1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldCountOut1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldCTNAGENT1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldCTNSIZE1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldCTNSTRING1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldvoyname1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldvoynamen1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldvoyvesnamen1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fielddatenMonth As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colprgtemp_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidmasprgtemplate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents collayout As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colf_default As DevExpress.XtraGrid.Columns.GridColumn
End Class
