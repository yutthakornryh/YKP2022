﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmdel_vessel
      Inherits DevComponents.DotNetBar.RibbonForm
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVESMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSNAM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSMASTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSNATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVSSDATE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(5, 1)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(836, 492)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl1
        Me.SearchControl1.Location = New System.Drawing.Point(45, 12)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl1
        Me.SearchControl1.Size = New System.Drawing.Size(779, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 5
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "vesmain"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.Location = New System.Drawing.Point(12, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnDelete})
        Me.GridControl1.Size = New System.Drawing.Size(812, 442)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVESMAINID, Me.colVSSNAM, Me.colVSSMASTER, Me.colVSSNATION, Me.colVSSINS, Me.colVSSDATE, Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colVESMAINID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colVESMAINID
        '
        Me.colVESMAINID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVESMAINID.AppearanceCell.Options.UseFont = True
        Me.colVESMAINID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVESMAINID.AppearanceHeader.Options.UseFont = True
        Me.colVESMAINID.FieldName = "VESMAINID"
        Me.colVESMAINID.Name = "colVESMAINID"
        Me.colVESMAINID.OptionsColumn.AllowEdit = False
        Me.colVESMAINID.OptionsColumn.ReadOnly = True
        Me.colVESMAINID.Visible = True
        Me.colVESMAINID.VisibleIndex = 0
        Me.colVESMAINID.Width = 125
        '
        'colVSSNAM
        '
        Me.colVSSNAM.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSNAM.AppearanceCell.Options.UseFont = True
        Me.colVSSNAM.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSNAM.AppearanceHeader.Options.UseFont = True
        Me.colVSSNAM.FieldName = "VSSNAM"
        Me.colVSSNAM.Name = "colVSSNAM"
        Me.colVSSNAM.OptionsColumn.AllowEdit = False
        Me.colVSSNAM.OptionsColumn.ReadOnly = True
        Me.colVSSNAM.Visible = True
        Me.colVSSNAM.VisibleIndex = 1
        Me.colVSSNAM.Width = 242
        '
        'colVSSMASTER
        '
        Me.colVSSMASTER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSMASTER.AppearanceCell.Options.UseFont = True
        Me.colVSSMASTER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSMASTER.AppearanceHeader.Options.UseFont = True
        Me.colVSSMASTER.FieldName = "VSSMASTER"
        Me.colVSSMASTER.Name = "colVSSMASTER"
        Me.colVSSMASTER.OptionsColumn.AllowEdit = False
        Me.colVSSMASTER.OptionsColumn.ReadOnly = True
        Me.colVSSMASTER.Visible = True
        Me.colVSSMASTER.VisibleIndex = 2
        Me.colVSSMASTER.Width = 242
        '
        'colVSSNATION
        '
        Me.colVSSNATION.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSNATION.AppearanceCell.Options.UseFont = True
        Me.colVSSNATION.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSNATION.AppearanceHeader.Options.UseFont = True
        Me.colVSSNATION.FieldName = "VSSNATION"
        Me.colVSSNATION.Name = "colVSSNATION"
        Me.colVSSNATION.OptionsColumn.AllowEdit = False
        Me.colVSSNATION.OptionsColumn.ReadOnly = True
        Me.colVSSNATION.Visible = True
        Me.colVSSNATION.VisibleIndex = 3
        Me.colVSSNATION.Width = 242
        '
        'colVSSINS
        '
        Me.colVSSINS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSINS.AppearanceCell.Options.UseFont = True
        Me.colVSSINS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSINS.AppearanceHeader.Options.UseFont = True
        Me.colVSSINS.FieldName = "VSSINS"
        Me.colVSSINS.Name = "colVSSINS"
        Me.colVSSINS.OptionsColumn.AllowEdit = False
        Me.colVSSINS.OptionsColumn.ReadOnly = True
        Me.colVSSINS.Visible = True
        Me.colVSSINS.VisibleIndex = 4
        Me.colVSSINS.Width = 242
        '
        'colVSSDATE
        '
        Me.colVSSDATE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSDATE.AppearanceCell.Options.UseFont = True
        Me.colVSSDATE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colVSSDATE.AppearanceHeader.Options.UseFont = True
        Me.colVSSDATE.FieldName = "VSSDATE"
        Me.colVSSDATE.Name = "colVSSDATE"
        Me.colVSSDATE.OptionsColumn.AllowEdit = False
        Me.colVSSDATE.OptionsColumn.ReadOnly = True
        Me.colVSSDATE.Visible = True
        Me.colVSSDATE.VisibleIndex = 5
        Me.colVSSDATE.Width = 242
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.Caption = "Delete"
        Me.GridColumn1.ColumnEdit = Me.btnDelete
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 6
        Me.GridColumn1.Width = 252
        '
        'btnDelete
        '
        Me.btnDelete.AutoHeight = False
        Me.btnDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)})
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(836, 492)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(816, 446)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.SearchControl1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(816, 26)
        Me.LayoutControlItem2.Text = "ค้นหา"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(30, 16)
        '
        'frmdel_vessel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(846, 495)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmdel_vessel"
        Me.Text = "frmdel_vessel"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colVESMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSNAM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSMASTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSNATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVSSDATE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
End Class
