﻿Imports MySql.Data.MySqlClient
Public Class frmdel_vessel

    Dim respone As Object
    Dim id_primary As String
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim ykpset As New ykpdtset
    Private Sub frmdel_vessel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture

        searchData()
    End Sub


    Public Sub searchData()

        Dim sql As String
        Dim dt As New DataTable
        sql = "Select * from vesmain;"
        ykpset.Tables("vesmain").Clear()
        connectDB.GetTable(sql, ykpset.Tables("vesmain"))
        GridControl1.DataSource = ykpset.Tables("vesmain")


    End Sub

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        searchData()

    End Sub



    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        deleteData()
    End Sub

    Public Sub deleteData()

        Dim sql As String

        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
            If respone = 1 Then

            Try

                Dim VESMAINID As String
                VESMAINID = GridView1.GetDataRow(GridView1.FocusedRowHandle)("VESMAINID").ToString
                sql = "DELETE FROM vesmain where VESMAINID = '" & VESMAINID & "';"
                connectDB.ExecuteNonQuery(Sql)

                searchData()

                Catch ex As Exception

                    MsgBox(ex.ToString)
                    Exit Sub
                End Try


        End If

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        deleteData()
    End Sub
End Class