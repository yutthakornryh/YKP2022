﻿Imports MySql.Data.MySqlClient
Public Class frmsearch_codedepot
    Dim mySqlCommand As New MySqlCommand
    Dim mySqlAdaptor As New MySqlDataAdapter
    Dim mySqlReader As MySqlDataReader
    Dim respone As Object

    Public Shared iddepot As String
    Dim mysql As MySqlConnection = main_form.mysqlconection
    Private Sub frmsearch_codedepot_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        mysql.Close()

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

        mySqlCommand.CommandText = "Select * from ctndepotunit;"
        ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql

        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())

                With ListView1.Items.Add(mySqlReader("idctndepotunit"))

                   
                    .SubItems.Add(mySqlReader("depotname"))

                    .SubItems.Add(mySqlReader("depotmanhr"))

                End With

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()
    End Sub
    Public Sub searchdata()
        mysql.Close()

        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If

      
        mySqlCommand.CommandText = "Select * from ctndepotunit where  depotname ='%" & TextBoxItem2.Text & "%' ;"
        ' mySqlCommand.CommandText -0mySqlCommand.Connection = mysql

        mySqlCommand.Connection = mysql
        mySqlAdaptor.SelectCommand = mySqlCommand
        Try
            mySqlReader = mySqlCommand.ExecuteReader

            While (mySqlReader.Read())

                  With ListView1.Items.Add(mySqlReader("idctndepotunit"))


                    .SubItems.Add(mySqlReader("depotname"))

                    .SubItems.Add(mySqlReader("depotmanhr"))

                End With

            End While
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        mysql.Close()


    End Sub

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem1.Click
        searchdata()
    End Sub

    Private Sub TextBoxItem2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBoxItem2.KeyDown
        If e.KeyCode = Keys.Enter Then
            searchdata()
        End If
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        If ListView1.SelectedItems.Count > 0 Then
            iddepot = ListView1.SelectedItems(0).SubItems(0).Text
            Dim cf As New frmedit_depot

            cf.MdiParent = Me.MdiParent
            Me.Close()
            cf.Dock = DockStyle.Fill
            cf.Show()
        Else
            MsgBox("กรุณาเลือกข้อมูล")
        End If
    End Sub
End Class