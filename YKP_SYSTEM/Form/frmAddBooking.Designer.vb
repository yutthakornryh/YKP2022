﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAddBooking
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.btnAddRow = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBOOKINGID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBLANDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCTNTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBCOM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RadioGroup3 = New DevExpress.XtraEditors.RadioGroup()
        Me.BTYPEMOVE = New DevExpress.XtraEditors.RadioGroup()
        Me.txt_tsport = New DevExpress.XtraEditors.TextEdit()
        Me.txtUnitCtn = New System.Windows.Forms.ComboBox()
        Me.txtNumberWord = New DevExpress.XtraEditors.TextEdit()
        Me.BCHECKLOAD = New DevExpress.XtraEditors.RadioGroup()
        Me.txt_type = New System.Windows.Forms.ComboBox()
        Me.TextBox4 = New DevExpress.XtraEditors.TextEdit()
        Me.txt_pol = New DevExpress.XtraEditors.TextEdit()
        Me.txt_localforwared = New DevExpress.XtraEditors.TextEdit()
        Me.txt_TUG = New DevExpress.XtraEditors.TextEdit()
        Me.txt_notify = New DevExpress.XtraEditors.MemoEdit()
        Me.txt_Gross = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox3 = New DevExpress.XtraEditors.TextEdit()
        Me.txtPlace = New DevExpress.XtraEditors.TextEdit()
        Me.txtNumber = New DevExpress.XtraEditors.TextEdit()
        Me.billboking = New DevExpress.XtraEditors.TextEdit()
        Me.BDESCRIPT = New DevExpress.XtraEditors.MemoEdit()
        Me.txtShippingAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.BMOTH = New DevExpress.XtraEditors.TextEdit()
        Me.BREMARK = New DevExpress.XtraEditors.TextEdit()
        Me.BINCOTERM = New DevExpress.XtraEditors.TextEdit()
        Me.BINVOICE = New DevExpress.XtraEditors.TextEdit()
        Me.txtconsignee = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.consigneeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit3View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCONSIGNEEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONADD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONTELL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONFAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtForwarder = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.shipperBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit2View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIPPERID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPNICKNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPADD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPTELL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIPFAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txtconsigneeAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.txtForwarderAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidbooking_bl_ctn = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colidnumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbookid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname_1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.slcmasterbl_ctn = New DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit()
        Me.MasterblctnBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RepositoryItemSearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colidmasterbl_ctn = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname_2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnUpdate = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.BPENANG = New DevExpress.XtraEditors.TextEdit()
        Me.txt_remark = New DevExpress.XtraEditors.TextEdit()
        Me.txt_comodity = New DevExpress.XtraEditors.TextEdit()
        Me.txt_scn = New DevExpress.XtraEditors.TextEdit()
        Me.BNICKAGENT = New DevExpress.XtraEditors.TextEdit()
        Me.txtShipping = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.txt_booking_no = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BTYPEMOVE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_tsport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumberWord.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BCHECKLOAD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_pol.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_localforwared.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_TUG.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_notify.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_Gross.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPlace.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.billboking.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BDESCRIPT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShippingAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BMOTH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BREMARK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BINCOTERM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BINVOICE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtconsignee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.consigneeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForwarder.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.shipperBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtconsigneeAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtForwarderAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.slcmasterbl_ctn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MasterblctnBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnUpdate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BPENANG.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_remark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_comodity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_scn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BNICKAGENT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtShipping.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_booking_no.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.btnAddRow)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup3)
        Me.LayoutControl1.Controls.Add(Me.BTYPEMOVE)
        Me.LayoutControl1.Controls.Add(Me.txt_tsport)
        Me.LayoutControl1.Controls.Add(Me.txtUnitCtn)
        Me.LayoutControl1.Controls.Add(Me.txtNumberWord)
        Me.LayoutControl1.Controls.Add(Me.BCHECKLOAD)
        Me.LayoutControl1.Controls.Add(Me.txt_type)
        Me.LayoutControl1.Controls.Add(Me.TextBox4)
        Me.LayoutControl1.Controls.Add(Me.txt_pol)
        Me.LayoutControl1.Controls.Add(Me.txt_localforwared)
        Me.LayoutControl1.Controls.Add(Me.txt_TUG)
        Me.LayoutControl1.Controls.Add(Me.txt_notify)
        Me.LayoutControl1.Controls.Add(Me.txt_Gross)
        Me.LayoutControl1.Controls.Add(Me.TextBox3)
        Me.LayoutControl1.Controls.Add(Me.txtPlace)
        Me.LayoutControl1.Controls.Add(Me.txtNumber)
        Me.LayoutControl1.Controls.Add(Me.billboking)
        Me.LayoutControl1.Controls.Add(Me.BDESCRIPT)
        Me.LayoutControl1.Controls.Add(Me.txtShippingAddress)
        Me.LayoutControl1.Controls.Add(Me.BMOTH)
        Me.LayoutControl1.Controls.Add(Me.BREMARK)
        Me.LayoutControl1.Controls.Add(Me.BINCOTERM)
        Me.LayoutControl1.Controls.Add(Me.BINVOICE)
        Me.LayoutControl1.Controls.Add(Me.txtconsignee)
        Me.LayoutControl1.Controls.Add(Me.txtForwarder)
        Me.LayoutControl1.Controls.Add(Me.txtconsigneeAddress)
        Me.LayoutControl1.Controls.Add(Me.txtForwarderAddress)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.BPENANG)
        Me.LayoutControl1.Controls.Add(Me.txt_remark)
        Me.LayoutControl1.Controls.Add(Me.txt_comodity)
        Me.LayoutControl1.Controls.Add(Me.txt_scn)
        Me.LayoutControl1.Controls.Add(Me.BNICKAGENT)
        Me.LayoutControl1.Controls.Add(Me.txtShipping)
        Me.LayoutControl1.Controls.Add(Me.txt_booking_no)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(752, 293, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1168, 606)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'btnAddRow
        '
        Me.btnAddRow.Location = New System.Drawing.Point(371, 406)
        Me.btnAddRow.Name = "btnAddRow"
        Me.btnAddRow.Size = New System.Drawing.Size(103, 22)
        Me.btnAddRow.StyleController = Me.LayoutControl1
        Me.btnAddRow.TabIndex = 45
        Me.btnAddRow.Text = "Add"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(1030, 572)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(126, 22)
        Me.SimpleButton2.StyleController = Me.LayoutControl1
        Me.SimpleButton2.TabIndex = 44
        Me.SimpleButton2.Text = "Clear"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(912, 572)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(114, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 43
        Me.SimpleButton1.Text = "Save"
        '
        'GridControl2
        '
        Me.GridControl2.DataMember = "booking"
        Me.GridControl2.DataSource = Me.Ykpdtset1
        Me.GridControl2.Location = New System.Drawing.Point(12, 12)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(462, 245)
        Me.GridControl2.TabIndex = 42
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBOOKINGID, Me.colBNO, Me.colBLANDNO, Me.colBCTNTYPE, Me.colBCOM, Me.colBSHIPNAME})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colBOOKINGID
        '
        Me.colBOOKINGID.FieldName = "BOOKINGID"
        Me.colBOOKINGID.Name = "colBOOKINGID"
        Me.colBOOKINGID.OptionsColumn.AllowEdit = False
        Me.colBOOKINGID.OptionsColumn.ReadOnly = True
        Me.colBOOKINGID.Width = 43
        '
        'colBNO
        '
        Me.colBNO.Caption = "Booking No."
        Me.colBNO.FieldName = "BNO"
        Me.colBNO.Name = "colBNO"
        Me.colBNO.OptionsColumn.AllowEdit = False
        Me.colBNO.OptionsColumn.ReadOnly = True
        Me.colBNO.Visible = True
        Me.colBNO.VisibleIndex = 0
        Me.colBNO.Width = 80
        '
        'colBLANDNO
        '
        Me.colBLANDNO.Caption = "Bill of No."
        Me.colBLANDNO.FieldName = "BLANDNO"
        Me.colBLANDNO.Name = "colBLANDNO"
        Me.colBLANDNO.OptionsColumn.AllowEdit = False
        Me.colBLANDNO.OptionsColumn.ReadOnly = True
        Me.colBLANDNO.Visible = True
        Me.colBLANDNO.VisibleIndex = 1
        Me.colBLANDNO.Width = 80
        '
        'colBCTNTYPE
        '
        Me.colBCTNTYPE.Caption = "CTN Type"
        Me.colBCTNTYPE.FieldName = "BCTNTYPE"
        Me.colBCTNTYPE.Name = "colBCTNTYPE"
        Me.colBCTNTYPE.OptionsColumn.AllowEdit = False
        Me.colBCTNTYPE.OptionsColumn.ReadOnly = True
        Me.colBCTNTYPE.Visible = True
        Me.colBCTNTYPE.VisibleIndex = 2
        Me.colBCTNTYPE.Width = 80
        '
        'colBCOM
        '
        Me.colBCOM.FieldName = "BCOM"
        Me.colBCOM.Name = "colBCOM"
        Me.colBCOM.OptionsColumn.AllowEdit = False
        Me.colBCOM.OptionsColumn.ReadOnly = True
        Me.colBCOM.Visible = True
        Me.colBCOM.VisibleIndex = 3
        Me.colBCOM.Width = 80
        '
        'colBSHIPNAME
        '
        Me.colBSHIPNAME.FieldName = "BSHIPNAME"
        Me.colBSHIPNAME.Name = "colBSHIPNAME"
        Me.colBSHIPNAME.OptionsColumn.AllowEdit = False
        Me.colBSHIPNAME.OptionsColumn.ReadOnly = True
        Me.colBSHIPNAME.Visible = True
        Me.colBSHIPNAME.VisibleIndex = 4
        Me.colBSHIPNAME.Width = 81
        '
        'RadioGroup3
        '
        Me.RadioGroup3.Location = New System.Drawing.Point(822, 540)
        Me.RadioGroup3.Name = "RadioGroup3"
        Me.RadioGroup3.Properties.Columns = 2
        Me.RadioGroup3.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(0, Short), "CY-CY"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "CFS/CFS")})
        Me.RadioGroup3.Size = New System.Drawing.Size(331, 25)
        Me.RadioGroup3.StyleController = Me.LayoutControl1
        Me.RadioGroup3.TabIndex = 41
        '
        'BTYPEMOVE
        '
        Me.BTYPEMOVE.Location = New System.Drawing.Point(822, 511)
        Me.BTYPEMOVE.Name = "BTYPEMOVE"
        Me.BTYPEMOVE.Properties.Columns = 2
        Me.BTYPEMOVE.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(0, Short), "FCL/FCL"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "LCL/LCL")})
        Me.BTYPEMOVE.Size = New System.Drawing.Size(331, 25)
        Me.BTYPEMOVE.StyleController = Me.LayoutControl1
        Me.BTYPEMOVE.TabIndex = 40
        '
        'txt_tsport
        '
        Me.txt_tsport.Location = New System.Drawing.Point(1068, 439)
        Me.txt_tsport.Name = "txt_tsport"
        Me.txt_tsport.Size = New System.Drawing.Size(85, 20)
        Me.txt_tsport.StyleController = Me.LayoutControl1
        Me.txt_tsport.TabIndex = 39
        '
        'txtUnitCtn
        '
        Me.txtUnitCtn.FormattingEnabled = True
        Me.txtUnitCtn.Items.AddRange(New Object() {"KGM", "KGS"})
        Me.txtUnitCtn.Location = New System.Drawing.Point(1091, 228)
        Me.txtUnitCtn.Name = "txtUnitCtn"
        Me.txtUnitCtn.Size = New System.Drawing.Size(62, 21)
        Me.txtUnitCtn.TabIndex = 38
        '
        'txtNumberWord
        '
        Me.txtNumberWord.Location = New System.Drawing.Point(996, 228)
        Me.txtNumberWord.Name = "txtNumberWord"
        Me.txtNumberWord.Size = New System.Drawing.Size(91, 20)
        Me.txtNumberWord.StyleController = Me.LayoutControl1
        Me.txtNumberWord.TabIndex = 37
        '
        'BCHECKLOAD
        '
        Me.BCHECKLOAD.Location = New System.Drawing.Point(481, 470)
        Me.BCHECKLOAD.Name = "BCHECKLOAD"
        Me.BCHECKLOAD.Properties.Columns = 2
        Me.BCHECKLOAD.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem("True", "มี"), New DevExpress.XtraEditors.Controls.RadioGroupItem("False", "ไม่มี")})
        Me.BCHECKLOAD.Size = New System.Drawing.Size(331, 25)
        Me.BCHECKLOAD.StyleController = Me.LayoutControl1
        Me.BCHECKLOAD.TabIndex = 36
        '
        'txt_type
        '
        Me.txt_type.FormattingEnabled = True
        Me.txt_type.Items.AddRange(New Object() {"40'HC", "20'GP"})
        Me.txt_type.Location = New System.Drawing.Point(386, 261)
        Me.txt_type.Name = "txt_type"
        Me.txt_type.Size = New System.Drawing.Size(88, 21)
        Me.txt_type.TabIndex = 35
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(281, 261)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(68, 20)
        Me.TextBox4.StyleController = Me.LayoutControl1
        Me.TextBox4.TabIndex = 34
        '
        'txt_pol
        '
        Me.txt_pol.Location = New System.Drawing.Point(946, 487)
        Me.txt_pol.Name = "txt_pol"
        Me.txt_pol.Size = New System.Drawing.Size(207, 20)
        Me.txt_pol.StyleController = Me.LayoutControl1
        Me.txt_pol.TabIndex = 33
        '
        'txt_localforwared
        '
        Me.txt_localforwared.Location = New System.Drawing.Point(946, 463)
        Me.txt_localforwared.Name = "txt_localforwared"
        Me.txt_localforwared.Size = New System.Drawing.Size(207, 20)
        Me.txt_localforwared.StyleController = Me.LayoutControl1
        Me.txt_localforwared.TabIndex = 32
        '
        'txt_TUG
        '
        Me.txt_TUG.Location = New System.Drawing.Point(894, 439)
        Me.txt_TUG.Name = "txt_TUG"
        Me.txt_TUG.Size = New System.Drawing.Size(108, 20)
        Me.txt_TUG.StyleController = Me.LayoutControl1
        Me.txt_TUG.TabIndex = 31
        '
        'txt_notify
        '
        Me.txt_notify.EditValue = "SAME AS CONSIGNEE"
        Me.txt_notify.Location = New System.Drawing.Point(946, 325)
        Me.txt_notify.Name = "txt_notify"
        Me.txt_notify.Size = New System.Drawing.Size(207, 83)
        Me.txt_notify.StyleController = Me.LayoutControl1
        Me.txt_notify.TabIndex = 30
        '
        'txt_Gross
        '
        Me.txt_Gross.Location = New System.Drawing.Point(946, 301)
        Me.txt_Gross.Name = "txt_Gross"
        Me.txt_Gross.Size = New System.Drawing.Size(207, 20)
        Me.txt_Gross.StyleController = Me.LayoutControl1
        Me.txt_Gross.TabIndex = 29
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(946, 277)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(207, 20)
        Me.TextBox3.StyleController = Me.LayoutControl1
        Me.TextBox3.TabIndex = 28
        '
        'txtPlace
        '
        Me.txtPlace.Location = New System.Drawing.Point(946, 253)
        Me.txtPlace.Name = "txtPlace"
        Me.txtPlace.Size = New System.Drawing.Size(207, 20)
        Me.txtPlace.StyleController = Me.LayoutControl1
        Me.txtPlace.TabIndex = 27
        '
        'txtNumber
        '
        Me.txtNumber.Location = New System.Drawing.Point(872, 228)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(84, 20)
        Me.txtNumber.StyleController = Me.LayoutControl1
        Me.txtNumber.TabIndex = 26
        '
        'billboking
        '
        Me.billboking.Location = New System.Drawing.Point(946, 204)
        Me.billboking.Name = "billboking"
        Me.billboking.Size = New System.Drawing.Size(207, 20)
        Me.billboking.StyleController = Me.LayoutControl1
        Me.billboking.TabIndex = 25
        '
        'BDESCRIPT
        '
        Me.BDESCRIPT.Location = New System.Drawing.Point(822, 36)
        Me.BDESCRIPT.Name = "BDESCRIPT"
        Me.BDESCRIPT.Size = New System.Drawing.Size(331, 137)
        Me.BDESCRIPT.StyleController = Me.LayoutControl1
        Me.BDESCRIPT.TabIndex = 24
        '
        'txtShippingAddress
        '
        Me.txtShippingAddress.Location = New System.Drawing.Point(481, 214)
        Me.txtShippingAddress.Name = "txtShippingAddress"
        Me.txtShippingAddress.Size = New System.Drawing.Size(331, 83)
        Me.txtShippingAddress.StyleController = Me.LayoutControl1
        Me.txtShippingAddress.TabIndex = 23
        '
        'BMOTH
        '
        Me.BMOTH.Location = New System.Drawing.Point(605, 571)
        Me.BMOTH.Name = "BMOTH"
        Me.BMOTH.Size = New System.Drawing.Size(207, 20)
        Me.BMOTH.StyleController = Me.LayoutControl1
        Me.BMOTH.TabIndex = 22
        '
        'BREMARK
        '
        Me.BREMARK.EditValue = "Insured untill penang port"
        Me.BREMARK.Location = New System.Drawing.Point(605, 547)
        Me.BREMARK.Name = "BREMARK"
        Me.BREMARK.Size = New System.Drawing.Size(207, 20)
        Me.BREMARK.StyleController = Me.LayoutControl1
        Me.BREMARK.TabIndex = 21
        '
        'BINCOTERM
        '
        Me.BINCOTERM.Location = New System.Drawing.Point(605, 523)
        Me.BINCOTERM.Name = "BINCOTERM"
        Me.BINCOTERM.Size = New System.Drawing.Size(207, 20)
        Me.BINCOTERM.StyleController = Me.LayoutControl1
        Me.BINCOTERM.TabIndex = 20
        '
        'BINVOICE
        '
        Me.BINVOICE.Location = New System.Drawing.Point(605, 499)
        Me.BINVOICE.Name = "BINVOICE"
        Me.BINVOICE.Size = New System.Drawing.Size(207, 20)
        Me.BINVOICE.StyleController = Me.LayoutControl1
        Me.BINVOICE.TabIndex = 19
        '
        'txtconsignee
        '
        Me.txtconsignee.Location = New System.Drawing.Point(605, 328)
        Me.txtconsignee.Name = "txtconsignee"
        Me.txtconsignee.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtconsignee.Properties.Appearance.Options.UseFont = True
        Me.txtconsignee.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtconsignee.Properties.DataSource = Me.consigneeBindingSource
        Me.txtconsignee.Properties.DisplayMember = "CONNAME"
        Me.txtconsignee.Properties.ValueMember = "CONSIGNEEID"
        Me.txtconsignee.Properties.View = Me.SearchLookUpEdit3View
        Me.txtconsignee.Size = New System.Drawing.Size(207, 22)
        Me.txtconsignee.StyleController = Me.LayoutControl1
        Me.txtconsignee.TabIndex = 18
        '
        'consigneeBindingSource
        '
        Me.consigneeBindingSource.DataMember = "consignee"
        Me.consigneeBindingSource.DataSource = Me.Ykpdtset3
        Me.consigneeBindingSource.Sort = ""
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit3View
        '
        Me.SearchLookUpEdit3View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCONSIGNEEID, Me.colCONNAME, Me.colCONADD, Me.colCONTELL, Me.colCONFAX})
        Me.SearchLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit3View.Name = "SearchLookUpEdit3View"
        Me.SearchLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit3View.OptionsView.ShowGroupPanel = False
        '
        'colCONSIGNEEID
        '
        Me.colCONSIGNEEID.FieldName = "CONSIGNEEID"
        Me.colCONSIGNEEID.Name = "colCONSIGNEEID"
        Me.colCONSIGNEEID.Visible = True
        Me.colCONSIGNEEID.VisibleIndex = 0
        Me.colCONSIGNEEID.Width = 265
        '
        'colCONNAME
        '
        Me.colCONNAME.FieldName = "CONNAME"
        Me.colCONNAME.Name = "colCONNAME"
        Me.colCONNAME.Visible = True
        Me.colCONNAME.VisibleIndex = 1
        Me.colCONNAME.Width = 661
        '
        'colCONADD
        '
        Me.colCONADD.FieldName = "CONADD"
        Me.colCONADD.Name = "colCONADD"
        Me.colCONADD.Visible = True
        Me.colCONADD.VisibleIndex = 2
        Me.colCONADD.Width = 661
        '
        'colCONTELL
        '
        Me.colCONTELL.FieldName = "CONTELL"
        Me.colCONTELL.Name = "colCONTELL"
        '
        'colCONFAX
        '
        Me.colCONFAX.FieldName = "CONFAX"
        Me.colCONFAX.Name = "colCONFAX"
        '
        'txtForwarder
        '
        Me.txtForwarder.Location = New System.Drawing.Point(605, 36)
        Me.txtForwarder.Name = "txtForwarder"
        Me.txtForwarder.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtForwarder.Properties.Appearance.Options.UseFont = True
        Me.txtForwarder.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtForwarder.Properties.DataSource = Me.shipperBindingSource
        Me.txtForwarder.Properties.DisplayMember = "SHIPNAME"
        Me.txtForwarder.Properties.ValueMember = "SHIPPERID"
        Me.txtForwarder.Properties.View = Me.SearchLookUpEdit2View
        Me.txtForwarder.Size = New System.Drawing.Size(207, 22)
        Me.txtForwarder.StyleController = Me.LayoutControl1
        Me.txtForwarder.TabIndex = 17
        '
        'shipperBindingSource
        '
        Me.shipperBindingSource.DataMember = "shipper"
        Me.shipperBindingSource.DataSource = Me.Ykpdtset2
        Me.shipperBindingSource.Sort = ""
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit2View
        '
        Me.SearchLookUpEdit2View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIPPERID, Me.colSHIPNAME, Me.colSHIPNICKNAME, Me.colSHIPADD, Me.colSHIPTELL, Me.colSHIPFAX, Me.colCOMPANYNO})
        Me.SearchLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit2View.Name = "SearchLookUpEdit2View"
        Me.SearchLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit2View.OptionsView.ShowGroupPanel = False
        Me.SearchLookUpEdit2View.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCOMPANYNO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSHIPPERID
        '
        Me.colSHIPPERID.FieldName = "SHIPPERID"
        Me.colSHIPPERID.Name = "colSHIPPERID"
        Me.colSHIPPERID.Width = 176
        '
        'colSHIPNAME
        '
        Me.colSHIPNAME.FieldName = "SHIPNAME"
        Me.colSHIPNAME.Name = "colSHIPNAME"
        Me.colSHIPNAME.Visible = True
        Me.colSHIPNAME.VisibleIndex = 0
        Me.colSHIPNAME.Width = 352
        '
        'colSHIPNICKNAME
        '
        Me.colSHIPNICKNAME.FieldName = "SHIPNICKNAME"
        Me.colSHIPNICKNAME.Name = "colSHIPNICKNAME"
        Me.colSHIPNICKNAME.Visible = True
        Me.colSHIPNICKNAME.VisibleIndex = 1
        Me.colSHIPNICKNAME.Width = 352
        '
        'colSHIPADD
        '
        Me.colSHIPADD.FieldName = "SHIPADD"
        Me.colSHIPADD.Name = "colSHIPADD"
        Me.colSHIPADD.Visible = True
        Me.colSHIPADD.VisibleIndex = 2
        Me.colSHIPADD.Width = 352
        '
        'colSHIPTELL
        '
        Me.colSHIPTELL.FieldName = "SHIPTELL"
        Me.colSHIPTELL.Name = "colSHIPTELL"
        '
        'colSHIPFAX
        '
        Me.colSHIPFAX.FieldName = "SHIPFAX"
        Me.colSHIPFAX.Name = "colSHIPFAX"
        '
        'colCOMPANYNO
        '
        Me.colCOMPANYNO.FieldName = "COMPANYNO"
        Me.colCOMPANYNO.Name = "colCOMPANYNO"
        Me.colCOMPANYNO.Visible = True
        Me.colCOMPANYNO.VisibleIndex = 3
        Me.colCOMPANYNO.Width = 355
        '
        'txtconsigneeAddress
        '
        Me.txtconsigneeAddress.Location = New System.Drawing.Point(481, 354)
        Me.txtconsigneeAddress.Name = "txtconsigneeAddress"
        Me.txtconsigneeAddress.Size = New System.Drawing.Size(331, 85)
        Me.txtconsigneeAddress.StyleController = Me.LayoutControl1
        Me.txtconsigneeAddress.TabIndex = 16
        '
        'txtForwarderAddress
        '
        Me.txtForwarderAddress.Location = New System.Drawing.Point(481, 62)
        Me.txtForwarderAddress.Name = "txtForwarderAddress"
        Me.txtForwarderAddress.Size = New System.Drawing.Size(331, 95)
        Me.txtForwarderAddress.StyleController = Me.LayoutControl1
        Me.txtForwarderAddress.TabIndex = 15
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "booking_bl_ctn"
        Me.GridControl1.DataSource = Me.Ykpdtset4
        Me.GridControl1.Location = New System.Drawing.Point(12, 432)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.slcmasterbl_ctn, Me.btnDelete, Me.btnUpdate})
        Me.GridControl1.Size = New System.Drawing.Size(462, 162)
        Me.GridControl1.TabIndex = 12
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidbooking_bl_ctn, Me.colidnumber, Me.colbookid, Me.colname_1, Me.colname_2, Me.GridColumn9, Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colidnumber, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colidbooking_bl_ctn
        '
        Me.colidbooking_bl_ctn.FieldName = "idbooking_bl_ctn"
        Me.colidbooking_bl_ctn.Name = "colidbooking_bl_ctn"
        '
        'colidnumber
        '
        Me.colidnumber.Caption = "Index"
        Me.colidnumber.FieldName = "idnumber"
        Me.colidnumber.Name = "colidnumber"
        Me.colidnumber.Visible = True
        Me.colidnumber.VisibleIndex = 0
        Me.colidnumber.Width = 93
        '
        'colbookid
        '
        Me.colbookid.FieldName = "bookid"
        Me.colbookid.Name = "colbookid"
        Me.colbookid.Width = 469
        '
        'colname_1
        '
        Me.colname_1.Caption = "ลำดับ"
        Me.colname_1.ColumnEdit = Me.slcmasterbl_ctn
        Me.colname_1.FieldName = "name_1"
        Me.colname_1.Name = "colname_1"
        Me.colname_1.Visible = True
        Me.colname_1.VisibleIndex = 1
        Me.colname_1.Width = 327
        '
        'slcmasterbl_ctn
        '
        Me.slcmasterbl_ctn.AutoHeight = False
        Me.slcmasterbl_ctn.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcmasterbl_ctn.DataSource = Me.MasterblctnBindingSource
        Me.slcmasterbl_ctn.DisplayMember = "name"
        Me.slcmasterbl_ctn.Name = "slcmasterbl_ctn"
        Me.slcmasterbl_ctn.ValueMember = "name"
        Me.slcmasterbl_ctn.View = Me.RepositoryItemSearchLookUpEdit1View
        '
        'MasterblctnBindingSource
        '
        Me.MasterblctnBindingSource.DataMember = "masterbl_ctn"
        Me.MasterblctnBindingSource.DataSource = Me.Ykpdtset1
        '
        'RepositoryItemSearchLookUpEdit1View
        '
        Me.RepositoryItemSearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colidmasterbl_ctn, Me.colname})
        Me.RepositoryItemSearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.RepositoryItemSearchLookUpEdit1View.Name = "RepositoryItemSearchLookUpEdit1View"
        Me.RepositoryItemSearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.RepositoryItemSearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colidmasterbl_ctn
        '
        Me.colidmasterbl_ctn.FieldName = "idmasterbl_ctn"
        Me.colidmasterbl_ctn.Name = "colidmasterbl_ctn"
        '
        'colname
        '
        Me.colname.Caption = "Name"
        Me.colname.FieldName = "name"
        Me.colname.Name = "colname"
        Me.colname.Visible = True
        Me.colname.VisibleIndex = 0
        '
        'colname_2
        '
        Me.colname_2.Caption = "Address"
        Me.colname_2.FieldName = "name_2"
        Me.colname_2.Name = "colname_2"
        Me.colname_2.Visible = True
        Me.colname_2.VisibleIndex = 2
        Me.colname_2.Width = 348
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Update"
        Me.GridColumn9.ColumnEdit = Me.btnUpdate
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 3
        Me.GridColumn9.Width = 131
        '
        'btnUpdate
        '
        Me.btnUpdate.AutoHeight = False
        Me.btnUpdate.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up)})
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Delete"
        Me.GridColumn1.ColumnEdit = Me.btnDelete
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 4
        Me.GridColumn1.Width = 134
        '
        'btnDelete
        '
        Me.btnDelete.AutoHeight = False
        Me.btnDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)})
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'BPENANG
        '
        Me.BPENANG.Location = New System.Drawing.Point(136, 382)
        Me.BPENANG.Name = "BPENANG"
        Me.BPENANG.Size = New System.Drawing.Size(338, 20)
        Me.BPENANG.StyleController = Me.LayoutControl1
        Me.BPENANG.TabIndex = 11
        '
        'txt_remark
        '
        Me.txt_remark.Location = New System.Drawing.Point(136, 358)
        Me.txt_remark.Name = "txt_remark"
        Me.txt_remark.Size = New System.Drawing.Size(338, 20)
        Me.txt_remark.StyleController = Me.LayoutControl1
        Me.txt_remark.TabIndex = 10
        '
        'txt_comodity
        '
        Me.txt_comodity.Location = New System.Drawing.Point(136, 334)
        Me.txt_comodity.Name = "txt_comodity"
        Me.txt_comodity.Size = New System.Drawing.Size(338, 20)
        Me.txt_comodity.StyleController = Me.LayoutControl1
        Me.txt_comodity.TabIndex = 9
        '
        'txt_scn
        '
        Me.txt_scn.Location = New System.Drawing.Point(136, 310)
        Me.txt_scn.Name = "txt_scn"
        Me.txt_scn.Size = New System.Drawing.Size(338, 20)
        Me.txt_scn.StyleController = Me.LayoutControl1
        Me.txt_scn.TabIndex = 8
        '
        'BNICKAGENT
        '
        Me.BNICKAGENT.Location = New System.Drawing.Point(136, 286)
        Me.BNICKAGENT.Name = "BNICKAGENT"
        Me.BNICKAGENT.Size = New System.Drawing.Size(338, 20)
        Me.BNICKAGENT.StyleController = Me.LayoutControl1
        Me.BNICKAGENT.TabIndex = 7
        '
        'txtShipping
        '
        Me.txtShipping.Location = New System.Drawing.Point(605, 188)
        Me.txtShipping.Name = "txtShipping"
        Me.txtShipping.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtShipping.Properties.Appearance.Options.UseFont = True
        Me.txtShipping.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtShipping.Properties.DataSource = Me.shipperBindingSource
        Me.txtShipping.Properties.DisplayMember = "SHIPNAME"
        Me.txtShipping.Properties.ValueMember = "SHIPPERID"
        Me.txtShipping.Properties.View = Me.SearchLookUpEdit1View
        Me.txtShipping.Size = New System.Drawing.Size(207, 22)
        Me.txtShipping.StyleController = Me.LayoutControl1
        Me.txtShipping.TabIndex = 5
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "SHIPPERID"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "SHIPNAME"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        Me.GridColumn3.Width = 314
        '
        'GridColumn4
        '
        Me.GridColumn4.FieldName = "SHIPNICKNAME"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        Me.GridColumn4.Width = 418
        '
        'GridColumn5
        '
        Me.GridColumn5.FieldName = "SHIPADD"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        Me.GridColumn5.Width = 418
        '
        'GridColumn6
        '
        Me.GridColumn6.FieldName = "SHIPTELL"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        Me.GridColumn6.Width = 421
        '
        'GridColumn7
        '
        Me.GridColumn7.FieldName = "SHIPFAX"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Width = 248
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "COMPANYNO"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Width = 257
        '
        'txt_booking_no
        '
        Me.txt_booking_no.Location = New System.Drawing.Point(84, 261)
        Me.txt_booking_no.Name = "txt_booking_no"
        Me.txt_booking_no.Size = New System.Drawing.Size(145, 20)
        Me.txt_booking_no.StyleController = Me.LayoutControl1
        Me.txt_booking_no.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceGroup.Options.UseFont = True
        Me.LayoutControlGroup1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem1, Me.LayoutControlGroup3, Me.LayoutControlGroup4, Me.LayoutControlGroup5, Me.LayoutControlGroup6, Me.LayoutControlItem28, Me.LayoutControlGroup2, Me.LayoutControlGroup7, Me.LayoutControlGroup8, Me.LayoutControlItem36, Me.LayoutControlItem29, Me.LayoutControlItem37, Me.LayoutControlItem38, Me.EmptySpaceItem2, Me.LayoutControlItem39, Me.EmptySpaceItem5})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1168, 606)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.BNICKAGENT
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 274)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(466, 24)
        Me.LayoutControlItem4.Text = "Agent Line"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.txt_scn
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 298)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(466, 24)
        Me.LayoutControlItem5.Text = "SCN"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.txt_comodity
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 322)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(466, 24)
        Me.LayoutControlItem6.Text = "COMMODITY"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txt_remark
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 346)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(466, 24)
        Me.LayoutControlItem7.Text = "REMARK"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.BPENANG
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 370)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(466, 24)
        Me.LayoutControlItem8.Text = "FORWARDER"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.GridControl1
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 420)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(466, 166)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.txt_booking_no
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 249)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(221, 25)
        Me.LayoutControlItem1.Text = "Booking No."
        Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(67, 16)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12, Me.LayoutControlItem3})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(466, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(341, 152)
        Me.LayoutControlGroup3.Text = "Forwarder"
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.txtForwarder
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(335, 26)
        Me.LayoutControlItem12.Text = "Company Name"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.txtForwarderAddress
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(335, 99)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem13, Me.LayoutControlItem10})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(466, 292)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(341, 142)
        Me.LayoutControlGroup4.Text = "Consignee"
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.txtconsignee
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(335, 26)
        Me.LayoutControlItem13.Text = "Company Name"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.txtconsigneeAddress
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(335, 89)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem18})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(807, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(341, 168)
        Me.LayoutControlGroup5.Text = "Description Of Goods"
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.BDESCRIPT
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(335, 141)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem24, Me.LayoutControlItem23, Me.LayoutControlItem22, Me.LayoutControlItem21, Me.LayoutControlItem20, Me.LayoutControlItem19, Me.LayoutControlItem31, Me.LayoutControlItem32})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(807, 168)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(341, 235)
        Me.LayoutControlGroup6.Text = "Invoice"
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.txt_notify
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 121)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(335, 87)
        Me.LayoutControlItem24.Text = "Notify Party"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.txt_Gross
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 97)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem23.Text = "Gross Weight"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.TextBox3
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 73)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem22.Text = "Bill Of No."
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.txtPlace
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 49)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem21.Text = "Place Of Delivery"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.txtNumber
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(138, 25)
        Me.LayoutControlItem20.Text = "Number"
        Me.LayoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(45, 16)
        Me.LayoutControlItem20.TextToControlDistance = 5
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.billboking
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem19.Text = "INV NO."
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.txtNumberWord
        Me.LayoutControlItem31.Location = New System.Drawing.Point(138, 24)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(131, 25)
        Me.LayoutControlItem31.Text = "Word"
        Me.LayoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(31, 16)
        Me.LayoutControlItem31.TextToControlDistance = 5
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.txtUnitCtn
        Me.LayoutControlItem32.Location = New System.Drawing.Point(269, 24)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(66, 25)
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.TextBox4
        Me.LayoutControlItem28.Location = New System.Drawing.Point(221, 249)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(120, 25)
        Me.LayoutControlItem28.Text = "No.CTN"
        Me.LayoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(43, 16)
        Me.LayoutControlItem28.TextToControlDistance = 5
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem11})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(466, 152)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(341, 140)
        Me.LayoutControlGroup2.Text = "Shipping"
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.txtShipping
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(335, 26)
        Me.LayoutControlItem2.Text = "Company Name"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.txtShippingAddress
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(335, 87)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem30})
        Me.LayoutControlGroup7.Location = New System.Drawing.Point(466, 434)
        Me.LayoutControlGroup7.Name = "LayoutControlGroup7"
        Me.LayoutControlGroup7.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(341, 152)
        Me.LayoutControlGroup7.Text = "Loading List"
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.BINVOICE
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 29)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem14.Text = "Invoice Value"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.BINCOTERM
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 53)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem15.Text = "Incoterm"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.BREMARK
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 77)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem16.Text = "Remark"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.BMOTH
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 101)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem17.Text = "Mother Vessel"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.BCHECKLOAD
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(335, 29)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem27, Me.LayoutControlItem33, Me.LayoutControlItem34, Me.LayoutControlItem35})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(807, 403)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(341, 157)
        Me.LayoutControlGroup8.Text = "Consignors"
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.txt_TUG
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(184, 24)
        Me.LayoutControlItem25.Text = "TUG/BARGE"
        Me.LayoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(67, 16)
        Me.LayoutControlItem25.TextToControlDistance = 5
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.txt_localforwared
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem26.Text = "LOCAL FORWARDING"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.txt_pol
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(335, 24)
        Me.LayoutControlItem27.Text = "PORT OF LANDING"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(121, 16)
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.txt_tsport
        Me.LayoutControlItem33.Location = New System.Drawing.Point(184, 0)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(151, 24)
        Me.LayoutControlItem33.Text = "T/S PORT"
        Me.LayoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(57, 16)
        Me.LayoutControlItem33.TextToControlDistance = 5
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.BTYPEMOVE
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(335, 29)
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem34.TextVisible = False
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.RadioGroup3
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 101)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(335, 29)
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem35.TextVisible = False
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.GridControl2
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(466, 249)
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem36.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.txt_type
        Me.LayoutControlItem29.Location = New System.Drawing.Point(341, 249)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(125, 25)
        Me.LayoutControlItem29.Text = "Type"
        Me.LayoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(28, 16)
        Me.LayoutControlItem29.TextToControlDistance = 5
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.SimpleButton1
        Me.LayoutControlItem37.Location = New System.Drawing.Point(900, 560)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(118, 26)
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = False
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.SimpleButton2
        Me.LayoutControlItem38.Location = New System.Drawing.Point(1018, 560)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(130, 26)
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem38.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(807, 560)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(93, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.btnAddRow
        Me.LayoutControlItem39.Location = New System.Drawing.Point(359, 394)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(107, 26)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 394)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(359, 26)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'frmAddBooking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1168, 606)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmAddBooking"
        Me.Text = "Add Booking"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BTYPEMOVE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_tsport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumberWord.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BCHECKLOAD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_pol.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_localforwared.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_TUG.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_notify.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_Gross.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPlace.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.billboking.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BDESCRIPT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShippingAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BMOTH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BREMARK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BINCOTERM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BINVOICE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtconsignee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.consigneeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit3View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForwarder.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.shipperBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit2View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtconsigneeAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtForwarderAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.slcmasterbl_ctn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MasterblctnBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnUpdate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BPENANG.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_remark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_comodity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_scn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BNICKAGENT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtShipping.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_booking_no.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtShippingAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents BMOTH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BREMARK As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BINCOTERM As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BINVOICE As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtconsignee As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit3View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtForwarder As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit2View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txtconsigneeAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txtForwarderAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BPENANG As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_remark As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_comodity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_scn As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BNICKAGENT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtShipping As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents txt_booking_no As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents txt_pol As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_localforwared As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_TUG As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txt_notify As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents txt_Gross As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtPlace As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents billboking As DevExpress.XtraEditors.TextEdit
    Friend WithEvents BDESCRIPT As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_type As ComboBox
    Friend WithEvents TextBox4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents BCHECKLOAD As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtUnitCtn As ComboBox
    Friend WithEvents txtNumberWord As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txt_tsport As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioGroup3 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents BTYPEMOVE As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colBOOKINGID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBLANDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCTNTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBCOM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents shipperBindingSource As BindingSource
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colSHIPPERID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPNICKNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPADD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPTELL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIPFAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents consigneeBindingSource As BindingSource
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents colCONSIGNEEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONADD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONTELL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONFAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnAddRow As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents colidbooking_bl_ctn As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colidnumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbookid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname_1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname_2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents slcmasterbl_ctn As DevExpress.XtraEditors.Repository.RepositoryItemSearchLookUpEdit
    Friend WithEvents RepositoryItemSearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents MasterblctnBindingSource As BindingSource
    Friend WithEvents colidmasterbl_ctn As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnUpdate As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
End Class
