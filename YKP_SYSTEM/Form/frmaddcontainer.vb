﻿Imports Microsoft.Office.Interop
Imports MySql.Data.MySqlClient

Public Class frmaddcontainer
    Dim connectDB As CONDBTTH = CONDBTTH.NewConnection
    Dim idvoyage As String
    Dim dtset As New ykpdtset
    Public Sub New(ByVal Voyageid As String, ByVal voyagename As String)

        ' This call is required by the designer.
        InitializeComponent()
        idvoyage = Voyageid
        txt_voyage.Text = Voyageid
        txt_voyagename.Text = voyagename
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Structure ExcelRows
        Dim C1 As String
        Dim C2 As String
        Dim C3 As String
        Dim C4 As String
        Dim C5 As String
    End Structure
    Structure shipper
        Dim id As String
        Dim name As String
    End Structure
    Dim insertshipper() As shipper
    Dim count As Integer = 0
    Dim max As Integer
    Private ExcelRowlist As List(Of ExcelRows) = New List(Of ExcelRows)
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Public Sub add_voyage()

        Dim dt As New DataTable
        connectDB.GetTable("Select COUNT(SHIPPERID) from shipper;", dt)
        For i As Integer = 0 To dt.Rows.Count - 1
            ReDim insertshipper(dt.Rows(i)("COUNT(SHIPPERID)"))
            max = dt.Rows(i)("COUNT(SHIPPERID)")

        Next

        dt = New DataTable
        connectDB.GetTable("Select * from shipper;", dt)
        For i As Integer = 0 To dt.Rows.Count - 1

            insertshipper(i).id = dt.Rows(i)("SHIPPERID")
            If dt.Rows(i)("SHIPNICKNAME") IsNot DBNull.Value Then

                insertshipper(i).name = dt.Rows(i)("SHIPNICKNAME")
            Else
                insertshipper(i).name = ""
            End If



        Next

    End Sub
    Private Function getInfo() As Boolean
        ExcelRowlist.Clear()
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        Dim Excel As New Excel.Application
        If TextBox1.Text <> Nothing Then

            Excel.Workbooks.Open(TextBox1.Text)
            dtset.Tables("CTNUPLOAD").Clear()

            Excel.Sheets(1).Activate()
            Excel.Range("A8").Activate()
            Dim trimChars As Char() = {" "c, ControlChars.Tab, ControlChars.Lf, ControlChars.Cr, "\"c, "s"c}

            Dim row As New ExcelRows

            Do
                If Excel.ActiveCell.Value > Nothing Or Excel.ActiveCell.Text > Nothing Then

                    Dim dtrow As DataRow
                    '()
                    dtrow = dtset.Tables("CTNUPLOAD").NewRow()


                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C1 = Trim(Excel.ActiveCell.Value)
                    dtrow("CTNSTRING") = Trim(Excel.ActiveCell.Value)

                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C2 = Trim(Excel.ActiveCell.Value)
                    dtrow("CTNSIZE") = Trim(Excel.ActiveCell.Value).ToString.Trim(trimChars)

                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C3 = Trim(Excel.ActiveCell.Value)
                    dtrow("CTNAGENT") = Trim(Excel.ActiveCell.Value).ToString.Trim(trimChars)

                    Excel.ActiveCell.Offset(0, 1).Activate()
                    row.C4 = Trim(Excel.ActiveCell.Value)

                    Excel.ActiveCell.Offset(0, 2).Activate()
                    row.C5 = Trim(Excel.ActiveCell.Value)
                    dtrow("CTNSHIPNAME") = Trim(Excel.ActiveCell.Value)

                    ExcelRowlist.Add(row)
                    Excel.ActiveCell.Offset(1, -6).Activate()
                    dtset.Tables("CTNUPLOAD").Rows.Add(dtrow)

                Else

                    Exit Do

                End If

            Loop


        Else


        End If
        Excel.Quit()
        GridControl1.DataSource = dtset.Tables("CTNUPLOAD")




        Return True
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)




    End Sub



    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub frmadd_container_Load_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        add_voyage()
        loadmascmrunning()
    End Sub
    Public Sub loadmascmrunning()
        Dim sql As String
        sql = "SELECT * FROM mascmrunning;"
        connectDB.GetTable(sql, dtset.Tables("mascmrunning"))
        slcVoyage.Properties.DataSource = dtset.Tables("mascmrunning")
        slcVoyage.EditValue = 1
    End Sub
    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click


        If getInfo() Then
            'For Each Xitem In ExcelRowlist
            '    Dim ivite As ListViewItem
            '    ivite = Me.ListView1.Items.Add(Xitem.C1)
            '    ivite.SubItems.AddRange(New String() {Xitem.C2, Xitem.C3, Xitem.C4, Xitem.C5})
            'Next
        End If
        txtCount.Text = dtset.Tables("CTNUPLOAD").Rows.Count
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        With OpenFileDialog1
            .Filter = "Excel Files (*.xlsx)|*.xlsx|All files (*.*)|*.*"
            If (.ShowDialog() = DialogResult.OK) Then
                TextBox1.Text = .FileName
            End If
        End With
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        Dim sql As String
        If txt_voyage.Text Is Nothing Then
            Exit Sub
        End If
        If txt_voyage.Text = "" Then
            Exit Sub
        End If
        If CheckEdit1.Checked = True Then
            sql = "DELETE FROM ctnmain WHERE CTNVOYN = '" & idvoyage & "'"
            connectDB.ExecuteNonQuery(sql)
        End If

        Dim sql1 As String
        Dim sql2 As String

        Dim idshipper As String
        Dim checkshipper As Integer = 0
        For counter1 = 0 To dtset.Tables("CTNUPLOAD").Rows.Count - 1
            Try
                For count2 = 0 To max - 1


                    If dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSHIPNAME") <> "" Then

                        If Trim(insertshipper(count2).name) = Trim(dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSHIPNAME")) Then
                            idshipper = insertshipper(count2).id
                            checkshipper = 1
                            Exit For
                        End If

                    End If

                Next
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            If checkshipper = 1 Then
                sql1 = "CTNSTRING , "
                sql2 = " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSTRING") & "' , "
                sql1 += " CTNSIZE , "
                sql2 += " '" & MySqlHelper.EscapeString(dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSIZE")) & "' , "
                sql1 += " CTNAGENT , "
                sql2 += " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNAGENT") & "' , "
                sql1 += " CTNSTAT , "
                sql2 += " '1' , "
                sql1 += " CTNCONSI , "
                sql2 += " '" & idshipper & "' , "
                'idmascmrunning
                'menuname
                sql1 += " idmascmrunning , "
                sql2 += " '" & slcVoyage.EditValue & "' , "
                sql1 += " menuname , "
                sql2 += " '" & slcVoyage.Text & "' , "
                sql1 += " CTNVOYN , "
                sql2 += " '" & idvoyage & "' , "


                sql1 += " CTNSHIPNAME  "
                sql2 += " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSHIPNAME") & "'  "
                sql = "INSERT INTO ctnmain ( " & sql1 & ") VALUES ( " & sql2 & " );"
                connectDB.ExecuteNonQuery(sql)
                checkshipper = 0
            ElseIf dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSHIPNAME") = "DEPOT" Then
                Dim idlast As String
                Try
                    sql1 = "CTNSTRING , "
                    sql2 = " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSTRING") & "' , "
                    sql1 += " CTNSIZE , "
                    sql2 += " '" & MySqlHelper.EscapeString(dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSIZE")) & "' , "
                    sql1 += " CTNAGENT , "
                    sql2 += " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNAGENT") & "' , "
                    sql1 += " CTNSTAT , "
                    sql2 += " '0' , "
                    sql1 += " CTNCONSI , "
                    sql2 += " 'DEPOT' , "

                    sql1 += " idmascmrunning , "
                    sql2 += " '" & slcVoyage.EditValue & "' , "
                    sql1 += " menuname , "
                    sql2 += " '" & slcVoyage.Text & "' , "

                    sql1 += " CTNVOYN , "
                    sql2 += " '" & idvoyage & "' , "
                    sql1 += " CTNSHIPNAME  "
                    sql2 += " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSHIPNAME") & "'  "
                    sql = "INSERT INTO ctnmain ( " & sql1 & ") VALUES ( " & sql2 & " ); SELECT LAST_INSERT_ID() "
                    idlast = connectDB.ExecuteScalar(sql)

                Catch ex As Exception
                    MsgBox(ex.ToString)
                End Try

                sql1 = "CTNID , "
                sql2 = " '" & idlast & "' , "
                sql1 += " CTNDATEIN , "
                sql2 += " '0' , "
                sql1 += " CTNSTAT , "
                sql2 += " '1' , "
                sql1 += " CTNDAYRE "
                sql2 += " '0' "

                sql = "INSERT INTO ctndepot ( " & sql1 & ") VALUES ( " & sql2 & " ); "
                connectDB.ExecuteNonQuery(sql)
            Else
                sql1 = "CTNSTRING , "
                sql2 = " '" & dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSTRING") & "' , "
                sql1 += " CTNSIZE , "
                sql2 += " '" & MySqlHelper.EscapeString(dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNSIZE")) & "' , "
                sql1 += " CTNAGENT , "
                sql2 += " '" & MySqlHelper.EscapeString(dtset.Tables("CTNUPLOAD").Rows(counter1)("CTNAGENT")) & "' , "
                sql1 += " CTNSTAT , "
                sql2 += " '1' , "
                sql1 += " idmascmrunning , "
                sql2 += " '" & slcVoyage.EditValue & "' , "
                sql1 += " menuname , "
                sql2 += " '" & slcVoyage.Text & "' , "

                sql1 += " CTNCONSI , "
                sql2 += " '' , "
                sql1 += " CTNVOYN  "
                sql2 += " '" & idvoyage & "'  "
                sql = "INSERT INTO ctnmain ( " & sql1 & ") VALUES ( " & sql2 & " );"
                connectDB.ExecuteNonQuery(sql)

            End If






            checkshipper = 0
        Next

        'frmsearch_addcontainer.idvoyage = Conversions.ToInteger(Me.ListView1.SelectedItems(0).SubItems(0).Text)
        'frmsearch_addcontainer.timehhmm = Me.ListView1.SelectedItems(0).SubItems(4).Text
        'frmsearch_addcontainer.datetime = Me.ListView1.SelectedItems(0).SubItems(5).Text
        'Me.stringDate = Strings.Split(frmsearch_addcontainer.timehhmm, "-", -1, CompareMethod.Binary)
        'Me.stringhour = Strings.Split(frmsearch_addcontainer.datetime, ":", -1, CompareMethod.Binary)
        'Me.sumtxt = String.Concat(New String() {Me.stringDate(2), Me.stringDate(1), Me.stringDate(0), Me.stringhour(0), Me.stringhour(1)})
        'frmsearch_addcontainer.sumint = Conversions.ToDouble(Me.sumtxt) + 100
        'frmsearch_addcontainer.datethai = String.Concat(Me.sumtxt, "00")
        'frmsearch_addcontainer.datepenang = String.Concat(frmsearch_addcontainer.sumint.ToString(), "00")



        'Me.mySqlCommand.CommandText(String.Concat("Select * from ctnmain where CTNVOYN = '", Conversions.ToString(frmsearch_addcontainer.idvoyage), 
        'Me.mySqlCommand1.CommandText("insert into rptwhl ( rptctn, rptstat, rptctndate, rptctndatetpe,rpttype,rptidctn,rptagent,rptdatetype1) values (@rptctn,@rptstat,@rptctndate,@rptctndatetpe,@rpttype,@rptidctn,@rptagent,@rptdatetype1)")
        'Me.mySqlCommand1(Me.mysql1)
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptctn", RuntimeHelpers.GetObjectValue(Me.mySqlReader("CTNSTRING")))
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptstat", "MA")
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptctndate", frmsearch_addcontainer.datethai)
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptctndatetpe", frmsearch_addcontainer.datepenang)
        'If (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(Me.mySqlReader("CTNSIZE"), "40'HC", False)) Then
        '    Me.mySqlCommand1.get_Parameters().AddWithValue("@rpttype", "45G1")
        'ElseIf (Microsoft.VisualBasic.CompilerServices.Operators.ConditionalCompareObjectEqual(Me.mySqlReader("CTNSIZE"), "20'GP", False)) Then
        '    Me.mySqlCommand1.get_Parameters().AddWithValue("@rpttype", "22G1")
        'End If
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptidctn", RuntimeHelpers.GetObjectValue(Me.mySqlReader("CTNMAINID")))
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptagent", RuntimeHelpers.GetObjectValue(Me.mySqlReader("CTNAGENT")))
        'Me.mySqlCommand1.get_Parameters().AddWithValue("@rptdatetype1", frmsearch_addcontainer.timehhmm)
        sql = "SELECT * FROM voyage WHERE VOYAGEID = '" & idvoyage & "'"
        Dim dt3 As New DataTable
        connectDB.GetTable(sql, dt3)
        Dim stringDate As String()
        Dim stringhour As String()
        Dim sumtxt As String
        Dim sumint As Decimal
        stringDate = dt3.Rows(0)("VOYDATEEN").ToString.Split("-")


        If dt3.Rows(0)("VOYTIMEHHMMNN") IsNot DBNull.Value Then
            stringhour = dt3.Rows(0)("VOYTIMEHHMMNN").ToString().ToString.Split(":")

        Else
            Dim hour As String = "00:00"
            stringhour = hour.ToString().ToString.Split(":")
        End If

        If stringDate.ToArray.Length > 1 Then
            sumtxt = String.Concat(New String() {stringDate(2), stringDate(1), stringDate(0), stringhour(1), stringhour(0)})
            sumint = Convert.ToDecimal(sumtxt) + 100
        Else
            sumtxt = Nothing
            sumint = 0
        End If
        Dim str As String

        sql = "Select * from ctnmain where CTNVOYN = '" & idvoyage & "';"
        Dim dt2 As New DataTable
        connectDB.GetTable(sql, dt2)
        For i As Integer = 0 To dt2.Rows.Count - 1
            str = If(dt2.Rows(i)("CTNSIZE").ToString.Trim = "40'HC", "45G1", "22G1")

            sql1 = "rptctn , "
            sql2 = " '" & dt2.Rows(i)("CTNSTRING").ToString & "' , "
            sql1 += " rptstat , "
            sql2 += " 'MA' , "
            sql1 += " rptctndate , "
            sql2 += " '" & If(sumtxt Is Nothing, "", MySqlHelper.EscapeString(String.Concat(sumtxt, "00"))) & "' , "
            sql1 += " rptctndatetpe, "
            sql2 += " '" & If(sumint = 0, "", MySqlHelper.EscapeString(String.Concat(sumint.ToString(), "00"))) & "' , "
            sql1 += " rpttype, "
            sql2 += " '" & str & "' , "
            sql1 += " rptidctn, "
            sql2 += " '" & dt2.Rows(i)("CTNMAINID").ToString & "' , "
            sql1 += " rptagent, "
            sql2 += " '" & dt2.Rows(i)("CTNAGENT").ToString & "' , "
            sql1 += " rptdatetype1 "
            sql2 += " '" & dt3.Rows(0)("VOYDATEEN").ToString & "'  "

            sql = "INSERT INTO rptwhl ( " & sql1 & ") VALUES ( " & sql2 & " ); "
            connectDB.ExecuteNonQuery(sql)
        Next

        MsgBox("บันทึกข้อมูลเรียบร้อย")

        dtset.Tables("CTNUPLOAD").Rows.Clear()

        GridControl1.DataSource = dtset.Tables("CTNUPLOAD")
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        GridView1.DeleteRow(GridView1.FocusedRowHandle)
        GridView1.RefreshData()
        txtCount.Text = dtset.Tables("CTNUPLOAD").Rows.Count
    End Sub
End Class
