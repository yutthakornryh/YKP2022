﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDel_Container
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.slcVoyage = New DevExpress.XtraEditors.SearchLookUpEdit()
        Me.voyageBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Ykpdtset4 = New YKP_SYSTEM.ykpdtset()
        Me.SearchLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colVOYAGEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESIDS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMEN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYDATEES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYVESNAMES = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYTIMEHHMMNN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVOYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset2 = New YKP_SYSTEM.ykpdtset()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCTNMAINID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTRING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSEALID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNAGENT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSIZE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONDI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSTAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNCONSI = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNVOYS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNINS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNSHIPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNBOOKING = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNFORWARDER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNYARD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNUPGRADE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSTRBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKSEALBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEINBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKDATEOUTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKWEIGHTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCTNMARKTRANSPORTBOO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.Ykpdtset3 = New YKP_SYSTEM.ykpdtset()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.colmenuname = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.slcVoyage)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl4)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(864, 286, 450, 400)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1056, 581)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'slcVoyage
        '
        Me.slcVoyage.Location = New System.Drawing.Point(69, 42)
        Me.slcVoyage.Name = "slcVoyage"
        Me.slcVoyage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slcVoyage.Properties.Appearance.Options.UseFont = True
        Me.slcVoyage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.slcVoyage.Properties.DataSource = Me.voyageBindingSource
        Me.slcVoyage.Properties.DisplayMember = "VOYNAME"
        Me.slcVoyage.Properties.ValueMember = "VOYAGEID"
        Me.slcVoyage.Properties.View = Me.SearchLookUpEdit1View
        Me.slcVoyage.Size = New System.Drawing.Size(457, 22)
        Me.slcVoyage.StyleController = Me.LayoutControl1
        Me.slcVoyage.TabIndex = 10
        '
        'voyageBindingSource
        '
        Me.voyageBindingSource.DataMember = "voyage"
        Me.voyageBindingSource.DataSource = Me.Ykpdtset4
        Me.voyageBindingSource.Sort = ""
        '
        'Ykpdtset4
        '
        Me.Ykpdtset4.DataSetName = "ykpdtset"
        Me.Ykpdtset4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SearchLookUpEdit1View
        '
        Me.SearchLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colVOYAGEID, Me.colVOYVESIDN, Me.colVOYVESIDS, Me.colVOYDATESN, Me.colVOYDATESS, Me.colVOYDATEEN, Me.colVOYVESNAMEN, Me.colVOYDATEES, Me.colVOYVESNAMES, Me.colVOYTIMEHHMMNN, Me.colVOYNAME})
        Me.SearchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.SearchLookUpEdit1View.Name = "SearchLookUpEdit1View"
        Me.SearchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.SearchLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colVOYAGEID
        '
        Me.colVOYAGEID.FieldName = "VOYAGEID"
        Me.colVOYAGEID.Name = "colVOYAGEID"
        '
        'colVOYVESIDN
        '
        Me.colVOYVESIDN.FieldName = "VOYVESIDN"
        Me.colVOYVESIDN.Name = "colVOYVESIDN"
        '
        'colVOYVESIDS
        '
        Me.colVOYVESIDS.FieldName = "VOYVESIDS"
        Me.colVOYVESIDS.Name = "colVOYVESIDS"
        '
        'colVOYDATESN
        '
        Me.colVOYDATESN.FieldName = "VOYDATESN"
        Me.colVOYDATESN.Name = "colVOYDATESN"
        '
        'colVOYDATESS
        '
        Me.colVOYDATESS.FieldName = "VOYDATESS"
        Me.colVOYDATESS.Name = "colVOYDATESS"
        '
        'colVOYDATEEN
        '
        Me.colVOYDATEEN.FieldName = "VOYDATEEN"
        Me.colVOYDATEEN.Name = "colVOYDATEEN"
        '
        'colVOYVESNAMEN
        '
        Me.colVOYVESNAMEN.FieldName = "VOYVESNAMEN"
        Me.colVOYVESNAMEN.Name = "colVOYVESNAMEN"
        Me.colVOYVESNAMEN.Visible = True
        Me.colVOYVESNAMEN.VisibleIndex = 1
        '
        'colVOYDATEES
        '
        Me.colVOYDATEES.FieldName = "VOYDATEES"
        Me.colVOYDATEES.Name = "colVOYDATEES"
        '
        'colVOYVESNAMES
        '
        Me.colVOYVESNAMES.FieldName = "VOYVESNAMES"
        Me.colVOYVESNAMES.Name = "colVOYVESNAMES"
        Me.colVOYVESNAMES.Visible = True
        Me.colVOYVESNAMES.VisibleIndex = 2
        '
        'colVOYTIMEHHMMNN
        '
        Me.colVOYTIMEHHMMNN.FieldName = "VOYTIMEHHMMNN"
        Me.colVOYTIMEHHMMNN.Name = "colVOYTIMEHHMMNN"
        '
        'colVOYNAME
        '
        Me.colVOYNAME.FieldName = "VOYNAME"
        Me.colVOYNAME.Name = "colVOYNAME"
        Me.colVOYNAME.Visible = True
        Me.colVOYNAME.VisibleIndex = 0
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl4
        Me.SearchControl1.Location = New System.Drawing.Point(575, 42)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl4
        Me.SearchControl1.Size = New System.Drawing.Size(457, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 9
        '
        'GridControl4
        '
        Me.GridControl4.DataMember = "ctnmain"
        Me.GridControl4.DataSource = Me.Ykpdtset2
        Me.GridControl4.Location = New System.Drawing.Point(24, 68)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(1008, 463)
        Me.GridControl4.TabIndex = 8
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'Ykpdtset2
        '
        Me.Ykpdtset2.DataSetName = "ykpdtset"
        Me.Ykpdtset2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCTNMAINID, Me.colCTNSTRING, Me.colCTNSEALID, Me.colCTNAGENT, Me.colCTNSIZE, Me.colCTNCONDI, Me.colCTNSTAT, Me.colCTNCONSI, Me.colCTNVOYN, Me.colCTNVOYS, Me.colCTNWEIGHT, Me.colCTNINS, Me.colCTNSHIPNAME, Me.colCTNDATEOUT, Me.colCTNBOOKING, Me.colCTNFORWARDER, Me.colCTNYARD, Me.colCTNUPGRADE, Me.colCTNMARKSTR, Me.colCTNMARKSEAL, Me.colCTNMARKSTRBOO, Me.colCTNMARKSEALBOO, Me.colCTNMARKDATEIN, Me.colCTNMARKDATEINBOO, Me.colCTNMARKDATEOUT, Me.colCTNMARKDATEOUTBOO, Me.colCTNMARKWEIGHT, Me.colCTNMARKWEIGHTBOO, Me.colCTNMARKTRANSPORT, Me.colCTNMARKTRANSPORTBOO, Me.colmenuname})
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.MultiSelect = True
        Me.GridView4.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView4.OptionsView.ShowGroupPanel = False
        Me.GridView4.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCTNSHIPNAME, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCTNMAINID
        '
        Me.colCTNMAINID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMAINID.AppearanceCell.Options.UseFont = True
        Me.colCTNMAINID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMAINID.AppearanceHeader.Options.UseFont = True
        Me.colCTNMAINID.FieldName = "CTNMAINID"
        Me.colCTNMAINID.Name = "colCTNMAINID"
        '
        'colCTNSTRING
        '
        Me.colCTNSTRING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTRING.AppearanceCell.Options.UseFont = True
        Me.colCTNSTRING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTRING.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTRING.Caption = "Container No."
        Me.colCTNSTRING.FieldName = "CTNSTRING"
        Me.colCTNSTRING.Name = "colCTNSTRING"
        Me.colCTNSTRING.Visible = True
        Me.colCTNSTRING.VisibleIndex = 1
        Me.colCTNSTRING.Width = 88
        '
        'colCTNSEALID
        '
        Me.colCTNSEALID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSEALID.AppearanceCell.Options.UseFont = True
        Me.colCTNSEALID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSEALID.AppearanceHeader.Options.UseFont = True
        Me.colCTNSEALID.Caption = "Seal"
        Me.colCTNSEALID.FieldName = "CTNSEALID"
        Me.colCTNSEALID.Name = "colCTNSEALID"
        Me.colCTNSEALID.Visible = True
        Me.colCTNSEALID.VisibleIndex = 2
        Me.colCTNSEALID.Width = 115
        '
        'colCTNAGENT
        '
        Me.colCTNAGENT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNAGENT.AppearanceCell.Options.UseFont = True
        Me.colCTNAGENT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNAGENT.AppearanceHeader.Options.UseFont = True
        Me.colCTNAGENT.Caption = "Agent"
        Me.colCTNAGENT.FieldName = "CTNAGENT"
        Me.colCTNAGENT.Name = "colCTNAGENT"
        Me.colCTNAGENT.Visible = True
        Me.colCTNAGENT.VisibleIndex = 3
        Me.colCTNAGENT.Width = 61
        '
        'colCTNSIZE
        '
        Me.colCTNSIZE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSIZE.AppearanceCell.Options.UseFont = True
        Me.colCTNSIZE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSIZE.AppearanceHeader.Options.UseFont = True
        Me.colCTNSIZE.Caption = "Size"
        Me.colCTNSIZE.FieldName = "CTNSIZE"
        Me.colCTNSIZE.Name = "colCTNSIZE"
        Me.colCTNSIZE.Visible = True
        Me.colCTNSIZE.VisibleIndex = 4
        Me.colCTNSIZE.Width = 102
        '
        'colCTNCONDI
        '
        Me.colCTNCONDI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONDI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONDI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONDI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONDI.FieldName = "CTNCONDI"
        Me.colCTNCONDI.Name = "colCTNCONDI"
        '
        'colCTNSTAT
        '
        Me.colCTNSTAT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTAT.AppearanceCell.Options.UseFont = True
        Me.colCTNSTAT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSTAT.AppearanceHeader.Options.UseFont = True
        Me.colCTNSTAT.FieldName = "CTNSTAT"
        Me.colCTNSTAT.Name = "colCTNSTAT"
        '
        'colCTNCONSI
        '
        Me.colCTNCONSI.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONSI.AppearanceCell.Options.UseFont = True
        Me.colCTNCONSI.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNCONSI.AppearanceHeader.Options.UseFont = True
        Me.colCTNCONSI.FieldName = "CTNCONSI"
        Me.colCTNCONSI.Name = "colCTNCONSI"
        '
        'colCTNVOYN
        '
        Me.colCTNVOYN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYN.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYN.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYN.FieldName = "CTNVOYN"
        Me.colCTNVOYN.Name = "colCTNVOYN"
        '
        'colCTNVOYS
        '
        Me.colCTNVOYS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYS.AppearanceCell.Options.UseFont = True
        Me.colCTNVOYS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNVOYS.AppearanceHeader.Options.UseFont = True
        Me.colCTNVOYS.FieldName = "CTNVOYS"
        Me.colCTNVOYS.Name = "colCTNVOYS"
        '
        'colCTNWEIGHT
        '
        Me.colCTNWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNWEIGHT.Caption = "Weight"
        Me.colCTNWEIGHT.FieldName = "CTNWEIGHT"
        Me.colCTNWEIGHT.Name = "colCTNWEIGHT"
        Me.colCTNWEIGHT.Visible = True
        Me.colCTNWEIGHT.VisibleIndex = 5
        Me.colCTNWEIGHT.Width = 77
        '
        'colCTNINS
        '
        Me.colCTNINS.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNINS.AppearanceCell.Options.UseFont = True
        Me.colCTNINS.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNINS.AppearanceHeader.Options.UseFont = True
        Me.colCTNINS.FieldName = "CTNINS"
        Me.colCTNINS.Name = "colCTNINS"
        '
        'colCTNSHIPNAME
        '
        Me.colCTNSHIPNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSHIPNAME.AppearanceCell.Options.UseFont = True
        Me.colCTNSHIPNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNSHIPNAME.AppearanceHeader.Options.UseFont = True
        Me.colCTNSHIPNAME.FieldName = "CTNSHIPNAME"
        Me.colCTNSHIPNAME.Name = "colCTNSHIPNAME"
        '
        'colCTNDATEOUT
        '
        Me.colCTNDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNDATEOUT.FieldName = "CTNDATEOUT"
        Me.colCTNDATEOUT.Name = "colCTNDATEOUT"
        '
        'colCTNBOOKING
        '
        Me.colCTNBOOKING.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNBOOKING.AppearanceCell.Options.UseFont = True
        Me.colCTNBOOKING.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNBOOKING.AppearanceHeader.Options.UseFont = True
        Me.colCTNBOOKING.FieldName = "CTNBOOKING"
        Me.colCTNBOOKING.Name = "colCTNBOOKING"
        '
        'colCTNFORWARDER
        '
        Me.colCTNFORWARDER.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNFORWARDER.AppearanceCell.Options.UseFont = True
        Me.colCTNFORWARDER.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNFORWARDER.AppearanceHeader.Options.UseFont = True
        Me.colCTNFORWARDER.FieldName = "CTNFORWARDER"
        Me.colCTNFORWARDER.Name = "colCTNFORWARDER"
        '
        'colCTNYARD
        '
        Me.colCTNYARD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNYARD.AppearanceCell.Options.UseFont = True
        Me.colCTNYARD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNYARD.AppearanceHeader.Options.UseFont = True
        Me.colCTNYARD.FieldName = "CTNYARD"
        Me.colCTNYARD.Name = "colCTNYARD"
        '
        'colCTNUPGRADE
        '
        Me.colCTNUPGRADE.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNUPGRADE.AppearanceCell.Options.UseFont = True
        Me.colCTNUPGRADE.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNUPGRADE.AppearanceHeader.Options.UseFont = True
        Me.colCTNUPGRADE.FieldName = "CTNUPGRADE"
        Me.colCTNUPGRADE.Name = "colCTNUPGRADE"
        '
        'colCTNMARKSTR
        '
        Me.colCTNMARKSTR.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTR.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTR.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTR.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTR.FieldName = "CTNMARKSTR"
        Me.colCTNMARKSTR.Name = "colCTNMARKSTR"
        '
        'colCTNMARKSEAL
        '
        Me.colCTNMARKSEAL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEAL.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEAL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEAL.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEAL.FieldName = "CTNMARKSEAL"
        Me.colCTNMARKSEAL.Name = "colCTNMARKSEAL"
        '
        'colCTNMARKSTRBOO
        '
        Me.colCTNMARKSTRBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTRBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSTRBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSTRBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSTRBOO.FieldName = "CTNMARKSTRBOO"
        Me.colCTNMARKSTRBOO.Name = "colCTNMARKSTRBOO"
        '
        'colCTNMARKSEALBOO
        '
        Me.colCTNMARKSEALBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEALBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKSEALBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKSEALBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKSEALBOO.FieldName = "CTNMARKSEALBOO"
        Me.colCTNMARKSEALBOO.Name = "colCTNMARKSEALBOO"
        '
        'colCTNMARKDATEIN
        '
        Me.colCTNMARKDATEIN.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEIN.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEIN.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEIN.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEIN.FieldName = "CTNMARKDATEIN"
        Me.colCTNMARKDATEIN.Name = "colCTNMARKDATEIN"
        '
        'colCTNMARKDATEINBOO
        '
        Me.colCTNMARKDATEINBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEINBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEINBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEINBOO.FieldName = "CTNMARKDATEINBOO"
        Me.colCTNMARKDATEINBOO.Name = "colCTNMARKDATEINBOO"
        '
        'colCTNMARKDATEOUT
        '
        Me.colCTNMARKDATEOUT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUT.FieldName = "CTNMARKDATEOUT"
        Me.colCTNMARKDATEOUT.Name = "colCTNMARKDATEOUT"
        '
        'colCTNMARKDATEOUTBOO
        '
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKDATEOUTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKDATEOUTBOO.FieldName = "CTNMARKDATEOUTBOO"
        Me.colCTNMARKDATEOUTBOO.Name = "colCTNMARKDATEOUTBOO"
        '
        'colCTNMARKWEIGHT
        '
        Me.colCTNMARKWEIGHT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHT.FieldName = "CTNMARKWEIGHT"
        Me.colCTNMARKWEIGHT.Name = "colCTNMARKWEIGHT"
        '
        'colCTNMARKWEIGHTBOO
        '
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKWEIGHTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKWEIGHTBOO.FieldName = "CTNMARKWEIGHTBOO"
        Me.colCTNMARKWEIGHTBOO.Name = "colCTNMARKWEIGHTBOO"
        '
        'colCTNMARKTRANSPORT
        '
        Me.colCTNMARKTRANSPORT.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORT.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORT.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORT.FieldName = "CTNMARKTRANSPORT"
        Me.colCTNMARKTRANSPORT.Name = "colCTNMARKTRANSPORT"
        '
        'colCTNMARKTRANSPORTBOO
        '
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORTBOO.AppearanceCell.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colCTNMARKTRANSPORTBOO.AppearanceHeader.Options.UseFont = True
        Me.colCTNMARKTRANSPORTBOO.FieldName = "CTNMARKTRANSPORTBOO"
        Me.colCTNMARKTRANSPORTBOO.Name = "colCTNMARKTRANSPORTBOO"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(575, 547)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(469, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "ลบตู้"
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlItem3, Me.EmptySpaceItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1056, 581)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem1, Me.LayoutControlItem2})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1036, 535)
        Me.LayoutControlGroup2.Text = "ลบตู้ ขาเข้า"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.GridControl4
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(1012, 467)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.SearchControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(506, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(506, 26)
        Me.LayoutControlItem1.Text = "ค้นหา"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(42, 16)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.slcVoyage
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(506, 26)
        Me.LayoutControlItem2.Text = "Voyage"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(42, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.SimpleButton1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(563, 535)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(473, 26)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 535)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(563, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'Ykpdtset3
        '
        Me.Ykpdtset3.DataSetName = "ykpdtset"
        Me.Ykpdtset3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'colmenuname
        '
        Me.colmenuname.Caption = "Port"
        Me.colmenuname.FieldName = "menuname"
        Me.colmenuname.Name = "colmenuname"
        Me.colmenuname.Visible = True
        Me.colmenuname.VisibleIndex = 6
        '
        'frmDel_Container
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1056, 581)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmDel_Container"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ลบตู้"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.slcVoyage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.voyageBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents Ykpdtset2 As ykpdtset
    Friend WithEvents colCTNMAINID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTRING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSEALID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNAGENT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSIZE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONDI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSTAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNCONSI As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNVOYS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNINS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNSHIPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNBOOKING As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNFORWARDER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNYARD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNUPGRADE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSTRBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKSEALBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEINBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKDATEOUTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKWEIGHTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCTNMARKTRANSPORTBOO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Ykpdtset3 As ykpdtset
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents slcVoyage As DevExpress.XtraEditors.SearchLookUpEdit
    Friend WithEvents SearchLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents voyageBindingSource As BindingSource
    Friend WithEvents Ykpdtset4 As ykpdtset
    Friend WithEvents colVOYAGEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESIDS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMEN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYDATEES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYVESNAMES As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYTIMEHHMMNN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVOYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmenuname As DevExpress.XtraGrid.Columns.GridColumn
End Class
