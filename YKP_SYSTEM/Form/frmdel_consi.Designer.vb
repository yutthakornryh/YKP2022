﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmdel_consi
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmdel_consi))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.Ykpdtset1 = New YKP_SYSTEM.ykpdtset()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCONSIGNEEID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONADD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONTELL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCONFAX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1066, 600)
        Me.LayoutControl1.TabIndex = 184
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.ImageOptions.Image = CType(resources.GetObject("SimpleButton1.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(906, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(148, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Export Excel"
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl1
        Me.SearchControl1.Location = New System.Drawing.Point(45, 12)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchControl1.Properties.Appearance.Options.UseFont = True
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl1
        Me.SearchControl1.Size = New System.Drawing.Size(857, 22)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 5
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "consignee"
        Me.GridControl1.DataSource = Me.Ykpdtset1
        Me.GridControl1.Location = New System.Drawing.Point(12, 38)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnDelete})
        Me.GridControl1.Size = New System.Drawing.Size(1042, 550)
        Me.GridControl1.TabIndex = 4
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'Ykpdtset1
        '
        Me.Ykpdtset1.DataSetName = "ykpdtset"
        Me.Ykpdtset1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCONSIGNEEID, Me.colCONNAME, Me.colCONADD, Me.colCONTELL, Me.colCONFAX, Me.GridColumn1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCONSIGNEEID, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colCONSIGNEEID
        '
        Me.colCONSIGNEEID.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONSIGNEEID.AppearanceCell.Options.UseFont = True
        Me.colCONSIGNEEID.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONSIGNEEID.AppearanceHeader.Options.UseFont = True
        Me.colCONSIGNEEID.FieldName = "CONSIGNEEID"
        Me.colCONSIGNEEID.Name = "colCONSIGNEEID"
        Me.colCONSIGNEEID.OptionsColumn.AllowEdit = False
        Me.colCONSIGNEEID.OptionsColumn.ReadOnly = True
        Me.colCONSIGNEEID.Visible = True
        Me.colCONSIGNEEID.VisibleIndex = 0
        Me.colCONSIGNEEID.Width = 91
        '
        'colCONNAME
        '
        Me.colCONNAME.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONNAME.AppearanceCell.Options.UseFont = True
        Me.colCONNAME.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONNAME.AppearanceHeader.Options.UseFont = True
        Me.colCONNAME.Caption = "Name"
        Me.colCONNAME.FieldName = "CONNAME"
        Me.colCONNAME.Name = "colCONNAME"
        Me.colCONNAME.OptionsColumn.AllowEdit = False
        Me.colCONNAME.OptionsColumn.ReadOnly = True
        Me.colCONNAME.Visible = True
        Me.colCONNAME.VisibleIndex = 1
        Me.colCONNAME.Width = 229
        '
        'colCONADD
        '
        Me.colCONADD.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONADD.AppearanceCell.Options.UseFont = True
        Me.colCONADD.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONADD.AppearanceHeader.Options.UseFont = True
        Me.colCONADD.Caption = "Address"
        Me.colCONADD.FieldName = "CONADD"
        Me.colCONADD.Name = "colCONADD"
        Me.colCONADD.OptionsColumn.AllowEdit = False
        Me.colCONADD.OptionsColumn.ReadOnly = True
        Me.colCONADD.Visible = True
        Me.colCONADD.VisibleIndex = 2
        Me.colCONADD.Width = 229
        '
        'colCONTELL
        '
        Me.colCONTELL.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONTELL.AppearanceCell.Options.UseFont = True
        Me.colCONTELL.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONTELL.AppearanceHeader.Options.UseFont = True
        Me.colCONTELL.Caption = "Tell."
        Me.colCONTELL.FieldName = "CONTELL"
        Me.colCONTELL.Name = "colCONTELL"
        Me.colCONTELL.OptionsColumn.AllowEdit = False
        Me.colCONTELL.OptionsColumn.ReadOnly = True
        Me.colCONTELL.Visible = True
        Me.colCONTELL.VisibleIndex = 3
        Me.colCONTELL.Width = 229
        '
        'colCONFAX
        '
        Me.colCONFAX.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONFAX.AppearanceCell.Options.UseFont = True
        Me.colCONFAX.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colCONFAX.AppearanceHeader.Options.UseFont = True
        Me.colCONFAX.Caption = "Fax."
        Me.colCONFAX.FieldName = "CONFAX"
        Me.colCONFAX.Name = "colCONFAX"
        Me.colCONFAX.OptionsColumn.AllowEdit = False
        Me.colCONFAX.OptionsColumn.ReadOnly = True
        Me.colCONFAX.Visible = True
        Me.colCONFAX.VisibleIndex = 4
        Me.colCONFAX.Width = 236
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceCell.Options.UseFont = True
        Me.GridColumn1.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridColumn1.AppearanceHeader.Options.UseFont = True
        Me.GridColumn1.Caption = "Delete"
        Me.GridColumn1.ColumnEdit = Me.btnDelete
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 5
        Me.GridColumn1.Width = 126
        '
        'btnDelete
        '
        Me.btnDelete.AutoHeight = False
        Me.btnDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)})
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1066, 600)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1046, 554)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.SearchControl1
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(894, 26)
        Me.LayoutControlItem2.Text = "ค้นหา"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(30, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.SimpleButton1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(894, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(152, 26)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'frmdel_consi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1066, 600)
        Me.Controls.Add(Me.LayoutControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmdel_consi"
        Me.Text = " "
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ykpdtset1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Ykpdtset1 As ykpdtset
    Friend WithEvents colCONSIGNEEID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONADD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONTELL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCONFAX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
End Class
