﻿Imports MySql.Data.MySqlClient
Public Class frmadd_depot
    Dim mySqlCommand As New MySqlCommand
    Dim mySqlAdaptor As New MySqlDataAdapter
    Dim mySqlReader As MySqlDataReader
    Dim respone As Object
    Dim mysql As MySqlConnection = main_form.mysqlconection
    Private Sub frmadd_depot_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        System.Threading.Thread.CurrentThread.CurrentUICulture = System.Threading.Thread.CurrentThread.CurrentCulture
        mysql.Close()
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        mySqlCommand = New MySqlCommand
        If mysql.State = ConnectionState.Closed Then
            mysql.Open()
        End If
        respone = MsgBox("ยืนยันข้อมูลถูกต้อง", MsgBoxStyle.OkCancel + MsgBoxStyle.Information, "Warning Messsage")
        If respone = 1 Then
            Try
                mySqlCommand.Parameters.Clear()
                mySqlCommand.CommandText = "insert into ctndepotunit ( depotname,depotmanhr) values(@depotname,@depotmanhr)"
                mySqlCommand.Connection = mysql



                mySqlCommand.Parameters.AddWithValue("@depotname", txtcustomer.Text)

                mySqlCommand.Parameters.AddWithValue("@depotmanhr", TextBox1.Text)
                mySqlCommand.ExecuteNonQuery()
                mysql.Close()

                MsgBox("Save Complete")
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
        mysql.Close()
    End Sub
End Class