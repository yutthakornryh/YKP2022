CREATE DATABASE  IF NOT EXISTS `ykp_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ykp_db`;
-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: localhost    Database: ykp_db
-- ------------------------------------------------------
-- Server version	5.5.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ctndepotdet`
--

DROP TABLE IF EXISTS `ctndepotdet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ctndepotdet` (
  `CTNDEPOTDETID` int(11) NOT NULL AUTO_INCREMENT,
  `CTNDEPOTUNITID` varchar(45) DEFAULT NULL,
  `CTNDEPOTID` varchar(45) DEFAULT NULL,
  `CTNCOMMENT` varchar(45) DEFAULT NULL,
  `CTNPRICE` varchar(45) DEFAULT NULL,
  `CTNPRICEDEPART` varchar(45) DEFAULT NULL,
  `CTNDEPOTSIZE` varchar(45) DEFAULT NULL,
  `CTNDEPOTHR` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CTNDEPOTDETID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ctndepotdet`
--

LOCK TABLES `ctndepotdet` WRITE;
/*!40000 ALTER TABLE `ctndepotdet` DISABLE KEYS */;
INSERT INTO `ctndepotdet` VALUES (1,'1','516','ซ่อม','5','3','4','0.25'),(2,'1','0','ซ่อม','3','3','4','0.25'),(3,'','516','ซ่อม','4','3','4','0.25'),(4,'','516','ซ่อม','2','2','5','0.25'),(5,'1','516','321','3','4','3','3'),(6,'1','0','32','4','1','33','3'),(7,'1','516','3','5','5','3','4'),(8,'1','517','ทดสอบ','1','1','1','1'),(9,'1','516','ทดสอบ DM','3','4','1x2','0.25'),(10,'3','516','4','1','1','1','2'),(11,'2','516','ทดสอบ WT','4','4','1z3','0.25'),(12,'3','516','3','1','1','2','1');
/*!40000 ALTER TABLE `ctndepotdet` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-20 17:09:05
