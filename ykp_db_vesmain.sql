CREATE DATABASE  IF NOT EXISTS `ykp_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ykp_db`;
-- MySQL dump 10.13  Distrib 5.5.9, for Win32 (x86)
--
-- Host: localhost    Database: ykp_db
-- ------------------------------------------------------
-- Server version	5.5.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vesmain`
--

DROP TABLE IF EXISTS `vesmain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vesmain` (
  `VESMAINID` int(11) NOT NULL AUTO_INCREMENT,
  `VSSNAM` varchar(45) DEFAULT NULL,
  `VSSMASTER` varchar(45) DEFAULT NULL,
  `VSSNATION` varchar(45) DEFAULT NULL,
  `VSSINS` varchar(45) DEFAULT NULL,
  `VSSDATE` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`VESMAINID`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vesmain`
--

LOCK TABLES `vesmain` WRITE;
/*!40000 ALTER TABLE `vesmain` DISABLE KEYS */;
INSERT INTO `vesmain` VALUES (4,'Majestic','Toon','CHINA','AIA','29-11-2556'),(3,'NAMTHONG','Stonzz','CHINA','AIA','28-12-2556'),(5,'SAKAWA','JUNEKUNG','Japanese','MITTAE','29-11-2556'),(7,'TESTING VES','TESTING VES','THAILAND','191990085451SDE','30-01-2008');
/*!40000 ALTER TABLE `vesmain` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-20 17:09:05
